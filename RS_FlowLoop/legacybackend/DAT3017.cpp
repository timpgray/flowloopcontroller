//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "DAT3017.h"
#include "RS232Port.h"
#include "Utility.h"
#include "ModBus.h"

DAT3017Class DAT3017;

//---------------------------------------------------------------------------
int DAT3017Class::StatusRead(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DAT3017_ADDRESS_NONE;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DAT3017, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    Status = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int DAT3017Class::ChannelEnableRead(unsigned short *ChannelEnable)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DAT3017_ADDRESS_CH_ENABLE;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DAT3017, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    *ChannelEnable = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int DAT3017Class::ChannelEnableWrite(unsigned short ChannelEnable)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DAT3017_ADDRESS_CH_ENABLE;
    RegisterValue = ChannelEnable;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DAT3017, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DAT3017Class::ChannelRead(int Channel, float *MilliAmps)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    switch(Channel)
    {
        case 0:
            RegisterAddress = DAT3017_ADDRESS_CH0;
            break;
        case 1:
            RegisterAddress = DAT3017_ADDRESS_CH1;
            break;
        case 2:
            RegisterAddress = DAT3017_ADDRESS_CH2;
            break;
        case 3:
            RegisterAddress = DAT3017_ADDRESS_CH3;
            break;
        case 4:
            RegisterAddress = DAT3017_ADDRESS_CH4;
            break;
        case 5:
            RegisterAddress = DAT3017_ADDRESS_CH5;
            break;
        case 6:
            RegisterAddress = DAT3017_ADDRESS_CH6;
            break;
        case 7:
            RegisterAddress = DAT3017_ADDRESS_CH7;
            break;
    }
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DAT3017, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    *MilliAmps = ((float)RegisterValue) /1000.0;

    return(true);

}
//---------------------------------------------------------------------------

