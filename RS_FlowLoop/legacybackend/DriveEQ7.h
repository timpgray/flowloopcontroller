//---------------------------------------------------------------------------
#ifndef DriveEq7H
#define DriveEq7H
//---------------------------------------------------------------------------

// Node ID
#define MODBUS_NODE_ID_DRIVE_EQ7    0x01

// Register addresses
#define EQ7_ADDRESS_NONE            0x0000
#define EQ7_ADDRESS_CONTROL         0x0706
#define EQ7_ADDRESS_SPEED_REF       0x0705
#define EQ7_ADDRESS_SPEED_ACTUAL    0x0809

// Control word
#define EQ7_CONTROL_STOP    0x0000
#define EQ7_CONTROL_RUN_FWD 0x0001
#define EQ7_CONTROL_RUN_REV 0x0002

class DriveEQ7Class
{
    public:
    DriveEQ7Class(){;}

    int LoopTest(void);
    int StatusRead(void);
    int Run(void);
    int Stop(void);
    int SpeedReferenceWrite(float SpeedRpm);
    int SpeedReferenceRead(float *SpeedRpm);
    int SpeedActualRead(float *SpeedRpm);

    unsigned short Status;

    private:

};
extern DriveEQ7Class DriveEQ7;


#endif





