//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "DriveDCS800.h"
#include "RS232Port.h"
#include "Utility.h"
#include "ModBus.h"

DriveDCS800Class DriveDCS800;

//---------------------------------------------------------------------------
int DriveDCS800Class::LoopTest(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_NONE;
    RegisterValue = 0xAA55;
    Result = ModBus.LoopTest(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);

}
//---------------------------------------------------------------------------
int DriveDCS800Class::StatusRead(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_STATUS;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    Status = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int DriveDCS800Class::On(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_CONTROL;
    RegisterValue = DCS800_CONTROL_ON;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);

}
//---------------------------------------------------------------------------
int DriveDCS800Class::Run(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_CONTROL;
    RegisterValue = DCS800_CONTROL_RUN;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);

}
//---------------------------------------------------------------------------
int DriveDCS800Class::Stop(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_CONTROL;
    RegisterValue = DCS800_CONTROL_STOP;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DriveDCS800Class::Off(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_CONTROL;
    RegisterValue = DCS800_CONTROL_OFF;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DriveDCS800Class::SpeedReferenceWrite(float SpeedRpm)
{
    unsigned short SpeedRpmInt;
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    SpeedRpmInt = (unsigned short)(SpeedRpm *1.0);

    RegisterAddress = DCS800_ADDRESS_SPEED_REF;
    RegisterValue = SpeedRpmInt;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DriveDCS800Class::SpeedReferenceRead(float *SpeedRpm)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_SPEED_REF;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    *SpeedRpm = ((float)RegisterValue) /1.0;

    return(true);

}
//---------------------------------------------------------------------------
int DriveDCS800Class::SpeedActualRead(float *SpeedRpm)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = DCS800_ADDRESS_SPEED_ACTUAL;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_DCS800, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    *SpeedRpm = ((float)RegisterValue) /1.0;

    return(true);

}
//---------------------------------------------------------------------------

