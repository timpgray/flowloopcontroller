#-------------------------------------------------
#
# Project created by QtCreator 2017-05-06T21:54:59
#
#-------------------------------------------------

QT       += core gui widgets serialport

TARGET = legacybackend
TEMPLATE = lib
CONFIG += staticlib

SOURCES += legacybackend.cpp \
    Utility.cpp \
    Timer.cpp \
    RS232Port.cpp \
    ModBus.cpp \
    IEC.cpp \
    RotaryNavCommand.cpp \
    IECThread.cpp \
    DAT3017.cpp \
    DriveDCS800.cpp \
    DriveEQ7.cpp \
    NUFLO.cpp \
    iecnotifier.cpp \
    witscache.cpp \
    witsmsghandler.cpp \
    WarriorCommand.cpp

HEADERS += legacybackend.h \
    Utility.h \
    Timer.h \
    RS232Port.h \
    ModBus.h \
    CcsCompiler.h \
    NodeCommon.h \
    NodeId.h \
    IEC.h \
    IECBox.h \
    RotaryNav.h \
    RotaryNavCommand.h \
    IECThread.h \
    DAT3017.h \
    DriveDCS800.h \
    DriveEQ7.h \
    NUFLO.h \
    iecnotifier.h \
    witscache.h \
    witsmsghandler.h \
    WarriorCommand.h \
    WarriorTech.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
