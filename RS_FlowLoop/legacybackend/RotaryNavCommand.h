//---------------------------------------------------------------------------
#ifndef RotaryNavCommandH
#define RotaryNavCommandH
//---------------------------------------------------------------------------

#include <qstring.h>
#include <qserialport.h>
#include "RotaryNav.h"

class RotaryNavCommandClass
{
	public:
    RotaryNavCommandClass(){;}
    bool initialize(int port, int baudrate);
    void close();
    int SendCommand(void);

	int ReadFirmwareId(char *FirmwareId);
    int ModeWrite(unsigned char Mode);
    int SerialNumberRead(void);
    int SerialNumberWrite(void);
    int NonVolatileRead(unsigned char NonVolatileType);
    int NonVolatileWrite(unsigned char NonVolatileType);
    int DatagroupRead(unsigned char DataGroupType);
    int RunAction(unsigned char ActionType, int ActionParameterSize, char *ActionParameters);
    int SystemRead(void);
    int SystemWrite(void);
	int TimeRead(unsigned char *TimeStampBuffer);
    int TimeWrite(unsigned char *TimeStampBuffer);
    int MemoryRead(unsigned short PageNumber, unsigned char Paragraph, unsigned char *Buffer);
	int MemoryErase(void);

	int getToolFaceDataSize(){return 82;}
	int getSurveySize(){return 82;}
	void unpackToolFace(unsigned char* buffer, RotaryNavDataGroup4Structure* tfdata);
	void unpackSurvey(unsigned char* buffer, RotaryNavDataGroup3Structure* sdata);

    private:
    struct MESSAGE_STRUCTURE Message;
    QSerialPort m_serialPort;

};
extern RotaryNavCommandClass RotaryNavCommand;


#endif





