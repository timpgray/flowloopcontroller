//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "WarriorCommand.h"
#include "RS232Port.h"
#include "Utility.h"
#include "NodeId.h"
#include "WarriorTech.h"

WarriorCommandClass WarriorCommand;

RotoSlideDataGroup1Structure RotoSlideDataGroup1;
WarriorDataGroup1Structure WarriorDataGroup1;

//---------------------------------------------------------------------------
int WarriorCommandClass::SendCommand(void)
{
    int Result;
    int i;
    unsigned char CheckSum;
    unsigned short MessageLength;
    int FlagLengthGood;
    int FlagIdGood;
    int FlagCheckSumGood;

    // Clear the RxBuffer
    memset(RS232Port3.RxBuffer, 0, PC_TXRXBUFFER_SIZE);
    memset(RS232Port3.TxBuffer, 0, PC_TXRXBUFFER_SIZE);

    // Add addresses
    Message.Buffer[MSG_TO] = ADDRESS_WARRIOR;
    Message.Buffer[MSG_FROM] = NodeIdHost[ID_APPLICATION];
    // Calculate total length
    Message.TxLength = MSG_OVERHEAD +Message.TxDataLength;
    // Add length
    Message.Buffer[MSG_LENGTH0] = (unsigned char)((Message.TxLength >> 0) & 0x00FF);
    Message.Buffer[MSG_LENGTH1] = (unsigned char)((Message.TxLength >> 8) & 0x00FF);

    // Add checksum
    CheckSum = 0;
    for (i=0; i<(int)(Message.TxLength -1); i++)
        CheckSum += Message.Buffer[i];
     Message.Buffer[Message.TxLength -1] = CheckSum;

    // Copy the message over to the serial structure
    RS232Port3.TxCount = Message.TxLength;
    memcpy(RS232Port3.TxBuffer, Message.Buffer, RS232Port3.TxCount);
    // The expected number of bytes returned
    RS232Port3.ExpectRxCount = Message.ExpectRxCount;

    // Clear out the message buffer
    memset(Message.Buffer, 0, sizeof(Message.Buffer));

    // Send the message
    Result = RS232Port3.Transmit(TX_OPTION_NONE);
    //if (Result == COM_GOOD)
    if (RS232Port3.RxCount >= MSG_OVERHEAD)
        // Extract data from message
        memcpy(&Message.Buffer[MSG_DATA],&RS232Port3.RxBuffer[MSG_DATA], RS232Port3.RxCount -MSG_OVERHEAD);

    if (RS232Port3.RxCount >= MSG_OVERHEAD)
    {
        // Check it
        FlagLengthGood = false;
        FlagIdGood = false;
        FlagCheckSumGood = false;

        if (RS232Port3.RxCount == RS232Port3.ExpectRxCount)
            FlagLengthGood = true;
        if (RS232Port3.RxBuffer[MSG_FROM] == ADDRESS_WARRIOR)
            FlagIdGood = true;
        CheckSum = 0;
        for (i=0; i<(int)(RS232Port3.RxCount -1); i++)
            CheckSum += RS232Port3.RxBuffer[i];
        if ((unsigned char)RS232Port3.RxBuffer[RS232Port3.RxCount -1] == CheckSum)
            FlagCheckSumGood = true;
        if (FlagLengthGood && FlagIdGood && FlagCheckSumGood)
        {
            // Extract data from message
            memcpy(&Message.Buffer[MSG_DATA],&RS232Port3.RxBuffer[MSG_DATA], RS232Port3.RxCount -MSG_OVERHEAD);
        }
        else
            Result = COM_ERROR;
    }

    return (Result);
}
//---------------------------------------------------------------------------
int WarriorCommandClass::Exchange(unsigned char DataGroupType)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_DATAGROUP_RDWR;
    Message.Buffer[MSG_DATA] = DataGroupType;
    switch(DataGroupType)
    {
        case ROTOSLIDE_DATAGROUP_1:
            memcpy(&Message.Buffer[MSG_DATA +1], &RotoSlideDataGroup1, sizeof(RotoSlideDataGroup1));
            Message.TxDataLength = sizeof(DataGroupType) +sizeof(RotoSlideDataGroup1);
            Message.ExpectRxCount = MSG_OVERHEAD +sizeof(DataGroupType) +sizeof(WarriorDataGroup1);
            break;
        default:
            return(COM_ERROR);
    }
    Result = SendCommand();
    if (Result == COM_GOOD)
    {
        DataGroupType = Message.Buffer[MSG_DATA];
        switch(DataGroupType)
        {
            case WARRIOR_DATAGROUP_1:
                memcpy(&WarriorDataGroup1, &Message.Buffer[MSG_DATA +1], sizeof(WarriorDataGroup1));
                break;
            default:
                return(COM_ERROR);
        }
    }

    return(Result);
}
//---------------------------------------------------------------------------

