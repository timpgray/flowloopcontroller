//---------------------------------------------------------------------------
#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "NUFLO.h"
#include "RS232Port.h"
#include "Utility.h"
#include "ModBus.h"

NUFLOClass NUFLO;

//---------------------------------------------------------------------------
int NUFLOClass::StatusRead(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = NUFLO_ADDRESS_NONE;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_NUFLO, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    Status = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int NUFLOClass::InputSensitivityRead(unsigned short *InputSensitivity)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = NUFLO_ADDRESS_INPUT_SENSITIVITY;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_NUFLO, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    *InputSensitivity = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int NUFLOClass::InputSensitivityWrite(unsigned short InputSensitivity)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = NUFLO_ADDRESS_INPUT_SENSITIVITY;
    RegisterValue = InputSensitivity;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_NUFLO, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int NUFLOClass::FlowRateRead(float *FlowRateGallonsPerMinute)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;
    unsigned long RegisterValue32;

    RegisterAddress = NUFLO_ADDRESS_FLOW_RATE0;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_NUFLO, RegisterAddress, &RegisterValue);
    //Form3->DisplayTxRx();
    if (Result != COM_GOOD)
        return(false);
    RegisterValue32 = (unsigned long)RegisterValue;

    Sleep(25);

    RegisterAddress = NUFLO_ADDRESS_FLOW_RATE1;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_NUFLO, RegisterAddress, &RegisterValue);
    //Form3->DisplayTxRx();
    if (Result != COM_GOOD)
        return(false);
    RegisterValue32 += (((unsigned long)RegisterValue) << 16);

    *FlowRateGallonsPerMinute = *(float *)(&RegisterValue32);

    return(true);

}
//---------------------------------------------------------------------------

