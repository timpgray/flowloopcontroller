#include "witsmsghandler.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <qstring.h>
#include <qdebug.h>



WitsMsgHandler::WitsMsgHandler()
{
	m_comPort = nullptr;
}


WitsMsgHandler::~WitsMsgHandler()
{
	if(m_comPort != nullptr)
	{
        m_comPort->close();
        delete m_comPort;
    }
}

bool WitsMsgHandler::parseConfigInputLine(string inputline)
{
    vector<string> parts = splitStr(inputline);
    bool status = false;

    if(parts.size() == 2)
    {
        string key = trim(parts.at(0));
        string value = trim(parts.at(1));
        status = true;

        m_witsNameMap[key] = value;
    }

    return status;
}

bool WitsMsgHandler::parseWITSMsg(string witsline)
{
    bool status = true;

    try
	{
		string key = witsline.size() > 4 ? witsline.substr(0, 4):witsline;
		string valstr = witsline.size() > 4 ? witsline.substr(4):"";

        publishWITSMsgRaw(key, valstr);

        for (std::vector<WitsRawMsgHandler*>::iterator it = m_rawListeners.begin() ; it != m_rawListeners.end(); ++it)
        {
            (*it)->handleRawWITSMsg(key, valstr);
        }

        map<string,string>::iterator it = m_witsNameMap.find(key);

		if (it != m_witsNameMap.end())
		{
			float value = stof(valstr);
            string mappedkey = m_witsNameMap.at(key);

            publishWITSMsg(mappedkey, value);

			for (std::vector<WitsProcessedMsgHandler*>::iterator it = m_processedListeners.begin() ; it != m_processedListeners.end(); ++it)
			{
				(*it)->handleProcessedWITSMsg(witsline, mappedkey, value);
			}
		}
		else
		{
			for (std::vector<WitsProcessedMsgHandler*>::iterator it = m_processedListeners.begin() ; it != m_processedListeners.end(); ++it)
			{
				(*it)->handleProcessedWITSMsg(witsline, string(""), 0.0F);
			}
		}
    }
    catch(std::invalid_argument ex)
    {
        status = false;
    }
    catch(std::out_of_range ex)
    {
        status = false;
    }

    return status;
}


vector<string> WitsMsgHandler::splitStr(string inputstr, char spliton)
{
    vector<string> list;
    istringstream stringstr(inputstr);
    char buffer[32];

    for(;stringstr.getline(buffer, 32, spliton);)
    {
        list.push_back(string(buffer));
    }

    return list;
}

vector<string> WitsMsgHandler::splitStrSpecial(string inputstr, QString* premainder)
{
    vector<string> list;
    QString msg = QString::fromStdString(inputstr);
    QString currentmessage = "";

    for(int i=0;i<msg.size();i++)
    {
        QString sub = msg.at(i);

        if(sub == "\n" || sub == "\r")
        {
            if(!currentmessage.isEmpty())
            {
                string part = currentmessage.toStdString();

                list.push_back(part);
                qDebug() << "adding this message " << currentmessage;
                currentmessage.clear();
            }
        }
        else
        {
            currentmessage += sub;
        }

    }

    if(!currentmessage.isEmpty())
    {
        *premainder = currentmessage;
        qDebug() << "remainder message = " << currentmessage;
    }

    return list;
}

string WitsMsgHandler::trim(string str)
{
    // trim trailing spaces
    size_t endpos = str.find_last_not_of(" \n\r\t");

    if( string::npos != endpos )
    {
        str = str.substr( 0, endpos+1 );
    }

    endpos = str.find_last_not_of(" \n\r\t");
    if( string::npos != endpos )
    {
        str = str.substr( 0, endpos+1 );
    }

    // trim leading spaces
    size_t startpos = str.find_first_not_of(" \n\r\t");

    if( string::npos != startpos )
    {
        str = str.substr( startpos );
    }

    startpos = str.find_first_not_of(" \n\r\t");

    if( string::npos != startpos )
    {
        str = str.substr( startpos );
    }

    return str;
}


bool WitsMsgHandler::loadConfigFile(string filename)
{
     fstream fs;
     fs.open (filename, std::fstream::in);
     bool status = fs.is_open();

     if(status)
     {
         string line;

         while (std::getline(fs, line))
         {
            parseConfigInputLine(line);
         }

     }

    return status;
}


bool WitsMsgHandler::init(int port, int baudrate)
{
    if(m_comPort != nullptr)
    {
        delete m_comPort;
    }
    QString portstr = "COM" + QString::number(port);

    m_comPort = new QSerialPort(portstr);

//    bool status = m_comPort->Initialize(port, baudrate, 8, PARITY_NONE, DUPLEX_HALF);
    m_comPort->setBaudRate(baudrate);
    bool status = m_comPort->open(QSerialPort::ReadOnly);
//    m_comPort->SetBaud(baudrate);
//    m_comPort->SetParity(PARITY_NONE);

    qDebug() << "WITS serial port opened with this status " << status << " error = " << m_comPort->errorString();

    return status;
}

bool WitsMsgHandler::init(bool initserialport, string configfile)
{
    string configstr = "config.csv";

    if(configfile.size() != 0)
    {
       configstr = configfile;
    }

    bool configstatus = loadConfigFile(configstr);

	m_comPortStr = "ND";

    if(configstatus && initserialport)
    {
        map<string,string>::iterator comport_it = m_witsNameMap.find("comport");
		map<string,string>::iterator baud_it = m_witsNameMap.find("baudrate");

        if ((comport_it != m_witsNameMap.end()) && (baud_it != m_witsNameMap.end()))
        {
            string comportstr = m_witsNameMap.at("comport");
            string baudratestr = m_witsNameMap.at("baudrate");

            try
            {
                int comport = stoi(comportstr);
                int baudrate = stoi(baudratestr);
				bool comportstatus = init(comport, baudrate);

				m_comPortStr = "COM" + comportstr;
                configstatus = comportstatus;
            }
            catch(std::invalid_argument ex)
            {
                configstatus = false;
            }
            catch(std::out_of_range ex)
            {
                configstatus = false;
            }

        }

    }

    return configstatus;
}


void WitsMsgHandler::tick()
{
    if(m_comPort != nullptr)
    {
        bool datapending = true;

        while(datapending)
        {
            QByteArray bytes = m_comPort->readAll();
            QString readmessage(bytes);

            if(!readmessage.isEmpty())
            {
                qDebug() << "WITS serial port read this string " << readmessage;
            }


            if(datapending)
            {
                string readstr = readmessage.toStdString();

                m_currentmsg += readstr;

                bool complete = m_currentmsg.back() == '\n' || m_currentmsg.back() == '\r';
                QString partial = "";
                vector<string> list = splitStrSpecial(m_currentmsg, &partial);

//                vector<string> list = splitStr(m_currentmsg, '\n');

                for(unsigned int i=0;i<list.size();i++)
                {
                    if(i == list.size()-1 && !complete)
                    {
						m_currentmsg = list.at(i);
                    }
                    else
                    {
                        string witsline = trim(list.at(i));
						bool status = parseWITSMsg(witsline);

                        if(!status)
                        {

                        }
                    }
				}

//				if(complete)
//				{
//                    m_currentmsg = "";
//                }
                m_currentmsg = partial.toStdString();
            }

            datapending = false;
        }
    }
}


void WitsMsgHandler::removeProcessedListener(WitsProcessedMsgHandler* handler)
{
    for (std::vector<WitsProcessedMsgHandler*>::iterator it = m_processedListeners.begin() ; it != m_processedListeners.end(); ++it)
    {
        if(*it == handler)
        {
            m_processedListeners.erase(it);
            break;
        }
    }
}

void WitsMsgHandler::removeRawListener(WitsRawMsgHandler* handler)
{
    for (std::vector<WitsRawMsgHandler*>::iterator it = m_rawListeners.begin() ; it != m_rawListeners.end(); ++it)
    {
        if(*it == handler)
        {
            m_rawListeners.erase(it);
            break;
        }
    }

}
