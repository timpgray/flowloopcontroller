//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "ModBus.h"
#include "RS232Port.h"
#include "Utility.h"

ModBusClass ModBus;

//---------------------------------------------------------------------------
int ModBusClass::SendCommand(void)
{
    int Result;
    int i;
    unsigned short Crc16Calculated;
    unsigned short Crc16Rx;
    unsigned short MessageLength;
    int FlagLengthGood;
    int FlagIdGood;
    int FlagCrcGood;

    // Clear the RxBuffer
    memset(RS232Port1.RxBuffer, 0, PC_TXRXBUFFER_SIZE);
    memset(RS232Port1.TxBuffer, 0, PC_TXRXBUFFER_SIZE);

    // Calculate total length
    Message.TxLength = MODBUS_MSG_OVERHEAD +Message.TxDataLength;

    // Add CRC16
    Crc16Calculated = Utility.SwitchEndianShort(Utility.ModRTU_CRC(Message.Buffer, Message.TxLength -2));
    Message.Buffer[Message.TxLength -2] = Crc16Calculated >> 8;
    Message.Buffer[Message.TxLength -1] = Crc16Calculated >> 0;

    // Copy the message over to the serial structure
    RS232Port1.TxCount = Message.TxLength;
    memcpy(RS232Port1.TxBuffer, Message.Buffer, RS232Port1.TxCount);
    // The expected number of bytes returned
    RS232Port1.ExpectRxCount = Message.ExpectRxCount;

    // Clear out the message buffer
    memset(Message.Buffer, 0, sizeof(Message.Buffer));

    // Send the message
    Result = RS232Port1.Transmit(TX_OPTION_NONE);
    if (RS232Port1.RxCount >= MODBUS_MSG_OVERHEAD)
        // Extract data from message
        memcpy(&Message.Buffer[MODBUS_MSG_DATA],&RS232Port1.RxBuffer[MODBUS_MSG_DATA], RS232Port1.RxCount -MODBUS_MSG_OVERHEAD);

    if (RS232Port1.RxCount >= MODBUS_MSG_OVERHEAD)
    {
        // Check it
        FlagLengthGood = false;
        FlagIdGood = false;
        FlagCrcGood = false;

        if (RS232Port1.RxCount == RS232Port1.ExpectRxCount)
            FlagLengthGood = true;
        if (RS232Port1.RxBuffer[MODBUS_MSG_NODEID] == RS232Port1.TxBuffer[MODBUS_MSG_NODEID])
            FlagIdGood = true;
        Crc16Rx = ((unsigned short)RS232Port1.RxBuffer[RS232Port1.RxCount -2]) << 8;
        Crc16Rx += ((unsigned short)RS232Port1.RxBuffer[RS232Port1.RxCount -1]) << 0;
        Crc16Calculated = 0;
        Crc16Calculated = Utility.SwitchEndianShort(Utility.ModRTU_CRC(RS232Port1.RxBuffer, RS232Port1.RxCount -2));
        if (Crc16Rx == Crc16Calculated)
            FlagCrcGood = true;
        if (FlagLengthGood && FlagIdGood && FlagCrcGood)
        {
            // Extract data from message
            memcpy(&Message.Buffer[0],&RS232Port1.RxBuffer[0], RS232Port1.RxCount);
        }
        else
            Result = COM_ERROR;
    }

    return (Result);
}
//---------------------------------------------------------------------------
int ModBusClass::LoopTest(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue)
{
    int i;
    int Result;
    unsigned short RegisterValueB;

    RegisterValueB = Utility.SwitchEndianShort(RegisterValue);

    i = 0;
    Message.Buffer[i++] = NodeId;
    Message.Buffer[i++] = MODBUS_CMD_LOOP;
    Message.Buffer[i++] = RegisterAddress >> 8;
    Message.Buffer[i++] = RegisterAddress >> 0;
    memcpy(&Message.Buffer[MODBUS_MSG_DATA], &RegisterValueB, sizeof(RegisterValueB));
    Message.TxDataLength = sizeof(RegisterValue);
    Message.ExpectRxCount = MODBUS_MSG_OVERHEAD +sizeof(RegisterValueB);

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
    }

    return(Result);
}
//---------------------------------------------------------------------------
int ModBusClass::ReadRegister(unsigned char NodeId, unsigned short RegisterAddress, unsigned short *RegisterValue)
{
    int i;
    int Result;
    unsigned short RegisterValueB;

    RegisterValueB = 0;

    i = 0;
    Message.Buffer[i++] = NodeId;
    Message.Buffer[i++] = MODBUS_CMD_READ_WORD;
    Message.Buffer[i++] = RegisterAddress >> 8;
    Message.Buffer[i++] = RegisterAddress >> 0;
    Message.Buffer[i++] = 0;
    Message.Buffer[i++] = 1;
    Message.TxDataLength = 2;
    // RX: Address, Command, ByteCount, Data1, Data0, CRC1, CRC0
    Message.ExpectRxCount = 7;

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
        memcpy(&RegisterValueB, &Message.Buffer[3], sizeof(RegisterValueB));
        *RegisterValue = Utility.SwitchEndianShort(RegisterValueB);
    }

    return(Result);
}
//---------------------------------------------------------------------------
int ModBusClass::WriteRegister(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue)
{
    int i;
    int Result;
    unsigned short RegisterValueB;

    RegisterValueB = Utility.SwitchEndianShort(RegisterValue);

    i = 0;
    Message.Buffer[i++] = NodeId;
    Message.Buffer[i++] = MODBUS_CMD_WRITE_WORD;
    Message.Buffer[i++] = RegisterAddress >> 8;
    Message.Buffer[i++] = RegisterAddress >> 0;
    memcpy(&Message.Buffer[MODBUS_MSG_DATA], &RegisterValueB, sizeof(RegisterValueB));
    Message.TxDataLength = sizeof(RegisterValue);
    // RX: Address, Command, Address1, Address0, Data1, Data0, CRC1, CRC0
    Message.ExpectRxCount = 8;

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
    }

    return(Result);
}
//---------------------------------------------------------------------------
int ModBusClass::ReadBit(unsigned char NodeId, unsigned short RegisterAddress, unsigned short *RegisterValue)
{
    int i;
    int Result;

    i = 0;
    Message.Buffer[i++] = NodeId;
    Message.Buffer[i++] = MODBUS_CMD_READ_BIT;
    Message.Buffer[i++] = RegisterAddress >> 8;
    Message.Buffer[i++] = RegisterAddress >> 0;
    Message.Buffer[i++] = 0;
    Message.Buffer[i++] = 1;
    Message.TxDataLength = 2;
    // RX: Address, Command, ByteCount, Data1, CRC1, CRC0
    Message.ExpectRxCount = 6;

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
        *RegisterValue = Message.Buffer[3];
    }

    return(Result);
}
//---------------------------------------------------------------------------
int ModBusClass::WriteBit(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue)
{
    int i;
    int Result;
    unsigned short RegisterValueB;

    RegisterValueB = 0x0000;
    if (RegisterValue != 0)
        RegisterValueB = 0xFF00;
    RegisterValueB = Utility.SwitchEndianShort(RegisterValueB);

    i = 0;
    Message.Buffer[i++] = NodeId;
    Message.Buffer[i++] = MODBUS_CMD_WRITE_BIT;
    Message.Buffer[i++] = RegisterAddress >> 8;
    Message.Buffer[i++] = RegisterAddress >> 0;
    memcpy(&Message.Buffer[MODBUS_MSG_DATA], &RegisterValueB, sizeof(RegisterValueB));
    Message.TxDataLength = sizeof(RegisterValueB);
    // RX: Address, Command, Address1, Address0, Data1, Data0, CRC1, CRC0
    Message.ExpectRxCount = 8;

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
    }

    return(Result);
}
//---------------------------------------------------------------------------

