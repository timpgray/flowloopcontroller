//---------------------------------------------------------------------------
// 2014.08.16
//---------------------------------------------------------------------------
#ifndef RS232PortH
#define RS232PortH
//---------------------------------------------------------------------------
#include <stdio.h>
#include <winioctl.h>
//---------------------------------------------------------------------------
/*
Serial port pinout
1   DCD     IN
2   RX      IN
3   TX      OUT
4   DTR     OUT
5   GND
6   DSR     IN
7   RTS     OUT
8   CTS     IN
9   RI      IN
*/
#define PC_TXRXBUFFER_SIZE  5000

//#define READ_INTERVAL_TIMEOUT           0
//#define READ_TOTAL_TIMEOUT_CONSTANT     20
//#define READ_TOTAL_TIMEOUT_MULTIPLIER   1
//#define WRITE_TOTAL_TIMEOUT_CONSTANT    20
//#define WRITE_TOTAL_TIMEOUT_MULTIPLIER  1
#define READ_INTERVAL_TIMEOUT           0xFFFFFFFF
#define READ_TOTAL_TIMEOUT_CONSTANT     1
#define READ_TOTAL_TIMEOUT_MULTIPLIER   1
#define WRITE_TOTAL_TIMEOUT_CONSTANT    1
#define WRITE_TOTAL_TIMEOUT_MULTIPLIER  1

#define COM1      0x3f8
#define COM2      0x2f8
#define COM3      0x3e8
#define COM4      0x2e8
#define COM5      0x2f0
#define COM6      0x3e0
#define LPT1      0x378
#define IOCTL_SERIAL_SET_LINE_CONTROL   CTL_CODE(FILE_DEVICE_SERIAL_PORT, 3,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define IOCTL_SERIAL_GET_LINE_CONTROL   CTL_CODE(FILE_DEVICE_SERIAL_PORT,21,METHOD_BUFFERED,FILE_ANY_ACCESS)

#define TX_OPTION_NONE  (unsigned short)0x0000
#define DUPLEX_HALF 1
#define DUPLEX_FULL 2

enum {COM_GOOD=0,COM_ERROR,COM_NO_REPLY,COM_TIMEOUT,COM_FAIL};

typedef struct SERIAL_LINE_CONTROL_STRUCTURE
{
   unsigned char StopBits;
   unsigned char Parity;
   unsigned char WordLength;
}SerialLineControlStructure;

class RS232PortClass
{
    public:
    RS232PortClass(){InitializeFlag = false;}
    int InitializeFlagGet(void);
    int Initialize(int PortNumber, int Baud, int DataBits, int ParityType, int Duplex);
    int SetBaud(int Baud);
    int SetParity(int Parity);
    int Transmit(unsigned short Options);
    int Receive(int MaxBytes);
    int CheckReceive(void);
    void Release(void);
    void SetDTR(int Level);
    void SetRTS(int Level);
    int CheckCtsLevel(void);
    int CheckDsrLevel(void);

    unsigned char TxBuffer[PC_TXRXBUFFER_SIZE];
    unsigned long TxCount;
    unsigned char RxBuffer[PC_TXRXBUFFER_SIZE];
    unsigned long RxCount;
    unsigned long ExpectRxCount;
    unsigned long TxLength;

    int FlagTxEcho;
    int Flag9Bit;
    unsigned long InterCharacterDelayMicroSeconds;
    unsigned long TransmitTimeoutMilliSeconds;
    unsigned long ReceiveTimeoutMilliSeconds;
    int InitializeFlag;

    private:
    HANDLE HandleSerialPort;
    COMMTIMEOUTS SerialTimeouts;
    DCB DcbSerialPort;
    struct SERIAL_LINE_CONTROL_STRUCTURE SerialLineControl;

};
extern RS232PortClass RS232Port1;
extern RS232PortClass RS232Port2;
extern RS232PortClass RS232Port3;
extern RS232PortClass RS232Port4;


//---------------------------------------------------------------------------

#endif
