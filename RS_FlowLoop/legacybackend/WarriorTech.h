

//------------------------------------------------------------------------------

// Communications
// RS232 Full Duplex, 9600 baud, 1 start bit, 8 data bits, 1 stop bit, no parity.
// Messaage format: TO, FROM, LENGTH0, LENGHT1, COMMAND, [DATA], CHECKSUM
// RotoSlide address is 0x01.  Warrior address is 0x02.
// LENGTH0/1 is 16 bits (LO,HI) and includes all characters in the message.
// COMMAND is 0x05.
// [DATA} contains the 8 bit datagroup type followed by the datagroup structure.
// CHECKSUM is the 8 bit sum of all preceding characters.
// To/From addresses
#define ADDRESS_ROTOSLIDE   0x01
#define ADDRESS_WARRIOR     0x02
// Command codes
#define COMMAND_EXCHANGE    0x05
// Datagroup types
#define ROTOSLIDE_DATAGROUP_1   1
#define WARRIOR_DATAGROUP_1     1

// Mode
enum {ROTOSLIDE_MODE_DRILL_AHEAD=0, ROTOSLIDE_MODE_STEERING, ROTOSLIDE_MODE_RPM_NULL};


// Misc defines


//------------------------------------------------------------------------------

typedef struct ROTOSLIDE_DATAGROUP_1_STRUCTURE
{
    unsigned short SequenceNumberTx;
    unsigned short SequenceNumberRx;
    long UnixGmtTimeSeconds;

    // DRILL_AHEAD, STEERING, RPM_NULL
    unsigned short Mode;
    float TargetToolFaceDegrees;

    // Stationary survey
    float StationaryInclinationDegrees;
    float StationaryAzimuthDegrees;

    // Circulating survey
    float CirculatingInclinationDegrees;
    float CirculatingAzimuthDegrees;
    float CirculatingToolFaceDegrees;
    float FlagMagnetic;

    // Gamma
    float GammaCountsPerSecond;

}RotoSlideDataGroup1Structure;

typedef struct WARRIOR_DATAGROUP_1_STRUCTURE
{
    unsigned short SequenceNumberTx;
    unsigned short SequenceNumberRx;
    long UnixGmtTimeSeconds;

    // DRILL_AHEAD, STEERING
    unsigned short Mode;

    // Target ToolFace
    float TargetToolFaceDegrees;

}WarriorDataGroup1Structure;

//------------------------------------------------------------------------------
/*
SequenceNumberTx is incremented after each transmission.

SequenceNumberRx is the last SequenceNumberTx.

If Warrior sets UnixGmtTimeSeconds to a nonzero value, RotoSlide will
update it's clock with this value.

Warrior may set RotoSlide Mode to ROTOSLIDE_MODE_DRILL_AHEAD
or ROTOSLIDE_MODE_STEERING.

If RotoSlide has set FlagMagnetic to a nonzero value, the transmitted
toolface is magnetic, otherwise it is gravity.

*/
//------------------------------------------------------------------------------

