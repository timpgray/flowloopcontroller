//---------------------------------------------------------------------------
// 2014.08.16
//---------------------------------------------------------------------------
#ifndef TimerH
#define TimerH
#include <time.h>
//---------------------------------------------------------------------------

#define TIME_UTC    1
#define TIME_LOCAL  2

class TimerClass
{
    public:
    TimerClass(){InitializeFlag = false;Initialize();}
    int InitializeFlagGet(void);
    void Initialize(void);

    // Time
    unsigned char Hour,Minute,Second,SecondHundredth;
    int Year;
    unsigned char Day,Month,WeekDay;
    void TimeOfDayGet(int TimeZone);
    long GetUnixUtcTime(void);
    int ConvertUnixUtcTimeToUtcDateTime(long UnixUtcTime, int *Year, int *Month, int *Day, int *Hour, int *Minute, int *Second);
    int ConvertUnixUtcTimeToLocalDateTime(long UnixUtcTime, int *Year, int *Month, int *Day, int *Hour, int *Minute, int *Second);
    int ConvertLocalDateTimeToUnixUtcTime(int Year, int Month, int Day, int Hour, int Minute, int Second, long *UnixUtcTime);

    // Delays
    void WaitNMicroSeconds(int n);
    void WaitNMilliSeconds(int n);

    // Performance
    float GetMillisecondsSinceLastCall(int Index);

    private:
    int InitializeFlag;
    double TickCount1;
    double TickCount2;
    double MilliSecondCountDelta;
    long GetMillisecondsSinceLastCallBuffer[100];
    double MicroSecondsToTicks(int MicroSeconds);
    int WaitTicks(unsigned long NumberTicksToWait);

};

extern TimerClass Timer;


#endif
