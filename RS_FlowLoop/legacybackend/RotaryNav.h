#ifndef ROTARYNAV_H
#define ROTARYNAV_H

#define ROTOSLIDE_WIRELESS_CABLE


//------------------------------------------------------------------------------

// Communications
// NodeIdX[7] = {IdBoot, IdApplication, BaudBoot, BaudApplication, BaudDownload, DataBits, Duplex};
// #define DUPLEX_HALF 1
// #define DUPLEX_FULL 2
const unsigned long NodeIdRotaryNav[7]  = {0x85, 0x05, 115200, 9600, 307200, 8, 1};
#define ROTARYNAV_MAX_MESSAGE_SIZE  600

// Status bits
#define ROTARYNAV_STATUS_FLOW_ON            (unsigned int16)0x0001
#define ROTARYNAV_STATUS_SNAPSHOT           (unsigned int16)0x0002
#define ROTARYNAV_STATUS_ROTATING           (unsigned int16)0x0004
#define ROTARYNAV_STATUS_LOGGING_ON         (unsigned int16)0x0008
#define ROTARYNAV_STATUS_BIT4               (unsigned int16)0x0010
#define ROTARYNAV_STATUS_MEM_SCAN           (unsigned int16)0x0020
#define ROTARYNAV_STATUS_MEM_ERASE          (unsigned int16)0x0040
#define ROTARYNAV_STATUS_MEM_FULL           (unsigned int16)0x0080
#define ROTARYNAV_STATUS_TIME_NOINIT        (unsigned int16)0x0100
#define ROTARYNAV_STATUS_BIT9               (unsigned int16)0x0200
#define ROTARYNAV_STATUS_BIT10              (unsigned int16)0x0400
#define ROTARYNAV_STATUS_BIT11              (unsigned int16)0x0800
#define ROTARYNAV_STATUS_SERIALNUMBER_ERR   (unsigned int16)0x1000
#define ROTARYNAV_STATUS_CAL_ERR            (unsigned int16)0x2000
#define ROTARYNAV_STATUS_CONFIG_ERR         (unsigned int16)0x4000
#define ROTARYNAV_STATUS_SYSTEMRECORD_ERR   (unsigned int16)0x8000

// Action types
#define ROTARYNAV_ACTION_NONE               0
#define ROTARYNAV_ACTION_SET_MAG_POWER      1
#define ROTARYNAV_ACTION_MAG_COMP           2
#define ROTARYNAV_ACTION_SET_DAC_BX         3
#define ROTARYNAV_ACTION_SET_DAC_BY         4
#define ROTARYNAV_ACTION_SET_DAC_BZ         5
#define ROTARYNAV_ACTION_SET_DAC_MAG_CAL    6
#define ROTARYNAV_ACTION_TAKE_SURVEY        7
#define ROTARYNAV_ACTION_FORCE_FLOW         8
#define ROTARYNAV_ACTION_SET_MAG_RESET      9
#define ROTARYNAV_ACTION_MAG_AUTOCAL        10

// Datagroup types
#define ROTARYNAV_DATAGROUP_1   1
#define ROTARYNAV_DATAGROUP_2   2
#define ROTARYNAV_DATAGROUP_3   3
#define ROTARYNAV_DATAGROUP_4   4
#define ROTARYNAV_DATAGROUP_5   5

// NonVolatile types
#define ROTARYNAV_NONVOLATILE_CONFIGURATION 1
#define ROTARYNAV_NONVOLATILE_CALIBRATION   2

// Generic variable assignments
enum {GV_DISABLED=0, GV_INC, GV_AZM, GV_RADIAL_VIB, GV_AXIAL_VIB, GV_STICKSLIP};

// Default configuration
#define ROTARYNAV_DEFAULT_CONFIG_FLOW_THRESHOLD_ON_G        0.25
#define ROTARYNAV_DEFAULT_CONFIG_FLOW_THRESHOLD_OFF_G       0.10
#define ROTARYNAV_DEFAULT_CONFIG_SURVEY_DELAY_S             30.0
#define ROTARYNAV_DEFAULT_CONFIG_SNAPSHOT_PERIOD_S          0
#define ROTARYNAV_DEFAULT_CONFIG_LOGGING_PERIOD_S           60
#define ROTARYNAV_DEFAULT_CONFIG_QBUS_ADDRESS               25.0
#define ROTARYNAV_DEFAULT_CONFIG_GV0_ASSIGNMENT             GV_DISABLED
#define ROTARYNAV_DEFAULT_CONFIG_GV1_ASSIGNMENT             GV_DISABLED
#define ROTARYNAV_DEFAULT_CONFIG_GV2_ASSIGNMENT             GV_DISABLED
#define ROTARYNAV_DEFAULT_CONFIG_GV3_ASSIGNMENT             GV_DISABLED
#define ROTARYNAV_DEFAULT_CONFIG_GV4_ASSIGNMENT             GV_INC
#define ROTARYNAV_DEFAULT_CONFIG_GV5_ASSIGNMENT             GV_RADIAL_VIB
#define ROTARYNAV_DEFAULT_CONFIG_GV6_ASSIGNMENT             GV_AXIAL_VIB
#define ROTARYNAV_DEFAULT_CONFIG_GV7_ASSIGNMENT             GV_STICKSLIP

// Default calibration
#define ROTARYNAV_DEFAULT_CAL_GXOFFSET_MV_K0            2500.0
#define ROTARYNAV_DEFAULT_CAL_GYOFFSET_MV_K0            2500.0
#define ROTARYNAV_DEFAULT_CAL_GZOFFSET_MV_K0            2500.0
#define ROTARYNAV_DEFAULT_CAL_GXSCALE_MV_PER_G_K0       ADXL206_SENSITIVITY_MILLIVOLTS_PER_G
#define ROTARYNAV_DEFAULT_CAL_GYSCALE_MV_PER_G_K0       ADXL206_SENSITIVITY_MILLIVOLTS_PER_G
#define ROTARYNAV_DEFAULT_CAL_GZSCALE_MV_PER_G_K0       ADXL206_SENSITIVITY_MILLIVOLTS_PER_G
#define ROTARYNAV_DEFAULT_CAL_BXOFFSET_MV_K0            0
#define ROTARYNAV_DEFAULT_CAL_BYOFFSET_MV_K0            0
#define ROTARYNAV_DEFAULT_CAL_BZOFFSET_MV_K0            0
#define ROTARYNAV_DEFAULT_CAL_BXSCALE_MV_PER_GAUSS_K0   HMC1021S_SCALE_MILLIVOLTS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BYSCALE_MV_PER_GAUSS_K0   HMC1021S_SCALE_MILLIVOLTS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BZSCALE_MV_PER_GAUSS_K0   HMC1021S_SCALE_MILLIVOLTS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BXCAL_MA_PER_GAUSS_K0     HMC1021S_CAL_MILLIAMPS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BYCAL_MA_PER_GAUSS_K0     HMC1021S_CAL_MILLIAMPS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BZCAL_MA_PER_GAUSS_K0     HMC1021S_CAL_MILLIAMPS_PER_GAUSS
#define ROTARYNAV_DEFAULT_CAL_BX_INTERFERENCE_GAUSS     0
#define ROTARYNAV_DEFAULT_CAL_BY_INTERFERENCE_GAUSS     0
#define ROTARYNAV_DEFAULT_CAL_BZ_INTERFERENCE_GAUSS     0

// Memory
#define ROTARYNAV_MEMORY_CHIPS              1
#define ROTARYNAV_MEMORY_BLOCKS_PER_CHIP    1024
#define ROTARYNAV_MEMORY_PAGES_PER_CHIP     8192
#define ROTARYNAV_MEMORY_PAGES_PER_BLOCK    8
#define ROTARYNAV_MEMORY_PAGE_SIZE          528
#define ROTARYNAV_MEMORY_PARAGRAPH_SIZE     528
#define ROTARYNAV_MEMORY_START_PAGE         1

// Memory sync
#define ROTARYNAV_MEMORY_PAGE_HEADER            (unsigned int16)0xABAB
#define ROTARYNAV_MEMORY_RECORD_SYNC_HEADER     (unsigned int16)0xCDCD

// Memory record IDs
#define ROTARYNAV_MEMORY_RECORD_TIMESTAMP   1
#define ROTARYNAV_MEMORY_RECORD_SURVEY      2
#define ROTARYNAV_MEMORY_RECORD_LOGGING     3
#define ROTARYNAV_MEMORY_RECORD_VIBRATION   4
#define ROTARYNAV_MEMORY_RECORD_CALIBRATION 5
#define ROTARYNAV_MEMORY_RECORD_TOOLFACE    6

// ADXL206
#define ADXL206_SENSITIVITY_MILLIVOLTS_PER_G    312.0

// HMC1021S/HMC1022 - 1mV/V/Gauss @ 5V into amp with gain of 251.
// So, 1.0mV/V * 5V *251 = 1.255V (1255.0 mV)
#define HMC1021S_SCALE_MILLIVOLTS_PER_GAUSS     1255.0
#define HMC1021S_CAL_MILLIAMPS_PER_GAUSS        4.6
#define MAG_CAL_MILLIAMPS_PER_MILLIVOLT         (2.0/1000.0)
#define MAG_CAL_MILLIVOLTS_PER_GAUSS            HMC1021S_CAL_MILLIAMPS_PER_GAUSS /MAG_CAL_MILLIAMPS_PER_MILLIVOLT;
#define MAG_BIAS_MILLIVOLTS                     1500.0

// Misc defines
#define ROTARYNAV_TOOLFACE_LOGGING_PERIOD_SECONDS   60


//------------------------------------------------------------------------------

// Everything here needs to be on even byte boundaries (16 bits)

typedef struct ROTARYNAV_CONFIGURATION_STRUCTURE
{
    float FlowThresholdOnG;
    float FlowThresholdOffG;
    float SurveyDelaySeconds;
    float SnapShotPeriodSeconds;
    float LoggingPeriodSeconds;
    float QBusAddress;
    unsigned int8 GenericVariables[8];
    unsigned int16 CheckSum;
}RotaryNavConfigurationStructure;

typedef struct ROTARYNAV_CALIBRATION_STRUCTURE
{
    float GxOffsetMilliVoltsK[3];
    float GyOffsetMilliVoltsK[3];
    float GzOffsetMilliVoltsK[3];
    float GxScaleMilliVoltsPerGK[3];
    float GyScaleMilliVoltsPerGK[3];
    float GzScaleMilliVoltsPerGK[3];

    float BxOffsetMilliVoltsK[3];
    float ByOffsetMilliVoltsK[3];
    float BzOffsetMilliVoltsK[3];
    float BxScaleMilliVoltsPerGaussK[3];
    float ByScaleMilliVoltsPerGaussK[3];
    float BzScaleMilliVoltsPerGaussK[3];

    float BxCalMilliAmpsPerGauss;
    float ByCalMilliAmpsPerGauss;
    float BzCalMilliAmpsPerGauss;
    
    float BxInterferenceGauss;
    float ByInterferenceGauss;
    float BzInterferenceGauss;
    
    unsigned int16 CheckSum;
}RotaryNavCalibrationStructure;

typedef struct ROTARYNAV_DATAGROUP_1_STRUCTURE
{
    unsigned int16 NodeStatus;
    unsigned int16 Mode;
    unsigned int32 TimeStampSeconds;
    unsigned int8 TimeStampDate[6];

    float BatteryVolts;
    float TemperatureCelsius;
    float FlowxMilliVolts;
    float FlowzMilliVolts;
    float VibxMilliVolts;
    float VibyMilliVolts;
    float VibzMilliVolts;
    float GxMilliVolts;
    float GyMilliVolts;
    float GzMilliVolts;
    float BxMilliVolts;
    float ByMilliVolts;
    float BzMilliVolts;

    float DacBxMilliVolts;
    float DacByMilliVolts;
    float DacBzMilliVolts;
    float DacMagCalMilliVolts;
    float BxpMilliVolts;
    float BxnMilliVolts;
    float BypMilliVolts;
    float BynMilliVolts;
    float BzpMilliVolts;
    float BznMilliVolts;

    // Circulation/Rotation
    unsigned int16 FlowThresholdTimer;
    unsigned int16 FlowState;
    unsigned int32 FlowOffTimerSeconds;
    unsigned int32 FlowOnTimerSeconds;
    float FlowxzG;
    float CirculatingHours;
    float RevsPerMinute;
    float RevsPerMinuteMin;
    float RevsPerMinuteMax;
    float StickSlipSeverity;

    // Vibration
    float VibrationXG;
    float VibrationYG;
    float VibrationZG;

    float TestVariable1;
    float TestVariable2;
    float TestVariable3;
    float TestVariable4;
}RotaryNavDataGroup1Structure;

typedef struct ROTARYNAV_DATAGROUP_2_STRUCTURE
{
    float GxMilliVolts;
    float GyMilliVolts;
    float GzMilliVolts;
    float BxMilliVolts;
    float ByMilliVolts;
    float BzMilliVolts;

    float GxOffsetMilliVolts;
    float GyOffsetMilliVolts;
    float GzOffsetMilliVolts;
    float GxScaleMilliVoltsPerG;
    float GyScaleMilliVoltsPerG;
    float GzScaleMilliVoltsPerG;

    float BxOffsetMilliVolts;
    float ByOffsetMilliVolts;
    float BzOffsetMilliVolts;
    float BxScaleMilliVoltsPerGauss;
    float ByScaleMilliVoltsPerGauss;
    float BzScaleMilliVoltsPerGauss;

    float BxBiasMilliVolts;
    float ByBiasMilliVolts;
    float BzBiasMilliVolts;

}RotaryNavDataGroup2Structure;

typedef struct ROTARYNAV_DATAGROUP_3_STRUCTURE
{
    unsigned int16 NodeStatus;
    unsigned int16 Mode;
    unsigned int32 TimeStampSeconds;
    unsigned int8 TimeStampDate[6];

    // Stationary survey
    float BatteryVolts;
    float TemperatureCelsius;
    float Gx, Gy, Gz, Gxy;
    float Bx, By, Bz;
    float InclinationDegrees;
    float AzimuthDegrees;
    float MagneticDipDegrees;
    float TotalMagneticFieldGauss;
    float TotalGravityFieldG;
    float MagneticToolFaceDegrees;
    float GravityToolFaceDegrees;
    float RevsPerMinute;

}RotaryNavDataGroup3Structure;

typedef struct ROTARYNAV_DATAGROUP_4_STRUCTURE
{
    unsigned int16 NodeStatus;
    unsigned int16 Mode;
    unsigned int32 TimeStampSeconds;
    unsigned int8 TimeStampDate[6];

    // Circulating survey
    float BatteryVolts;
    float TemperatureCelsius;
    float Gx, Gy, Gz, Gxy;
    float Bx, By, Bz;
    float InclinationDegrees;
    float AzimuthDegrees;
    float MagneticDipDegrees;
    float TotalMagneticFieldGauss;
    float TotalGravityFieldG;
    float MagneticToolFaceDegrees;
    float GravityToolFaceDegrees;
    float RevsPerMinute;

}RotaryNavDataGroup4Structure;

//------------------------------------------------------------------------------
// Memory records

typedef struct ROTARYNAV_MEMORY_RECORD_TIMESTAMP_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordTimeStampStructure;

typedef struct ROTARYNAV_MEMORY_RECORD_SURVEY_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    signed int16 BatteryVoltsX10;
    signed int16 TemperatureCelsiusX10;
    signed int16 InclinationDegreesX10;
    signed int16 AzimuthDegreesX10;
    signed int16 GravityToolFaceDegreesX10;
    signed int16 MagneticToolFaceDegreesX10;
    signed int16 MagneticDipDegreesX10;
    signed int16 GxGX1000;
    signed int16 GyGX1000;
    signed int16 GzGX1000;
    signed int16 TotalGravityFieldGX1000;
    signed int16 BxGaussX1000;
    signed int16 ByGaussX1000;
    signed int16 BzGaussX1000;
    signed int16 TotalMagneticFieldGaussX1000;
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordSurveyStructure;

typedef struct ROTARYNAV_MEMORY_RECORD_LOGGING_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    unsigned int16 NodeStatus;
    signed int16 InclinationDegreesX100;
    signed int16 AzimuthDegreesX10;
    signed int16 GxyGX1000;
    signed int16 GzGX1000;
    signed int16 TemperatureCelsiusX10;
    signed int16 FlowxzGX100;
    signed int16 VibrationxGX10;
    signed int16 VibrationyGX10;
    signed int16 VibrationzGX10;
    signed int16 RpmAvgX10;
    signed int16 StickSlipSeverity;
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordLoggingStructure;

typedef struct ROTARYNAV_MEMORY_RECORD_VIBRATION_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    unsigned int16 NodeStatus;
    signed int16 VibxGX10[500];
    signed int16 VibyGX10[500];
    signed int16 VibzGX10[500];
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordVibrationStructure;

typedef struct ROTARYNAV_MEMORY_RECORD_CALIBRATION_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    signed int16 TemperatureCelsiusX10;
    signed int16 BxOffsetMilliVoltsX10;
    signed int16 ByOffsetMilliVoltsX10;
    signed int16 BzOffsetMilliVoltsX10;
    signed int16 BxScaleMilliVoltsPerGaussX10;
    signed int16 ByScaleMilliVoltsPerGaussX10;
    signed int16 BzScaleMilliVoltsPerGaussX10;
    signed int16 BxBiasMilliVoltsX10;
    signed int16 ByBiasMilliVoltsX10;
    signed int16 BzBiasMilliVoltsX10;
    signed int16 DacBxMilliVolts;
    signed int16 DacByMilliVolts;
    signed int16 DacBzMilliVolts;
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordCalibrationStructure;

typedef struct ROTARYNAV_MEMORY_RECORD_TOOLFACE_STRUCTURE
{
    // Header
    unsigned int16 SyncHeader;
    unsigned int16 RecordId;
    unsigned int16 RecordLength;
    unsigned int8 TimeStamp[6];
    // Data
    unsigned int16 NodeStatus;
    signed int16 RpmX10[ROTARYNAV_TOOLFACE_LOGGING_PERIOD_SECONDS];
    signed int16 ToolFacex10[ROTARYNAV_TOOLFACE_LOGGING_PERIOD_SECONDS];
    // CheckSum
    unsigned int16 CheckSum;
}RotaryNavMemoryRecordToolFaceStructure;

//------------------------------------------------------------------------------

#endif // ROTARYNAV_H
