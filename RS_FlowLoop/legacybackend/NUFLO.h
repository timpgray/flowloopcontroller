//---------------------------------------------------------------------------
#ifndef NufloH
#define NufloH
//---------------------------------------------------------------------------

// Node ID
#define MODBUS_NODE_ID_NUFLO            0x04

// Register addresses
#define NUFLO_ADDRESS_NONE              0x0000
#define NUFLO_ADDRESS_INPUT_SENSITIVITY 0x07D6
#define NUFLO_ADDRESS_FLOW_RATE1        0x4275
#define NUFLO_ADDRESS_FLOW_RATE0        0x4276
#define NUFLO_ADDRESS_FLOW_RATE32       0x1B5E

// Input sensitivity levels
#define NUFLO_INPUT_SENSITIVITY_LOW     1
#define NUFLO_INPUT_SENSITIVITY_MED     2
#define NUFLO_INPUT_SENSITIVITY_HI      3

// Control word

class NUFLOClass
{
    public:
    NUFLOClass(){;}

    int StatusRead(void);
    int InputSensitivityRead(unsigned short *InputSensitivity);
    int InputSensitivityWrite(unsigned short InputSensitivity);
    int FlowRateRead(float *FlowRateGallonsPerMinute);

    unsigned short Status;

    private:

};
extern NUFLOClass NUFLO;


#endif





