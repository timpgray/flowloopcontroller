#ifndef WITSCACHE_H
#define WITSCACHE_H

#include <string>
#include <map>
#include <vector>

using namespace std;

class WITSCache
{
public:
    WITSCache();

    void clear();
    bool add(string key, float value);
    void addKey(string key);
    bool isWanted(string key);
    float getValue(string key, bool* status = nullptr);

protected:
    map<string,float> m_dataCache;
    vector<string> m_desiredKeys;
};

#endif // WITSCACHE_H
