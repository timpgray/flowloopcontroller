//---------------------------------------------------------------------------
// 2014.08.16
//------------------------------------------------------------------------------
#ifndef NodecommonH
#define NodecommonH
//------------------------------------------------------------------------------

// Common to all nodes

// Communication protocol
enum {MSG_TO=0, MSG_FROM, MSG_LENGTH0, MSG_LENGTH1, MSG_CMD, MSG_DATA};
#define MSG_OVERHEAD    6
#define ACK             0x00
#define NACK            0xFF
#define COM_PROTOCOL_TIMEOUT_MS 5

// Node Indentity
#ifndef BROADCAST_ID
#define BROADCAST_ID    0xFF
enum {ID_BOOT=0, ID_APPLICATION, BAUD_BOOT, BAUD_APPLICATION, BAUD_DOWNLOAD, DATA_BITS, DUPLEX};
#endif

// Operating modes
#define MODE_NONE           0
#define MODE_STARTUP        1
#define MODE_IDLE           2
#define MODE_TRIP           3
#define MODE_ACTIVE         4
#define MODE_DOWNLOAD       5
#define MODE_CALIBRATION    6
#define MODE_BOOT           0x0B

// Command codes
#define CMD_FIRMWARE_ID_RD      0x01
#define CMD_MODE_RDWR           0x02
#define CMD_SERIALNUMBER_RDWR   0x03
#define CMD_NONVOLATILE_RDWR    0x04
#define CMD_DATAGROUP_RDWR      0x05
#define CMD_ACTION              0x06
#define CMD_SYSTEM_RDWR         0x07
#define CMD_TIME_RDWR           0x08
#define CMD_MEMORY_RDWR         0x09
#define CMD_MEMORY_ERASE        0x0A
#define CMD_PROGRAM_WR          0x0B

// Misc
enum {YEAR=0, MONTH, DAY, HOUR, MINUTE, SECOND};

// ASCII characters
#define ASCII_DEGREE    176
#define ASCII_COPYRIGHT 169
#define ASCII_RESERVED  174
#define ASCII_TOLERANCE 177

// Math constants
#define PI                  3.141592654
#define DEGREES_PER_RADIAN  (180.0/PI)
#define RADIANS_PER_DEGREE  (PI/180.0)
#define NANOTESLA_PER_GAUSS 100000.0
#define GAUSS_PER_NANOTESLA 0.00001

// Misc
enum {FAIL=-1, NEITHER, PASS};

//------------------------------------------------------------------------------

// This must be compatible with SCI TX/RX buffer sizes
#define MAX_MESSAGE_SIZE    600
typedef struct MESSAGE_STRUCTURE
{
    unsigned int8 Buffer[MAX_MESSAGE_SIZE];
    unsigned int16 TxDataLength;
    unsigned int16 TxLength;
    unsigned int16 ExpectRxCount;
    unsigned int16 RxLength;
}MessageStructure;

typedef struct SERIAL_NUMBER_STRUCTURE
{
    int8 String[16];
    unsigned int16 CheckSum;
}SerialNumberStructure;

typedef struct SYSTEM_RECORD_STRUCTURE
{
    unsigned int16 TotalOpHours;
    unsigned int16 MaxTemperatureCelsius;
    unsigned int32 MemoryPageCount;
    unsigned int16 MemoryPageSize;
    unsigned int16 MemoryParagraphSize;
    unsigned int16 MemoryPageWritePointer;
    unsigned int16 CheckSum;
}SystemRecordStructure;

//------------------------------------------------------------------------------

#endif

