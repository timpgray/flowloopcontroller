#include "witscache.h"

using namespace std;

WITSCache::WITSCache()
{

}


void WITSCache::clear()
{
    m_dataCache.clear();
}

bool WITSCache::add(string key, float value)
{

    if(isWanted(key))
    {
        m_dataCache[key] = value;
    }

    return m_dataCache.size() == m_desiredKeys.size();
}

void WITSCache::addKey(string key)
{
    if(!isWanted(key)) // kind of a side effect, but i will take it
    {
        m_desiredKeys.push_back(key);
    }
}

bool WITSCache::isWanted(string key)
{
    bool wanted = false;

    for(unsigned int i=0;i<m_desiredKeys.size();i++)
    {
        if(m_desiredKeys.at(i) == key)
        {
            wanted = true;
            break;
        }
    }

    return wanted;
}


float WITSCache::getValue(string key, bool* status)
{
    bool convstatus = false;
    float value = 0;

    map<string,float>::iterator it = m_dataCache.find(key);

    if (it != m_dataCache.end())
    {
        value = m_dataCache.at(key);
        convstatus = true;
    }

    if(status != nullptr)
    {
        *status = convstatus;
    }

    return value;
}
