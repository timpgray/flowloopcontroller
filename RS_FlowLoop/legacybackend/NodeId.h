// 2013.06.15


//------------------------------------------------------------------------------
/*

Triton Node IDs
Node            Boot    App     BootBaud    AppBaud     DownloadBaud    Bits    Duplex
Broadcast       FF      FF
Host            81      01      115200      9600        115200          8       1

All nodes may react, but never respond to BROADCAST IDs

*/
//------------------------------------------------------------------------------

#ifndef BROADCAST_ID
#define BROADCAST_ID    0xFF
enum {ID_BOOT=0, ID_APPLICATION, BAUD_BOOT, BAUD_APPLICATION, BAUD_DOWNLOAD, DATA_BITS, DUPLEX};
#define DUPLEX_HALF 1
#define DUPLEX_FULL 2
#endif

// NodeIdX[7] = {IdBoot, IdApplication, BaudBoot, BaudApplication, BaudDownload, DataBits, Duplex};
const unsigned long NodeIdHost[7]       = {0x81, 0x01, 115200, 115200, 115200, 8, 1};


//------------------------------------------------------------------------------
