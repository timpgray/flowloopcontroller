//---------------------------------------------------------------------------
// 2014.08.19
//---------------------------------------------------------------------------

#include <windows.h>
#include "RS232Port.h"
#include "Timer.h"
#include <stdio.h>
#include <winioctl.h>
#include <qapplication.h>

//---------------------------------------------------------------------------

RS232PortClass RS232Port1;
RS232PortClass RS232Port2;
RS232PortClass RS232Port3;
RS232PortClass RS232Port4;

#define INTERCHARACTER_DELAY_US 1000
#define TRANSMIT_TIMEOUT_MS     20
#define RECEIVE_TIMEOUT_MS      200

//---------------------------------------------------------------------------
int RS232PortClass::InitializeFlagGet(void)
{
    return(InitializeFlag);
}
//---------------------------------------------------------------------------
int RS232PortClass::Initialize(int PortNumber, int Baud, int DataBits, int ParityType, int Duplex)
{
    char Buffer[64];
    DWORD BytesReturned;
    char ParityCharacter;

    FlagTxEcho = false;
    switch (ParityType)
    {
        default:
        case PARITY_NONE:
            ParityCharacter = 'N';
            break;
        case PARITY_EVEN:
            ParityCharacter = 'E';
            break;
        case PARITY_ODD:
            ParityCharacter = 'O';
            break;
    }

    Release();

    // Try to open the port
    sprintf(Buffer,"\\\\.\\COM%d",PortNumber);
    WCHAR target[120];
    MultiByteToWideChar(CP_ACP, 0, Buffer, -1, target, 120);

    HandleSerialPort = NULL;
    HandleSerialPort = CreateFile(target,GENERIC_READ | GENERIC_WRITE,0,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
    if (HandleSerialPort == (void *)0xFFFFFFFF)
    {
        CloseHandle(HandleSerialPort);
        HandleSerialPort = NULL;
        InitializeFlag = false;
        return(false);
    }

    // The port is open so set it up
    SerialTimeouts.ReadIntervalTimeout = READ_INTERVAL_TIMEOUT;
    SerialTimeouts.ReadTotalTimeoutConstant = READ_TOTAL_TIMEOUT_CONSTANT;
    SerialTimeouts.ReadTotalTimeoutMultiplier = READ_TOTAL_TIMEOUT_MULTIPLIER;
    SerialTimeouts.WriteTotalTimeoutConstant = WRITE_TOTAL_TIMEOUT_CONSTANT;
    SerialTimeouts.WriteTotalTimeoutMultiplier = WRITE_TOTAL_TIMEOUT_MULTIPLIER;
    SetCommTimeouts(HandleSerialPort, &SerialTimeouts);
    DcbSerialPort.DCBlength = sizeof(DCB);
    GetCommState(HandleSerialPort, &DcbSerialPort);
    MultiByteToWideChar(CP_ACP, 0, Buffer, -1, target, 120);
    sprintf(Buffer,"%d,%c,8,1", Baud, ParityCharacter);
    BuildCommDCB(target, &DcbSerialPort);
    SetCommState(HandleSerialPort, &DcbSerialPort);
    // Doing this here to init SerialLineControl so I can use DeviceIoControl later to set parity
    DeviceIoControl(HandleSerialPort, IOCTL_SERIAL_GET_LINE_CONTROL, NULL, 0, &SerialLineControl, sizeof(SerialLineControl), &BytesReturned, NULL);

    // Purge
    PurgeComm(HandleSerialPort, (PURGE_RXCLEAR | PURGE_TXCLEAR));

    FlagTxEcho = false;
    if (Duplex == DUPLEX_HALF)
        FlagTxEcho = true;
    Flag9Bit = false;
    if (DataBits == 9)
        Flag9Bit = true;

    InterCharacterDelayMicroSeconds = INTERCHARACTER_DELAY_US;

    InitializeFlag = true;

    return(true);
}
//---------------------------------------------------------------------------
int RS232PortClass::SetBaud(int Baud)
{
    char Buffer[64];
    WCHAR target[120];

    // The port is open so set it up
    SerialTimeouts.ReadIntervalTimeout = READ_INTERVAL_TIMEOUT;
    SerialTimeouts.ReadTotalTimeoutConstant = READ_TOTAL_TIMEOUT_CONSTANT;
    SerialTimeouts.ReadTotalTimeoutMultiplier = READ_TOTAL_TIMEOUT_MULTIPLIER;
    SerialTimeouts.WriteTotalTimeoutConstant = WRITE_TOTAL_TIMEOUT_CONSTANT;
    SerialTimeouts.WriteTotalTimeoutMultiplier = WRITE_TOTAL_TIMEOUT_MULTIPLIER;
    SetCommTimeouts(HandleSerialPort, &SerialTimeouts);
    DcbSerialPort.DCBlength = sizeof(DCB);
    GetCommState(HandleSerialPort, &DcbSerialPort);
    sprintf(Buffer,"%d,N,8,1", Baud);
    MultiByteToWideChar(CP_ACP, 0, Buffer, -1, target, 120);
    BuildCommDCB(target, &DcbSerialPort);
    SetCommState(HandleSerialPort, &DcbSerialPort);

    return(true);
}
//---------------------------------------------------------------------------
int RS232PortClass::SetParity(int Parity)
{
    // Options are NOPARITY, ODDPARITY, EVENPARITY, MARKPARITY, SPACEPARITY

    DWORD BytesReturned;
    int Result;
    SerialLineControl.Parity = Parity;
    Result = DeviceIoControl(HandleSerialPort, IOCTL_SERIAL_SET_LINE_CONTROL, &SerialLineControl, sizeof(SerialLineControl), NULL, 0, &BytesReturned, NULL);

    return(Result);

}
//---------------------------------------------------------------------------
int RS232PortClass::Transmit(unsigned short Options)
{
    unsigned short i;
    unsigned long Sent;
    DWORD NumBytesRead;
    unsigned long TimeoutMilliSeconds;

    if (InitializeFlag == false)
        return(COM_FAIL);

    // Purge rx
    RxCount = CheckReceive();
    if (RxCount > 0)
        Receive(RxCount);
    RxCount = 0;

    // Send message
    if (Flag9Bit == true)
    {
        // Send the first byte with 9th bit set
        SetParity(MARKPARITY);
        TransmitTimeoutMilliSeconds = TRANSMIT_TIMEOUT_MS;
        TimeoutMilliSeconds = 0;
        Sent = 0;
        WriteFile(HandleSerialPort,TxBuffer,1,&Sent,NULL);
        while(Sent < 1)
        {
            QApplication::instance()->processEvents();
            Timer.WaitNMilliSeconds(1);
            TimeoutMilliSeconds++;
            if (TimeoutMilliSeconds > TransmitTimeoutMilliSeconds)
                return(COM_FAIL);
        }
        // Send the remaining bytes with 9th bit clear
        SetParity(SPACEPARITY);
        // Make timeout a function of the number of bytes being sent.
        TransmitTimeoutMilliSeconds = TRANSMIT_TIMEOUT_MS +TxCount;
        TimeoutMilliSeconds = 0;
        Sent = 0;
        WriteFile(HandleSerialPort,TxBuffer +1,TxCount -1,&Sent,NULL);
        while(Sent < (unsigned)TxCount-1)
        {
            QApplication::instance()->processEvents();
            Timer.WaitNMilliSeconds(1);
            TimeoutMilliSeconds++;
            if (TimeoutMilliSeconds > TransmitTimeoutMilliSeconds)
                return(COM_FAIL);
        }
    }
    else
    {
        if (InterCharacterDelayMicroSeconds == 0)
        {
            // Make timeout a function of the number of bytes being sent.
            TransmitTimeoutMilliSeconds = TRANSMIT_TIMEOUT_MS +TxCount;
            TimeoutMilliSeconds = 0;
            Sent = 0;
            WriteFile(HandleSerialPort,TxBuffer,TxCount,&Sent,NULL);
            while(Sent < (unsigned)TxCount)
            {
                QApplication::instance()->processEvents();
                Timer.WaitNMilliSeconds(1);
                TimeoutMilliSeconds++;
                if (TimeoutMilliSeconds > TransmitTimeoutMilliSeconds)
                    return(COM_FAIL);
            }
        }
        else
        {
            for (i=0; i<(unsigned short)TxCount; i++)
            {
                TransmitTimeoutMilliSeconds = TRANSMIT_TIMEOUT_MS;
                TimeoutMilliSeconds = 0;
                Sent = 0;
                WriteFile(HandleSerialPort,&TxBuffer[i],1,&Sent,NULL);
                while(Sent < 1)
                {
                    QApplication::instance()->processEvents();
                    Timer.WaitNMilliSeconds(1);
                    TimeoutMilliSeconds++;
                    if (TimeoutMilliSeconds > TransmitTimeoutMilliSeconds)
                        return(COM_FAIL);
                }
                // Insert the intercharacter delay.
                Timer.WaitNMicroSeconds(InterCharacterDelayMicroSeconds);
            }
        }
    }

    // Get the loopback
    if (FlagTxEcho == true)
    {
        // Make timeout a function of the number of bytes being received.
        ReceiveTimeoutMilliSeconds = RECEIVE_TIMEOUT_MS +TxCount;
        TimeoutMilliSeconds = 0;
        while( (CheckReceive() < (int)TxCount) && (TimeoutMilliSeconds < ReceiveTimeoutMilliSeconds) )
        {
            QApplication::instance()->processEvents();
            Timer.WaitNMilliSeconds(1);
            TimeoutMilliSeconds++;
        }
        if (TimeoutMilliSeconds >= ReceiveTimeoutMilliSeconds)
            return(COM_FAIL);
        ReadFile(HandleSerialPort, RxBuffer, TxCount, &NumBytesRead, NULL);
    }

    // Get the response
    if (ExpectRxCount == 0)
    {
        RxCount = 0;
        return(COM_GOOD);
    }
    else
    {
        ReceiveTimeoutMilliSeconds = RECEIVE_TIMEOUT_MS +ExpectRxCount;
        TimeoutMilliSeconds = 0;
        while( (CheckReceive() < (int)ExpectRxCount) && (TimeoutMilliSeconds < ReceiveTimeoutMilliSeconds) )
        {
            QApplication::instance()->processEvents();
            Timer.WaitNMilliSeconds(1);
            TimeoutMilliSeconds++;
        }
        ReadFile(HandleSerialPort, RxBuffer, ExpectRxCount, &NumBytesRead, NULL);
        RxCount = (int)NumBytesRead;
    }

    // Node should always respond
    if (RxCount == 0)
        return(COM_NO_REPLY);
    if ( (RxCount > 0) && (TimeoutMilliSeconds >= ReceiveTimeoutMilliSeconds) )
        return(COM_TIMEOUT);

    return(COM_GOOD);

}
//---------------------------------------------------------------------------
int RS232PortClass::Receive(int MaxBytes)
{
    int NumberOfBytes;
    DWORD NumBytesRead;
    unsigned long Errors;
    COMSTAT Stat;

    if (MaxBytes > PC_TXRXBUFFER_SIZE)
        MaxBytes = PC_TXRXBUFFER_SIZE;

    NumberOfBytes = 0;
    RxCount = 0;
    if (InitializeFlag == true)
    {
        ClearCommError(HandleSerialPort, &Errors, &Stat);
        if (Stat.cbInQue > 0)
        {
            NumberOfBytes = Stat.cbInQue;
            if (NumberOfBytes > MaxBytes)
                NumberOfBytes = MaxBytes;
            ReadFile(HandleSerialPort, RxBuffer, NumberOfBytes, &NumBytesRead, NULL);
            RxCount = (int)NumBytesRead;
        }
    }

    return(NumberOfBytes);
}
//---------------------------------------------------------------------------
int RS232PortClass::CheckReceive(void)
{
    int NumberOfBytes;
    DWORD NumBytesRead;
    unsigned long Errors;
    COMSTAT Stat;

    NumberOfBytes = 0;
    if (InitializeFlag == true)
    {
        ClearCommError(HandleSerialPort, &Errors, &Stat);
        NumberOfBytes = Stat.cbInQue;
    }

    return(NumberOfBytes);
}
//---------------------------------------------------------------------------
void RS232PortClass::Release(void)
{
    if (HandleSerialPort != NULL)
        CloseHandle(HandleSerialPort);
    HandleSerialPort = NULL;
    InitializeFlag = false;

}
//---------------------------------------------------------------------------
void RS232PortClass::SetDTR(int Level)
{
    switch (Level)
    {
        case 0:
            EscapeCommFunction(HandleSerialPort,CLRDTR);
            break;
        case 1:
            EscapeCommFunction(HandleSerialPort,SETDTR);
            break;
    }
}
//---------------------------------------------------------------------------
void RS232PortClass::SetRTS(int Level)
{
    switch (Level)
    {
        case 0:
            EscapeCommFunction(HandleSerialPort,CLRRTS);
            break;
        case 1:
            EscapeCommFunction(HandleSerialPort,SETRTS);
            break;
    }

}
//---------------------------------------------------------------------------
int RS232PortClass::CheckDsrLevel(void)
{
    DWORD ModemStatus;

    GetCommModemStatus(HandleSerialPort,&ModemStatus);
    if (ModemStatus & MS_DSR_ON)
        return(1);
    else
        return(0);
}
//---------------------------------------------------------------------------
int RS232PortClass::CheckCtsLevel(void)
{
    DWORD ModemStatus;

    GetCommModemStatus(HandleSerialPort,&ModemStatus);
    if (ModemStatus & MS_CTS_ON)
        return(1);
    else
        return(0);
}
//---------------------------------------------------------------------------

