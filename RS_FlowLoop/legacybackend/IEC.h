//---------------------------------------------------------------------------
#ifndef IecH
#define IecH
//---------------------------------------------------------------------------
#include "IECBox.h"
#include "IECThread.h"
//---------------------------------------------------------------------------

class IECClass
{
    public:
    IECClass(){}
    int Initialize(int ComPort, int BaudRate);
    int SetComPort(int ComPort, int BaudRate);
    int Start(void);
    int Start(IECThreadClass* pthreadclass);
    int Stop(void);
    int ReadIecStructure(void);
    int WriteIecStructure(void);
    int RequestRotoSlideControl(int State);
    int CheckRotoSlideControlGranted(void);
    int SetPumpRun(int State);
    int SetPumpSpeed(float RevsPerMinute);
    int SetTopDriveRun(int State);
    int SetTopDriveSpeed(float RevsPerMinute);

    int FlagIecEnabled;
    int ComPortNumber;
    int Baud;
    long MessageCounter;
    int FlagTopDriveControlGranted;

    private:

};

extern IECClass IEC;


#endif

