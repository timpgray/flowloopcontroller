//---------------------------------------------------------------------------
/*

This class provides the command/data interface to the IEC box.

Usage:
    Include IEC.h
    call IEC.Initialize(); at startup
    call IEC.Start(); to start
    call IEC.Stop(); to stop

*/
//---------------------------------------------------------------------------

#include <windows.h>
#include "IEC.h"
#include "RS232Port.h"

IECClass IEC;
IecInterfaceStructure IecInterfaceRead;
IecInterfaceStructure IecInterfaceWrite;

//---------------------------------------------------------------------------
int IECClass::Initialize(int ComPort, int BaudRate)
{
    int Result;

    FlagIecEnabled = false;
    FlagTopDriveControlGranted = false;

    Result = SetComPort(ComPort, BaudRate);
    if (Result == false)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::SetComPort(int ComPort, int BaudRate)
{
    if (FlagIecEnabled == true)
        return(false);

    ComPortNumber = ComPort;
    Baud = BaudRate;

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::Start(void)
{

    if (FlagIecEnabled == true)
    {
        FlagIecEnabled = false;
        IECThread->Stop();
        Sleep(100);
        RS232Port1.Release();
    }

    // Initialize the port.
    Baud = IEC_BUAD_RATE;
    if (RS232Port1.Initialize(ComPortNumber, Baud, 8, PARITY_NONE, DUPLEX_HALF) == false)
        return(false);

    // Create the thread.
    IECThread = new IECThreadClass(false);
    // Don't need to call IECThread->Start because the thread was created
    // with CreateSuspended set to false.
    // DaqThread->Start();
    FlagIecEnabled = true;

    return(true);
}

int IECClass::Start(IECThreadClass* pthreadclass)
{

    // Create the thread.
    IECThread = pthreadclass;
    FlagIecEnabled = true;

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::Stop(void)
{
    if (FlagIecEnabled == false)
    {
        // Nothing to stop.
        return(true);
    }
    FlagIecEnabled = false;

    IECThread->Stop();
    Sleep(100);
    RS232Port1.Release();

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::ReadIecStructure(void)
{
    if (FlagIecEnabled == false)
        return(false);

    IECThread->ReadIecStructure((void *)(&IecInterfaceRead));
    MessageCounter = IECThread->MessageCounter;

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::WriteIecStructure(void)
{
    if (FlagIecEnabled == false)
        return(false);

    IECThread->WriteIecStructure((void *)(&IecInterfaceWrite));

    return(true);
}
//---------------------------------------------------------------------------
int IECClass::RequestRotoSlideControl(int State)
{
    int Result;

    if (State == true)
    {
        IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_PUMP_REQUEST;
        IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
        IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_MOTOR_FORWARD;
    }
    else
    {
        IecInterfaceWrite.ControlWord &= ~IEC_CONTROLWORD_PUMP_REQUEST;
        IecInterfaceWrite.ControlWord &= ~IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
        IecInterfaceWrite.ControlWord &= ~IEC_CONTROLWORD_MOTOR_FORWARD;
    }

    IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_HEARTBEAT;

    Result = WriteIecStructure();

    return(Result);
}
//---------------------------------------------------------------------------
int IECClass::CheckRotoSlideControlGranted(void)
{
    if (IecInterfaceRead.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)
        return(true);
    else
        return(false);
}
//---------------------------------------------------------------------------
int IECClass::SetPumpRun(int State)
{
    int Result;

    if (State == true)
    {
        IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_PUMP_RUN;
    }
    else
    {
        IecInterfaceWrite.ControlWord &= ~IEC_CONTROLWORD_PUMP_RUN;
    }
    Result = WriteIecStructure();

    return(Result);
}
//---------------------------------------------------------------------------
int IECClass::SetPumpSpeed(float RevsPerMinute)
{
    int Result;

    IecInterfaceWrite.PumpSpeedCommandRpmX10 = (short)(RevsPerMinute *10.0 +0.5);
    Result = WriteIecStructure();

    return(Result);
}
//---------------------------------------------------------------------------
int IECClass::SetTopDriveRun(int State)
{
    int Result;

    if (State == true)
    {
        IecInterfaceWrite.ControlWord |= IEC_CONTROLWORD_TOPDRIVE_RUN;
    }
    else
    {
        IecInterfaceWrite.ControlWord &= ~IEC_CONTROLWORD_TOPDRIVE_RUN;
    }
    Result = WriteIecStructure();

    return(Result);
}
//---------------------------------------------------------------------------
int IECClass::SetTopDriveSpeed(float RevsPerMinute)
{
    int Result;

    IecInterfaceWrite.TopDriveSpeedCommandRpmX100 = (short)(RevsPerMinute *100.0 +0.5);
    Result = WriteIecStructure();

    return(Result);
}
//---------------------------------------------------------------------------

