//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "RotaryNavCommand.h"
#include "RS232Port.h"
#include "Utility.h"
#include "NodeId.h"
#include "RotaryNav.h"
#include <qloggingcategory.h>
#include <qfile.h>
#include <qapplication.h>
#include <qdatetime.h>


QLoggingCategory ROTARYNAVCOMMANDCLASS("RotaryNavCommandClass");



RotaryNavCommandClass RotaryNavCommand;

char RotaryNavFirmwareId[2];
SerialNumberStructure RotaryNavSerialNumber;
RotaryNavDataGroup1Structure RotaryNavData1;
RotaryNavDataGroup2Structure RotaryNavData2;
RotaryNavDataGroup3Structure RotaryNavData3;
RotaryNavDataGroup4Structure RotaryNavData4;
RotaryNavConfigurationStructure RotaryNavConfiguration;
RotaryNavCalibrationStructure RotaryNavCalibration;
SystemRecordStructure RotaryNavSystemRecord;

//---------------------------------------------------------------------------
int RotaryNavCommandClass::SendCommand(void)
{
    int Result;
    int i;
    unsigned char CheckSum;
    unsigned short MessageLength;
    int FlagLengthGood;
    int FlagIdGood;
    int FlagCheckSumGood;

    // Clear the RxBuffer
    memset(RS232Port2.RxBuffer, 0, PC_TXRXBUFFER_SIZE);
    memset(RS232Port2.TxBuffer, 0, PC_TXRXBUFFER_SIZE);

    // Add addresses
    Message.Buffer[MSG_TO] = NodeIdRotaryNav[ID_APPLICATION];
    Message.Buffer[MSG_FROM] = NodeIdHost[ID_APPLICATION];
    // Calculate total length
    Message.TxLength = MSG_OVERHEAD +Message.TxDataLength;
    // Add length
    Message.Buffer[MSG_LENGTH0] = (unsigned char)((Message.TxLength >> 0) & 0x00FF);
    Message.Buffer[MSG_LENGTH1] = (unsigned char)((Message.TxLength >> 8) & 0x00FF);

    // Add checksum
    CheckSum = 0;
    for (i=0; i<(int)(Message.TxLength -1); i++)
        CheckSum += Message.Buffer[i];
     Message.Buffer[Message.TxLength -1] = CheckSum;

     m_serialPort.clear();      // discard any pending data at this point...

     int transmitted = m_serialPort.write((const char*)Message.Buffer, Message.TxLength);
     long long starttime = QDateTime::currentMSecsSinceEpoch();

     qCDebug(ROTARYNAVCOMMANDCLASS) << "transmit count = " << Message.TxLength << " transmitted = " << transmitted;
     qCDebug(ROTARYNAVCOMMANDCLASS) << "receive count = " << Message.ExpectRxCount;

     Result = COM_GOOD;

     // reading the transmitted data back - and then tossing it aside
     while(m_serialPort.bytesAvailable() < Message.TxLength)
     {
         QApplication::instance()->processEvents();
         long long delta = QDateTime::currentMSecsSinceEpoch() - starttime;

         if(delta > 200)
         {
             Result = COM_TIMEOUT;
             qCInfo(ROTARYNAVCOMMANDCLASS) << "timed out  reanding tx data after receiving this many bytes " << m_serialPort.bytesAvailable();
             break;
         }
     }

     if( Result == COM_GOOD)
     {  // read the transmitted data and toss it
         m_serialPort.read(Message.TxLength);

         qCDebug(ROTARYNAVCOMMANDCLASS) << "read transmit data and discarded it count = " << Message.TxLength;
     }
     else
     {
         return COM_TIMEOUT;        // need to handle this better...
     }

     while(m_serialPort.bytesAvailable() < Message.ExpectRxCount)
     {
         QApplication::instance()->processEvents();
         long long delta = QDateTime::currentMSecsSinceEpoch() - starttime;

         if(delta > 200)
         {
             Result = COM_TIMEOUT;
             qCInfo(ROTARYNAVCOMMANDCLASS) << "timed out after receiving this many bytes " << m_serialPort.bytesAvailable();
             break;
         }
     }

     int receivedcount = 0;
     // Clear out the message buffer
     memset(Message.Buffer, 0, sizeof(Message.Buffer));

     if(Result == COM_GOOD)
     {
         QByteArray bytes = m_serialPort.readAll();

         qCDebug(ROTARYNAVCOMMANDCLASS) << "recieved this many bytes " << bytes.size();

         receivedcount = bytes.size();

         if(bytes.size() >= Message.ExpectRxCount)
         {
             for(int i=0;i<bytes.size();i++)
             {
                 Message.Buffer[i] = bytes.at(i);
             }
         }
         else
         {
             Result = COM_FAIL;
         }
     }


    qCDebug(ROTARYNAVCOMMANDCLASS) << "comand sent result = " << Result << " rx count = " << RS232Port2.RxCount;


    if (receivedcount >= MSG_OVERHEAD)
    {
        // Check it
        FlagLengthGood = false;
        FlagIdGood = false;
        FlagCheckSumGood = false;

        if (receivedcount == Message.ExpectRxCount)
        {
            FlagLengthGood = true;
            qCDebug(ROTARYNAVCOMMANDCLASS) << "received length is good";
        }
        if (Message.Buffer[MSG_FROM] == (unsigned char)NodeIdRotaryNav[ID_APPLICATION])
        {
            FlagIdGood = true;
            qCDebug(ROTARYNAVCOMMANDCLASS) << "id is good";
        }

        CheckSum = 0;

        for (i=0; i<(int)(receivedcount -1); i++)
        {
            CheckSum += Message.Buffer[i];
        }

        qCDebug(ROTARYNAVCOMMANDCLASS) << "calculated checksum  = " << CheckSum << " actual = " << (unsigned char)Message.Buffer[RS232Port2.RxCount -1];

        if ((unsigned char)Message.Buffer[receivedcount -1] == CheckSum)
        {
            FlagCheckSumGood = true;
            qCDebug(ROTARYNAVCOMMANDCLASS) << "checksum is good";
        }
        else
        {
            FlagCheckSumGood = true;
            qCInfo(ROTARYNAVCOMMANDCLASS) << "checksum failed";
        }

        if (FlagLengthGood && FlagIdGood && FlagCheckSumGood)
        {
        }
        else
        {
            qCInfo(ROTARYNAVCOMMANDCLASS) << "com error";

            Result = COM_ERROR;
        }
    }

    return (Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::ReadFirmwareId(char *FirmwareId)
{
    int Result;
    unsigned char FirmwareIdB[2];

    Message.Buffer[MSG_CMD] = CMD_FIRMWARE_ID_RD;
    Message.TxDataLength = 0;
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(FirmwareIdB);

    Result = SendCommand();
    if (Result == COM_GOOD)
        memcpy(FirmwareId, &Message.Buffer[MSG_DATA], sizeof(FirmwareIdB));

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::ModeWrite(unsigned char Mode)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_MODE_RDWR;
    memcpy(&Message.Buffer[MSG_DATA], &Mode, sizeof(Mode));
    Message.TxDataLength = sizeof(Mode);
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

	Result = SendCommand();

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::SerialNumberRead(void)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_SERIALNUMBER_RDWR;
    Message.TxDataLength = 0;
	Message.ExpectRxCount = MSG_OVERHEAD +sizeof(RotaryNavSerialNumber);

	Result = SendCommand();
    if (Result == COM_GOOD)
        memcpy(&RotaryNavSerialNumber, &Message.Buffer[MSG_DATA], sizeof(RotaryNavSerialNumber));

    return(Result);

}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::SerialNumberWrite(void)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_SERIALNUMBER_RDWR;
    memcpy(&Message.Buffer[MSG_DATA], &RotaryNavSerialNumber, sizeof(RotaryNavSerialNumber));
    Message.TxDataLength = sizeof(RotaryNavSerialNumber);
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();

    return(Result);

}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::NonVolatileRead(unsigned char NonVolatileType)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_NONVOLATILE_RDWR;
    Message.Buffer[MSG_DATA] = NonVolatileType;
    Message.TxDataLength = 1;
    switch(NonVolatileType)
    {
        case ROTARYNAV_NONVOLATILE_CONFIGURATION:
            Message.ExpectRxCount = MSG_OVERHEAD +sizeof(NonVolatileType) +sizeof(RotaryNavConfiguration);
            break;
        case ROTARYNAV_NONVOLATILE_CALIBRATION:
            Message.ExpectRxCount = MSG_OVERHEAD +sizeof(NonVolatileType) +sizeof(RotaryNavCalibration);
            break;
        default:
            return(COM_ERROR);
    }

    Result = SendCommand();
    if (Result == COM_GOOD)
    {
        switch(NonVolatileType)
        {
            case ROTARYNAV_NONVOLATILE_CONFIGURATION:
                memcpy(&RotaryNavConfiguration, &Message.Buffer[MSG_DATA +1], sizeof(RotaryNavConfiguration));
                break;
            case ROTARYNAV_NONVOLATILE_CALIBRATION:
                memcpy(&RotaryNavCalibration, &Message.Buffer[MSG_DATA +1], sizeof(RotaryNavCalibration));
                break;
            default:
                return(COM_ERROR);
        }
    }

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::NonVolatileWrite(unsigned char NonVolatileType)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_NONVOLATILE_RDWR;
    Message.Buffer[MSG_DATA] = NonVolatileType;
    switch(NonVolatileType)
    {
        case ROTARYNAV_NONVOLATILE_CONFIGURATION:
            memcpy(&Message.Buffer[MSG_DATA +1], &RotaryNavConfiguration, sizeof(RotaryNavConfiguration));
            Message.TxDataLength = sizeof(NonVolatileType) +sizeof(RotaryNavConfiguration);
            break;
        case ROTARYNAV_NONVOLATILE_CALIBRATION:
            memcpy(&Message.Buffer[MSG_DATA +1], &RotaryNavCalibration, sizeof(RotaryNavCalibration));
            Message.TxDataLength = sizeof(NonVolatileType) +sizeof(RotaryNavCalibration);
            break;
        default:
            return(COM_ERROR);
    }
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::DatagroupRead(unsigned char DataGroupType)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_DATAGROUP_RDWR;
    Message.Buffer[MSG_DATA] = DataGroupType;
    Message.TxDataLength = 1;
    switch(DataGroupType)
    {
        case ROTARYNAV_DATAGROUP_1:
            Message.ExpectRxCount = MSG_OVERHEAD +1 +sizeof(RotaryNavData1);
            break;
        case ROTARYNAV_DATAGROUP_2:
            Message.ExpectRxCount = MSG_OVERHEAD +1 +sizeof(RotaryNavData2);
            break;
        case ROTARYNAV_DATAGROUP_3:
			Message.ExpectRxCount = MSG_OVERHEAD +1 + getSurveySize();
            break;
        case ROTARYNAV_DATAGROUP_4:
			Message.ExpectRxCount = MSG_OVERHEAD +1 + getToolFaceDataSize();
            break;
        case ROTARYNAV_DATAGROUP_5:
            Message.ExpectRxCount = MSG_OVERHEAD +1 +100;
            break;
        default:
            return(COM_ERROR);
    }
    Result = SendCommand();
    if (Result == COM_GOOD)
	{
        switch(DataGroupType)
        {
            case ROTARYNAV_DATAGROUP_1:
                memcpy(&RotaryNavData1, &Message.Buffer[MSG_DATA +1], sizeof(RotaryNavData1));
                break;
            case ROTARYNAV_DATAGROUP_2:
                memcpy(&RotaryNavData2, &Message.Buffer[MSG_DATA +1], sizeof(RotaryNavData2));
				break;
            case ROTARYNAV_DATAGROUP_3:
				unpackSurvey(&Message.Buffer[MSG_DATA +1], &RotaryNavData3);
                break;
			case ROTARYNAV_DATAGROUP_4:
				unpackToolFace(&Message.Buffer[MSG_DATA +1], &RotaryNavData4);
				break;
            default:
                return(COM_ERROR);
        }
    }

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::RunAction(unsigned char ActionType, int ActionParameterSize, char *ActionParameters)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_ACTION;
    Message.Buffer[MSG_DATA] = ActionType;
    memcpy(&Message.Buffer[MSG_DATA +1], ActionParameters, ActionParameterSize);
    Message.TxDataLength = 1 +ActionParameterSize;
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::SystemRead(void)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_SYSTEM_RDWR;
    Message.TxDataLength = 0;
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(RotaryNavSystemRecord);

    Result = SendCommand();
    if (Result == COM_GOOD)
        memcpy(&RotaryNavSystemRecord, &Message.Buffer[MSG_DATA], sizeof(RotaryNavSystemRecord));

    return(Result);

}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::SystemWrite(void)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_SYSTEM_RDWR;
    memcpy(&Message.Buffer[MSG_DATA], &RotaryNavSystemRecord, sizeof(RotaryNavSystemRecord));
    Message.TxDataLength = sizeof(RotaryNavSystemRecord);
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();


    return(Result);

}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::TimeRead(unsigned char *TimeStampBuffer)
{
    int Result;
    char Buffer[6];

    Message.Buffer[MSG_CMD] = CMD_TIME_RDWR;
    Message.TxDataLength = 0;
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(Buffer);

    Result = SendCommand();
    if (Result == COM_GOOD)
        memcpy(TimeStampBuffer, &Message.Buffer[MSG_DATA], sizeof(Buffer));

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::TimeWrite(unsigned char *TimeStampBuffer)
{
    int Result;
    char Buffer[6];

    Message.Buffer[MSG_CMD] = CMD_TIME_RDWR;
    memcpy(&Message.Buffer[MSG_DATA], TimeStampBuffer, sizeof(Buffer));
    Message.TxDataLength = sizeof(Buffer);
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();

    return(Result);

}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::MemoryRead(unsigned short PageNumber, unsigned char Paragraph, unsigned char *Buffer)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_MEMORY_RDWR;
    memcpy(&Message.Buffer[MSG_DATA], &PageNumber, sizeof(PageNumber));
    memcpy(&Message.Buffer[MSG_DATA +sizeof(PageNumber)], &Paragraph, sizeof(Paragraph));
    Message.TxDataLength = sizeof(PageNumber) +sizeof(Paragraph);
    Message.ExpectRxCount = MSG_OVERHEAD +ROTARYNAV_MEMORY_PARAGRAPH_SIZE;

    Result = SendCommand();
    if (Result == COM_GOOD)
        memcpy(Buffer, &Message.Buffer[MSG_DATA], RS232Port2.RxCount -MSG_OVERHEAD);

    return(Result);
}
//---------------------------------------------------------------------------
int RotaryNavCommandClass::MemoryErase(void)
{
    int Result;

    Message.Buffer[MSG_CMD] = CMD_MEMORY_ERASE;
    Message.TxDataLength = 0;
    Message.ExpectRxCount = MSG_OVERHEAD +sizeof(unsigned char);

    Result = SendCommand();

    return(Result);
}
//---------------------------------------------------------------------------


void RotaryNavCommandClass::unpackToolFace(unsigned char* buffer, RotaryNavDataGroup4Structure* tfdata)
{
	unsigned int16* p16 = (unsigned int16*)  buffer;

	tfdata->NodeStatus = *p16++;
	tfdata->Mode = *p16++;

	unsigned int32* p32 =  (unsigned int32*) p16;

	tfdata->TimeStampSeconds = *p32++;

	unsigned int8* p8 = (unsigned int8*) p32;

	for(int i=0;i<6;i++)
	{
		tfdata->TimeStampDate[i] = *p8++;
	}

	float* pfloat = (float*) p8;

	tfdata->BatteryVolts = *pfloat++;
	tfdata->TemperatureCelsius = *pfloat++;
	tfdata->Gx = *pfloat++;
	tfdata->Gy = *pfloat++;
	tfdata->Gz = *pfloat++;
	tfdata->Gxy = *pfloat++;
	tfdata->Bx = *pfloat++;
	tfdata->By = *pfloat++;
	tfdata->Bz = *pfloat++;
	tfdata->InclinationDegrees = *pfloat++;
	tfdata->AzimuthDegrees = *pfloat++;
	tfdata->MagneticDipDegrees = *pfloat++;
	tfdata->TotalMagneticFieldGauss = *pfloat++;
	tfdata->TotalGravityFieldG = *pfloat++;
	tfdata->MagneticToolFaceDegrees = *pfloat++;
	tfdata->GravityToolFaceDegrees = *pfloat++;
	tfdata->RevsPerMinute = *pfloat++;
}


void RotaryNavCommandClass::unpackSurvey(unsigned char* buffer, RotaryNavDataGroup3Structure* sdata)
{
	unsigned int16* p16 = (unsigned int16*)  buffer;

	sdata->NodeStatus = *p16++;
	sdata->Mode = *p16++;

	unsigned int32* p32 =  (unsigned int32*) p16;

	sdata->TimeStampSeconds = *p32++;

	unsigned int8* p8 = (unsigned int8*) p32;

	for(int i=0;i<6;i++)
	{
		sdata->TimeStampDate[i] = *p8++;
	}

	float* pfloat = (float*) p8;

	sdata->BatteryVolts = *pfloat++;
	sdata->TemperatureCelsius = *pfloat++;
	sdata->Gx = *pfloat++;
	sdata->Gy = *pfloat++;
	sdata->Gz = *pfloat++;
	sdata->Gxy = *pfloat++;
	sdata->Bx = *pfloat++;
	sdata->By = *pfloat++;
	sdata->Bz = *pfloat++;
	sdata->InclinationDegrees = *pfloat++;
	sdata->AzimuthDegrees = *pfloat++;
	sdata->MagneticDipDegrees = *pfloat++;
	sdata->TotalMagneticFieldGauss = *pfloat++;
	sdata->TotalGravityFieldG = *pfloat++;
	sdata->MagneticToolFaceDegrees = *pfloat++;
	sdata->GravityToolFaceDegrees = *pfloat++;
	sdata->RevsPerMinute = *pfloat++;
}

bool RotaryNavCommandClass::initialize(int port, int baudrate)
{
    bool status = false;
    QString comport = QString("COM") + QString::number(port);

    m_serialPort.setPortName(comport);
    m_serialPort.setBaudRate(baudrate);
    status = m_serialPort.open(QFile::ReadWrite);

    return status;
}

void RotaryNavCommandClass::close()
{
    m_serialPort.close();
}
