//---------------------------------------------------------------------------
#ifndef ModBusH
#define ModBusH
//---------------------------------------------------------------------------
enum {MODBUS_MSG_NODEID=0, MODBUS_MSG_CMD, MODBUS_MSG_ADDRESS1, MODBUS_MSG_ADDRESS0, MODBUS_MSG_DATA};
#define MODBUS_MSG_OVERHEAD 6
#define MODBUS_CMD_READ_BIT     0x02
#define MODBUS_CMD_READ_WORD    0x03
#define MODBUS_CMD_WRITE_BIT    0x05
#define MODBUS_CMD_WRITE_WORD   0x06
#define MODBUS_CMD_LOOP         0x08

class ModBusClass
{
    public:
    ModBusClass(){;}
    int LoopTest(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue);
    int ReadRegister(unsigned char NodeId, unsigned short RegisterAddress, unsigned short *RegisterValue);
    int WriteRegister(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue);
    int ReadBit(unsigned char NodeId, unsigned short RegisterAddress, unsigned short *RegisterValue);
    int WriteBit(unsigned char NodeId, unsigned short RegisterAddress, unsigned short RegisterValue);

    private:
    struct MESSAGE_STRUCTURE Message;
    int SendCommand(void);

};
extern ModBusClass ModBus;


#endif





