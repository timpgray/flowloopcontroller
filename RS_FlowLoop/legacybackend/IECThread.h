//---------------------------------------------------------------------------

#ifndef IecThreadH
#define IecThreadH

#include <qstring.h>
#include <qserialport.h>
#include <qbytearray.h>
#include <qsemaphore.h>
#include <iecnotifier.h>
#include "IECBox.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class IECThreadClass //: public TThread
{
private:
    int FlagAlive;
    int FlagTerminate;

public:
    void Execute();
public:
    IECThreadClass(bool CreateSuspended, IECNotifier* notifier = nullptr);
    void Stop(void);

    bool initialize(int port, int baudrate);

    // Port1
    int ReceiveHandler(void);
    int TransmitIecStructure(void);
    int ReadIecStructure(void *VoidPointer);
    int WriteIecStructure(void *VoidPointer);

    void dumpIECStructure(QString desc, IecInterfaceStructure* iecdata);

    unsigned char RxBuffer[256];
    int RxBufferIndex;
    int ExpectRxCount;
    int FlagIecSync;
    unsigned char FunctionCode;
    int FlagIecMessageReceived;
    long MessageCounter;
    int FlagTransmitMessage;
    int m_badCRCCounter;
    int m_overrunEventCounter;


private:
    QSerialPort m_serialPort;
    QByteArray m_txBuffer;
    IECNotifier* m_notifier;
    QSemaphore m_dataLock;

};
//---------------------------------------------------------------------------
extern IECThreadClass *IECThread;
//---------------------------------------------------------------------------
#define MAX_MESSAGE_SIZE    600
typedef struct MODBUS_MESSAGE_STRUCTURE
{
    unsigned char Buffer[MAX_MESSAGE_SIZE];
    unsigned short TxDataLength;
    unsigned short TxLength;
    unsigned short ExpectRxCount;
    unsigned short RxLength;
}ModBusMessageStructure;
//---------------------------------------------------------------------------
#endif
