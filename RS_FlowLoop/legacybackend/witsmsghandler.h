#ifndef WITSMSGHANDLER_H
#define WITSMSGHANDLER_H
#include <string>
#include <map>
#include <vector>
#include <windows.h>
#include "RS232Port.h"
#include <qstring.h>
#include <qserialport.h>

using namespace std;

class WitsProcessedMsgHandler
{
public:
    virtual void handleProcessedWITSMsg(string witsmsg, string name, float value) = 0;
};

class WitsRawMsgHandler
{
public:
	virtual void handleRawWITSMsg(string name, string value) = 0;
};

class WitsMsgHandler
{
public:
    WitsMsgHandler();
    virtual ~WitsMsgHandler();

    virtual bool init(int port, int baudrate);
    virtual bool init(bool initserialport, string configfile="");
    virtual void addProcessedListener(WitsProcessedMsgHandler* handler){m_processedListeners.push_back(handler);}
    virtual void removeProcessedListener(WitsProcessedMsgHandler* handler);
    virtual void addRawListener(WitsRawMsgHandler* handler){m_rawListeners.push_back(handler);}
    virtual void removeRawListener(WitsRawMsgHandler* handler);

	virtual void tick();
	virtual string getComPort(){return m_comPortStr;}

    virtual bool loadConfigFile(string filename);
    virtual bool parseConfigInputLine(string inputline);
    virtual bool parseWITSMsg(string witsline);
	virtual void publishWITSMsg(string name, float value) = 0;
	virtual void publishWITSMsgRaw(string name, string value) = 0;
	virtual vector<string> splitStr(string inputstr, char spliton = ',');
    virtual string trim(string str);

    virtual vector<string> splitStrSpecial(string inputstr, QString* premainder);


protected:
    map<string,string> m_witsNameMap;
    vector<WitsProcessedMsgHandler*> m_processedListeners;
    vector<WitsRawMsgHandler*> m_rawListeners;
    QSerialPort* m_comPort;
	string m_currentmsg;
	string m_comPortStr;
};


class WitsMsgHandlerA : public WitsMsgHandler
{
 public:
	virtual void publishWITSMsg(string name, float value){}
	virtual void publishWITSMsgRaw(string name, string value){}

};

#endif // WITSMSGHANDLER_H
