//---------------------------------------------------------------------------
/*

This thread continuously receives the data streaming from the DAQ.

*/
//---------------------------------------------------------------------------


#include <windows.h>
#include "IECThread.h"

#include "IEC.h"
#include "RS232Port.h"
#include "Utility.h"

#include<qfile.h>

#include <qloggingcategory.h>
#include <qthread.h>
#include <qstring.h>

QLoggingCategory IECTHREADCLASS("IECThreadClass");
QLoggingCategory IECCOMMDATA("IECCommData");



IECThreadClass *IECThread;

IecInterfaceStructure IecInterfaceReadThread;
IecInterfaceStructure IecInterfaceReadThreadB;
IecInterfaceStructure IecInterfaceWriteThread;
IecInterfaceStructure IecInterfaceWriteThreadB;
ModBusMessageStructure Message;
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall Unit2::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
IECThreadClass::IECThreadClass(bool CreateSuspended, IECNotifier* notifier)
//    : TThread(CreateSuspended)
{
    FlagTerminate = false;
    m_dataLock.release();
    m_notifier = notifier;
}

bool IECThreadClass::initialize(int port, int baudrate)
{
    bool status = false;
    QString comport = QString("COM") + QString::number(port);

    m_serialPort.setPortName(comport);
    m_serialPort.setBaudRate(baudrate);
    status = m_serialPort.open(QFile::ReadWrite);

    m_badCRCCounter = 0;
    m_overrunEventCounter = 0;


    return status;

}

//---------------------------------------------------------------------------
void IECThreadClass::Execute()
{
    //---- Place thread code here ----
    int RxCount;
    int disregardcount = 0;

    FlagTerminate = false;
    FlagTransmitMessage = false;

    ExpectRxCount = 0;
    FlagIecSync = false;
    RxBufferIndex = 0;
    FlagIecMessageReceived = false;
    MessageCounter = 0;

    while (FlagTerminate == false)
    {
        if (FlagTransmitMessage == true)
        {
            FlagTransmitMessage = false;
            qCDebug(IECTHREADCLASS) << "IEC writing this many bytes " << m_txBuffer.size();
            int byteswritten = m_serialPort.write(m_txBuffer);
            qCDebug(IECTHREADCLASS) << "IEC wrote this many bytes " << byteswritten << " error = " << m_serialPort.errorString();
            m_serialPort.flush();   // this must be done - there is no event loop and the bytes will not be written otherwise.
            disregardcount = m_txBuffer.size();
            m_txBuffer.clear();     // clear the transmit buffer
        }

        QThread::currentThread()->msleep(10);
        RxCount = m_serialPort.bytesAvailable();

        if(RxCount > 0)
        {
            qCDebug(IECTHREADCLASS) << "This many bytes available " << RxCount << " Reading them now...";
        }

        while (m_serialPort.bytesAvailable() > 0)
        {
            if(disregardcount > 0)
            {
                QByteArray values = m_serialPort.read(1);
                qCDebug(IECTHREADCLASS) << "Tossing byte " << disregardcount << " value = " << (unsigned int)values.at(0);
                disregardcount--;
            }
            else
            {
                ReceiveHandler();
            }

            if(FlagTransmitMessage && m_serialPort.bytesAvailable() > 0)
            {
                m_txBuffer.clear();
                FlagTransmitMessage = false;
                FlagIecMessageReceived = false;
                FlagIecSync = false;

                qCDebug(IECTHREADCLASS) << "!!! looks like a buffer overrun - dumping response and hope to rsync with " << m_serialPort.bytesAvailable() << " bytes available";

                m_overrunEventCounter++;
            }

        }
    }

    // We only get here when the thread is terminated.
}
//---------------------------------------------------------------------------
void IECThreadClass::Stop(void)
{
    FlagTerminate = true;
}
//---------------------------------------------------------------------------
int IECThreadClass::ReceiveHandler(void)
{
    int RxCount;
    char RxCharacter;
    unsigned short Crc16Rx;
    unsigned short Crc16Calculated;
    char buffer[10];

    // Scan for the SyncHeader.
    RxCount = m_serialPort.read(buffer, 1);
    qCDebug(IECTHREADCLASS) << "IEC received this count " << RxCount << " got this value " << (int)buffer[0];

    if (FlagIecSync == false)
    {
        if (RxCount > 0)
        {
            RxCharacter = buffer[0];
            RxBuffer[0] = RxBuffer[1];
            RxBuffer[1] = RxBuffer[2];
            RxBuffer[2] = RxBuffer[3];
            RxBuffer[3] = RxCharacter;
            if ( (RxBuffer[0] == MODBUS_ADDRESS_ROTOSLIDE) && (RxBuffer[2] == 0) && (RxBuffer[3] == 0) )
            {
                FunctionCode = RxBuffer[1];
                switch(FunctionCode)
                {
                    case 0x02:
                        ExpectRxCount = 8;
                        break;
                    case 0x03:
                        ExpectRxCount = 8;
                        break;
                    case 0x0F:
                        ExpectRxCount = 11;
                        break;
                    case 0x10:
                        ExpectRxCount = 41;
                        break;
                default:
                    qCDebug(IECTHREADCLASS) << "inside undefined command value = " << FunctionCode;
                    break;
                }
                FlagIecSync = true;
                RxBufferIndex = 3;
                qCDebug(IECTHREADCLASS) << "IEC expecting this many bytes..." << ExpectRxCount;
            }
        }
    }
    if (FlagIecSync == true)
    {
        // We're synced!
        // Get the rest of the message.
        if (RxCount > 0)
        {
            RxBuffer[RxBufferIndex++] = buffer[0];
            if (RxBufferIndex >= ExpectRxCount)
            {
                if(IECTHREADCLASS.isDebugEnabled())
                {
                    QString testmsg;

                    for(int i=0;i<RxBufferIndex;i++)
                    {
                        testmsg += QString::number((unsigned char)RxBuffer[i], 16) + ", ";
                    }

                    qCDebug(IECTHREADCLASS) << "IEC received this many bytes " << RxCount << " msg = " << testmsg;
                }

                // Check CRC
                Crc16Rx = ((unsigned short)RxBuffer[RxBufferIndex -2]) << 8;
                Crc16Rx += ((unsigned short)RxBuffer[RxBufferIndex -1]) << 0;
                Crc16Calculated = 0;
                Crc16Calculated = Utility.ModRTU_CRC(RxBuffer, RxBufferIndex -2);
                Crc16Calculated = Utility.SwitchEndianShort(Crc16Calculated);

                if (Crc16Rx == Crc16Calculated)
                {
                    MessageCounter++;
                    FlagIecMessageReceived = true;
                    qCDebug(IECTHREADCLASS) << "Passed CRC check for command 0x" << FunctionCode;
                }
                else
                {
                    FlagIecMessageReceived = false;
                    qCDebug(IECTHREADCLASS) << "Failed CRC check for command 0x" << FunctionCode;
                    m_badCRCCounter++;
                }
                FlagIecSync = false;
                RxBufferIndex = 0;
            }
        }

        if (FlagIecMessageReceived == true)
        {
            FlagIecMessageReceived = false;

            switch(FunctionCode)
            {
                case 0x02:
                    // Grab the semaphore.
                    m_dataLock.acquire();
                    // Switch Endians.
                    IecInterfaceWriteThreadB.SoftwareInterfaceRev = Utility.SwitchEndianShort(IecInterfaceWriteThread.SoftwareInterfaceRev);
                    // Release the semaphore.
                    m_dataLock.release();
                    // Build the response and transmit.
                    Message.Buffer[0] = 0x07;
                    Message.Buffer[1] = FunctionCode;
                    Message.Buffer[2] = 0x02;
                    Message.Buffer[3] = (char)IecInterfaceWriteThread.SoftwareInterfaceRev >> 8;
                    Message.Buffer[4] = (char)IecInterfaceWriteThread.SoftwareInterfaceRev >> 0;
                    // We're the slave so we do not expect a response.
                    Message.ExpectRxCount = 0;
                    // Calculate total length
                    Message.TxLength = 7;
                    // Add CRC16
                    Crc16Calculated = Utility.ModRTU_CRC(Message.Buffer, Message.TxLength -2);
                    Message.Buffer[Message.TxLength -2] = Crc16Calculated >> 0;
                    Message.Buffer[Message.TxLength -1] = Crc16Calculated >> 8;
                    // Copy the message over to the serial structure

                    for(int i=0;i<Message.TxLength;i++)
                    {
                       m_txBuffer.append(Message.Buffer[i]);
                    }

                    // Signal thread to send the message.
                    FlagTransmitMessage = true;
                    qCDebug(IECTHREADCLASS) << "Received IEC command 0x02";
                    break;

                case 0x03:
                    TransmitIecStructure();
                    qCDebug(IECTHREADCLASS) << "Received IEC command 0x03";
                    break;

                case 0x0F:
                    // Build the response and transmit.
                    Message.Buffer[0] = 0x07;
                    Message.Buffer[1] = FunctionCode;
                    Message.Buffer[2] = 0;
                    Message.Buffer[3] = 0;
                    Message.Buffer[4] = 0;
                    Message.Buffer[5] = 0x0F;
                    // We're the slave so we do not expect a response.
                    Message.ExpectRxCount = 0;
                    // Calculate total length
                    Message.TxLength = 8;
                    // Add CRC16
                    Crc16Calculated = Utility.ModRTU_CRC(Message.Buffer, Message.TxLength -2);
                    Message.Buffer[Message.TxLength -2] = Crc16Calculated >> 0;
                    Message.Buffer[Message.TxLength -1] = Crc16Calculated >> 8;
                    // Copy the message over to the serial structure
                    for(int i=0;i<Message.TxLength;i++)
                    {
                       m_txBuffer.append(Message.Buffer[i]);
                    }

                    // Signal thread to send the message.
                    FlagTransmitMessage = true;
                    qCDebug(IECTHREADCLASS) << "Received IEC command 0x0F";
                    break;

                case 0x10:
                    // Grab the semaphore.
                    m_dataLock.acquire();
                    memcpy(&IecInterfaceReadThreadB, &RxBuffer[7], sizeof(IecInterfaceReadThreadB));
                    // Switch Endians of the incoming data.
                    IecInterfaceReadThread.SoftwareInterfaceRev = Utility.SwitchEndianShort(IecInterfaceReadThreadB.SoftwareInterfaceRev);
                    IecInterfaceReadThread.ControlWord = Utility.SwitchEndianShort(IecInterfaceReadThreadB.ControlWord);
                    IecInterfaceReadThread.TopDriveSpeedCommandRpmX100 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.TopDriveSpeedCommandRpmX100);
                    IecInterfaceReadThread.TorqueLimitFtLbsX10 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.TorqueLimitFtLbsX10);
                    IecInterfaceReadThread.AutoDriveRateOfPenetrationFtPerHourX10 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.AutoDriveRateOfPenetrationFtPerHourX10);
                    IecInterfaceReadThread.AutoDriveWeightOnBitKlbsX100 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.AutoDriveWeightOnBitKlbsX100);
                    IecInterfaceReadThread.AutoDrivePressureControlDrillingPsi = Utility.SwitchEndianShort(IecInterfaceReadThreadB.AutoDrivePressureControlDrillingPsi);
                    IecInterfaceReadThread.OffBottomHookLoadKlbsX10 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.OffBottomHookLoadKlbsX10);
                    IecInterfaceReadThread.OffBottomPressurePsi = Utility.SwitchEndianShort(IecInterfaceReadThreadB.OffBottomPressurePsi);
                    IecInterfaceReadThread.TotalStrokes = Utility.SwitchEndianShort(IecInterfaceReadThreadB.TotalStrokes);
                    IecInterfaceReadThread.FlowRateGpm = Utility.SwitchEndianShort(IecInterfaceReadThreadB.FlowRateGpm);
                    IecInterfaceReadThread.PumpSpeedCommandRpmX10 = Utility.SwitchEndianShort(IecInterfaceReadThreadB.PumpSpeedCommandRpmX10);
                    IecInterfaceReadThread.TopPressurePsi = Utility.SwitchEndianShort(IecInterfaceReadThreadB.TopPressurePsi);
                    IecInterfaceReadThread.BottomPressurePsi = Utility.SwitchEndianShort(IecInterfaceReadThreadB.BottomPressurePsi);
                    IecInterfaceReadThread.HoleDepthFeet = Utility.SwitchEndianShort(IecInterfaceReadThreadB.HoleDepthFeet);
                    IecInterfaceReadThread.BitDepthFeet = Utility.SwitchEndianShort(IecInterfaceReadThreadB.BitDepthFeet);

                    dumpIECStructure("Read IEC Structure From backend", &IecInterfaceReadThread);

                    // Release the semaphore.
                    m_dataLock.release();
                    // Build the response and transmit.
                    Message.Buffer[0] = 0x07;
                    Message.Buffer[1] = FunctionCode;
                    Message.Buffer[2] = 0;
                    Message.Buffer[3] = 0;
                    Message.Buffer[4] = 0;
                    Message.Buffer[5] = 0x010;
                    // We're the slave so we do not expect a response.
                    Message.ExpectRxCount = 0;
                    // Calculate total length
                    Message.TxLength = 8;
                    // Add CRC16
                    Crc16Calculated = Utility.ModRTU_CRC(Message.Buffer, Message.TxLength -2);
                    Message.Buffer[Message.TxLength -2] = Crc16Calculated >> 0;
                    Message.Buffer[Message.TxLength -1] = Crc16Calculated >> 8;
                    // Copy the message over to the serial structure
                    for(int i=0;i<Message.TxLength;i++)
                    {
                       m_txBuffer.append(Message.Buffer[i]);
                    }

                    // Signal thread to send the message.

                    qCDebug(IECTHREADCLASS) << "Received IEC Structure flow rate = " << IecInterfaceReadThread.FlowRateGpm <<
                                               " pumpspeedX10 = " << IecInterfaceReadThread.PumpSpeedCommandRpmX10 <<
                                               " topdriveX100  = " << IecInterfaceReadThread.TopDriveSpeedCommandRpmX100 <<
                                               " toppressure = " << IecInterfaceReadThread.TopPressurePsi <<
                                               " bottom pressure = " << IecInterfaceReadThread.BottomPressurePsi <<
                                               " controlword = 0x" << QString::number(IecInterfaceReadThread.ControlWord, 16);

                    qCDebug(IECTHREADCLASS) << "Control Granted = " << ((IecInterfaceReadThread.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)?"true":"false");
                    qCDebug(IECTHREADCLASS) << "Heartbeat = " << ((IecInterfaceReadThread.ControlWord & IEC_CONTROLWORD_HEARTBEAT)?"true":"false");
                    qCDebug(IECTHREADCLASS) << "AutoDrill control = " << ((IecInterfaceReadThread.ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL)?"true":"false");
                    FlagTransmitMessage = true;

                    if(m_notifier != nullptr)
                    {
                        m_notifier->changed();
                    }

                    break;
            }
        }
    }

    return(true);
}
//---------------------------------------------------------------------------
int IECThreadClass::TransmitIecStructure(void)
{
    int i;
    unsigned short Crc16Calculated;

    // Grab the semaphore.
    m_dataLock.acquire();

    // Switch Endians.
    IecInterfaceWriteThreadB.SoftwareInterfaceRev = Utility.SwitchEndianShort(IecInterfaceWriteThread.SoftwareInterfaceRev);
    IecInterfaceWriteThreadB.ControlWord = Utility.SwitchEndianShort(IecInterfaceWriteThread.ControlWord);
    IecInterfaceWriteThreadB.TopDriveSpeedCommandRpmX100 = Utility.SwitchEndianShort(IecInterfaceWriteThread.TopDriveSpeedCommandRpmX100);
    IecInterfaceWriteThreadB.TorqueLimitFtLbsX10 = Utility.SwitchEndianShort(IecInterfaceWriteThread.TorqueLimitFtLbsX10);
    IecInterfaceWriteThreadB.AutoDriveRateOfPenetrationFtPerHourX10 = Utility.SwitchEndianShort(IecInterfaceWriteThread.AutoDriveRateOfPenetrationFtPerHourX10);
    IecInterfaceWriteThreadB.AutoDriveWeightOnBitKlbsX100 = Utility.SwitchEndianShort(IecInterfaceWriteThread.AutoDriveWeightOnBitKlbsX100);
    IecInterfaceWriteThreadB.AutoDrivePressureControlDrillingPsi = Utility.SwitchEndianShort(IecInterfaceWriteThread.AutoDrivePressureControlDrillingPsi);
    IecInterfaceWriteThreadB.OffBottomHookLoadKlbsX10 = Utility.SwitchEndianShort(IecInterfaceWriteThread.OffBottomHookLoadKlbsX10);
    IecInterfaceWriteThreadB.OffBottomPressurePsi = Utility.SwitchEndianShort(IecInterfaceWriteThread.OffBottomPressurePsi);
    IecInterfaceWriteThreadB.TotalStrokes = Utility.SwitchEndianShort(IecInterfaceWriteThread.TotalStrokes);
    IecInterfaceWriteThreadB.FlowRateGpm = Utility.SwitchEndianShort(IecInterfaceWriteThread.FlowRateGpm);
    IecInterfaceWriteThreadB.PumpSpeedCommandRpmX10 = Utility.SwitchEndianShort(IecInterfaceWriteThread.PumpSpeedCommandRpmX10);
    IecInterfaceWriteThreadB.TopPressurePsi = Utility.SwitchEndianShort(IecInterfaceWriteThread.TopPressurePsi);
    IecInterfaceWriteThreadB.BottomPressurePsi = Utility.SwitchEndianShort(IecInterfaceWriteThread.BottomPressurePsi);
    IecInterfaceWriteThreadB.HoleDepthFeet = Utility.SwitchEndianShort(IecInterfaceWriteThread.HoleDepthFeet);
    IecInterfaceWriteThreadB.BitDepthFeet = Utility.SwitchEndianShort(IecInterfaceWriteThread.BitDepthFeet);


    qCDebug(IECTHREADCLASS) << "Writing IEC structure to backend control = 0x" << QString::number(IecInterfaceWriteThread.ControlWord, 16);
    qCDebug(IECTHREADCLASS) << "    W Control Granted = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W Heartbeat = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_HEARTBEAT)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W AutoDrill control = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W Pump Run = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_PUMP_RUN)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W Motor Forward = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_MOTOR_FORWARD)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W Motor Reverse = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_MOTOR_REVERSE)?"true":"false");
    qCDebug(IECTHREADCLASS) << "    W TopDrive Run = " << ((IecInterfaceWriteThread.ControlWord & IEC_CONTROLWORD_TOPDRIVE_RUN)?"true":"false");


    dumpIECStructure("Writing IEC structure to backend", &IecInterfaceWriteThread);

    // Build the response and transmit.
    i = 0;
    Message.Buffer[i++] = 0x07;
    Message.Buffer[i++] = 0x03;
    Message.Buffer[i++] = sizeof(IecInterfaceWriteThreadB);
    memcpy(&Message.Buffer[i], &IecInterfaceWriteThreadB, sizeof(IecInterfaceWriteThreadB));
    Message.TxDataLength = sizeof(IecInterfaceWriteThreadB);
    // We're the slave so we do not expect a response.
    Message.ExpectRxCount = 0;

    // Release the semaphore.
    m_dataLock.release();

    // Calculate total length
    Message.TxLength = 5 +Message.TxDataLength;
    // Add CRC16
    Crc16Calculated = Utility.ModRTU_CRC(Message.Buffer, Message.TxLength -2);
    Message.Buffer[Message.TxLength -2] = Crc16Calculated >> 0;
    Message.Buffer[Message.TxLength -1] = Crc16Calculated >> 8;
    // Copy the message over to the serial structure
    for(int i=0;i<Message.TxLength;i++)
    {
       m_txBuffer.append(Message.Buffer[i]);
    }

    // Signal thread to send the message.
    FlagTransmitMessage = true;

    return(true);
}
//---------------------------------------------------------------------------
int IECThreadClass::ReadIecStructure(void *VoidPointer)
{
    if (FlagTerminate == true)
        return(false);

    // We don't need to suspend the thread while we retrieve the data
    // because we're using software semaphores.

    // Grab the semaphore.
    m_dataLock.acquire();

    // Copy the structure.
    memcpy(VoidPointer, &IecInterfaceReadThread, sizeof(IecInterfaceReadThread));

    // Release the semaphore.
    m_dataLock.release();


    return(true);
}
//---------------------------------------------------------------------------
int IECThreadClass::WriteIecStructure(void *VoidPointer)
{
    if (FlagTerminate == true)
        return(false);

    // We don't need to suspend the thread while we retrieve the data
    // because we're using software semaphores.

    // Grab the semaphore.
    m_dataLock.acquire();

    // Copy the structure.
    memcpy(&IecInterfaceWriteThread, VoidPointer, sizeof(IecInterfaceWriteThread));

    // Release the semaphore.
    m_dataLock.release();

    return(true);
}
//---------------------------------------------------------------------------

void IECThreadClass::dumpIECStructure(QString desc, IecInterfaceStructure* iecdata)
{
    qCDebug(IECCOMMDATA) << desc;
    qCDebug(IECCOMMDATA) << "\tTop Drive RPM = " << (iecdata->TopDriveSpeedCommandRpmX100/100.0);
    qCDebug(IECCOMMDATA) << "\tMud Pump RPM = " << (iecdata->PumpSpeedCommandRpmX10/10.0);
}
