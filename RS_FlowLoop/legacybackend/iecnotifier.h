#ifndef IECNOTIFIER_H
#define IECNOTIFIER_H

#include <QObject>

class IECNotifier : public QObject
{
    Q_OBJECT
public:
    explicit IECNotifier(QObject *parent = 0);
    virtual void changed(){emit iecDataChanged();}

signals:   
    void iecDataChanged();

public slots:
};

#endif // IECNOTIFIER_H
