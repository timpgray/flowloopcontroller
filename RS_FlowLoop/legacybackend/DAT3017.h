//---------------------------------------------------------------------------
#ifndef Dat3017H
#define Dat3017H
//---------------------------------------------------------------------------

// Node ID
#define MODBUS_NODE_ID_DAT3017      0x03

// Register addresses
#define DAT3017_ADDRESS_NONE        0x0000
#define DAT3017_ADDRESS_CH_ENABLE   11
#define DAT3017_ADDRESS_CH0         14
#define DAT3017_ADDRESS_CH1         15
#define DAT3017_ADDRESS_CH2         16
#define DAT3017_ADDRESS_CH3         17
#define DAT3017_ADDRESS_CH4         18
#define DAT3017_ADDRESS_CH5         19
#define DAT3017_ADDRESS_CH6         20
#define DAT3017_ADDRESS_CH7         21

// Control word

class DAT3017Class
{
    public:
    DAT3017Class(){;}

    int StatusRead(void);
    int ChannelEnableRead(unsigned short *ChannelEnable);
    int ChannelEnableWrite(unsigned short ChannelEnable);
    int ChannelRead(int Channel, float *MilliAmps);

    unsigned short Status;

    private:

};
extern DAT3017Class DAT3017;


#endif





