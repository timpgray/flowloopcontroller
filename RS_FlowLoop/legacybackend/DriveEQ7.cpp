//---------------------------------------------------------------------------

#include <windows.h>
#include <io.h>
#include <stdio.h>
#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "DriveEq7.h"
#include "RS232Port.h"
#include "Utility.h"
#include "ModBus.h"

DriveEQ7Class DriveEQ7;

//---------------------------------------------------------------------------
int DriveEQ7Class::LoopTest(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = EQ7_ADDRESS_NONE;
    RegisterValue = 0xAA55;
    Result = ModBus.LoopTest(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);

}
//---------------------------------------------------------------------------
int DriveEQ7Class::StatusRead(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = EQ7_ADDRESS_CONTROL;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    Status = RegisterValue;

    return(true);

}
//---------------------------------------------------------------------------
int DriveEQ7Class::Run(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = EQ7_ADDRESS_CONTROL;
    RegisterValue = EQ7_CONTROL_RUN_FWD;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);

}
//---------------------------------------------------------------------------
int DriveEQ7Class::Stop(void)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    RegisterAddress = EQ7_ADDRESS_CONTROL;
    RegisterValue = EQ7_CONTROL_STOP;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DriveEQ7Class::SpeedReferenceWrite(float SpeedRpm)
{
    unsigned short SpeedFrequencyInt;
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;

    // Gearhead ratio is 17.29:1
    SpeedRpm *= 17.29;
    // Speed = Frequency *30.0
    SpeedFrequencyInt = (unsigned short)(SpeedRpm /30.0 *100.0);

    RegisterAddress = EQ7_ADDRESS_SPEED_REF;
    RegisterValue = SpeedFrequencyInt;
    Result = ModBus.WriteRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    return(true);
}
//---------------------------------------------------------------------------
int DriveEQ7Class::SpeedReferenceRead(float *SpeedRpm)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;
    float SpeedFrequency;

    RegisterAddress = EQ7_ADDRESS_SPEED_REF;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);

    SpeedFrequency = ((float)RegisterValue) /100.0;
    // Speed = Frequency *30.0
    // Gearhead ratio is 17.29:1
    *SpeedRpm = (SpeedFrequency *30.0 /17.29);

    return(true);

}
//---------------------------------------------------------------------------
int DriveEQ7Class::SpeedActualRead(float *SpeedRpm)
{
    unsigned short RegisterAddress;
    unsigned short RegisterValue;
    int Result;
    float SpeedFrequency;

    RegisterAddress = EQ7_ADDRESS_SPEED_ACTUAL;
    RegisterValue = 0x0000;
    Result = ModBus.ReadRegister(MODBUS_NODE_ID_DRIVE_EQ7, RegisterAddress, &RegisterValue);
    if (Result != COM_GOOD)
        return(false);
    SpeedFrequency = ((float)RegisterValue) /100.0;
    // Speed = Frequency *30.0
    // Gearhead ratio is 17.29:1
    *SpeedRpm = (SpeedFrequency *30.0 /17.29);

    return(true);

}
//---------------------------------------------------------------------------

