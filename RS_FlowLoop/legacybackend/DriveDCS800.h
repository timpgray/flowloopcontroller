//---------------------------------------------------------------------------
#ifndef DriveDcs800H
#define DriveDcs800H
//---------------------------------------------------------------------------

// Node ID
#define MODBUS_NODE_ID_DRIVE_DCS800 0x02

// Register addresses
// These are the parameter numbers-1
#define DCS800_ADDRESS_NONE         0x0000
#define DCS800_ADDRESS_CONTROL      0x02BC
#define DCS800_ADDRESS_STATUS       0x0320
#define DCS800_ADDRESS_SPEED_REF    0x08FC
#define DCS800_ADDRESS_SPEED_ACTUAL 0x0064

// Status word
#define DCS800_STATUS_RDYON         0x0001
#define DCS800_STATUS_RDYRUN        0x0002
#define DCS800_STATUS_RDYREF        0x0004
#define DCS800_STATUS_TRIPPED       0x0008
#define DCS800_STATUS_OFF2NSTATUS   0x0010
#define DCS800_STATUS_OFF3NSTATUS   0x0020
#define DCS800_STATUS_ONINHIBITED   0x0040
#define DCS800_STATUS_ALARM         0x0050
#define DCS800_STATUS_ATSETPOINT    0x0100
#define DCS800_STATUS_REMOTE        0x0200
#define DCS800_STATUS_ABOVELIMIT    0x0400


// Control word
#define DCS800_CONTROL_RESET    0x04F6
#define DCS800_CONTROL_OFF      0x0476
#define DCS800_CONTROL_ON       0x0477
#define DCS800_CONTROL_RUN      0x047F
#define DCS800_CONTROL_STOP     0x0477
#define DCS800_CONTROL_ESTOP    0x047B
#define DCS800_CONTROL_INHIBIT  0x0474

class DriveDCS800Class
{
    public:
    DriveDCS800Class(){;}

    int LoopTest(void);
    int StatusRead(void);
    int On(void);
    int Run(void);
    int Stop(void);
    int Off(void);
    int SpeedReferenceWrite(float SpeedRpm);
    int SpeedReferenceRead(float *SpeedRpm);
    int SpeedActualRead(float *SpeedRpm);

    unsigned short Status;

    private:

};
extern DriveDCS800Class DriveDCS800;


#endif





