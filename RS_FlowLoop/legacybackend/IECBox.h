//---------------------------------------------------------------------------
#ifndef IecboxH
#define IecboxH
//------------------------------------------------------------------------------

// Communications.
enum {IEC_MSG_NODEID=0, IEC_MSG_CMD, IEC_MSG_ADDRESS1, IEC_MSG_ADDRESS0, IEC_MSG_DATA};
#define IEC_MSG_OVERHEAD            8
#define MODBUS_ADDRESS_ROTOSLIDE    7
#define MODBUS_ADDRESS_IEC          10
#define IEC_BUAD_RATE               19200
#define IEC_REGISTER_ADDRESS        0x0000
#define MODBUS_CMD_WRITE_WORDS      0x10
#define MODBUS_CMD_READ_WORDS       0x0F

// IEC ControlWord bits.
#define IEC_CONTROLWORD_AUTODRILL_CONTROL           (unsigned short)0x0001
#define IEC_CONTROLWORD_ROP_REQUEST                 (unsigned short)0x0002
#define IEC_CONTROLWORD_WOB_REQUEST                 (unsigned short)0x0004
#define IEC_CONTROLWORD_PCD_REQUEST                 (unsigned short)0x0008
#define IEC_CONTROLWORD_PUMP_REQUEST                (unsigned short)0x0010
#define IEC_CONTROLWORD_PUMP_RUN                    (unsigned short)0x0020
#define IEC_CONTROLWORD_SPARE3_6                    (unsigned short)0x0040
#define IEC_CONTROLWORD_HEARTBEAT                   (unsigned short)0x0080
#define IEC_CONTROLWORD_ROTOSLIDE_CONTROL           (unsigned short)0x0100
#define IEC_CONTROLWORD_MOTOR_FORWARD               (unsigned short)0x0200
#define IEC_CONTROLWORD_MOTOR_REVERSE               (unsigned short)0x0400
#define IEC_CONTROLWORD_TOPDRIVE_RUN                (unsigned short)0x0800
#define IEC_CONTROLWORD_SPARE2_4                    (unsigned short)0x1000
#define IEC_CONTROLWORD_SPARE2_5                    (unsigned short)0x2000
#define IEC_CONTROLWORD_SPARE2_6                    (unsigned short)0x4000
#define IEC_CONTROLWORD_SPARE2_7                    (unsigned short)0x8000


//------------------------------------------------------------------------------

// Everything here needs to be on even byte boundaries (16 bits)

// 32 bytes.
typedef struct IEC_INTERFACE_STRUCTURE
{
    short SoftwareInterfaceRev;
    short ControlWord;
    short TopDriveSpeedCommandRpmX100;
    short TorqueLimitFtLbsX10;
    short AutoDriveRateOfPenetrationFtPerHourX10;
    short AutoDriveWeightOnBitKlbsX100;
    short AutoDrivePressureControlDrillingPsi;
    short OffBottomHookLoadKlbsX10;
    short OffBottomPressurePsi;
    short TotalStrokes;
    short FlowRateGpm;
    short PumpSpeedCommandRpmX10;
    short TopPressurePsi;
    short BottomPressurePsi;
    short HoleDepthFeet;
    short BitDepthFeet;
}IecInterfaceStructure;

extern IecInterfaceStructure IecInterfaceRead;
extern IecInterfaceStructure IecInterfaceWrite;


//------------------------------------------------------------------------------

#endif

