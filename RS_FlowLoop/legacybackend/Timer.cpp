//---------------------------------------------------------------------------
// 2016.10.22
//---------------------------------------------------------------------------

#include <windows.h>
#include "Timer.h"
#include <Time.h>

TimerClass Timer;
LARGE_INTEGER PerformanceFrequency,PerformanceCount;
double SystemFrequency;
float TicksPerMicroSecond;

//---------------------------------------------------------------------------
int TimerClass::InitializeFlagGet(void)
{
    return(InitializeFlag);
}
//---------------------------------------------------------------------------
void TimerClass::Initialize(void)
{
    QueryPerformanceFrequency(&PerformanceFrequency);
    SystemFrequency = (double)PerformanceFrequency.QuadPart;
    TicksPerMicroSecond = (float)(SystemFrequency/1000000.0);
    MilliSecondCountDelta = -1;
    memset(GetMillisecondsSinceLastCallBuffer, 0, sizeof(GetMillisecondsSinceLastCallBuffer));

    InitializeFlag = true;
}
//---------------------------------------------------------------------------
void TimerClass::TimeOfDayGet(int TimeZone)
{
    long UnixUtcTime;
    struct tm *DateTime;

    UnixUtcTime = time(&UnixUtcTime);
    switch (TimeZone)
    {
        case TIME_UTC:
            DateTime = gmtime(&UnixUtcTime);
            break;
        case TIME_LOCAL:
            DateTime = localtime(&UnixUtcTime);
        default:
            break;
    }

	Second = (unsigned char)DateTime->tm_sec;
	Minute = (unsigned char)DateTime->tm_min;
	Hour = (unsigned char)DateTime->tm_hour;
    Day = (unsigned char)DateTime->tm_mday;
    WeekDay = (unsigned char)DateTime->tm_wday;
    Month = (unsigned char)(DateTime->tm_mon +1);
    Year = (int)DateTime->tm_year +1900;
}
//---------------------------------------------------------------------------
long TimerClass::GetUnixUtcTime(void)
{
    time_t UnixUtcTime1;
    time_t UnixUtcTime2;

    // This will return immediately after the transition so milliseconds will
    // be very close to zero.
    UnixUtcTime1 = time(0);
    UnixUtcTime2 = UnixUtcTime1;
    while(UnixUtcTime2 == UnixUtcTime1)
        UnixUtcTime2 = time(0);

    return(UnixUtcTime2);
}
//---------------------------------------------------------------------------
int TimerClass::ConvertUnixUtcTimeToUtcDateTime(long UnixUtcTime, int *Year, int *Month, int *Day, int *Hour, int *Minute, int *Second)
{
    struct tm *DateTime;

    DateTime = gmtime(&UnixUtcTime);
    if (DateTime == NULL)
    {
        // We really should never get here.
        // Default to UNIX time zero.
        *Year = 1970;
        *Month = 1;
        *Day = 1;
        *Hour = 0;
        *Minute = 0;
        *Second = 0;
        return(false);
    }

    *Year = DateTime->tm_year +1900;
    *Month = DateTime->tm_mon +1;
    *Day = DateTime->tm_mday;
    *Hour = DateTime->tm_hour;
    *Minute = DateTime->tm_min;
    *Second = DateTime->tm_sec;

    return(true);
}
//---------------------------------------------------------------------------
int TimerClass::ConvertUnixUtcTimeToLocalDateTime(long UnixUtcTime, int *Year, int *Month, int *Day, int *Hour, int *Minute, int *Second)
{
    struct tm *DateTime;
    int HourB, MinuteB, SecondB;

    DateTime = localtime(&UnixUtcTime);
    if (DateTime == NULL)
    {
        // This happens when the argument is less than the minimum difference
        // between UTC and local time.  i.e. CST = UTC -6hrs, so UnixUtcTime should
        // be at least 6hrs (21600 seconds).  Anything less is before UNIX time zero
        // (01/01/1970 00:00:00) and therefore the output of the above function is
        // undefined.
        // Compensate by backing up to before UNIX time zero as required per
        // local time zone.
        *Year = 1969;
        *Month = 12;
        *Day = 31;
        HourB = UnixUtcTime /3600;
        MinuteB = (UnixUtcTime -(HourB *3600)) /60;
        SecondB = UnixUtcTime -(HourB *3600) -(MinuteB *60);
        // Compensate for local time zone.
        HourB += (24 -(_timezone /3600));
        *Hour = HourB;
        *Minute = MinuteB;
        *Second = SecondB;
    }
    else
    {
        *Year = DateTime->tm_year +1900;
        *Month = DateTime->tm_mon +1;
        *Day = DateTime->tm_mday;
        *Hour = DateTime->tm_hour;
        *Minute = DateTime->tm_min;
        *Second = DateTime->tm_sec;
    }

    return(true);
}
//---------------------------------------------------------------------------
int TimerClass::ConvertLocalDateTimeToUnixUtcTime(int Year, int Month, int Day, int Hour, int Minute, int Second, long *UnixUtcTime)
{
    struct tm DateTime;
    long UnixUtcTimeB;

    // A properly formatted date should have been sent.
    // But just in case it wasn't...
    if (Year < 1970)
        Year = 1970;
    if (Month == 0)
        Month = 1;
    if (Day == 0)
        Day = 1;

    memset(&DateTime, 0, sizeof(DateTime));
    DateTime.tm_year = Year -1900;
    DateTime.tm_mon = Month -1;
    DateTime.tm_mday = Day;
    DateTime.tm_hour = Hour;
    DateTime.tm_min = Minute;
    DateTime.tm_sec = Second;
    DateTime.tm_isdst = -1;

    UnixUtcTimeB = mktime(&DateTime);

    if (UnixUtcTimeB == -1)
    {
        // This happens when the argument is less than the minimum difference
        // between UTC and local time.  i.e. CST = UTC -6hrs, so the date/time
        // should be no earlier than 01/01/1970 06:00:00.  Anything earlier is
        // before local UNIX time zero (01/01/1970 00:00:00) and therefore the
        // output of the above function is undefined.
        // With the correction above, we should never get here.
        // But just in case, default to UNIX time zero.
        *UnixUtcTime = 0;
        return(false);
    }

    *UnixUtcTime = UnixUtcTimeB;

    return(true);
}
//---------------------------------------------------------------------------
void TimerClass::WaitNMicroSeconds(int n)
{
    WaitTicks((unsigned long)(n *TicksPerMicroSecond));
}
//---------------------------------------------------------------------------
void TimerClass::WaitNMilliSeconds(int n)
{
    WaitTicks((unsigned long)(n *1000.0 *TicksPerMicroSecond));
}
//---------------------------------------------------------------------------
float TimerClass::GetMillisecondsSinceLastCall(int Index)
{
    _LARGE_INTEGER Counter,Frequency;
    static long T2;
    long dT;
    float PeriodMs;

    // Get the clock frequency
    QueryPerformanceFrequency(&Frequency);

    // Get the current counts
    QueryPerformanceCounter(&Counter);
    T2 = Counter.LowPart;

    // Calculate dT in counts and finally in milliseconds
    dT = T2 -GetMillisecondsSinceLastCallBuffer[Index];
    PeriodMs = (float)dT /(float)Frequency.LowPart *1000.0;

    // Save the current counts for next time.
    GetMillisecondsSinceLastCallBuffer[Index] = T2;

    // Returns the number of milliseconds since the last time
    // this method was called.
    return (PeriodMs);

}
//---------------------------------------------------------------------------
double TimerClass::MicroSecondsToTicks(int MicroSeconds)
{
    return((double)MicroSeconds * TicksPerMicroSecond);
}
//---------------------------------------------------------------------------
int TimerClass::WaitTicks(unsigned long NumberTicksToWait)
{
    unsigned long CountStop;
    int LoopCount = 0;

    QueryPerformanceCounter(&PerformanceCount);
    CountStop = (unsigned long)PerformanceCount.LowPart +NumberTicksToWait;

    //while ( ((double)PerformanceCount.QuadPart < CountStop) && (LoopCount < 50000000) )
    while ((unsigned long)PerformanceCount.LowPart < CountStop)
    {
        QueryPerformanceCounter(&PerformanceCount);
        LoopCount++;
    }

    return(LoopCount);
}
//---------------------------------------------------------------------------

