//---------------------------------------------------------------------------
#ifndef WarriorCommandH
#define WarriorCommandH
//---------------------------------------------------------------------------


class WarriorCommandClass
{
    public:
    WarriorCommandClass(){;}
    int SendCommand(void);

    int Exchange(unsigned char DataGroupType);

    private:
    struct MESSAGE_STRUCTURE Message;

};
extern WarriorCommandClass WarriorCommand;


#endif





