#ifndef GRAPHPARSER_H
#define GRAPHPARSER_H

#include <qstring.h>
#include <QtCharts/qchartview.h>

QT_CHARTS_USE_NAMESPACE


#define CHARTNAME "chartname"
#define MAXVALUE "max"
#define MINVALUE "min"
#define AXISLABEL "axislablel"
#define CURVENAME "name"
#define COLOR "color"

#define PROPERTYSEPARATOR ","
#define VALUESEPARATOR "#"



//chartname=bob,xxx#yyyy#zzz
//xxxx=max#100,min#0,name#yyy,axislabel#zzz,color#



class GraphParser
{
public:
    GraphParser();

    static bool parseConfig(QString configfile, QString logdatafile, QChartView* chart);
    static QHash<QString,QHash<unsigned int,float>> readSeries(QString logdatafile, QList<QString> curvenames);
    static QHash<unsigned int,QHash<QString,QString>> readLogFile(QString logdatafile);
};

#endif // GRAPHPARSER_H
