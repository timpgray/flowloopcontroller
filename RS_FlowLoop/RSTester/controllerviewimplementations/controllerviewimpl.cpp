#include "ui_testmainwindow.h"
#include "controllerviewimpl.h"
#include "testmainwindow.h"
#include "rs_constants.h"

#include <qloggingcategory.h>
#include <qdatetime.h>
#include <QTableWidgetItem>

QLoggingCategory CONTROLLERVIEWIMPL("ControllerViewImpl");



ControllerViewImpl::ControllerViewImpl(Ui::TestMainWindow *ui, QObject *parent) : ControllerView(parent)
{

    m_ui = ui;
    m_ui->tableWidget->setColumnCount(4);
    m_ui->tableWidget->setSortingEnabled(false);
    m_ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_lastTFUpdateTime = QDateTime::currentDateTime().toTime_t();

    connect(&m_surveyTimer, SIGNAL(timeout()), this, SLOT(handleTimer()));
}


void ControllerViewImpl::initialize()
{
    connect(m_ui->facilityon, SIGNAL(clicked()), this, SLOT(onClickedFacilityOn()));
    connect(m_ui->quickstop, SIGNAL(clicked()), this, SLOT(onClickedQuickStop()));
    connect(m_ui->runpumps, SIGNAL(clicked(bool)), this, SLOT(onClickedRunMudPump()));
    connect(m_ui->runtopdrive, SIGNAL(clicked(bool)), this, SLOT(onClickedRunTopDrive()));
    connect(m_ui->configurecontroller, SIGNAL(clicked(bool)), this, SLOT(onClickedConfigureController()));
    connect(m_ui->configurecontrollera, SIGNAL(clicked(bool)), this, SLOT(onClickedConfigureController()));
    connect(m_ui->showdiagnostics, SIGNAL(clicked(bool)), this, SLOT(onClickedShowDiagnostics()));

    connect(m_ui->autodrive_on, SIGNAL(clicked(bool)), this, SLOT(onClickedAutoDriveOn()));
    connect(m_ui->autodrive_takesurvey, SIGNAL(clicked(bool)), this, SLOT(onClickedTakeSurvey()));
    connect(m_ui->autodrive_drillahead, SIGNAL(clicked(bool)), this, SLOT(onClickedDrillAhead()));
    connect(m_ui->autodrive_steer, SIGNAL(clicked(bool)), this, SLOT(onClickedSteer()));

    startTimer(1000);

}


void ControllerViewImpl::initialized(QHash<QString,QString> intistatus)
{
    Q_UNUSED(intistatus);

}

void ControllerViewImpl::newStatusUpdate(QHash<QString,QString> statusmap)
{
    if(statusmap.contains(FACILITY_STARTED))
    {
        bool started = statusmap.value(FACILITY_STARTED) == STATUS_TRUE;
        QString newtext = started ? "Facility Off":"Facility On";

//        m_ui->facilityon->setText(newtext);

        m_ui->autodrive_drillahead->setEnabled(started);
        m_ui->autodrive_takesurvey->setEnabled(started);
        m_ui->autodrive_on->setEnabled(started);
        m_ui->autodrive_steer->setEnabled(started);

    }

    bool showDiag = false;

    if(statusmap.contains(SHOWDIAGNOSTICS) && statusmap.value(SHOWDIAGNOSTICS) == STATUS_TRUE)
    {
        showDiag = true;
    }

    m_ui->showdiagnostics->setVisible(showDiag);
}


void ControllerViewImpl::updateTopDriveTargetRPM(float rpm)
{
    m_ui->reftopdriverpm->setText(QString::number(rpm, 'f', 2));
    m_ui->controllertopdriveactualrpm->setText(QString::number(rpm, 'f', 2));
    m_ui->controllertopdriverpmref->setText(QString::number(rpm, 'f', 2));
}

void ControllerViewImpl::updateActualTopDriveRPM(float rpm)
{
    m_ui->actualtopdriverpm->setText(QString::number(rpm, 'f', 2));
//    m_ui->controllertopdriveactualrpm->setText(QString::number(rpm, 'f', 2));
    m_topdriveRPM = rpm;
}


void ControllerViewImpl::updateActualMudPumpRPM(float rpm)
{
    m_ui->actualpumprpm->setText(QString::number(rpm, 'f', 2));
    m_mudpumpRPM = rpm;
}

void ControllerViewImpl::updateDownHoleValue(QString name, float toolface)
{

    qCDebug(CONTROLLERVIEWIMPL) << "!!!downhole data named " << name << " value = " << toolface;
    QString tooltext;

    m_toolDataCache.insert(name, QString::number(toolface, 'f', 2));

    QStringList keys = m_toolDataCache.keys();

    for(int i=0;i<keys.size();i++)
    {
        QString keyvalue = keys.at(i);

        tooltext += keyvalue + " = " + m_toolDataCache.value(keyvalue) + "\n";
    }

    m_ui->tooldata_2->setText(tooltext);
}

void ControllerViewImpl::updateDownHoleRPM(float rpm)
{
    m_ui->controllertopdriverdownholerpm->setText(QString::number(rpm, 'f', 2));
}

void ControllerViewImpl::updateDownHoleToolface(float toolface)
{
    m_ui->controllertoolfacedownholerpm->setText(QString::number(toolface, 'f', 1));

    m_ui->compass->addToolFaceValue(toolface);

    float tftarget = m_ui->controllertoolfaceref->text().toFloat();
    float tferror = tftarget - toolface;

    if (tferror < -180.0)
    {
        tferror += 360.0;
    }
    if (tferror > 180.0)
    {
        tferror -= 360.0;
    }

    m_ui->controllertoolfacesurfaceactual->setText(QString::number(-tferror, 'f', 1));

    m_tfErrorHistory.append(-tferror);

    if(m_tfErrorHistory.size() > 5)
    {
        m_tfErrorHistory.removeFirst();
    }

    float average = calcAverage(&m_tfErrorHistory);
    float stdev = calcSTDev(&m_tfErrorHistory, average);
    QString lastupdate = QString("Updated: ") + QDateTime::currentDateTime().toString("HH:mm:ss");

    m_ui->tferrordisp->setText(QString::number(average, 'f', 1));
    m_ui->tferrorstdevdisp->setText(QString::number(stdev, 'f', 1));
    m_ui->lastupdatetime->setText(lastupdate);

    m_lastTFUpdateTime = QDateTime::currentDateTime().toTime_t();

}

void ControllerViewImpl::updatePressureTop(float pressure)
{
    QString text = QString::number(pressure, 'f', 1);

    m_ui->pressuretop->setText(text);

}

void ControllerViewImpl::updatePressureBotton(float pressure)
{
    QString text = QString::number(pressure, 'f', 1);

    m_ui->pressurebottom->setText(text);
}

void ControllerViewImpl::updatePressureDrop(float pressure)
{
    QString text = QString::number(pressure, 'f', 2);

    m_ui->pressuredrop->setText(text);
}

void ControllerViewImpl::updateFlowRate(float flowrate)
{
    QString text = QString::number(flowrate, 'f', 0);

    m_ui->pumpflowrate->setText(text);
}

void ControllerViewImpl::updateTopDriveStatus(bool status)
{
    QString text = status?"Stop":"Run";

    m_ui->runtopdrive->setText(text);
}

void ControllerViewImpl::updateMudPumpStatus(bool status)
{
    QString text = status?"Stop":"Run";

    m_ui->runpumps->setText(text);
}


void ControllerViewImpl::onClickedFacilityOn()
{
    qCDebug(CONTROLLERVIEWIMPL) << "Inside onClickedFacilityOn()  ";
    QString text = m_ui->facilityon->text();
    bool onstatus = (text == "Facility On");

    if(m_ui->facilityon->isChecked())
    {
        m_ui->quickstop->setEnabled(true);
        if(!m_ui->runpumps->isChecked())
        {
            QTimer::singleShot(2000, this, SLOT(runMudPumpDelayed()));
        }

        if(!m_ui->runtopdrive->isChecked())
        {
            m_ui->runtopdrive->animateClick();
        }
    }

}

void ControllerViewImpl::onClickedQuickStop()
{
    qCDebug(CONTROLLERVIEWIMPL) << "Inside onClickedQuickStop()  ";
    m_ui->quickstop->setEnabled(false);
    emit facilityQuickStop();

    if(m_ui->runpumps->isChecked())
    {
        m_ui->runpumps->animateClick();
    }

    if(m_ui->runtopdrive->isChecked())
    {
        QTimer::singleShot(10000, this, SLOT(stopTopDriveDelayed()));
    }

    if(!m_ui->autodrive_on->isChecked())
    {
        m_ui->autodrive_on->animateClick();
    }

    if(m_ui->facilityon->isChecked())
    {
        m_ui->facilityon->animateClick();
    }

}

void ControllerViewImpl::onClickedRunTopDrive()
{
    bool status =  m_ui->runtopdrive->text() == "Run";
    qCDebug(CONTROLLERVIEWIMPL) << "running top drive status = " <<  status;

    emit runTopDrive(status);

    if(status)
    {
        QTimer::singleShot(1000, this, SLOT(enableMudPumpRun()));
    }
    else
    {
        m_ui->runpumps->setEnabled(false);
    }

}

void ControllerViewImpl::onClickedRunMudPump()
{
    bool status =  m_ui->runpumps->text() == "Run";
    qCDebug(CONTROLLERVIEWIMPL) << "running mud pump status = " << status;

    emit runMudPump(status);

    if(status)
    {
        m_ui->runtopdrive->setEnabled(false);
    }
    else
    {
        QTimer::singleShot(1000, this, SLOT(enableTopDriveRun()));
    }
}

void ControllerViewImpl::updateControllerInfo(QString information)
{
    qCDebug(CONTROLLERVIEWIMPL) << "!!! msg from controller " << information;
    m_statusList.append(information);

    if(m_statusList.size() > 12)
    {
        m_statusList.removeFirst();
    }

    QString text = m_statusList.join("\n");
    m_ui->controllerlog->setText(text);
}


void ControllerViewImpl::onClickedConfigureController()
{
    qCDebug(CONTROLLERVIEWIMPL) << "onClickedConfigureController() user clicked...";

    emit configure();
}


void ControllerViewImpl::updateLimits(float maxmudpumprpm, float maxtopdriverpm)
{
    qCDebug(CONTROLLERVIEWIMPL) << "inside updateLimits max pump rpm = " << maxmudpumprpm << " max top drive rpm = " << maxtopdriverpm;
}

void ControllerViewImpl::updateRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)
{
    qCDebug(CONTROLLERVIEWIMPL) << "inside update ranges Top range = " << toprange << " offset = " << topoffset;
    qCDebug(CONTROLLERVIEWIMPL) << "inside update ranges Bottom range = " << bottomrange << " offset = " << bottomoffset;


}

void ControllerViewImpl::controllerInitialized(QHash<QString,QString> props)
{
    qCDebug(CONTROLLERVIEWIMPL) << "Controller initialize response has these properties " << props;
//    bool showDiag = false;

//    if(props.contains(SHOWDIAGNOSTICS) && props.value(SHOWDIAGNOSTICS) == STATUS_TRUE)
//    {
//        showDiag = true;
//    }

//    m_ui->showdiagnostics->setVisible(showDiag);
}

void  ControllerViewImpl::updateModeStatus(QString msg)
{
    m_ui->controlmode->setText(msg);
}

void ControllerViewImpl::onClickedShowDiagnostics()
{
    showDiagnostics();
}


void ControllerViewImpl::onClickedAutoDriveOn()
{
    QString text = m_ui->autodrive_on->text();
    QString newtext = "ON";
    ControllerMode mode = OFF;

    if(m_ui->autodrive_on->isChecked())
    {
        newtext = "OFF";
        mode = OFF;
    }

    m_ui->settopdrivespeed->setEnabled(m_ui->autodrive_on->isChecked());


//    m_ui->autodrive_on->setText(newtext);
    emit controllerModeChanged(mode);

}

void ControllerViewImpl::onClickedTakeSurvey()
{
    m_ui->quickstop->animateClick();

    m_surveyCounter = 60;
    m_startPumpsTimeout = 20;
    m_mudpumpHistory.clear();
    m_topdriveHistory.clear();

    m_surveySteps.enqueue(&stoppingPumps);
    m_surveySteps.enqueue(&takeSurvey);
    m_surveySteps.enqueue(&startingPumps);
    m_surveySteps.enqueue(&resumeSteering);

    m_surveyTimer.start(1000);
}


void ControllerViewImpl::onClickedDrillAhead()
{
    emit controllerModeChanged(DrillAhead);
}

void ControllerViewImpl::onClickedSteer()
{
    emit controllerModeChanged(Steering);
    m_ui->settopdrivespeed->setEnabled(!m_ui->autodrive_steer->isChecked());
}


void ControllerViewImpl::newWITSData(QString witsmsg, QString name, float value)
{
    qCDebug(CONTROLLERVIEWIMPL) << "new wits data msg = " << witsmsg << " name = " << name << " value = " << value;
    QString timestr = QDateTime::currentDateTime().toString("MM/dd/yy hh:mm:ss");
    int rowcount  = m_ui->tableWidget->rowCount();

    m_ui->tableWidget->setRowCount(rowcount+1);

    QTableWidgetItem* timeitem = new QTableWidgetItem(timestr);
    QTableWidgetItem* msgitem = new QTableWidgetItem(witsmsg);
    QTableWidgetItem* nameitem = new QTableWidgetItem(name);
    QTableWidgetItem* valueitem = new QTableWidgetItem(QString::number(value, 'f', 3));

    m_ui->tableWidget->setItem(rowcount, 0, timeitem);
    m_ui->tableWidget->setItem(rowcount, 1, msgitem);
    m_ui->tableWidget->setItem(rowcount, 2, nameitem);
    m_ui->tableWidget->setItem(rowcount, 3, valueitem);

    if(m_ui->autoscrollwitsdata->isChecked())
    {
        m_ui->tableWidget->scrollToBottom();
    }

    if(m_ui->tableWidget->rowCount() > 200)
    {
        m_ui->tableWidget->removeRow(0);
    }

    qCDebug(CONTROLLERVIEWIMPL) << "row Count = " << m_ui->tableWidget->rowCount();
}


void ControllerViewImpl::updateTopDriveTorque(float torque)
{
    m_ui->topdrivetorque->setText(QString::number(torque, 'f', 0));
}


void ControllerViewImpl::enableMudPumpRun()
{
    m_ui->runpumps->setEnabled(true);
}

void ControllerViewImpl::enableTopDriveRun()
{
    m_ui->runtopdrive->setEnabled(true);
}


void ControllerViewImpl::stopTopDriveDelayed()
{
   m_ui->runtopdrive->animateClick();
}

void ControllerViewImpl::runMudPumpDelayed()
{
    m_ui->runpumps->animateClick();
}

void ControllerViewImpl::handleNewRPMCorrection(float newcorrection, bool isrunning)
{
    QString text = "Surface";

    if(isrunning)
    {
        text += "RPM Delta: " + QString::number(newcorrection, 'f', 2);
    }

    m_ui->surfacelabel->setText(text);

}

bool ControllerViewImpl::stoppingPumps()
{
    bool finished = false;
    qCDebug(CONTROLLERVIEWIMPL) << "inside stoppingPumps()";

    updateModeStatus("Stopping pumps");

    m_mudpumpHistory << m_mudpumpRPM;
    m_topdriveHistory << m_topdriveRPM;

    if(m_mudpumpHistory.size() > 5)
    {
        m_mudpumpHistory.removeFirst();
    }

    if(m_topdriveHistory.size() > 5)
    {
        m_topdriveHistory.removeFirst();
    }

    float averagetdrpm = calcAverage(&m_topdriveHistory);
    float averagemprpm = calcAverage(&m_mudpumpHistory);
    float tdSTDev = calcSTDev(&m_topdriveHistory, averagetdrpm);
    float mpSTDev = calcSTDev(&m_mudpumpHistory, averagemprpm);

    averagetdrpm = fabs(averagetdrpm);
    averagemprpm = fabs(averagemprpm);

    qCDebug(CONTROLLERVIEWIMPL) << "inside stoppingPumps() average topdrive rpm = " << averagetdrpm << " average mud pump rpm = " << averagemprpm;
    qCDebug(CONTROLLERVIEWIMPL) << "inside stoppingPumps() topdrive stdev = " << tdSTDev << " mudpump stdev = " << mpSTDev;

    if((averagetdrpm < 2) && (averagemprpm < 2) && (tdSTDev < 1.0) && (mpSTDev < 1.0))
    {
        finished = true;
    }

    qCDebug(CONTROLLERVIEWIMPL) << "inside stoppingPumps() finished = " << finished;

    return finished;
}

bool ControllerViewImpl::takeSurvey()
{
    bool finished = false;
    QString msg = QString("Taking Survey ") + QString::number(m_surveyCounter) + QString(" seconds");
    qCDebug(CONTROLLERVIEWIMPL) << "inside take Survey";

    updateModeStatus(msg);

    if((m_surveyCounter-- <= 0))
    {
        finished = true;
        m_ui->facilityon->animateClick();
        m_mudpumpHistory.clear();
        m_topdriveHistory.clear();
    }

    return finished;
}

bool ControllerViewImpl::startingPumps()
{
    qCDebug(CONTROLLERVIEWIMPL) << "inside startingPumps()";

    bool finished = false;

    updateModeStatus("Starting Pumps");

    m_mudpumpHistory << m_mudpumpRPM;
    m_topdriveHistory << m_topdriveRPM;

    if(m_mudpumpHistory.size() > 5)
    {
        m_mudpumpHistory.removeFirst();
    }

    if(m_topdriveHistory.size() > 5)
    {
        m_topdriveHistory.removeFirst();
    }

    float averagetdrpm = calcAverage(&m_topdriveHistory);
    float averagemprpm = calcAverage(&m_mudpumpHistory);
    float tdSTDev = calcSTDev(&m_topdriveHistory, averagetdrpm);
    float mpSTDev = calcSTDev(&m_mudpumpHistory, averagemprpm);

    averagetdrpm = fabs(averagetdrpm);
    averagemprpm = fabs(averagemprpm);

    qCDebug(CONTROLLERVIEWIMPL) << "inside startingPumps() average topdrive rpm = " << averagetdrpm << " average mud pump rpm = " << averagemprpm << " top drive std = " << tdSTDev << " mud motor std = " << mpSTDev;
    qCDebug(CONTROLLERVIEWIMPL) << "inside startingPumps() topdrive stdev = " << tdSTDev << " mudpump stdev = " << mpSTDev;

    if(((averagetdrpm > 20) && (averagemprpm > 20) /*&& (tdSTDev < 10.0) && (mpSTDev < 10.0)*/) && (m_startPumpsTimeout-- <= 0))
    {
        finished = true;
    }

    return finished;
}

bool ControllerViewImpl::resumeSteering()
{
    qCDebug(CONTROLLERVIEWIMPL) << "inside resumeSteering";
    m_ui->autodrive_steer->animateClick();
    m_ui->autodrive_takesurvey->blockSignals(true);
    m_ui->autodrive_takesurvey->setChecked(false);
    m_ui->autodrive_takesurvey->blockSignals(false);

    return true;
}

void ControllerViewImpl::handleTimer()
{
    qCDebug(CONTROLLERVIEWIMPL) << "inside handleTimer";

    if(!m_surveySteps.isEmpty())
    {
        SURVEY_HANDLER fcn = m_surveySteps.first();

        bool finished = (this->*fcn)();

        if(finished)
        {
            m_surveySteps.dequeue();
        }
    }
    else
    {
        m_surveyTimer.stop();
    }

}

float ControllerViewImpl::calcAverage(QList<float>* list)
{
    float total = 0;

    if(!list->isEmpty())
    {
        for(int i=0;i<list->size();i++)
        {
            total += list->at(i);
        }

        total /= list->size();
    }

    return total;
}

float ControllerViewImpl::calcSTDev(QList<float>* list, float average)
{
    float total = 0;

    if(!list->isEmpty())
    {
        for(int i=0;i<list->size();i++)
        {
            float diff = average - list->at(i);

            diff = pow(diff, 2);
            total += diff;
        }

        total /= list->size();
        total = sqrt(total);
    }

    return total;
}

void ControllerViewImpl::timerEvent(QTimerEvent* event)
{
    unsigned int delta = QDateTime::currentDateTime().toTime_t() - m_lastTFUpdateTime;
    QString msg = QString("Last Update " + QString::number(delta) + " Seconds");

    m_ui->toolfacedeltat->setText(msg);
}
