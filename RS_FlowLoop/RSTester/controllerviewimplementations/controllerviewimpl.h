#ifndef CONTROLLERVIEWIMPL_H
#define CONTROLLERVIEWIMPL_H

#include <QObject>
#include "testmainwindow.h"
#include "controllerview.h"
#include <qstring.h>
#include <qhash.h>
#include <qstringlist.h>
#include <qqueue.h>
#include <qtimer.h>

class ControllerViewImpl;

typedef bool (ControllerViewImpl::*SURVEY_HANDLER)();

class ControllerViewImpl : public ControllerView
{
    Q_OBJECT
public:
    explicit ControllerViewImpl(Ui::TestMainWindow *ui, QObject *parent = 0);
    virtual ~ControllerViewImpl(){}

    virtual void initialize();
    virtual QString getName(){return "Default View";}


protected:
    float calcAverage(QList<float>* list);
    float calcSTDev(QList<float>* list, float average);

    bool stoppingPumps();
    bool startingPumps();
    bool takeSurvey();
    bool resumeSteering();
    virtual void timerEvent(QTimerEvent* event);


public slots:
    virtual void initialized(QHash<QString,QString> intistatus);
    virtual void newStatusUpdate(QHash<QString,QString> statusmap);
    virtual void updateTopDriveTargetRPM(float rpm);
    virtual void updateActualTopDriveRPM(float rpm);
    virtual void updateActualMudPumpRPM(float rpm);
    virtual void updateDownHoleRPM(float rpm);
    virtual void updateDownHoleToolface(float toolface);
    virtual void updateDownHoleValue(QString name, float toolface);
    virtual void updatePressureTop(float pressure);
    virtual void updatePressureBotton(float pressure);
    virtual void updatePressureDrop(float pressure);
    virtual void updateFlowRate(float flowrate);
    virtual void updateTopDriveTorque(float torque);

    virtual void updateTopDriveStatus(bool status);
    virtual void updateMudPumpStatus(bool status);

    virtual void updateControllerInfo(QString information);
    virtual void updateLimits(float maxmudpumprpm, float maxtopdriverpm);
    virtual void updateRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);

    virtual void controllerInitialized(QHash<QString,QString> props);
    virtual void updateModeStatus(QString msg);

    virtual void newWITSData(QString witsmsg, QString name, float value);

    virtual void handleNewRPMCorrection(float newcorrection, bool isrunning);



    void onClickedFacilityOn();
    void onClickedQuickStop();
    void onClickedRunTopDrive();
    void onClickedRunMudPump();
    void onClickedConfigureController();
    void onClickedShowDiagnostics();
    void onClickedAutoDriveOn();
    void onClickedTakeSurvey();
    void onClickedDrillAhead();
    void onClickedSteer();

    void enableMudPumpRun();
    void enableTopDriveRun();
    void stopTopDriveDelayed();
    void runMudPumpDelayed();

    void handleTimer();


private:
    Ui::TestMainWindow *m_ui;
    QHash<QString,QString> m_toolDataCache;
    QStringList m_statusList;

    QQueue<SURVEY_HANDLER> m_surveySteps;

    float m_mudpumpRPM;
    float m_topdriveRPM;
    int m_pumpsStableCount;
    int m_surveyCounter;
    QList<float> m_mudpumpHistory;
    QList<float> m_topdriveHistory;
    QTimer m_surveyTimer;
    int m_startPumpsTimeout;
    QList<float> m_tfErrorHistory;
    unsigned int m_lastTFUpdateTime;
};

#endif // CONTROLLERVIEWIMPL_H
