#include "graphparser.h"
#include "utils.h"

#include <qhash.h>
#include <qloggingcategory.h>


QLoggingCategory GRAPHPARSER("GraphParser");



GraphParser::GraphParser()
{

}


bool GraphParser::parseConfig(QString configfile, QString logdatafile, QChartView* chart)
{
    bool status = false;
    QHash<QString,QString> props = Utils::loadPropsFile(configfile, "=");
    QString chartname;
    QStringList curvelist;
    QHash<QString,QHash<QString,QString>> curvepropertymap;

    if(props.contains(CHARTNAME))
    {
        QString value = props.value(CHARTNAME);
        QStringList parts = value.split(PROPERTYSEPARATOR);

        status = true;

        if(parts.size() >= 2)
        {
            QString curvevalues = parts.at(1);

            chartname = parts.at(0);
            curvelist = curvevalues.split(VALUESEPARATOR);

            qCDebug(GRAPHPARSER) << "chart name = " << chartname << " curves = " << curvelist;

            if(curvelist.size() > 0)
            {
                for(int i=0;i<curvelist.size();i++)
                {
                    QString curvename = curvelist.at(i);
                    QHash<QString,QString> curvepropsmap;

                    qCDebug(GRAPHPARSER) << "  curve name = " << curvename;

                    if(props.contains(curvename))
                    {
                        QString curveprops = props.value(curvename);
                        QStringList propertylist = curveprops.split(PROPERTYSEPARATOR);

                        qCDebug(GRAPHPARSER) << "    curve property string = " << curveprops;

                        for(int j=0;j<propertylist.size();j++)
                        {
                            QString curveproperty = propertylist.at(j);
                            QStringList valuelist = curveproperty.split(VALUESEPARATOR);

                            if(valuelist.size() >= 2)
                            {
                                QString propertyname = valuelist.at(0);
                                QString propertyvalue = valuelist.at(1);

                                curvepropsmap.insert(propertyname, propertyvalue);
                                qCDebug(GRAPHPARSER) << "    property name = " << propertyname << " value = " << propertyvalue;
                            }
                            else
                            {
                                qCWarning(GRAPHPARSER) << "invalid property " << curveproperty;
                                status = false;
                            }
                        }

                        curvepropertymap.insert(curvename, curvepropsmap);

                    }
                    else
                    {
                        qCWarning(GRAPHPARSER) << "curve name " << curvename << " not found";
                        status = false;
                    }

                }

            }
            else
            {
                qCWarning(GRAPHPARSER) << "No curves found";
                status = false;
            }
        }
        else
        {
            qCWarning(GRAPHPARSER) << "Chart name contains no curves";
            status = false;
        }
    }
    else
    {
        qCWarning(GRAPHPARSER) << "Chart name not found";
        status = false;
    }

    if(status)
    {
        QList<QString> curvekeys = curvepropertymap.keys();
        QHash<QString,QHash<unsigned int,float>> curvevalues = readSeries(logdatafile, curvekeys);

        for(int i=0;i<curvekeys.size();i++)
        {
            QString curvename = curvekeys.at(i);
            QHash<QString,QString> curveattr = curvepropertymap.value(curvename);

            qCDebug(GRAPHPARSER) << "Curve named " << curvename << " attributes " << curveattr;
        }
    }

    return status;
}

QHash<QString,QHash<unsigned int,float>> GraphParser::readSeries(QString logdatafile, QList<QString> curvenames)
{
    QHash<QString,QHash<unsigned int,float>> seriesdata;
    QHash<unsigned int,QHash<QString,QString>> loggeddata =  readLogFile(logdatafile);
    QList<unsigned int> datakeys = loggeddata.keys();

    for(int i=0;i<datakeys.size();i++)
    {   // iterating all the keys - which are timestamps
        unsigned timestamp = datakeys.at(i);
        QHash<QString,QString> rowdata = loggeddata.value(timestamp);

        for(int j=0;j<curvenames.size();j++)
        {   // iterating all the specified curve names
            QString cname = curvenames.at(j);

            if(rowdata.contains(cname))
            {
                QHash<unsigned int,float> parsedrowdata = seriesdata.contains(cname) ? seriesdata.value(cname):QHash<unsigned int,float>();
                float curvevalue = rowdata.value(cname).toFloat();

                parsedrowdata.insert(timestamp, curvevalue);
                seriesdata.insert(cname, parsedrowdata);
            }
        }
    }


    return seriesdata;
}

QHash<unsigned int,QHash<QString,QString>> GraphParser::readLogFile(QString logdatafile)
{
    QHash<unsigned int,QHash<QString,QString>> logdata;

    return logdata;
}
