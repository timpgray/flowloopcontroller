#ifndef TOPDRIVE_H
#define TOPDRIVE_H

#include <QObject>
#include "baseinitializer.h"

class TopDrive : public BaseInitializer
{
    Q_OBJECT
public:
    explicit TopDrive(QObject *parent = 0);
    virtual ~TopDrive(){}

    virtual void setMaxRPM(float maxval){m_maximumRPM = maxval;}


signals:
    void newActualRPM(float rpm);

public slots:
    virtual void updateRPM(float rpm)=0;
    virtual void topDriveOn(bool status)=0;

protected:
    float m_maximumRPM;
};

#endif // TOPDRIVE_H
