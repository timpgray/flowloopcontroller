#ifndef TESTMAINWINDOW_H
#define TESTMAINWINDOW_H

#include <QMainWindow>
#include "backend.h"
#include "controllerview.h"
#include "controller.h"
#include "connectionmanager.h"

namespace Ui {
class TestMainWindow;
}

class TestMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestMainWindow(QWidget *parent = 0);
    ~TestMainWindow();

    void initialize();
    void swapBackend(BackEnd* pnewbackend);
    void swapController(Controller* pnewcontroller);

private slots:

    void newStatusUpdate(QHash<QString,QString> statusmap);


    void on_tooldatasource_currentIndexChanged(const QString &arg1);

    void on_backendsource_currentIndexChanged(const QString &arg1);

    void on_controllerselection_currentIndexChanged(const QString &arg1);

    void on_setpumpspeed_clicked();

    void on_settopdrivespeed_clicked();

    void on_autodrive_on_clicked();

    void on_autodrive_makeconnect_clicked();

    void on_autodrive_drillahead_clicked();

    void on_autodrive_steer_clicked();

    void on_autodrive_settoolface_clicked();

    void on_showdiagnostics_clicked();

    void on_maxmudpumprpm_editingFinished();

    void on_maxtopdriverpm_editingFinished();

    void on_toolsamplerate_editingFinished();

    void on_propogationdelay_editingFinished();

    void on_clearwitsdata_clicked();

    void on_drillaheadoffset_editingFinished();

    void on_exit_clicked();

private:
    Ui::TestMainWindow *ui;

    BackEnd* m_currentBackend;
    ControllerView* m_pControllerView;
    Controller* m_pController;
    ConnectionManager m_connMgr;
};

#endif // TESTMAINWINDOW_H
