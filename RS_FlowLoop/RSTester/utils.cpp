#include "utils.h"

#include <qloggingcategory.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qdir.h>
#include <qdatetime.h>
#include "rs_constants.h"


QLoggingCategory UTILS("Utils.EM");

QHash<int,LogFile*> Utils::m_fileCache;
int Utils::m_nextKeyValue = 1;
QString Utils::m_controllerConfigPath = "C:/ROTOSLIDE/contollers";
QString Utils::m_controllerLogPath = "C:/ROTOSLIDE/controllerlogs";
QString Utils::m_dataLogPath = "C:/ROTOSLIDE/datalogs";
QString Utils::m_appLogDir = "C:/ROTOSLIDE/applogs";
bool Utils::m_decorateLogFilesWithDate = false;
QString Utils::m_logFileDateFormat = "MM_dd_yy_hh-mm-ss_";



Utils::Utils()
{

}


bool Utils::writeFile(QString filename, QList<QString> data)
{
    QFile outfile(filename);
    bool status = outfile.open(QFile::WriteOnly);
    QTextStream tstr(&outfile);

    qCDebug(UTILS) << "write file named " << filename << " status = " << status;

    if(status)
    {
        for(int i=0;i<data.size();i++)
        {
            tstr << data.at(i) << "\n";
        }
    }

    outfile.close();
    return status;
}



int Utils::createLogFile(QString filename, bool decoratewithtimestamp)
{
    int key = -1;

    if(decoratewithtimestamp || m_decorateLogFilesWithDate)
    {
        QString decoration = QDateTime::currentDateTime().toString(m_logFileDateFormat);
        QStringList parts = filename.split("/");

        parts.replace(parts.size()-1, decoration + parts.last());
        filename = parts.join('/');
    }

    QFile* newfile = new QFile(filename);
    bool status = newfile->open(QFile::WriteOnly);

    if(status)
    {
        LogFile* lf = new LogFile(newfile);

        key = m_nextKeyValue++;
        m_fileCache.insert(key, lf);
    }
    else
    {
        delete newfile;
    }

    return key;
}

bool Utils::writeLogFile(int key, QString data)
{
    int status = m_fileCache.contains(key);

    if(status)
    {
        m_fileCache.value(key)->write(data);
    }

    return status;
}

bool Utils::closeFile(int key)
{
    int status = m_fileCache.contains(key);

    if(status)
    {
        LogFile* lf = m_fileCache.value(key);

        lf->close();
        delete lf;
        m_fileCache.remove(key);

    }

    return status;
}




QHash<QString,QString> Utils::loadPropsFile(QString filepath, QString separator)
{
    QHash<QString,QString> props;
    QFile inputfile(filepath);
    bool status = inputfile.open(QFile::ReadOnly);

    while(!inputfile.atEnd())
    {
        QString inputline = inputfile.readLine();
        QStringList parts = inputline.split(separator);

        if(parts.size() >= 3 && parts.at(1).startsWith("\"") && parts.last().endsWith("\"") )
        {
            QString key = parts.first();

            parts.removeFirst();

            QString value = parts.join(separator);

            parts.clear();
            parts << key << value;
        }

        if(parts.size() > 1)
        {
            props.insert(parts.first().trimmed(), parts.at(1).trimmed());
        }
    }

    return props;
}


bool Utils::writePropsFile(QString filename, QHash<QString,QString> props, QString separator)
{
    QFile propsfile(filename);
    bool status = propsfile.open(QFile::WriteOnly);
    QTextStream tstr(&propsfile);
    QList<QString> keys = props.keys();

    qSort(keys);

    for(int i=0;i<keys.size();i++)
    {
        QString line = keys.at(i) + separator + props.value(keys.at(i)) + "\n";

        tstr << line;
    }

    propsfile.flush();
    propsfile.close();
    return status;
}

bool Utils::updateConfigFile(QString propname, QString value)
{
    QHash<QString,QString> props = loadConfigFile();

    props.insert(propname, value);
    return Utils::writeConfigFile(props);
}

bool Utils::appShouldLog()
{
   QHash<QString,QString> properties = loadConfigFile();
   bool log = properties.contains(APP_SHOULD_LOG) && properties.value(APP_SHOULD_LOG) == STATUS_TRUE;

   return log;
}


void Utils::ensureDirectoriesExist()
{
    QHash<QString,QString> properties = loadConfigFile();

    if(properties.contains(CONTROLLERCONFIGPATH))
    {
        m_controllerConfigPath = properties.value(CONTROLLERCONFIGPATH);
    }

    if(properties.contains(CONTROLLERLOGPATH))
    {
        m_controllerLogPath = properties.value(CONTROLLERCONFIGPATH);
    }

    if(properties.contains(DATALOGPATH))
    {
        m_dataLogPath = properties.value(CONTROLLERCONFIGPATH);
    }

    if(properties.contains(APPLOGPATH))
    {
        m_appLogDir = properties.value(CONTROLLERCONFIGPATH);
    }


    QStringList pathlist;

    pathlist << m_controllerConfigPath << m_controllerLogPath << m_dataLogPath << m_appLogDir;

    for(int i=0;i<pathlist.size();i++)
    {
        QString path = pathlist.at(i);
        QDir directory(path);

        if (!directory.exists())
        {
            QDir().mkpath(path);
        }
    }


    if(properties.contains(DECORATELOGFILESWITHTIMESTAMP))
    {
        m_decorateLogFilesWithDate = properties.value(DECORATELOGFILESWITHTIMESTAMP) == STATUS_TRUE;
    }

    if(properties.contains(LOGFILETIMESTAMPFORMAT))
    {
        m_logFileDateFormat = properties.value(LOGFILETIMESTAMPFORMAT);
    }

}

QString Utils::getModeDesc(ControllerMode mode)
{
    QString desc = "undef";

    switch(mode)
    {
    case OFF:
        desc = "Off";
        break;
    case Idle:
        desc = "Idle";
        break;
    case DrillAhead:
        desc = "Drill Ahead";
        break;
    case RPMNull:
        desc = "RPMNull";
        break;
    case Steering:
        desc = "Steering";
        break;
    default:
        break;
    }

    return desc;
}


float Utils::calcStandardDev(QList<float>* list)
{
    float average = 0;
    float stdev = 0;

    if(!list->isEmpty())
    {
        for(int i=0;i<list->size();i++)
        {
            average += list->at(i);
        }

        average /= (float)list->size();

        for(int i=0;i<list->size();i++)
        {
            stdev += pow((average - list->at(i)), 2);
        }

        stdev /= (float)list->size();
        stdev = sqrt(stdev);
    }

    return stdev;
}

float Utils::calcAverage(QList<float>* list)
{
    float average = 0;

    if(!list->isEmpty())
    {
        for(int i=0;i<list->size();i++)
        {
            average += list->at(i);
        }

        average /= (float)list->size();
    }

    return average;
}

LogFile::LogFile(QFile* pfile)
{
    m_file = pfile;
    m_textstr = new QTextStream(m_file);
}

LogFile::~LogFile()
{
    close();
}

void LogFile::write(QString entry)
{
    if(m_textstr != nullptr)
    {
        *m_textstr << entry << "\n";
        m_textstr->flush();
    }
}

void LogFile::close()
{
    if(m_textstr != nullptr)
    {
        m_textstr->flush();
        delete m_textstr;
        m_textstr = nullptr;
    }

    if(m_file != nullptr)
    {
        m_file->close();
        delete m_file;
        m_file = nullptr;
    }

}


