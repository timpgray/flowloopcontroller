#ifndef RS_CONSTANTS_H
#define RS_CONSTANTS_H

#define FACILITY_STATUS "facilitystatus"    // true or false
#define FACILITY_STARTED "facilitystarted"  // true of falase

#define MUDPUMP_STATUS "mudpumpstatus"
#define TOOL_STATUS "toolstatus"
#define TOPDRIVE_STATUS "topdrivestatus"
#define CONTROL_GRANTED "controlgranted"
#define BATTERY_STATUS "battstatus"

#define OVERRUNEVENTS "overruncounts"
#define BADCRCS "badcrccounts"


#define MODBUS_COMPORT  "modbuscomport"
#define MODBUS_BAUDRATE "modbusbaudrate"
#define TOOL_COMPORT "toolcomport"
#define TOOL_BAUDRATE "toolbaudrate"
#define WITS_COMPORT "witscomport"
#define WITS_BAUDRATE "witsbaudrate"
#define IEC_COMPORT "iec_comport"
#define IEC_BAUDRATE "iec_baudrate"
#define ROTONAV_COMPORT "rotonavcomport"
#define ROTONAV_BAUDRATE "rotonavbaudrate"

#define WITS_CONFIGFILE "witsconfigfile"


#define TEST_BACKEND "Testing"
#define LEGACY_BACKEND "Legacy"
#define CURRENT_BACKEND "Current"

#define MUDPUMP_DEFAULT "mudpumpdefault"
#define TOOL_ROTONAV "RotoNav"
#define TOOL_WITS "Wits"
#define TOPDRIVE_DEFAULT "topdrivedefault"
#define TOOL_ROTONAV_LEGACY "RotoNav L"
#define TOOL_WITS_LEGACY "Wits L"
#define TOPDRIVE_LEGACY "Top Drive L"
#define MUDPUMP_LEGACY "Mud Pump L"


#define STATUS_TRUE "true"
#define STATUS_FALSE "false"
#define STATUS_WARN "warn"

#define CONTROLLER_SIMPLE "Simple Controller"
#define CONTROLLER_PID "PID Controller"
#define CONTROLLER_SINGLESHOT "Single Shot Controller"
#define CONTROLLER_DERIVEDRPM "Derived RPM Controller"

#define SHOWDIAGNOSTICS "showdiag"
#define HEADERSTRING "headerlist"
#define CONTROLLER_STARTLOG "controllerstartlog"

#define NEW_RPM_VALUE  "newrpmvalue"    // new immediate value for RPM
#define RPM_HOLD_TIME "rpmholdtime"     // if specified the time to hold the new RPM value
#define RPM_END_VALUE "rpmendvalue"     // if specified the RPM to fallback to

#define APP_SHOULD_LOG "appshouldlog"

#define LEGACY_TOOL_TIMERINTERVAL "legacytooltimerinterval"


#define CONTROLLERMODE "mode"
#define CONTROLLERMODE_RPMNULL "rpmnull"
#define CONTROLLERMODE_STEERING "steering"
#define CONTROLLERRANGE "range"
#define CONTROLLEROUTOFRANGE "outofrange"
#define CONTROLLERINRANGE "inrange"
#define CONTROLLERTRACKING "tracking"

#define TOOLFACE "gtfa"
#define RPM "rpm"
#define BATV "batv"
#define TEMPERATURE "temp"
#define INCLINATION "inc"
#define AZIMUTH "azm"
#define DIPANGLE "dipa"
#define TOTALMAG "magt"
#define TOTOLGRAV "gravt"


#define SIMPLECONTROLLER_LAST_CONFIG "simplecontrollerlastconfig"
#define SIMPLECONTROLLER_FILES "simplecontrolerfiles"
#define RPM_SCALING "rpmscaling"
#define TF_ANGLE_SCALING "tfanglescaling"
#define PIDCONTROLLER_LAST_CONFIG "pidcontrollerlastconfig"
#define PIDCONTROLLER_FILES "pidcontrollerfiles"
#define RPM_PROPORTIONAL_SCALE "rpmpropscale"
#define RPM_INTEGRAL_SCALE "rpmintegralscale"
#define RPM_DERIVATIVE_SCALE "rpmderivitavescale"
#define TF_PROPORTIONAL_SCALE "tfproportionalscale"
#define TF_INTEGRAL_SCALE "tfintegralscale"
#define TF_DERIVATIVE_SCALE "tfderivitavescale"
#define STEERINGTHRESHOLD_RPM "steeringthreshold"
#define SINGLESHOTCONTROLLER_LAST_CONFIG "singleshotcontrollerlastconfig"
#define SINGLESHOTCONTROLLER_FILES "singleshotcontrolerfiles"
#define SINGLESHOTPULSEWIDTH "singleshotpulsewidth"
#define RPM_STEERING_SCALING "rpmsteeringscaling"
#define SINGLESHOT_USE_LOWEND_SCALING "useloendscaling"
#define SINGLESHOT_USE_LINEAR_SCALING "uselinearscaling"
#define SINGLESHOT_LOWEND_THRESHOLD "loendscalingthreshold"
#define SINGLESHOT_MAX_LOWEND_ADJUSTMENT "loendmaxadjustment"
#define DERIVEDRPMCONTROLLER_LAST_CONFIG "derivedrpmcontrollerlastconfig"
#define DERIVEDRPMCONTROLLER_FILES "derivedrpmcontrollerfiles"


#define CONTROLLERCONFIGPATH "controllerconfigpath"
#define CONTROLLERLOGPATH "controllerlogpath"
#define DATALOGPATH "datalogpath"
#define APPLOGPATH "applicationlogpath"


#define SYSTEMLOGTIME "Time"
#define SYSTEMLOGUNIXTIME "Time(unix)"

#define CONTROLLER_CURRENTTOOLFACE "Current Toolface"
#define CONTROLLER_CURRENTRPM "Current RPM"
#define CONTROLLER_NEWRPM "New RPM"
#define CONTROLLER_RPMDELTA "RPM Delta"
#define CONTROLLER_TOOLFACEERROR "Toolface Error"
#define CONTROLLER_RPMERROR "RPM Error"
#define CONTROLLER_START "Controller"

#define CONTROLLER_DRILLAHEADOFFSET "drillaheadoffset"

#define DEFAULT_DRILLAHEADOFFSET 10.F


#define TOPDRIVE_RPM "Top Drive RPM"
#define MUDPUMP_RPM "Mud Pump RPM"
#define FLOWRATE "Flow Rate"
#define TOP_PRESSURE "Top Pressure"
#define LOWER_PRESSURE "Lower Pressure"
#define TORQUE "Torque"
#define IEC_VERSION "iecversion"
#define IEC_START "IEC"

#define TOOL_START "TOOL"

#define DECORATELOGFILESWITHTIMESTAMP "decoratelogfiles"
#define LOGFILETIMESTAMPFORMAT "logfiletstampformat"

#define CURRENTCONTROLLER "currentcontroller"
#define CURRENTBACKEND "currentbackend"
#define CURRENTTOOL "currenttool"
#define CURRENTSAMPLERATE "currentsamplerate"
#define CURRENTTOPDRIVERPM "currenttopdriverpm"
#define CURRENTMUDPUMPRPM "currentmudpumprpm"
#define CURRENTPROPOGATIONDELAY "currentpropogationdelay"
#define CURRENTTOOLFACETARGET "currenttoolface"
#define CURRENTLEGACYTOOL "currentlegacytool"

#define CONTROLLER_MAX_CACHE_AGE_MILLIS "controllermaxagemillis"

#define VERSIONSTRING "v 0.0.9a"




#endif // RS_CONSTANTS_H
