#include "controllerfactory.h"
#include "rs_constants.h"
#include "controllerimplementations/simplecontroller.h"
#include "controllerimplementations/pidcontroller.h"
#include "controllerimplementations/singleshotcontroller.h"
#include "controllerimplementations/tfbasedcontroller.h"
#include "controllerimplementations/derivedrpmcontroller.h"

ControllerFactory::ControllerFactory()
{

}


Controller* ControllerFactory::getController(QString name)
{
    Controller* pcontroller = nullptr;

    if(name.isEmpty() || (name == CONTROLLER_SIMPLE))
    {
        pcontroller = new SimpleController();
    }

    if(name == CONTROLLER_PID)
    {
        pcontroller = new PIDController();
    }

    if(name == CONTROLLER_SINGLESHOT)
    {
        pcontroller = new SingleShotController;
    }

    if(name == "TFBased Controller")
    {
        pcontroller = new TFBasedController();
    }

    if(name == CONTROLLER_DERIVEDRPM)
    {
        pcontroller = new DerivedRPMController();
    }

    return pcontroller;
}

QStringList ControllerFactory::getControllerNames()
{
    QStringList list;

    list << CONTROLLER_SIMPLE << CONTROLLER_PID << CONTROLLER_SINGLESHOT << "TFBased Controller" << CONTROLLER_DERIVEDRPM;

    return list;
}
