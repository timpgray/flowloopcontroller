#ifndef CONTROLLERVIEW_H
#define CONTROLLERVIEW_H

#include <QObject>
#include <qstring.h>
#include <qhash.h>
#include <controller.h>

class ControllerView : public QObject
{
    Q_OBJECT
public:
    explicit ControllerView(QObject *parent = 0);
    virtual ~ControllerView(){}

    virtual void initialize()=0;
    virtual QString getName()=0;

signals:
    void topDriveRPMChanged(float rpm);
    void mudPumpRPMChanged(float rpm);
    void facilityOn(bool onstatus);
    void facilityQuickStop();
    void runMudPump(bool status);
    void runTopDrive(bool status);
    void controllerModeChanged(ControllerMode mode);
    void TargetToolfaceChanged(float target);
    void configure();
    void setLimits(float maxmudpumprpm, float maxtopdriverpm);
    void setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);
    void showDiagnostics();


public slots:
    virtual void initialized(QHash<QString,QString> intistatus)=0;
    virtual void newStatusUpdate(QHash<QString,QString> statusmap)=0;
    virtual void updateTopDriveTargetRPM(float rpm)=0;
    virtual void updateActualTopDriveRPM(float rpm)=0;
    virtual void updateActualMudPumpRPM(float rpm)=0;
    virtual void updateDownHoleRPM(float rpm)=0;
    virtual void updateDownHoleToolface(float toolface)=0;
    virtual void updateDownHoleValue(QString name, float toolface)=0;
    virtual void updateTopDriveTorque(float torque) = 0;

    virtual void updatePressureTop(float pressure)=0;
    virtual void updatePressureBotton(float pressure)=0;
    virtual void updatePressureDrop(float pressure)=0;
    virtual void updateFlowRate(float flowrate)=0;

    virtual void updateTopDriveStatus(bool status) = 0;
    virtual void updateMudPumpStatus(bool status) = 0;
    virtual void updateControllerInfo(QString information) = 0;
    virtual void updateLimits(float maxmudpumprpm, float maxtopdriverpm)=0;
    virtual void updateRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)=0;

    virtual void controllerInitialized(QHash<QString,QString>)=0;
    virtual void updateModeStatus(QString msg)=0;
    virtual void newWITSData(QString witsmsg, QString name, float value)=0;
    virtual void handleNewRPMCorrection(float newcorrection, bool isrunning)=0;

};

#endif // CONTROLLERVIEW_H
