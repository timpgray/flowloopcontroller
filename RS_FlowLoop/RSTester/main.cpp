#include "testmainwindow.h"
#include <QApplication>
#include <qdir.h>
#include <qdatetime.h>
#include "utils.h"
#include <qsemaphore.h>
#include <qsharedmemory.h>
#include <qmessagebox.h>

int appLogFileHandle;
QSemaphore appLock;

void MessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    if(appLogFileHandle != -1)
    {
        QString curDateTime = QDate::currentDate().toString("yyyy-MM-dd") + " " + QTime::currentTime().toString("hh:mm:ss:zzz");
        QString txt = QString("[%1] %2").arg(curDateTime).arg(context.category);

        switch (type)
        {
        case QtDebugMsg:
            txt += QString(".Debug\t\t %1").arg(msg);
            break;
        case QtWarningMsg:
            txt += QString(".Warning\t %1").arg(msg);
            break;
        case QtCriticalMsg:
            txt += QString(".Critical\t %1").arg(msg);
            break;
        case QtInfoMsg:
            txt += QString(".Info\t %1").arg(msg);
            break;
        default:
            txt += QString(".Other\t %1").arg(msg);
            break;
        case QtFatalMsg:
            txt += QString(".Fatal\t\t %1").arg(msg);
            abort();
            break;
        }


        appLock.acquire();
        Utils::writeLogFile(appLogFileHandle, txt);
        appLock.release();
    }
}

int main(int argc, char *argv[])
{

    int status = 0;
    QApplication a(argc, argv);
    QSharedMemory shared("FlowLoopControllerLock");

    if(shared.create(100))
    {
        Utils::ensureDirectoriesExist();

        if(Utils::appShouldLog())
        {
            qInstallMessageHandler(MessageHandler);

            QString writableDir = Utils::getAppLogDir();
            QDir logDirectory(writableDir);

            if (!logDirectory.exists())
            {
                QDir().mkpath(writableDir);
            }

            QString logFileName = writableDir + "/rotoslide.log";

            appLogFileHandle = Utils::createLogFile(logFileName);
            appLock.release();
        }

        TestMainWindow w;
        w.show();

        status = a.exec();
    }
    else
    {
        QMessageBox mb;

        mb.setWindowTitle("Warn");
        mb.setText("Application already running");
        mb.exec();
    }

    if(appLogFileHandle != -1)
    {
        Utils::closeFile(appLogFileHandle);
    }
    return status;
}
