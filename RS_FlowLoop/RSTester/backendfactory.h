#ifndef BACKENDFACTORY_H
#define BACKENDFACTORY_H

#include <qstring.h>
#include <qstringlist.h>
#include "backend.h"

class BackEndFactory
{
private:
    BackEndFactory();

public:
    static QStringList listBackEnds();
    static BackEnd* getBackEnd(QString name);
};

#endif // BACKENDFACTORY_H
