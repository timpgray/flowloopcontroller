#ifndef UTILS_H
#define UTILS_H

#include <qlist.h>
#include <qstring.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qhash.h>
#include "controller.h"

class LogFile
{
public:
    LogFile(QFile* pfile);
    virtual ~LogFile();
    void write(QString entry);
    void close();

protected:
    QFile* m_file;
    QTextStream* m_textstr;
};

class Utils
{
private:
    Utils();

public:

    static float calcStandardDev(QList<float>* list);
    static float calcAverage(QList<float>* list);
    static bool writeFile(QString filename, QList<QString> data);
    static int createLogFile(QString filename, bool decoratewithtimestamp=false);
    static bool writeLogFile(int key, QString data);
    static bool closeFile(int key);


    static QHash<QString,QString> loadPropsFile(QString filepath, QString separator=",");
    static bool writePropsFile(QString filename, QHash<QString,QString> props, QString separator=",");
    static QHash<QString,QString> loadConfigFile(){return loadPropsFile("rotoslide.cfg");}
    static bool writeConfigFile(QHash<QString,QString> props){return writePropsFile("rotoslide.cfg", props);}
    static bool updateConfigFile(QString propname, QString value);
    static bool appShouldLog();

    static QString getControllerConfigPath(){return m_controllerConfigPath;}
    static QString getControllerLogPath(){return m_controllerLogPath;}
    static QString getDataLogPath(){return m_dataLogPath;}
    static QString getAppLogDir(){return m_appLogDir;}
    static void ensureDirectoriesExist();

    static QString getModeDesc(ControllerMode mode);


protected:
    static QHash<int,LogFile*> m_fileCache;
    static int m_nextKeyValue;

    static QString m_controllerConfigPath;
    static QString m_controllerLogPath;
    static QString m_dataLogPath;
    static QString m_appLogDir;
    static bool m_decorateLogFilesWithDate;
    static QString m_logFileDateFormat;

};

#endif // UTILS_H
