#include "testmainwindow.h"
#include "ui_testmainwindow.h"
#include "rs_constants.h"
#include "backendfactory.h"
#include "controllerviewfactory.h"
#include "controllerfactory.h"
#include "numberinput.h"
#include "utils.h"
#include "graphparser.h"

#include <qicon.h>
#include <qmessagebox.h>


#include <qloggingcategory.h>

QLoggingCategory MAINWINDOW("TestMainWindow");


TestMainWindow::TestMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestMainWindow)
{
    QString windowtitle = QString("RotoSlide Controller - ") + QString(VERSIONSTRING);

    ui->setupUi(this);
    ui->mudpumpindidicator->setColor(QColor(255,0,0));
    ui->toolindicator->setColor(QColor(255,0,0));
    ui->topdriveindicator->setColor(QColor(255,0,0));
    ui->battwarn->setColor(QColor(255,0,0));
    ui->controlgranted->setColor(QColor(255,0,0));
    ui->rpmnull->setColor(QColor(128,128,128));
    ui->steering->setColor(QColor(128,128,128));
    ui->mudpumpindidicator->setDiameter(2);
    ui->toolindicator->setDiameter(2);
    ui->topdriveindicator->setDiameter(2);
    ui->battwarn->setDiameter(2);
    ui->controlgranted->setDiameter(2);
    ui->rpmnull->setDiameter(2);
    ui->steering->setDiameter(2);

    ui->showdiagnostics->setVisible(false);

    ui->runpumps->setEnabled(false);

    m_currentBackend = nullptr;
    m_pControllerView = nullptr;
    m_pController = nullptr;

    initialize();

    setWindowIcon(QIcon(":/resources/PcFlowLoop_Icon.ico"));
    setWindowTitle(windowtitle);

//    GraphParser::parseConfig("C:/ROTOSLIDE/charttest.crt", "", nullptr);
}

TestMainWindow::~TestMainWindow()
{
    delete ui;

    if(m_currentBackend != nullptr)
    {
        delete m_currentBackend;
    }

    if(m_pController != nullptr)
    {
        delete m_pController;
    }

    qCDebug(MAINWINDOW) << "leaving MainWindow destructor";
}


void TestMainWindow::initialize()
{
    QHash<QString,QString> props = Utils::loadConfigFile();

    m_pControllerView = ControllerViewFactory::getControllerView(ui);

    if(m_pControllerView != nullptr)
    {
        m_pControllerView->initialize();
        m_connMgr.addControllerView(m_pControllerView);
    }


    ui->backendsource->addItem(TEST_BACKEND);
    ui->backendsource->addItem(LEGACY_BACKEND);
    ui->backendsource->addItem(CURRENT_BACKEND);
    ui->backendsource->setCurrentIndex(1);

    QStringList controllernames = ControllerFactory::getControllerNames();

    ui->controllerselection->blockSignals(true);

    for(int i=0;i<controllernames.size();i++)
    {
        ui->controllerselection->addItem(controllernames.at(i));
    }

    ui->controllerselection->setCurrentIndex(-1);
    ui->controllerselection->blockSignals(false);

    if(!controllernames.isEmpty())
    {
        if(props.contains(CURRENTCONTROLLER))
        {
            ui->controllerselection->setCurrentIndex(controllernames.indexOf(props.value(CURRENTCONTROLLER)));
        }
        else
        {
            ui->controllerselection->setCurrentIndex(0);
        }
    }


    if(m_pController != nullptr)
    {
        QHash<QString,QString> props;

        m_pController->initialize(props);
        qCDebug(MAINWINDOW) << "defaulting with controller named " << m_pController->getName();
    }
    else
    {
        qCDebug(MAINWINDOW) << "no controller loaded by default";
    }

    ui->maxmudpumprpm->setText("400");
    ui->maxtopdriverpm->setText("120");


    if(props.contains(LEGACY_TOOL_TIMERINTERVAL))
    {
        QString sampleinterval = props.value(LEGACY_TOOL_TIMERINTERVAL);
        int sampleintervalvalue = sampleinterval.toInt();

        if(sampleintervalvalue >= 1000)
        {
            sampleintervalvalue /=1000;
        }

        ui->toolsamplerate->setText(QString::number(sampleintervalvalue));
        m_connMgr.toolSampleIntervalChanged(sampleintervalvalue * 1000);
    }


    if(props.contains(CURRENTPROPOGATIONDELAY))
    {
        QString delay = props.value(CURRENTPROPOGATIONDELAY);

        ui->propogationdelay->setText(delay);
        m_connMgr.toolPropogationDelayChanged(delay.toInt());
    }
    else
    {
        ui->propogationdelay->setText("0");
    }


    if(props.contains(CURRENTTOPDRIVERPM))
    {
        QString rpm = props.value(CURRENTTOPDRIVERPM);

        ui->reftopdriverpm->setText(rpm);
        m_connMgr.topDriveRpmChanged(rpm.toFloat());
    }

    if(props.contains(CURRENTMUDPUMPRPM))
    {
        QString rpm = props.value(CURRENTMUDPUMPRPM);

        ui->refpumprpm->setText(rpm);
        m_connMgr.mudPumpRpmChanged(rpm.toFloat());
    }

    if(props.contains(CURRENTTOOLFACETARGET))
    {
        QString toolface = props.value(CURRENTTOOLFACETARGET);

        ui->controllertoolfaceref->setText(toolface);
        ui->compass->setTarget(toolface.toFloat());
        m_connMgr.targetToolFaceChanged(toolface.toFloat());
    }

    float drillaheadoffset = DEFAULT_DRILLAHEADOFFSET;

    if(props.contains(CONTROLLER_DRILLAHEADOFFSET))
    {
        drillaheadoffset = props.value(CONTROLLER_DRILLAHEADOFFSET).toFloat();
    }

    ui->drillaheadoffset->setText(QString::number(drillaheadoffset, 'f', 0));

}



void TestMainWindow::swapBackend(BackEnd* pnewbackend)
{
    m_connMgr.removeBackEnd(m_currentBackend);

    if(m_currentBackend != nullptr)
    {

        qCDebug(MAINWINDOW) << "deleting current backend named " << m_currentBackend->getName();

        delete m_currentBackend;
        m_currentBackend = nullptr;
    }

    m_currentBackend = pnewbackend;

    m_connMgr.addBackEnd(m_currentBackend);
    if(m_currentBackend != nullptr)
    {
        qCDebug(MAINWINDOW) << "initializing new backend named " << m_currentBackend->getName();
        connect(m_currentBackend, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SLOT(newStatusUpdate(QHash<QString,QString>)));

        QStringList toolinterfaces = m_currentBackend->getToolInterfaces();


        Tool* ptool = m_currentBackend->getTool();
        QString toolname = ptool != nullptr ? ptool->getName():"";

        ui->tooldatasource->blockSignals(true);     // no selection events in this process - block signals
        ui->tooldatasource->clear();

        for(int i=0;i<toolinterfaces.size();i++)
        {
            ui->tooldatasource->addItem(toolinterfaces.at(i));
        }

        ui->tooldatasource->setCurrentText(toolname);
        ui->tooldatasource->blockSignals(false);    // unblock signals


        QHash<QString,QString> props = Utils::loadConfigFile();

        m_currentBackend->initialize(props);
    }
    else
    {
        qCWarning(MAINWINDOW) << "specified new backend was null ";
    }

    if(m_currentBackend != nullptr)
    {
        m_currentBackend->setRPMLimits(120, 400);
    }
}

void TestMainWindow::swapController(Controller* pnewcontroller)
{
    m_connMgr.removeController(m_pController);

    if(m_pController != nullptr)
    {

        delete m_pController;
        m_pController = nullptr;
    }

    m_pController = pnewcontroller;
    m_connMgr.addController(m_pController);

    if(m_pController != nullptr)
    {
        m_pController->initialize(Utils::loadConfigFile());
        ui->controllerdesc->setText(QString("Using ") + m_pController->getName());

        connect(m_pController, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SLOT(newStatusUpdate(QHash<QString,QString>)));
    }
}



void TestMainWindow::newStatusUpdate(QHash<QString,QString> statusmap)
{
    qCDebug(MAINWINDOW) << "inside status updater with this status map " << statusmap;

    if(statusmap.contains(MUDPUMP_STATUS))
    {
        QString statusvalue = statusmap.value(MUDPUMP_STATUS);
        bool isup = statusvalue == STATUS_TRUE ?true:false;
        QColor newcolor = isup ? QColor(0,255,0):QColor(255,0,0);

        newcolor = (statusvalue == STATUS_WARN) ? QColor(255,255,0):newcolor;
        ui->mudpumpindidicator->setColor(newcolor);
    }

    if(statusmap.contains(TOOL_STATUS))
    {
        QString statusvalue = statusmap.value(TOOL_STATUS);
        bool isup = statusvalue == STATUS_TRUE ?true:false;
        QColor newcolor = isup ? QColor(0,255,0):QColor(255,0,0);

        newcolor = (statusvalue == STATUS_WARN) ? QColor(255,255,0):newcolor;
        ui->toolindicator->setColor(newcolor);
    }

    if(statusmap.contains(TOPDRIVE_STATUS))
    {
        QString statusvalue = statusmap.value(TOPDRIVE_STATUS);
        bool isup = statusvalue == STATUS_TRUE ?true:false;
        QColor newcolor = isup ? QColor(0,255,0):QColor(255,0,0);

        newcolor = (statusvalue == STATUS_WARN) ? QColor(255,255,0):newcolor;
        ui->topdriveindicator->setColor(newcolor);
    }

    if(statusmap.contains(BATTERY_STATUS))
    {
        QString statusvalue = statusmap.value(BATTERY_STATUS);
        bool isup = statusvalue == STATUS_TRUE ?true:false;
        QColor newcolor = isup ? QColor(0,255,0):QColor(255,0,0);

        newcolor = (statusvalue == STATUS_WARN) ? QColor(255,255,0):newcolor;

        ui->battwarn->setColor(newcolor);
    }

    if(statusmap.contains(CONTROL_GRANTED))
    {
        QString statusvalue = statusmap.value(CONTROL_GRANTED);
        bool isup = statusvalue == STATUS_TRUE ?true:false;
        QColor newcolor = isup ? QColor(0,255,0):QColor(255,0,0);

        ui->controlgranted->setColor(newcolor);
    }

    if(statusmap.contains(CONTROLLERMODE) && statusmap.contains(CONTROLLERRANGE))
    {
        QString mode = statusmap.value(CONTROLLERMODE);
        QString range = statusmap.value(CONTROLLERRANGE);

        if(mode == CONTROLLERMODE_RPMNULL)
        {
            QColor color = range == CONTROLLERINRANGE ? QColor(255,255,0):QColor(255,0,0);

            color = range == CONTROLLERTRACKING ? QColor(0,255,0):color;

            ui->steering->setColor(QColor(255,0,0));
            ui->rpmnull->setColor(color);
            ui->steering->setColor(QColor(128,128,128));

        }

        if(mode == CONTROLLERMODE_STEERING)
        {
            QColor color = range == CONTROLLERINRANGE ? QColor(255,255,0):QColor(255,0,0);

            color = range == CONTROLLERTRACKING ? QColor(0,255,0):color;

            ui->steering->setColor(color);
            ui->rpmnull->setColor(QColor(0,255,0));
        }
    }



    if(statusmap.contains(OVERRUNEVENTS) && statusmap.contains(BADCRCS))
    {
        float overruns = statusmap.value(OVERRUNEVENTS).toFloat();
        float crcs = statusmap.value(BADCRCS).toFloat();

        if(m_pControllerView != nullptr)
        {
            m_pControllerView->updateDownHoleValue("Bad CRCs", crcs);
            m_pControllerView->updateDownHoleValue("Overruns", overruns);
        }
    }
}




void TestMainWindow::on_tooldatasource_currentIndexChanged(const QString &arg1)
{
    Tool* ptool = m_currentBackend->getTool();
    QHash<QString,QString> props = Utils::loadConfigFile();

    m_connMgr.removeTool(ptool);
    ptool = m_currentBackend->getTool(arg1);
    m_connMgr.addTool(ptool);
    ptool->initialize(props);
}

void TestMainWindow::on_backendsource_currentIndexChanged(const QString &arg1)
{
    BackEnd* pbackend = BackEndFactory::getBackEnd(arg1);

    qCDebug(MAINWINDOW) << "swapping backend new backend is named " << arg1;

    swapBackend(pbackend);

}

void TestMainWindow::on_controllerselection_currentIndexChanged(const QString &arg1)
{
    Controller* pcontroller = ControllerFactory::getController(arg1);

    qCDebug(MAINWINDOW) << "swapping controller new controller is  named " << arg1;
    swapController(pcontroller);
    Utils::updateConfigFile(CURRENTCONTROLLER, arg1);
}


// should probably moved to the controller view...
void TestMainWindow::on_setpumpspeed_clicked()
{
    QString rpm = ui->refpumprpm->text();
    float value = rpm.toFloat();
    NumberInput dlg(this);

    dlg.setLabel("Mud Pump RPM");
    dlg.setValue(value);

    if(dlg.exec() == QDialog::Accepted)
    {
        float newvalue = dlg.getValue();
        float maxpumpspeed = ui->maxmudpumprpm->text().toFloat();

        if(newvalue > maxpumpspeed)
        {
            QString errormsg = "Max Mud Pump RPM is " + QString::number(maxpumpspeed);
            QMessageBox::about(this, QString("Mud Pump Max Limit Exceeded"), errormsg);

            newvalue = maxpumpspeed;
        }

        QString newvaluestr = QString::number(newvalue, 'f', 2);

        ui->refpumprpm->setText(newvaluestr);

        m_connMgr.mudPumpRpmChanged(newvalue);
        Utils::updateConfigFile(CURRENTMUDPUMPRPM, newvaluestr);
    }
}

// should probably moved to the controller view...
void TestMainWindow::on_settopdrivespeed_clicked()
{
    QString rpm = ui->reftopdriverpm->text();
    float value = rpm.toFloat();
    NumberInput dlg(this);

    dlg.setLabel("Top Drive RPM");
    dlg.setValue(value);

    if(dlg.exec() == QDialog::Accepted)
    {
        float newvalue = dlg.getValue();
        float maxtopdrivespeed = ui->maxtopdriverpm->text().toFloat();

        if(newvalue > maxtopdrivespeed)
        {
            QString errormsg = "Max Top Drive RPM is " + QString::number(maxtopdrivespeed);
            QMessageBox::about(this, QString("Top Drive Max Limit Exceeded"), errormsg);

            newvalue = maxtopdrivespeed;
        }

        QString newvaluestr = QString::number(newvalue, 'f', 2);

        ui->reftopdriverpm->setText(newvaluestr);
        ui->controllertopdriverpmref->setText(newvaluestr);

        m_connMgr.topDriveRpmChanged(newvalue);
        Utils::updateConfigFile(CURRENTTOPDRIVERPM, newvaluestr);
    }
}

void TestMainWindow::on_autodrive_on_clicked()
{
    if(ui->autodrive_on->text() == "OFF")
    {
        ui->rpmnull->setColor(QColor(128,128,128));
        ui->steering->setColor(QColor(128,128,128));
    }
}

void TestMainWindow::on_autodrive_makeconnect_clicked()
{

}

void TestMainWindow::on_autodrive_drillahead_clicked()
{

}

void TestMainWindow::on_autodrive_steer_clicked()
{

}

void TestMainWindow::on_autodrive_settoolface_clicked()
{
    QString currenttf = ui->controllertoolfaceref->text();
    float value = currenttf.toFloat();
    NumberInput dlg(this);

    dlg.setLabel("TF Target");
    dlg.setValue(value);

    if(dlg.exec() == QDialog::Accepted)
    {
        float newvalue = dlg.getValue();
        QString newvaluestr = QString::number(newvalue, 'f', 1);

        ui->controllertoolfaceref->setText(newvaluestr);
        ui->compass->setTarget(newvalue);

        m_connMgr.targetToolFaceChanged(newvalue);
        Utils::updateConfigFile(CURRENTTOOLFACETARGET, newvaluestr);
    }
}

void TestMainWindow::on_showdiagnostics_clicked()
{
    if(m_currentBackend != nullptr)
    {
        m_currentBackend->showDiagnostics();
    }
}

void TestMainWindow::on_maxmudpumprpm_editingFinished()
{
    QString text = ui->maxmudpumprpm->text();
    float maxrpm = text.toFloat();

    if((m_currentBackend != nullptr) && (m_currentBackend->getMudPump() != nullptr))
    {
        m_currentBackend->getMudPump()->setMaxRPM(maxrpm);
    }


}

void TestMainWindow::on_maxtopdriverpm_editingFinished()
{
    QString text = ui->maxtopdriverpm->text();
    float maxrpm = text.toFloat();

    if((m_currentBackend != nullptr) && (m_currentBackend->getTopDrive() != nullptr))
    {
        m_currentBackend->getTopDrive()->setMaxRPM(maxrpm);
    }
}

void TestMainWindow::on_toolsamplerate_editingFinished()
{
    QString text = ui->toolsamplerate->text();
    int seconds = text.toInt();

    m_connMgr.toolSampleIntervalChanged(seconds * 1000);
    Utils::updateConfigFile(LEGACY_TOOL_TIMERINTERVAL, text);
}

void TestMainWindow::on_propogationdelay_editingFinished()
{
    QString text = ui->propogationdelay->text();
    int millis = text.toInt();

    m_connMgr.toolPropogationDelayChanged(millis);
    Utils::updateConfigFile(CURRENTPROPOGATIONDELAY, text);
}

void TestMainWindow::on_clearwitsdata_clicked()
{
    ui->tableWidget->setRowCount(0);
}

void TestMainWindow::on_drillaheadoffset_editingFinished()
{
    QString text = ui->drillaheadoffset->text();
    float drillaheadoffset = text.toFloat();

    if(m_pController != nullptr)
    {
        m_pController->updateDrillAheadOffset(drillaheadoffset);
    }

    Utils::updateConfigFile(CONTROLLER_DRILLAHEADOFFSET, text);
}

void TestMainWindow::on_exit_clicked()
{
    close();
}
