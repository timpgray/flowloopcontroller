#include "tool.h"

Tool::Tool(QObject *parent) : BaseInitializer(parent)
{
    m_sampleRateMillis = 1000;
    m_timerID = -1;
}


void Tool::setToolSampleRateMillis(int samplerate)
{
    killTimer(m_timerID);
    m_sampleRateMillis = samplerate;
    m_timerID = startTimer(m_sampleRateMillis);
}
