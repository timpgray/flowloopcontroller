#ifndef TOOL_H
#define TOOL_H

#include <QObject>
#include "baseinitializer.h"

class Tool : public BaseInitializer
{
    Q_OBJECT
public:
    explicit Tool(QObject *parent = 0);
    virtual ~Tool(){}

    virtual void setToolSampleRateMillis(int samplerate);
    virtual void setToolPropogationDelayMillis(int delay){Q_UNUSED(delay);}

signals:
    void newTelemetryValue(QString name, float value);
    void newToolfaceValue(float toolface);
    void newRPMValue(float rpm);
    void newWITSMsg(QString witsmsg, QString name, float value);
    void newToolfaceValueFullRate(float toolface);

public slots:

protected:
    int m_sampleRateMillis;
    int m_timerID;
};

#endif // TOOL_H
