#ifndef SIMPLEROSE_H
#define SIMPLEROSE_H

#include <QWidget>
#include <qqueue.h>
#include <qpainter.h>
#include <qrect.h>
#include <qpushbutton.h>

class SimpleRose : public QWidget
{
    Q_OBJECT
public:
    explicit SimpleRose(QWidget *parent = 0);

    void addToolFaceValue(float value);
    void drawTick(float angle, QPainter* p, bool inside=false);
    void drawReferenceLine(float angle, QPainter* p);
    void setTarget(float angle){m_targetAngle = angle;}
    void drawUnits(QPainter* p);
    QRect constrainGeometry();

signals:

public slots:
    void switchToMag();
    void clear();


protected:
    void paintEvent(QPaintEvent* event);
    virtual void resizeEvent(QResizeEvent *event);

protected:
    QQueue<float> m_history;
    float m_targetAngle;
    QPushButton* m_switchButton;
    QPushButton* m_clearButton;
    QList<QString> m_unitsList;
};

#endif // SIMPLEROSE_H
