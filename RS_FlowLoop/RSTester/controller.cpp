#include "controller.h"
#include "utils.h"

Controller::Controller(QObject *parent) : BaseInitializer(parent)
{

}


bool Controller::writeConfig(QString currentconfigkey, QString configlistkey)
{
    QHash<QString,QString> props = Utils::loadConfigFile();
    QString filestring = m_configFiles.join("#");

    props.insert(configlistkey, filestring);
    props.insert(currentconfigkey, m_lastconfig);

    return Utils::writeConfigFile(props);
}
