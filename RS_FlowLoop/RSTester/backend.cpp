#include "backend.h"

BackEnd::BackEnd(QObject *parent) : BaseInitializer(parent)
{
    m_pTopDrive = nullptr;
    m_pTool = nullptr;
    m_pMudPump = nullptr;
}

void BackEnd::setRPMLimits(float topdrivemax, float mudpumpmax)
{
    if(m_pTopDrive != nullptr)
    {
        m_pTopDrive->setMaxRPM(topdrivemax);
    }

    if(m_pMudPump != nullptr)
    {
        m_pMudPump->setMaxRPM(mudpumpmax);
    }
}

void BackEnd::setToolSampleRateMillis(int millis)
{
    if(m_pTool != nullptr)
    {
        m_pTool->setToolSampleRateMillis(millis);
    }
}


void BackEnd::setToolPropogationDelayMillis(int millis)
{
    if(m_pTool != nullptr)
    {
        m_pTool->setToolPropogationDelayMillis(millis);
    }
}

BackEnd::~BackEnd()
{
    if(m_pTopDrive != nullptr)
    {
        delete m_pTopDrive;
    }

    if(m_pTool != nullptr)
    {
        delete m_pTool;
    }

    if(m_pMudPump != nullptr)
    {
        delete m_pMudPump;
    }

}
