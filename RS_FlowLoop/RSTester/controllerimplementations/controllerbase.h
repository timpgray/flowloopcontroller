#ifndef CONTROLLERBASE_H
#define CONTROLLERBASE_H

#include "controller.h"

class ControllerBase : public Controller
{
    Q_OBJECT

public:
    ControllerBase(QObject *parent = 0);

    virtual void initialize(QHash<QString,QString> props);

protected:
    virtual void handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap=nullptr)=0;
    virtual void handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap=nullptr)=0;
    virtual void populateLogDataMap(QHash<QString,QString>* pmap=nullptr)=0;
    virtual void handleToolFaceUpdate(QHash<QString,float>* valuemap);

    virtual void checkUpdateTimeAgainstCache();
    virtual void handleBulkDrillAheadAdjustment()=0;
    virtual float calculateTFError(float currenttoolface);


public slots:
    virtual void updateTopDriveRPM(float rpm);
    virtual void updateToolFace(float toolface);
    virtual void updateControllerMode(ControllerMode mode);
    virtual void setTargetToolface(float target);
    virtual void updateDownholeRPM(float rpm);
    virtual void updateDrillAheadOffset(float newoffset);

protected:
    float m_topDriveRPM;
    float m_downHoleRPM;
    float m_toolFace;
    float m_targetToolFace;
    ControllerMode m_currentMode;

    bool m_steeringStarted;
    bool m_drillAheadStarted;
    float m_drillAheadOffset;
    float m_lastSteeringRPM;

    QHash<QString,float> m_cachedToolValues;
    unsigned int m_maxCacheAgeMillis;
    unsigned int m_lastUpdate;

};

#endif // CONTROLLERBASE_H
