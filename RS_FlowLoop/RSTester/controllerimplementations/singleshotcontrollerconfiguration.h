#ifndef SINGLESHOTCONTROLLERCONFIGURATION_H
#define SINGLESHOTCONTROLLERCONFIGURATION_H

#include <QDialog>
#include "basecontrollerconfig.h"

namespace Ui {
class SingleShotControllerConfiguration;
}

class SingleShotControllerConfiguration : public BaseControllerConfig
{
    Q_OBJECT

public:
    explicit SingleShotControllerConfiguration(QWidget *parent = 0);
    ~SingleShotControllerConfiguration();

    virtual QComboBox* getDropdown();
    virtual void propertiesLoaded(QHash<QString,QString> props);
    virtual QHash<QString,QString> getCurrentProperties();
    virtual QPushButton* getRefreshButton();

    void setControllerConfig(float rpmscaling, float tfscaling, float rpmscalingsteering, int pulsewidth, float steeringthresh);
    void setLoEndScalingConfig(bool uselowendscaling, bool uselinearscaling, float lowendthreshold, float maxloendscaling);
    float getRPMScaling(){return m_rpmScaling;}
    float getTFScaling(){return m_tfScaling;}
    float getRPMScalingSteering(){return m_rpmSteeringScaling;}
    float getPulseWidth(){return m_pulseWidth;}
    float getSteeringThreshold(){return m_steeringThreshold;}

    bool getUseLoEndScaling(){return m_useLoEndScaling;}
    bool getUseLinearScaling(){return m_useLinearScaling;}
    float getLoEndScalingThreshold(){return m_tfErrorThreshold;}
    float getMaxLoEndScaling(){return m_maxLoEndScalePercent;}

    virtual int exec();



private slots:
    void on_rpmscaling_editingFinished();

    void on_tfscaling_editingFinished();

    void on_rpmscaling_steering_editingFinished();

    void on_pulseduration_editingFinished();

    void on_steeringthreshold_editingFinished();

    void on_lowendthreshold_editingFinished();

    void on_maxadjustment_editingFinished();

    void on_uselowendadjustment_clicked(bool checked);

    void on_uselinearscaling_clicked(bool checked);

private:
    Ui::SingleShotControllerConfiguration *ui;

    float m_rpmScaling;
    float m_tfScaling;
    float m_rpmSteeringScaling;
    int m_pulseWidth;
    float m_steeringThreshold;

    bool m_useLoEndScaling;
    bool m_useLinearScaling;
    float m_tfErrorThreshold;
    float m_maxLoEndScalePercent;

};

#endif // SINGLESHOTCONTROLLERCONFIGURATION_H
