#include "singleshotcontroller.h"
#include "singleshotcontrollerconfiguration.h"
#include "rs_constants.h"
#include "utils.h"

#include <qdatetime.h>
#include <qloggingcategory.h>

QLoggingCategory SINGLESHOTCONTROLLER("SingleShotController");


SingleShotController::SingleShotController(QObject *parent) : ControllerBase(parent)
{
    m_rpmScaling = 0.5;
    m_tfScaling = 1.0;
    m_rpmSteeringScaling = 1.0;
    m_pulseWidth = 1000;
    m_steeringThreshold = 4.0;
    m_currentSteeringThreshold = m_steeringThreshold;

}


QString SingleShotController::getDescription()
{
    return "Single Shot Controller";
}

QString SingleShotController::getLogFileName()
{
    return "singleshotcontroller.csv";
}

QString SingleShotController::getName()
{
    return CONTROLLER_SINGLESHOT;
}

void SingleShotController::initialize(QHash<QString,QString> props)
{

    QHash<QString,QString> initprops;
    QString header = "unix_time,time,target Toolface,topdrive RPM,downhole RPM,toolface,Mode,RPM Error Scaling,TF Error Scaling,RPM Error Steering Scaling,Pulse Width,Steering Threshold(RPM),RPM Error,ToolFace Error,Single Shot,Correction Mode,RPM Correction,New RPM,RPM Null Value,Steering Value,Hold Value";
    QHash<QString,QString> loggingprops;

    loggingprops.insert(HEADERSTRING, header);
    loggingprops.insert(CONTROLLER_STARTLOG, STATUS_TRUE);
    emit updateLogInfo(loggingprops);


    m_currentMode = OFF;

    initprops.insert(SHOWDIAGNOSTICS, STATUS_FALSE);
    emit initialized(initprops);
    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

    if(props.contains(SINGLESHOTCONTROLLER_LAST_CONFIG))
    {
        QString path = props.value(SINGLESHOTCONTROLLER_LAST_CONFIG);

        m_lastconfig = path;

        loadProps(path);
    }

    if(props.contains(SINGLESHOTCONTROLLER_FILES))
    {
        QString filelist = props.value(SINGLESHOTCONTROLLER_FILES);
        m_configFiles = filelist.split("#");
    }

    ControllerBase::initialize(props);
}

bool SingleShotController::loadProps(QString path)
{
    bool status = false;
    QHash<QString,QString> props = Utils::loadPropsFile(path);

    if(props.contains(RPM_SCALING) && props.contains(TF_ANGLE_SCALING) &&
            props.contains(SINGLESHOTPULSEWIDTH) && props.contains(RPM_STEERING_SCALING) &&
            props.contains(STEERINGTHRESHOLD_RPM))
    {
        m_rpmScaling = props.value(RPM_SCALING).toFloat();
        m_tfScaling = props.value(TF_ANGLE_SCALING).toFloat();
        m_rpmSteeringScaling = props.value(RPM_STEERING_SCALING).toFloat();
        m_pulseWidth = props.value(SINGLESHOTPULSEWIDTH).toInt();
        m_steeringThreshold = props.value(STEERINGTHRESHOLD_RPM).toFloat();
        status = true;
    }

    if(props.contains(SINGLESHOT_USE_LOWEND_SCALING) && props.contains(SINGLESHOT_USE_LINEAR_SCALING) &&
            props.contains(SINGLESHOT_LOWEND_THRESHOLD) && props.contains(SINGLESHOT_MAX_LOWEND_ADJUSTMENT))
    {
        m_useLoEndScaling = props.value(SINGLESHOT_USE_LOWEND_SCALING) == "true" ? true:false;
        m_tfErrorThreshold = props.value(SINGLESHOT_LOWEND_THRESHOLD).toFloat();
        m_useLinearScaling = props.value(SINGLESHOT_USE_LINEAR_SCALING) == "true" ? true:false;;
        m_maxLoEndScalePercent = props.value(SINGLESHOT_MAX_LOWEND_ADJUSTMENT).toFloat();
    }
    else
    {
        m_useLoEndScaling = true;
        m_tfErrorThreshold = 10.0;
        m_useLinearScaling = true;
        m_maxLoEndScalePercent = 2.0;
    }


    return status;
}


void SingleShotController::handleSteering(float tferror, float rpmerror,  bool steeringstarted, QHash<QString,QString>* pmap)
{

    if(pmap != nullptr)
    {
        pmap->insert("RPM Error", QString::number(rpmerror, 'f', 3));
        pmap->insert("ToolFace Error", QString::number(tferror, 'f', 2));
    }

    float rpmlimitederror = fabs(rpmerror) > 10 ? 10:fabs(rpmerror);
    float tfscaleA = log2(1 + (fabs(tferror)/90));
    float rpmscaleA = log2(1 + (rpmlimitederror/10));
    float scale = 1.0 * ((fabs(rpmerror) > 10.0)?10:fabs(rpmerror))/10.0;
    float tfscale = 1.0 * ((fabs(tferror) > 10.0) ? 10:fabs(tferror))/10;
    float tfcorrection = ((tferror * m_tfScaling) + (rpmerror * m_rpmSteeringScaling)) * tfscaleA;
    float rpmcorrection = rpmerror * m_rpmScaling/* * rpmscaleA*/;
    float newrpmnull = m_topDriveRPM - rpmcorrection;
    float newrpmsteering = m_topDriveRPM - tfcorrection;
    float combinedcorrection = m_topDriveRPM - (tfcorrection + rpmcorrection);
    // .1 rpm = .6 degrees/second
    float anglecorrection = (tferror/5.0) * 60.0;   // make the correction in 5 seconds
    float steeringcorrection = m_topDriveRPM - (anglecorrection * m_tfScaling);

    float newrpm = fabs(rpmerror) > m_steeringThreshold ? newrpmnull:steeringcorrection;

    QHash<QString,QString> controlstatus;

    if(fabs(rpmerror) > m_currentSteeringThreshold)
    {
        newrpm = newrpmnull;
        m_currentSteeringThreshold = m_steeringThreshold;

        QString error = fabs(tferror) < 20 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

        error = fabs(tferror) < 10 ? CONTROLLERTRACKING:error;

        controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_RPMNULL);
        controlstatus.insert(CONTROLLERRANGE, error);

        if(pmap != nullptr)
        {
            pmap->insert("Correction Mode", CONTROLLERMODE_RPMNULL);
            pmap->insert("RPM Correction", QString::number(rpmcorrection, 'f', 3));
        }

    }
    else
    {
        float correction = 0;

        if(((tfcorrection < 0) && (rpmcorrection < 0)) || ((tfcorrection > 0) && (rpmcorrection > 0)))
        {
            correction = (tfcorrection + rpmcorrection) / 2.0;  // average these - certainly do not add them
        }
        else
        {
            newrpm = m_topDriveRPM - (rpmcorrection + tfcorrection);
            correction = rpmcorrection + tfcorrection;

            if(((tfcorrection < 0) && (rpmcorrection > 0)))
            {

            }
            else
            {

            }

        }

//        newrpm = m_topDriveRPM - (correction);
        newrpm = m_topDriveRPM - (tfcorrection);

//        newrpm = newrpmsteering;
//        m_currentSteeringThreshold = m_steeringThreshold + 8.0;

        QString error = fabs(tferror) < 20 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

        error = fabs(tferror) < 10 ? CONTROLLERTRACKING:error;

        controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_STEERING);
        controlstatus.insert(CONTROLLERRANGE, error);

        if(pmap != nullptr)
        {
            pmap->insert("Correction Mode", CONTROLLERMODE_STEERING);
            pmap->insert("RPM Correction", QString::number(tfcorrection, 'f', 3));
        }
    }

    emit statusUpdate(controlstatus);



    QHash<QString,QString> systemlogvalues;
    QString timestr = QString::number(QDateTime::currentDateTime().toTime_t());

    systemlogvalues.insert(SYSTEMLOGUNIXTIME, timestr);
    systemlogvalues.insert(CONTROLLER_CURRENTTOOLFACE, QString::number(m_toolFace, 'f', 3));
    systemlogvalues.insert(CONTROLLER_CURRENTRPM, QString::number(m_downHoleRPM, 'f', 6));
    systemlogvalues.insert(CONTROLLER_TOOLFACEERROR, QString::number(tferror, 'f', 3));
    systemlogvalues.insert(CONTROLLER_RPMERROR, QString::number(rpmerror, 'f', 6));

//    if(pmap != nullptr)
//    {
//        pmap->insert(SYSTEMLOGUNIXTIME, timestr);
//    }


    if(m_pulseWidth == 0)
    {
        emit newRPMValue(newrpm);
//        emit newRPMValue(combinedcorrection);

        if(pmap != nullptr)
        {
            pmap->insert("New RPM", QString::number(newrpm, 'f', 3));
            pmap->insert("Single Shot", "false");
        }
    }
    else
    {
        if(pmap != nullptr)
        {
            pmap->insert("Single Shot", "true");
        }

        if(fabs(rpmerror) > m_steeringThreshold)
        {
            if(steeringstarted)
            {
                newrpmnull = m_lastSteeringRPM;
            }

            emit newRPMValue(newrpmnull);
            emit updateRPMCorrection(newrpmnull - m_topDriveRPM, true);


            systemlogvalues.insert(CONTROLLER_NEWRPM, QString::number(newrpmnull, 'f', 6));
            systemlogvalues.insert(CONTROLLER_RPMDELTA, QString::number(rpmcorrection, 'f', 6));

            if(pmap != nullptr)
            {
                pmap->insert(CONTROLLER_NEWRPM, QString::number(newrpmnull, 'f', 6));
                pmap->insert("RPM Null Value", QString::number(newrpmnull));
            }
        }
        else
        {
            QHash<QString,QString> controllervalues;

            float delta = ((tferror/((float)m_pulseWidth/1000.0F)) * 60.0F)/360.0F * m_rpmSteeringScaling;
            float adjusteddelta = calcRPMAdjustment(tferror, delta);
            float deltatouse = m_useLoEndScaling ? adjusteddelta:delta;


            newrpm = m_topDriveRPM - (deltatouse);
            systemlogvalues.insert(CONTROLLER_NEWRPM, QString::number(newrpm, 'f', 6));
            systemlogvalues.insert(CONTROLLER_RPMDELTA, QString::number(deltatouse, 'f', 6));
//            controllervalues.insert(NEW_RPM_VALUE, QString::number(newrpmsteering));
            controllervalues.insert(NEW_RPM_VALUE, QString::number(newrpm));
            controllervalues.insert(RPM_HOLD_TIME, QString::number(m_pulseWidth));
            controllervalues.insert(RPM_END_VALUE, QString::number(newrpmnull));
            emit newRPMValue(controllervalues);
            emit updateRPMCorrection(newrpm - m_topDriveRPM, true);

            if(pmap != nullptr)
            {
                pmap->insert(CONTROLLER_NEWRPM, QString::number(newrpmnull, 'f', 6));
                pmap->insert("Steering Value", QString::number(newrpm));
                pmap->insert("Hold Value", QString::number(newrpmnull));
                pmap->insert("Pulse Width", QString::number(m_pulseWidth));
                pmap->insert("Use Low End Scaling", (m_useLoEndScaling? "true":"false"));
                pmap->insert("Use Linear Scaling", (m_useLinearScaling? "true":"false"));
                pmap->insert("Low End Scaling Threshold", QString::number(m_tfErrorThreshold));
                pmap->insert("Low End Max Scaling", QString::number(m_maxLoEndScalePercent));
                pmap->insert("Raw Steering Delta", QString::number(delta));
                pmap->insert("Scaled Steering Delta", QString::number(adjusteddelta));
            }
        }

    }

//    emit systemLogUpdateEntries(systemlogvalues);

}

void SingleShotController::handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap)
{
    float rpmcorrection = (rpmerror - m_drillAheadOffset) * m_rpmScaling;
    float newrpmnull = m_topDriveRPM - rpmcorrection;

//    if(drillaheadstarted)
//    {
//        newrpmnull = m_topDriveRPM + m_drillAheadOffset;    // full step initially
//    }

    if(pmap != nullptr)
    {
        pmap->insert("RPM Error", QString::number(rpmerror, 'f', 3));
        pmap->insert("Single Shot", "false");
        pmap->insert("RPM Correction", QString::number(rpmcorrection, 'f', 4));
        pmap->insert("New RPM",  QString::number(newrpmnull, 'f', 4));
        pmap->insert("RPM Null Value", QString::number(newrpmnull, 'f', 4));
    }

    emit newRPMValue(newrpmnull);
    emit updateRPMCorrection(newrpmnull - m_topDriveRPM, true);

    QHash<QString,QString> controlstatus;
    QString error = fabs(fabs(m_downHoleRPM) - 10) < 10 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

    error = fabs(fabs(m_downHoleRPM) - 10) < 5 ? CONTROLLERTRACKING:error;

    controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_RPMNULL);
    controlstatus.insert(CONTROLLERRANGE, error);
    emit statusUpdate(controlstatus);


}

void SingleShotController::populateLogDataMap(QHash<QString,QString>* pmap)
{
    if(pmap != nullptr)
    {
        pmap->insert("RPM Error Scaling", QString::number(m_rpmScaling, 'f', 3));
        pmap->insert("TF Error Scaling", QString::number(m_tfScaling, 'f', 3));
        pmap->insert("RPM Error Steering Scaling", QString::number(m_rpmSteeringScaling, 'f', 3));
        pmap->insert("Pulse Width", QString::number(m_pulseWidth));
        pmap->insert("Steering Threshold(RPM)", QString::number(m_steeringThreshold, 'f', 1));
        pmap->insert(CONTROLLER_START, "Single Shot");

    }

}

void SingleShotController::configure()
{
    SingleShotControllerConfiguration dlg;

    dlg.setConfigurations(m_configFiles, m_lastconfig);
    dlg.setControllerConfig(m_rpmScaling, m_tfScaling, m_rpmSteeringScaling, m_pulseWidth, m_steeringThreshold);
    dlg.setLoEndScalingConfig(m_useLoEndScaling, m_useLinearScaling, m_tfErrorThreshold, m_maxLoEndScalePercent);


    if(dlg.exec() == QDialog::Accepted)
    {
        m_rpmScaling = dlg.getRPMScaling();
        m_tfScaling = dlg.getTFScaling();
        m_rpmSteeringScaling = dlg.getRPMScalingSteering();
        m_pulseWidth = dlg.getPulseWidth();
        m_steeringThreshold = dlg.getSteeringThreshold();
        m_configFiles = dlg.getConfigurations();
        m_lastconfig = dlg.getCurrentConfiguration();

        m_useLoEndScaling = dlg.getUseLoEndScaling();
        m_useLinearScaling = dlg.getUseLinearScaling();
        m_tfErrorThreshold = dlg.getLoEndScalingThreshold();
        m_maxLoEndScalePercent = dlg.getMaxLoEndScaling();

        writeConfig(SINGLESHOTCONTROLLER_LAST_CONFIG, SINGLESHOTCONTROLLER_FILES);
    }
}

void SingleShotController::showDiagnostics()
{

}

QStringList SingleShotController::getSystemLogHeaders()
{
    QString headerstring = "target Toolface,topdrive RPM,downhole RPM,toolface,Mode,RPM Error Scaling,TF Error Scaling,RPM Error Steering Scaling,Pulse Width,Steering Threshold(RPM),RPM Error,ToolFace Error,Single Shot,Correction Mode,RPM Correction,New RPM,RPM Null Value,Steering Value,Hold Value,Pulse Width,Use Low End Scaling,Use Linear Scaling,Low End Scaling Threshold,Low End Max Scaling,Raw Steering Delta,Scaled Steering Delta";
    QStringList headers;
    QStringList parts = headerstring.split(',');

//    headers << SYSTEMLOGUNIXTIME << CONTROLLER_START << CONTROLLER_CURRENTTOOLFACE << CONTROLLER_CURRENTRPM << CONTROLLER_NEWRPM << CONTROLLER_RPMDELTA << CONTROLLER_TOOLFACEERROR << CONTROLLER_RPMERROR;
    headers << SYSTEMLOGUNIXTIME << CONTROLLER_START;

    for(int i=0;i<parts.size();i++)
    {
        headers << parts.at(i);
    }


    return headers;
}


void SingleShotController::handleBulkDrillAheadAdjustment()
{
    float newrpm = m_topDriveRPM;

    if(m_drillAheadStarted)
    {
        newrpm = m_topDriveRPM + m_drillAheadOffset;    // full step initially
    }

    if(m_steeringStarted)
    {
        newrpm = m_topDriveRPM - m_drillAheadOffset;    // full step initially
    }

    emit newDrillAheadAdjustment(newrpm);
}


float SingleShotController::calcRPMAdjustment(float tferror, float steeringrpmdelta)
{
    if(m_useLoEndScaling && (m_tfErrorThreshold > 0) && (fabs(tferror) < m_tfErrorThreshold))
    {
        float rangepercent = fabs(tferror)/m_tfErrorThreshold;

        qCDebug(SINGLESHOTCONTROLLER) << "tferror = " << tferror << " steering delta " << steeringrpmdelta << " rangepercent = " << rangepercent;
        rangepercent = 1.0 - rangepercent;
        qCDebug(SINGLESHOTCONTROLLER) << "adjusted rangepercent = " << rangepercent;
        qCDebug(SINGLESHOTCONTROLLER) << "scale value = " << m_maxLoEndScalePercent << " error threshold = " << m_tfErrorThreshold;

        float sign = tferror < 0 ? -1.0:1.0;
        float adjustment = m_maxLoEndScalePercent * rangepercent * steeringrpmdelta;

        qCDebug(SINGLESHOTCONTROLLER) << "scaled adjustment = " << adjustment;

        steeringrpmdelta += adjustment;

    }

    return steeringrpmdelta;

}
