#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

#include <QObject>
#include "controller.h"

class PIDController : public Controller
{
    Q_OBJECT
public:
    explicit PIDController(QObject *parent = 0);

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();
    virtual QString getDescription();
    virtual QString getLogFileName(){return "PIDControllerLog.csv";}
    virtual bool loadProps(QString path);
    virtual QStringList getSystemLogHeaders();


    virtual float calculateRPMCorrection();
    virtual float calculateTFCorrection();

    void handleSteering(QHash<QString,QString>* pmap=nullptr);
    void handleDrillAhead(QHash<QString,QString>* pmap=nullptr);


signals:

public slots:
    virtual void updateTopDriveRPM(float rpm);
    virtual void updateToolFace(float toolface);
    virtual void updateControllerMode(ControllerMode mode);
    virtual void setTargetToolface(float target);
    virtual void updateDownholeRPM(float rpm);
    virtual void configure();
    virtual void showDiagnostics();
    virtual void updateDrillAheadOffset(float newoffset);

private:
    float m_topDriveRPM;
    float m_downHoleRPM;
    float m_toolFace;
    float m_targetToolFace;
    ControllerMode m_currentMode;

    float m_rpmProportionalScale;
    float m_rpmIntegralScale;
    float m_tfProportionalScale;
    float m_tfIntegralScale;
    float m_tfDerivativeScale;
    float m_steeringThreshold;

    long long m_lastToolfaceTime;
    long long m_lastRPMTime;
    float m_toolfaceTimeDelta;
    float m_rpmTimeDelta;
    float m_currrentTFError;
    float m_prevTFError;
    float m_currentRPMError;
    float m_prevRPMError;

};

#endif // PIDCONTROLLER_H
