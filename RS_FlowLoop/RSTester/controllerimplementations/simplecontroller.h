#ifndef SIMPLECONTROLLER_H
#define SIMPLECONTROLLER_H

#include <QObject>
#include <qmap.h>
#include "controller.h"
#include "controllerbase.h"

class SimpleController : public ControllerBase
{
    Q_OBJECT
public:
    explicit SimpleController(QObject *parent = 0);

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();
    virtual QString getDescription();
    virtual QString getLogFileName(){return "SimpleControllerLog.csv";}
    virtual bool loadProps(QString path);
    virtual QStringList getSystemLogHeaders();

protected:
    virtual void handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void populateLogDataMap(QHash<QString,QString>* pmap=nullptr);
    virtual void handleBulkDrillAheadAdjustment();


private:
//    void handleSteering(QHash<QString,QString>* pmap=nullptr);
//    void handleDrillAhead(QHash<QString,QString>* pmap=nullptr);

signals:

public slots:
    virtual void configure();
    virtual void showDiagnostics();

private:

    float m_rpmScaling;
    float m_toolFaceScaling;

};

#endif // SIMPLECONTROLLER_H
