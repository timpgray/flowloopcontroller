#include "simplecontroller.h"
#include "rs_constants.h"
#include "simplecontrollerconfig.h"
#include "utils.h"
#include "rs_constants.h"
#include <qdatetime.h>

#include <qloggingcategory.h>

QLoggingCategory SIMPLECONTROLLER("SimpleController");


SimpleController::SimpleController(QObject *parent) : ControllerBase(parent)
{
    m_currentMode = OFF;
    m_rpmScaling = 0.5;
    m_toolFaceScaling = 25;

}



void SimpleController::initialize(QHash<QString,QString> props)
{
    ControllerBase::initialize(props);

    QHash<QString,QString> initprops;
    QString header = "unix_time,time,target Toolface,topdrive RPM,downhole RPM,toolface,mode,RPM Scale Factor,Toolface Scale Factor,TermTheta,TermRPM,NewRpm,ToolFace Error";
    QHash<QString,QString> loggingprops;

    loggingprops.insert(HEADERSTRING, header);
    loggingprops.insert(CONTROLLER_STARTLOG, STATUS_TRUE);
    emit updateLogInfo(loggingprops);


    m_currentMode = OFF;

    initprops.insert(SHOWDIAGNOSTICS, STATUS_TRUE);
    emit initialized(initprops);
    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

    if(props.contains(SIMPLECONTROLLER_LAST_CONFIG))
    {
        QString path = props.value(SIMPLECONTROLLER_LAST_CONFIG);

        m_lastconfig = path;

        loadProps(path);
    }

    if(props.contains(SIMPLECONTROLLER_FILES))
    {
        QString filelist = props.value(SIMPLECONTROLLER_FILES);
        m_configFiles = filelist.split("#");
    }
}

bool SimpleController::loadProps(QString path)
{
    bool status = false;
    QHash<QString,QString> configprops = Utils::loadPropsFile(path);

    if(configprops.contains(RPM_SCALING) && configprops.contains(TF_ANGLE_SCALING))
    {
        m_rpmScaling = configprops.value(RPM_SCALING).toFloat();
        m_toolFaceScaling = configprops.value(TF_ANGLE_SCALING).toFloat();
        status = true;
    }

    return status;

}

QString SimpleController::getName()
{
    return CONTROLLER_SIMPLE;
}

QString SimpleController::getDescription()
{
    return "ipsum lorum...";
}



void SimpleController::configure()
{
    SimpleControllerConfig dlg;

    dlg.setConfigurations(m_configFiles, m_lastconfig);
    dlg.setScaleFactors(m_rpmScaling, m_toolFaceScaling);

    if(dlg.exec() == QDialog::Accepted)
    {
        m_rpmScaling = dlg.getRPMScale();
        m_toolFaceScaling = dlg.getTFScale();
        m_configFiles = dlg.getConfigurations();
        m_lastconfig = dlg.getCurrentConfiguration();

        writeConfig(SIMPLECONTROLLER_LAST_CONFIG, SIMPLECONTROLLER_FILES);

        qCDebug(SIMPLECONTROLLER) << "RPM scaling = " << m_rpmScaling << " toolface scaling = " << m_toolFaceScaling;

    }
}


void SimpleController::showDiagnostics()
{
    qCDebug(SIMPLECONTROLLER) << "inside show diagnostics";
}


void SimpleController::handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString, QString> *pmap)
{
    float TermTheta = (m_toolFaceScaling/360.0) * tferror;    //vg - 9/13/16
    // TPG ?????
    float TermRpm = -((m_topDriveRPM - (m_downHoleRPM * m_rpmScaling)) );    //vg - 9/13/16
    float TermRpmS = -((m_topDriveRPM - ((m_downHoleRPM * m_rpmScaling))/100) );    //vg - 9/13/16


    float newrpm = TermTheta + TermRpm;		//vg 9/13/16  TopDriveRpm[0] = TopDriveRpm[1] +TermTheta +TermRpm;
    float newrpmS = -TermTheta + TermRpmS;
    float newrpmN = TermRpm;

    newrpm *= -1;   // this accounts for the difference in the simulation and real world
    newrpmS *= -1;
    newrpmN *= -1;

    if(pmap != nullptr)
    {
        pmap->insert("TermTheta", QString::number(TermTheta, 'f', 3));
        pmap->insert("TermRPM", QString::number(TermRpm, 'f', 3));
        pmap->insert("NewRpm", QString::number(newrpm, 'f', 2));
        pmap->insert("ToolFace Error", QString::number(tferror, 'f', 2));
    }

    qCDebug(SIMPLECONTROLLER) << "inside handleSteering top drive RPM = " << m_topDriveRPM << " downhole RPM = " << m_downHoleRPM << " theta = " << TermTheta << " term rpm = " << TermRpm << " total correction = " <<  newrpm;
    qCDebug(SIMPLECONTROLLER) << "inside handleSteering rpm corection = " << m_topDriveRPM << " downhole rpm = " << m_downHoleRPM << " toolface = " << m_toolFace << " target toolface = " << m_targetToolFace;
    qCDebug(SIMPLECONTROLLER) << "inside handleSteering rpm corection = " << TermRpm << " angle correction = " << TermTheta << " new RPM = " << newrpm;

//    if(steeringstarted)
//    {
//        newrpm = m_lastSteeringRPM;
//    }

    emit newRPMValue(newrpm);
    emit updateRPMCorrection(newrpm - m_topDriveRPM, true);
//    QHash<QString,QString> controllervalues;

//    controllervalues.insert(NEW_RPM_VALUE, QString::number(newrpm1));
//    controllervalues.insert(RPM_HOLD_TIME, QString::number(4500));
//    controllervalues.insert(RPM_END_VALUE, QString::number(TermRpm));


    QHash<QString,QString> controlstatus;

    if(fabs(m_downHoleRPM) < 4)
    {
        emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));
//        emit newRPMValue(controllervalues);
//        emit newRPMValue(newrpmS);

        QString error = fabs(tferror) < 20 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

        error = fabs(tferror) < 10 ? CONTROLLERTRACKING:error;

        controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_STEERING);
        controlstatus.insert(CONTROLLERRANGE, error);

        if(pmap != nullptr)
        {
            pmap->insert("Correction Mode", CONTROLLERMODE_STEERING);
        }
    }
    else
    {
        emit updateMode("Mode:" + Utils::getModeDesc(RPMNull));
//        emit newRPMValue(TermRpm);
//        emit newRPMValue(newrpmN);
        QString error = fabs(m_downHoleRPM) < 10 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

        controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_RPMNULL);
        controlstatus.insert(CONTROLLERRANGE, error);

        if(pmap != nullptr)
        {
            pmap->insert("Correction Mode", CONTROLLERMODE_RPMNULL);
        }
    }

    emit statusUpdate(controlstatus);
}

void SimpleController::handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString, QString> *pmap)
{
    float newrpm = -(m_topDriveRPM - (m_downHoleRPM - m_drillAheadOffset)*m_rpmScaling);
    if(pmap != nullptr)
    {
        pmap->insert("NewRpm", QString::number(newrpm, 'f', 2));
        pmap->insert("Correction Mode", CONTROLLERMODE_RPMNULL);
    }

//    if(drillaheadstarted)
//    {
//        newrpm = m_topDriveRPM + m_drillAheadOffset;    // full step initially
//        newrpm *= -1;
//    }

    qCDebug(SIMPLECONTROLLER) << "inside handleDrillAhead topdrive RPM = " << m_topDriveRPM << " downhole rpm = " << m_downHoleRPM << " new rpm = " << newrpm;
    emit newRPMValue(-newrpm);
    emit updateRPMCorrection((-newrpm) - m_topDriveRPM, true);

    QHash<QString,QString> controlstatus;
    QString error = fabs(fabs(m_downHoleRPM) - 10) < 10 ? CONTROLLERINRANGE:CONTROLLEROUTOFRANGE;

    error = fabs(fabs(m_downHoleRPM) - 10) < 5 ? CONTROLLERTRACKING:error;

    controlstatus.insert(CONTROLLERMODE, CONTROLLERMODE_RPMNULL);
    controlstatus.insert(CONTROLLERRANGE, error);
    emit statusUpdate(controlstatus);

}

void SimpleController::populateLogDataMap(QHash<QString,QString>* pmap)
{
    if(pmap != nullptr)
    {
        pmap->insert("RPM Scale Factor", QString::number(m_rpmScaling, 'f', 3));
        pmap->insert("Toolface Scale Factor", QString::number(m_toolFaceScaling, 'f', 3));
        pmap->insert("Steering Threshold(RPM)", QString::number(4.0F, 'f', 1));
        pmap->insert(CONTROLLER_START, "Simple PI");

    }
}

QStringList SimpleController::getSystemLogHeaders()
{
    QString headerstring = "target Toolface,topdrive RPM,downhole RPM,toolface,Mode,RPM Scale Factor,Toolface Scale Factor,Correction Mode,TermTheta,TermRPM,NewRpm,ToolFace Error";
    QStringList headers;

    QStringList parts = headerstring.split(',');

    headers << SYSTEMLOGUNIXTIME << CONTROLLER_START;

    for(int i=0;i<parts.size();i++)
    {
        headers << parts.at(i);
    }

    return headers;
}

void SimpleController::handleBulkDrillAheadAdjustment()
{
    float newrpm = m_topDriveRPM;

    if(m_drillAheadStarted)
    {
        newrpm = m_topDriveRPM + m_drillAheadOffset;    // full step initially
    }

    if(m_steeringStarted)
    {
        newrpm = m_topDriveRPM - m_drillAheadOffset;    // full step initially
    }

    emit newDrillAheadAdjustment(newrpm);
}
