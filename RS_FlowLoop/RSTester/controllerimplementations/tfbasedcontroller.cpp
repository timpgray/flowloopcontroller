#include "tfbasedcontroller.h"

#include <qloggingcategory.h>
#include <qdatetime.h>

#include "rs_constants.h"
#include "utils.h"
#include "tfconfiguration.h"

// these are defined in ControllerBase as well - this is a convience at the moment
#define TOOLFACEFALUE "toolfacevalue"
#define RPMVALUE "rpmvalue"


QLoggingCategory TFBASEDCONTROLLER("TFBasedController");

TFBasedController::TFBasedController(QObject *parent) : ControllerBase(parent)
{

    m_timeOffset = QDateTime::currentMSecsSinceEpoch();
    m_rpmSteeringScaling = 1.1;
    m_rpmOffset = 0;
    m_pulseWidth = 2000;
    m_useRPMNULL = 0;

    m_ignoreWindow = 3;
    m_lowendThreshold = 15.0;
    m_lowendAdjustment = 1.0;
    m_useRPMNulling = true;
    m_lastTFUpdateTime = 0;
    m_rpmAdjustScaling = 0.9;
    m_useLoEndScaling = true;
    m_useLinearScaling = true;

    m_delaySteering = false;
    m_steeringSampleDelay = 1;
    m_steeringSampleDelayCounter = 0;


}

void TFBasedController::setTargetToolface(float target)
{
    m_useRPMNULL = 3;
    ControllerBase::setTargetToolface(target);
}

void TFBasedController::initialize(QHash<QString,QString> props)
{

}

void TFBasedController::handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap)
{

    if(m_delaySteering)         // wait a couple of samples after transitioning from drill ahead...
    {
        qCDebug(TFBASEDCONTROLLER) << "Ignoring data sample after transition from drill ahead delay count = " << m_steeringSampleDelayCounter << " delay interval = " << m_steeringSampleDelay;

        m_steeringSampleDelayCounter--;

        if(m_steeringSampleDelayCounter == 0)
        {
            m_delaySteering = false;
        }

    }
    else
    {
        bool inreg = isInRegulation(tferror, pmap);

        if(pmap != nullptr)
        {
            pmap->insert("Calculated RPM", QString::number(m_calculatedRPM, 'f', 3));
        }

        QHash<QString,QString> controllervalues;

        float delta = ((tferror/((float)m_pulseWidth/1000.0F)) * 60.0F)/360.0F * m_rpmSteeringScaling;
        float adjusteddelta = calcRPMAdjustment(tferror, delta);

        delta = m_useLoEndScaling ? adjusteddelta:delta;


        float newrpm = m_topDriveRPM - (delta);
        bool convstatus;
        float timedelta = 0;
        float calculatedrpmA = calculateRPM(tferror, &convstatus, &timedelta);
        float calculatedrpmB = calculateRPMFromFullData(0, &convstatus, &timedelta);
        float calculatedrpm = calculatedrpmB;
        float newrpmnull = m_topDriveRPM;

        emit updateDownholeRPMFromController(calculatedrpm);

        delta += calculatedrpm;

        qCDebug(TFBASEDCONTROLLER) << "Inside steering reference RPM = " << calculatedrpmA << " improved RPM = " << calculatedrpmB;
        if(m_useRPMNULL == 0)
        {
            newrpmnull -= (calculatedrpm * m_rpmAdjustScaling);
        }
        else
        {
            m_useRPMNULL--;
        }

        if(steeringstarted && false)
        {
            newrpmnull = m_lastSteeringRPM;
            emit newRPMValue(newrpmnull);
        }
        else
        {
            if(!m_useRPMNulling)
            {
                newrpmnull = m_rpmOffset;
            }


            controllervalues.insert(NEW_RPM_VALUE, QString::number(newrpm));
            controllervalues.insert(RPM_HOLD_TIME, QString::number(m_pulseWidth));
            controllervalues.insert(RPM_END_VALUE, QString::number(newrpmnull));
            emit newRPMValue(controllervalues);
            emit updateRPMCorrection(newrpm - m_topDriveRPM, true);

            //        m_rpmOffset = 0;
        }

        if(pmap != nullptr)
        {
            pmap->insert(CONTROLLER_NEWRPM, QString::number(newrpmnull, 'f', 6));
            pmap->insert("Steering Value", QString::number(newrpm));
            pmap->insert("Hold Value", QString::number(newrpmnull));
            pmap->insert("Pulse Width", QString::number(m_pulseWidth));
            pmap->insert("Calculated RPMA", QString::number(calculatedrpm, 'f', 3));
            pmap->insert("Delta Time", QString::number(timedelta));
            pmap->insert("ToolFace Error", QString::number(tferror, 'f', 2));
        }
    }

}

void TFBasedController::handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap)
{
//    if(drillaheadstarted)
//    {
//        float newrpmnull = m_topDriveRPM + m_drillAheadOffset;    // full step initially
//        emit newRPMValue(newrpmnull);
//    }
}

void TFBasedController::populateLogDataMap(QHash<QString,QString>* pmap)
{
    if(pmap != nullptr)
    {
        pmap->insert(CONTROLLER_START, "TF Derived RPM");
    }

}

void TFBasedController::handleBulkDrillAheadAdjustment()
{
    float newrpm = m_topDriveRPM;

    qCDebug(TFBASEDCONTROLLER) << "inside handleBulkDrillAheadAdjustment() " << " current mode = " << m_currentMode << " steering started = " << m_steeringStarted << " drillahead started = " << m_drillAheadStarted << " top drive RPM = " << m_topDriveRPM;

    if(m_drillAheadStarted)
    {
        newrpm = m_topDriveRPM + m_drillAheadOffset;    // full step initially
        qCDebug(TFBASEDCONTROLLER) << "inside handleBulkDrillAheadAdjustment()adjusting for drillahead";
    }

    if(m_steeringStarted)
    {
        newrpm = m_topDriveRPM - m_drillAheadOffset;    // full step initially
        qCDebug(TFBASEDCONTROLLER) << "inside handleBulkDrillAheadAdjustment()adjusting for steering started";
        m_delaySteering = true;
        m_steeringSampleDelayCounter = m_steeringSampleDelay;
    }

    qCDebug(TFBASEDCONTROLLER) << "inside handleBulkDrillAheadAdjustment() new rpm = " << newrpm;

    emit newDrillAheadAdjustment(newrpm);
}


void TFBasedController::updateToolFace(float toolface)
{
    float tfdelta = toolface - m_toolFace;
    long long currenttime = QDateTime::currentMSecsSinceEpoch() - m_timeOffset;
    bool isvalid = false;
    long long currentupdatetime = QDateTime::currentDateTime().toTime_t();

    if((currentupdatetime - m_lastTFUpdateTime) > m_ignoreWindow)
    {

        float calculatedrpm = calcRPMFromToolface(toolface, currenttime, &isvalid);

        m_lastTFUpdateTime = currentupdatetime;

        qCDebug(TFBASEDCONTROLLER) << "calculated rpm = " << calculatedrpm << " is valid " << isvalid;
        m_calculatedRPM = calculatedrpm;

        //    float timedelta = (currenttime - m_lastTFUpdateTime)/1000.0;



        //    if((toolface < m_toolFace))
        //    {
        //        tfdelta = 360 - m_toolFace + toolface;
        //    }

        //    qCDebug(TFBASEDCONTROLLER) << "new toolface = " << toolface << " prev toolface = " << m_toolFace << " toolface delta = " << tfdelta;
        //    qCDebug(TFBASEDCONTROLLER) << "time delta = " << timedelta;

        //    if(timedelta > 0)
        //    {
        //        float rpm = ((tfdelta/timedelta) * 60.0)/360.0;

        //        m_calculatedRPM = rpm;
        //        qCDebug(TFBASEDCONTROLLER) << "calculated rpm = " << rpm;
        //    }

        m_lastUpdate = QDateTime::currentDateTime().toTime_t();
        m_toolFace = toolface;
        m_cachedToolValues.insert(RPMVALUE, m_downHoleRPM);

        ControllerBase::updateToolFace(toolface);
    }
    else
    {
        qCDebug(TFBASEDCONTROLLER) << "ignored toolface sample at this time " << QDateTime::currentDateTime().toString();
    }


}


void TFBasedController::updateDownholeRPM(float rpm)
{
    m_downHoleRPM = rpm;

    qCDebug(TFBASEDCONTROLLER) << "latest rpm = " << rpm;
//    ControllerBase::updateDownholeRPM(rpm);
}


QStringList TFBasedController::getSystemLogHeaders()
{
    QString headerstring = "target Toolface,topdrive RPM,downhole RPM,toolface,Mode,Calculated RPM,Steering Value,Hold Value,Pulse Width,Calculated RPMA,STDev Error,Delta Time,ToolFace Error";
    QStringList headers;
    QStringList parts = headerstring.split(',');

    headers << SYSTEMLOGUNIXTIME << CONTROLLER_START;

    for(int i=0;i<parts.size();i++)
    {
        headers << parts.at(i);
    }

    return headers;
}

QString TFBasedController::getDescription()
{
   return "This is a test.  If this controller had been real, you would have been instructed...";
}

QString TFBasedController::getLogFileName()
{
   return "tf_based.csv";
}

bool TFBasedController::loadProps(QString path)
{
   return true;
}

QString TFBasedController::getName()
{
    return "TFBased Controller";
}


void TFBasedController::configure()
{
    TFConfiguration dlg(m_rpmOffset, m_rpmSteeringScaling, m_ignoreWindow, m_lowendThreshold, m_lowendAdjustment, m_useRPMNulling, m_rpmAdjustScaling, m_useLoEndScaling, m_useLinearScaling);

    if(dlg.exec() == QDialog::Accepted)
    {
        m_rpmOffset = dlg.m_rpmOffset;
        m_rpmSteeringScaling = dlg.m_steeringMultiplier;
        m_ignoreWindow = dlg.m_ignoreWindow;
        m_lowendThreshold = dlg.m_tfErrorThreshold;
        m_lowendAdjustment = dlg.m_maxLoEndScalePercent;
        m_useRPMNulling = dlg.m_useRPMNulling;
        m_rpmAdjustScaling = dlg.m_rpmScaling;
        m_useLinearScaling = dlg.m_useLinearScaling;
        m_useLoEndScaling = dlg.m_useLoEndScaling;
    }
}

void TFBasedController::showDiagnostics()
{

}



float TFBasedController::calcRPMFromToolface(float toolface, long long timestamp, bool* isvalid)
{
    float calculatedrpm = 0;
    QPoint tfpoint(timestamp, toolface);

    m_tfPoints.insert(0, tfpoint);

    if(m_tfPoints.size() > 10)
    {
        m_tfPoints.removeLast();
    }

    QPoint first;
    QPoint prev;
    float tffirst = -1;
    float tfprev = -1;
    int direction = 0;
    int validcount = 0;
    bool validcalculation = false;


    for(int i=0;i<m_tfPoints.size();i++)
    {
        if(tffirst == -1)
        {
            tffirst = m_tfPoints.at(i).y();
            qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....";
        }
        else
        {
            if(tfprev == -1)
            {
                qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....A";
                tfprev =  m_tfPoints.at(i).y();
            }
            else
            {
                qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....B";
                tffirst = tfprev;
                tfprev =  m_tfPoints.at(i).y();
            }

        }

        if(tffirst != -1 && (tfprev != -1))
        {
            qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....C";
            if(direction == 0)
            {
                qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....D";
                direction = ((tffirst - tfprev) >= 0) ? 1:-1;
            }
            else
            {
                qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....E";
                int newdirection = ((tffirst - tfprev) >= 0) ? 1:-1;

                if(direction == newdirection)
                {
                    qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....F";
                    validcount++;

                    if(validcount == 3)
                    {
                        qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....G";
                        float currenttime = m_tfPoints.at(i-1).x();
                        float prevtime = m_tfPoints.at(i).x();
                        float timedelta = (currenttime - prevtime)/1000.0;

                        qCDebug(TFBASEDCONTROLLER) << "currenttime = " << currenttime << " prev time = " << prevtime << " delta time = " << timedelta;

                        if(timedelta > 0)
                        {
                            qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....H";
                            float tfdelta = tffirst - tfprev;

                            calculatedrpm = ((tfdelta/timedelta) * 60.0)/360.0;
                            qCDebug(TFBASEDCONTROLLER) << "calculated rpm = " << calculatedrpm;
                            validcalculation = true;
                        }

                        break;
                    }
                }
                else
                {
                    qCDebug(TFBASEDCONTROLLER) << "calcRPMFromToolface()....I";
                    direction = newdirection;
                    validcount = 0;
                }


            }
        }
    }

    if(isvalid != nullptr)
    {
        *isvalid = validcalculation;
    }

    return calculatedrpm;
}


float TFBasedController::calculateRPM(float tferr, bool* isvalid, float *ptimediff)
{
    bool status = m_tfPoints.size() >= 2;
    float calculatedrpm = 0;

    if(status)
    {
        float timedelta = (m_tfPoints.at(0).x() - m_tfPoints.at(1).x())/1000.0;

        *ptimediff = timedelta;

        if(timedelta > 0)
        {
            calculatedrpm = ((tferr/timedelta) * 60.0)/360.0;
        }
        else
        {
            status = false;
        }
    }

    if(isvalid != nullptr)
    {
        *isvalid = status;
    }

    return calculatedrpm;
}


bool TFBasedController::isInRegulation(float currenterror, QHash<QString, QString> *pmap)
{
    m_errorhistory << currenterror;

    if(m_errorhistory.size() > 4)
    {
        m_errorhistory.removeFirst();
    }

    float stdev = Utils::calcStandardDev(&m_errorhistory);

    if(pmap != nullptr)
    {
        pmap->insert("STDev Error", QString::number(stdev));
    }

    if(stdev > 1.0)
    {
        m_inRegCounter++;
    }
    else
    {
        m_inRegCounter = 0;
    }

    bool inreg = m_inRegCounter > 4;

    qCDebug(TFBASEDCONTROLLER) << " TF error = " << currenterror << " error stdev = " << stdev << " in reg = " << inreg;
    return inreg;
}


void TFBasedController::updateToolFaceFullRate(float toolface)
{
    qCDebug(TFBASEDCONTROLLER) << "got this value for fullrate toolface " << toolface;

    long long currenttime = QDateTime::currentMSecsSinceEpoch() - m_timeOffset;

    QPoint tfpoint(currenttime, toolface);

    m_fullRateTFPoints.insert(0, tfpoint);

    if(m_fullRateTFPoints.size() > 7) // how many should this be???
    {
        m_fullRateTFPoints.removeLast();
    }

    bool isvalid;
    float diff;
    float currentrpm = calculateRPMFromFullData(0, &isvalid, &diff);

    qCDebug(TFBASEDCONTROLLER) << "Current rpm is currently " << currentrpm;

}

float TFBasedController::calculateTFDiff(float current, float last)
{
    float diff = current - last;

    if(fabs(diff) > 180)
    {
        if(diff > 0)
        {
            diff -= 360;
        }
        else
        {
            diff += 360;
        }
    }

    return diff;
}

float TFBasedController::calculateRPMFromFullData(float tferr, bool* isvalid, float* ptimediff)
{
    float calculatedrpm = 0;
    int terms = 0;

    for(int i=0;i<m_fullRateTFPoints.size()/2;i += 2)
    {
        bool status = i < m_fullRateTFPoints.size() - 2;

        if(status)
        {
            float timedelta = (m_fullRateTFPoints.at(i).x() - m_fullRateTFPoints.at(i+1).x())/1000.0;
//            float tferror = calculateTFError(m_fullRateTFPoints.at(i).y());
            float tferror = calculateTFDiff(m_fullRateTFPoints.at(i).y(), m_fullRateTFPoints.at(i+1).y());

            *ptimediff = timedelta;

            if(timedelta > 0)
            {
                calculatedrpm += ((tferror/timedelta) * 60.0)/360.0;
                terms++;
            }
            else
            {
                status = false;
            }
        }
    }

    if(isvalid != nullptr)
    {
        *isvalid = m_fullRateTFPoints.size() >= 2;
    }

    if(terms > 0)
    {
        calculatedrpm = calculatedrpm/terms;
    }

    return calculatedrpm;
}


float TFBasedController::calcRPMAdjustment(float tferror, float steeringrpmdelta)
{
    if(m_useLoEndScaling && (m_lowendThreshold > 0) && (fabs(tferror) < m_lowendThreshold))
    {
        float rangepercent = fabs(tferror)/m_lowendThreshold;

        qCDebug(TFBASEDCONTROLLER) << "tferror = " << tferror << " steering delta " << steeringrpmdelta << " rangepercent = " << rangepercent;
        rangepercent = 1.0 - rangepercent;
        qCDebug(TFBASEDCONTROLLER) << "adjusted rangepercent = " << rangepercent;
        qCDebug(TFBASEDCONTROLLER) << "scale value = " << m_lowendAdjustment << " error threshold = " << m_lowendThreshold;

        float adjustment = m_lowendAdjustment * rangepercent * steeringrpmdelta;

        qCDebug(TFBASEDCONTROLLER) << "scaled adjustment = " << adjustment;

        steeringrpmdelta += adjustment;

    }

    return steeringrpmdelta;

}
