#ifndef PIDCONTROLLERCONFIGDIALOG_H
#define PIDCONTROLLERCONFIGDIALOG_H

#include <QDialog>
#include "basecontrollerconfig.h"

namespace Ui {
class PIDControllerConfigDialog;
}

class PIDControllerConfigDialog : public BaseControllerConfig
{
    Q_OBJECT

public:
    explicit PIDControllerConfigDialog(QWidget *parent = 0);
    ~PIDControllerConfigDialog();

    void setRPMProportionalC(float value){m_rpmProportionalC = value;}
    float getRPMProportionalC(){return m_rpmProportionalC;}

    void setRPMIntegralC(float value){m_rpmIntegralC = value;}
    float getRPMIntegralC(){return m_rpmIntegralC;}

    void setTFProportionalC(float value){m_tfProportionalC = value;}
    float getTFProportionalC(){return m_tfProportionalC;}

    void setTFIntegralC(float value){m_tfIntegralC = value;}
    float getTFIntegralC(){return m_tfIntegralC;}

    void setTFDerivitaveC(float value){m_tfDeriativeC = value;}
    float getTFDerivitaveC(){return m_tfDeriativeC;}

    void setSteeringThresh(float value){m_steeringThresh = value;}
    float getSteeringThresh(){return m_steeringThresh;}

    virtual int exec();

    virtual QComboBox* getDropdown();
    virtual void propertiesLoaded(QHash<QString,QString> props);
    virtual QHash<QString,QString> getCurrentProperties();
    virtual QPushButton* getRefreshButton();


private slots:
    void on_rpmproportionalc_editingFinished();

    void on_rpmintegralconstant_editingFinished();

    void on_tfproportionalconstant_editingFinished();

    void on_tfintegralconstant_editingFinished();

    void on_tfderivativeconstant_editingFinished();

    void on_steeringthreshold_editingFinished();

private:
    Ui::PIDControllerConfigDialog *ui;

    float m_rpmProportionalC;
    float m_rpmIntegralC;
    float m_tfProportionalC;
    float m_tfIntegralC;
    float m_tfDeriativeC;
    float m_steeringThresh;
};

#endif // PIDCONTROLLERCONFIGDIALOG_H
