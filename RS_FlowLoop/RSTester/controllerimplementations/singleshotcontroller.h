#ifndef SINGLESHOTCONTROLLER_H
#define SINGLESHOTCONTROLLER_H

#include <QObject>
#include "controllerbase.h"

class SingleShotController : public ControllerBase
{
    Q_OBJECT
public:
    explicit SingleShotController(QObject *parent = 0);

    virtual QString getDescription();
    virtual QString getLogFileName();
    virtual QString getName();

    virtual void initialize(QHash<QString,QString> props);
    virtual bool loadProps(QString path);
    virtual QStringList getSystemLogHeaders();

    virtual void handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void populateLogDataMap(QHash<QString,QString>* pmap=nullptr);

protected:
    virtual void handleBulkDrillAheadAdjustment();
    virtual float calcRPMAdjustment(float tferror, float steeringrpmdelta);

signals:

public slots:
    virtual void configure();
    virtual void showDiagnostics();

protected:
    float m_rpmScaling;
    float m_tfScaling;
    float m_rpmSteeringScaling;
    int m_pulseWidth;
    float m_steeringThreshold;

    float m_currentSteeringThreshold;

    bool m_useLoEndScaling;
    bool m_useLinearScaling;
    float m_tfErrorThreshold;
    float m_maxLoEndScalePercent;


};

#endif // SINGLESHOTCONTROLLER_H
