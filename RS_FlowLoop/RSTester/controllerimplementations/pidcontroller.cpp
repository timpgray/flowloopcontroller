#include "pidcontroller.h"
#include <qdatetime.h>
#include "pidcontrollerconfigdialog.h"
#include "utils.h"
#include "rs_constants.h"

#include <qloggingcategory.h>

QLoggingCategory PIDCONTROLLER("PIDController");


PIDController::PIDController(QObject *parent) : Controller(parent)
{
    m_rpmProportionalScale = 0.5F;
    m_rpmIntegralScale = 0.016666F;
    m_tfProportionalScale = 25.0F;
    m_tfIntegralScale = 0.016666F;
    m_tfDerivativeScale = 0.0F;
    m_steeringThreshold = 4.0F;
}


void PIDController::initialize(QHash<QString,QString> props)
{
    Q_UNUSED(props);

    m_lastToolfaceTime = 0;
    m_lastRPMTime = 0;

    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

    QString header = "unix_time,time,target Toolface,topdrive RPM,downhole RPM,toolface,mode,RPM PropC,RPM IntegralC,TF PropC,TF IntegralC,TF DerivitiveC,TermTheta,TermRPM,Steering Thresh,RPM Correction,TF Correction,NewRpm,ToolFace Error";
    QHash<QString,QString> loggingprops;

    loggingprops.insert(HEADERSTRING, header);
    loggingprops.insert(CONTROLLER_STARTLOG, STATUS_TRUE);
    emit updateLogInfo(loggingprops);


    if(props.contains(PIDCONTROLLER_LAST_CONFIG))
    {
        QString path = props.value(PIDCONTROLLER_LAST_CONFIG);

        m_lastconfig = path;
        loadProps(path);
    }

    if(props.contains(PIDCONTROLLER_FILES))
    {
        QString filelist = props.value(PIDCONTROLLER_FILES);
        m_configFiles = filelist.split("#");
    }
}

bool PIDController::loadProps(QString path)
{
    QHash<QString,QString> configprops = Utils::loadPropsFile(path);
    bool status = false;

    if(configprops.contains(RPM_PROPORTIONAL_SCALE) && configprops.contains(RPM_INTEGRAL_SCALE) &&
            configprops.contains(TF_PROPORTIONAL_SCALE) && configprops.contains(TF_INTEGRAL_SCALE) &&
            configprops.contains(TF_DERIVATIVE_SCALE) && configprops.contains(STEERINGTHRESHOLD_RPM))
    {
        m_rpmProportionalScale = configprops.value(RPM_PROPORTIONAL_SCALE).toFloat();
        m_rpmIntegralScale = configprops.value(RPM_INTEGRAL_SCALE).toFloat();
        m_tfProportionalScale = configprops.value(TF_PROPORTIONAL_SCALE).toFloat();
        m_tfIntegralScale = configprops.value(TF_INTEGRAL_SCALE).toFloat();
        m_tfDerivativeScale = configprops.value(TF_DERIVATIVE_SCALE).toFloat();
        m_steeringThreshold = configprops.value(STEERINGTHRESHOLD_RPM).toFloat();
        status = true;
    }

    return status;
}

QString PIDController::getName()
{
   return "PID Controller";
}

QString PIDController::getDescription()
{
    return "";
}


void PIDController::updateTopDriveRPM(float rpm)
{
    m_topDriveRPM = rpm;
}

void PIDController::updateDownholeRPM(float rpm)
{
    long long currenttime = QDateTime::currentMSecsSinceEpoch();
    long long delta = currenttime - m_lastRPMTime;

    if(m_lastRPMTime == 0)
    {
        delta = 1000;
    }

    m_lastRPMTime = currenttime;
    m_downHoleRPM = rpm;
    m_rpmTimeDelta = delta/(1000.0F * 60.0F);
    m_prevRPMError = m_currentRPMError;
    m_currentRPMError = -1.0 * m_downHoleRPM;
}

void PIDController::updateToolFace(float toolface)
{
    long long currenttime = QDateTime::currentMSecsSinceEpoch();
    long long delta = currenttime - m_lastRPMTime;

    if(m_lastToolfaceTime == 0)
    {
        delta = 1000;
    }

    m_lastToolfaceTime = currenttime;
    m_toolFace = toolface;
    m_toolfaceTimeDelta = delta/(1000.0F * 60.0F);
    m_prevTFError = m_currrentTFError;
    m_currrentTFError = m_targetToolFace - m_toolFace;

    if (m_currrentTFError < -180.0)
    {
        m_currrentTFError += 360.0;
    }
    if (m_currrentTFError > 180.0)
    {
        m_currrentTFError -= 360.0;
    }

    QString timestring = QDateTime::currentDateTime().toString();
    unsigned int unixtime = QDateTime::currentDateTime().toTime_t();
    QString mode = Utils::getModeDesc(m_currentMode);

    QHash<QString,QString> map;
    QString msg = timestring  + QString("   DH RPM:") + QString::number(m_downHoleRPM, 'f', 2) + QString("   TF:") + QString::number(m_toolFace, 'f', 0) + "   Mode " + mode + QString("   Target TF:") + QString::number(m_targetToolFace, 'f', 0);

    map.insert(SYSTEMLOGUNIXTIME, QString::number(unixtime));
    map.insert("topdrive RPM", QString::number(m_topDriveRPM, 'f', 2));
    map.insert("downhole RPM", QString::number(m_downHoleRPM, 'f', 2));
    map.insert("toolface", QString::number(m_toolFace, 'f', 1));
    map.insert("target Toolface", QString::number(m_targetToolFace, 'f', 1));
    map.insert("mode", mode);
    map.insert("time", timestring);
    map.insert("RPM PropC", QString::number(m_rpmProportionalScale, 'f', 3));
    map.insert("RPM IntegralC", QString::number(m_rpmIntegralScale, 'f', 3));
    map.insert("TF PropC", QString::number(m_tfProportionalScale, 'f', 3));
    map.insert("TF IntegralC", QString::number(m_tfIntegralScale, 'f', 3));
    map.insert("TF DerivitiveC", QString::number(m_tfDerivativeScale, 'f', 3));
    map.insert("Steering Thresh", QString::number(m_steeringThreshold, 'f', 3));
    map.insert("ToolFace Error", QString::number(m_currrentTFError, 'f', 2));

    map.insert("unix_time", QString::number(unixtime));
    map.insert(CONTROLLER_START, "PID");

    qCDebug(PIDCONTROLLER) << "Toolface error = " << m_currrentTFError << " RPM error = " << m_currentRPMError;

    switch(m_currentMode)
    {
    case Steering:
        handleSteering(&map);
        break;
    case DrillAhead:
        handleDrillAhead(&map);
        break;
    default:
        break;
    }

    emit updateLogInfo(map);
    emit info(msg);
    emit systemLogUpdateEntries(map);


}

void PIDController::updateControllerMode(ControllerMode mode)
{
    m_currentMode = mode;
    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));
}

void PIDController::setTargetToolface(float target)
{
    m_targetToolFace = target;
}


void PIDController::configure()
{
    PIDControllerConfigDialog dlg;


    dlg.setConfigurations(m_configFiles, m_lastconfig);

    dlg.setRPMProportionalC(m_rpmProportionalScale);
    dlg.setRPMIntegralC(m_rpmIntegralScale);
    dlg.setTFProportionalC(m_tfProportionalScale);
    dlg.setTFDerivitaveC(m_tfDerivativeScale);
    dlg.setTFIntegralC(m_tfIntegralScale);
    dlg.setSteeringThresh(m_steeringThreshold);

    if(dlg.exec() == QDialog::Accepted)
    {
        m_rpmProportionalScale = dlg.getRPMProportionalC();
        m_rpmIntegralScale = dlg.getRPMIntegralC();
        m_tfProportionalScale = dlg.getTFProportionalC();
        m_tfDerivativeScale = dlg.getTFDerivitaveC();
        m_tfIntegralScale = dlg.getTFIntegralC();
        m_steeringThreshold = dlg.getSteeringThresh();

        m_configFiles = dlg.getConfigurations();
        m_lastconfig = dlg.getCurrentConfiguration();

        writeConfig(PIDCONTROLLER_LAST_CONFIG, PIDCONTROLLER_FILES);

    }
}

void PIDController::showDiagnostics()
{

}


//TermRpm = RPMpropscale *(ErrorRpm[0] +(((SamplePeriodRpmMinutes /RPMIngetralScale) -1) *ErrorRpm[1]));
float PIDController::calculateRPMCorrection()
{
    return m_rpmProportionalScale * (m_currentRPMError + (((m_rpmIntegralScale)) * m_prevRPMError));
}

//TermTheta = (TFPpropScale /360.0) *(ErrorTheta[0] +(((SamplePeriodThetaMinutes /TFIntegralScale) -1) *ErrorTheta[1]));

//     if (SamplePeriodThetaMinutes > 0)
//		TermTheta += (TFDervScale/360.0) *(ErrorTheta[0] -ErrorTheta[1]) /SamplePeriodThetaMinutes;

float PIDController::calculateTFCorrection()
{
   float correction = (m_tfProportionalScale/360.0F ) * (m_currrentTFError + (((m_tfIntegralScale)) * m_prevTFError));

   if(m_toolfaceTimeDelta > 0)
   {
       correction += (m_tfDerivativeScale/360.0F) * (m_currrentTFError - m_prevTFError)/m_toolfaceTimeDelta;
   }

   return correction;
}


void PIDController::handleSteering(QHash<QString,QString>* pmap)
{
    float newrpm = 0;

    if(fabs(m_currentRPMError) > m_steeringThreshold)
    {
        float rpmcorrection = calculateRPMCorrection();

        qCDebug(PIDCONTROLLER) << "RPM null rpmcorrection = " << rpmcorrection << " RPM time delta " << m_rpmTimeDelta << " downhole RPM = " << m_downHoleRPM << " topdrive RPM = " << m_topDriveRPM;

        newrpm = m_topDriveRPM + rpmcorrection;

        newrpm *= -1;       // differnce between sim and real...

        emit updateMode("Mode:" + Utils::getModeDesc(RPMNull));

        if(pmap != nullptr)
        {
            pmap->insert("RPM Correction", QString::number(rpmcorrection, 'f', 2));
        }

    }
    else
    {
        float tfcorrection = calculateTFCorrection();

        qCDebug(PIDCONTROLLER) << "Steering tf correction factor = " << tfcorrection << " TF time delta " << m_rpmTimeDelta;
        newrpm = m_topDriveRPM + tfcorrection;
        newrpm *= -1;       // differnce between sim and real...

        emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

        if(pmap != nullptr)
        {
            pmap->insert("TF Correction", QString::number(tfcorrection, 'f', 2));
        }
    }

    newrpm  *= -1;
    qCDebug(PIDCONTROLLER) << "new rpm value = " << newrpm;
    emit newRPMValue(newrpm);
    emit updateRPMCorrection(newrpm - m_topDriveRPM, true);

    if(pmap != nullptr)
    {
        pmap->insert("NewRpm", QString::number(newrpm, 'f', 2));
    }

}

void PIDController::handleDrillAhead(QHash<QString,QString>* pmap)
{
    float newrpm = -(m_topDriveRPM - m_downHoleRPM) + DEFAULT_DRILLAHEADOFFSET ;

    newrpm *= -1;       // differnce between sim and real...

    emit newRPMValue(newrpm);
    emit updateRPMCorrection(newrpm - m_topDriveRPM, true);


    if(pmap != nullptr)
    {
        pmap->insert("NewRpm", QString::number(newrpm, 'f', 2));
    }

}


QStringList PIDController::getSystemLogHeaders()
{
    QString headerstring = "target Toolface,topdrive RPM,downhole RPM,toolface,mode,RPM PropC,RPM IntegralC,TF PropC,TF IntegralC,TF DerivitiveC,TermTheta,TermRPM,Steering Thresh,RPM Correction,TF Correction,NewRpm,ToolFace Error";
    QStringList headers;
    QStringList parts = headerstring.split(',');

    headers << SYSTEMLOGUNIXTIME << CONTROLLER_START;

    for(int i=0;i<parts.size();i++)
    {
        headers << parts.at(i);
    }

    return headers;
}

void PIDController::updateDrillAheadOffset(float newoffset)
{

}
