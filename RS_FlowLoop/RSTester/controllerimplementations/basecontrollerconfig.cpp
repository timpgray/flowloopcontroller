#include "basecontrollerconfig.h"
#include "utils.h"
#include <qfiledialog.h>

BaseControllerConfig::BaseControllerConfig(QWidget *parent) : QDialog(parent)
{
    m_configPath = Utils::getControllerConfigPath();
}


void BaseControllerConfig::setConfigurations(QStringList configs, QString current)
{
    int currentsel = 0;

    m_configurations = configs;
    m_currentConfiguration = current;

    for(int i=0;i<m_configurations.size();i++)
    {
        QString path = m_configurations.at(i);
        QString filename = path.split("/").last();

        getDropdown()->addItem(filename, QVariant(path));

        if(path == current)
        {
            currentsel = i;
        }
    }

    getDropdown()->setCurrentIndex(currentsel);
}

QStringList BaseControllerConfig::getConfigurations()
{
    return m_configurations;
}

QString BaseControllerConfig::getCurrentConfiguration()
{
    return m_currentConfiguration;
}

void BaseControllerConfig::onCurrentIndexChanged(int index)
{
    Q_UNUSED(index);
    QString newconfig = getDropdown()->currentData().toString();
    QHash<QString,QString> props = Utils::loadPropsFile(newconfig);

    m_currentConfiguration = newconfig;
    propertiesLoaded(props);
    getRefreshButton()->setEnabled(false);
}

void BaseControllerConfig::onNewconfiguration_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "New Config", m_configPath, "*.cfg");

    if(!path.isEmpty())
    {
        m_currentConfiguration = path;
        m_configurations.append(path);

        QString filename = path.split("/").last();


        getDropdown()->addItem(filename, QVariant(path));
        getDropdown()->blockSignals(true);
        getDropdown()->setCurrentText(filename);
        getDropdown()->blockSignals(false);
    }
}

void BaseControllerConfig::onSaveconfig_clicked()
{
    QHash<QString,QString> props = getCurrentProperties();

    Utils::writePropsFile(m_currentConfiguration, props);
}

void BaseControllerConfig::onRemoveconfig_clicked()
{
    m_configurations.removeAll(m_currentConfiguration);
    getDropdown()->removeItem(getDropdown()->currentIndex());

    if(m_configurations.size() > 1)
    {
        m_currentConfiguration = m_configurations.first();
        getDropdown()->setCurrentIndex(0);
    }
    else
    {
        m_currentConfiguration = "";
    }
}

void BaseControllerConfig::configChanged()
{
    getRefreshButton()->setEnabled(true);
}

void BaseControllerConfig::refresh()
{
   onCurrentIndexChanged(getDropdown()->currentIndex());
   getRefreshButton()->setEnabled(false);
}

int BaseControllerConfig::exec()
{
    QHash<QString,QString> props = getCurrentProperties();
    QString newconfig = getDropdown()->currentData().toString();
    QHash<QString,QString> storedprops = Utils::loadPropsFile(newconfig);
    bool ischanged = false;
    QList<QString> keylist = storedprops.keys();

    for(int i=0;i<keylist.size();i++)
    {
        QString key = keylist.at(i);

        if(props.contains(key))
        {
            QString val1 = props.value(key);
            QString val2 = storedprops.value(key);
            bool matches = (val1 == val2);

            ischanged = !matches;

            if(ischanged)
            {
                break;
            }
        }
    }

    getRefreshButton()->setEnabled(ischanged);

    connect(getRefreshButton(), SIGNAL(clicked(bool)), this, SLOT(updateFromCurrent()));

    return QDialog::exec();
}

void BaseControllerConfig::updateFromCurrent()
{
    refresh();
}
