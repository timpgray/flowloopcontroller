#include "derivedrpmcontroller.h"

#include <qloggingcategory.h>
#include <qdatetime.h>

#include "rs_constants.h"
#include "utils.h"
#include "singleshotcontrollerconfiguration.h"


QLoggingCategory DERIVEDRPMCONTROLLER("DerivedRPMController");


DerivedRPMController::DerivedRPMController(QObject *parent) : SingleShotController(parent)
{
    m_timeOffset = QDateTime::currentMSecsSinceEpoch();
    m_useRPMNULL = 0;
}

DerivedRPMController::~DerivedRPMController()
{

}



void DerivedRPMController::setTargetToolface(float target)
{
    m_useRPMNULL = 3;
    SingleShotController::setTargetToolface(target);
}



void DerivedRPMController::updateToolFace(float toolface)
{
    long long currenttime = QDateTime::currentMSecsSinceEpoch() - m_timeOffset;
    float toolfaceerror = calculateTFError(toolface);

    cacheToolFace(toolface, currenttime);
    bool isvalid = false;
    float calculateddhrpm = calculateRPM(toolfaceerror, &isvalid);

    m_calculatedDHRPM = calculateddhrpm;

    SingleShotController::updateDownholeRPM(calculateddhrpm);

    SingleShotController::updateToolFace(toolface);
}

void DerivedRPMController::updateDownholeRPM(float rpm)
{
    // swallow this value
    m_givenDHRPM = rpm;
}


void DerivedRPMController::cacheToolFace(float toolface, long long timestamp)
{
    QPoint tfpoint(timestamp, toolface);

    m_tfPoints.insert(0, tfpoint);

    if(m_tfPoints.size() > 10)
    {
        m_tfPoints.removeLast();
    }

}

float DerivedRPMController::calculateRPM(float tferr, bool* isvalid)
{
    bool status = m_tfPoints.size() >= 2;
    float calculatedrpm = 0;

    if(status)
    {
        float timedelta = (m_tfPoints.at(0).x() - m_tfPoints.at(1).x())/1000.0;

        if(timedelta > 0)
        {
            calculatedrpm = ((tferr/timedelta) * 60.0)/360.0;
        }
        else
        {
            status = false;
        }
    }

    if(isvalid != nullptr)
    {
        *isvalid = status;
    }

    return calculatedrpm;
}

bool DerivedRPMController::isInRegulation(float currenterror, QHash<QString,QString>* pmap)
{
    m_errorhistory << currenterror;

    if(m_errorhistory.size() > 4)
    {
        m_errorhistory.removeFirst();
    }

    float stdev = Utils::calcStandardDev(&m_errorhistory);

    if(pmap != nullptr)
    {
        pmap->insert("STDev Error", QString::number(stdev));
    }

    if(stdev > 1.0)
    {
        m_inRegCounter++;
    }
    else
    {
        m_inRegCounter = 0;
    }

    bool inreg = m_inRegCounter > 4;

    qCDebug(DERIVEDRPMCONTROLLER) << " TF error = " << currenterror << " error stdev = " << stdev << " in reg = " << inreg;
    return inreg;
}


QString DerivedRPMController::getName()
{
    return CONTROLLER_DERIVEDRPM;
}


QString DerivedRPMController::getDescription()
{
    return "Derives RPM from ToolFace error";
}

QString DerivedRPMController::getLogFileName()
{
    return "derivedRPMController.csv";
}


void DerivedRPMController::handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap)
{
    if(pmap != nullptr)
    {
        pmap->insert("givenDHRPM", QString::number(m_givenDHRPM));
        pmap->insert("calculatedDHRPM", QString::number(m_calculatedDHRPM));
    }

    if(m_useRPMNULL == 0)
    {
        SingleShotController::handleSteering(tferror, rpmerror, steeringstarted, pmap);
    }
    else
    {
        m_useRPMNULL--;
    }
}

QStringList DerivedRPMController::getSystemLogHeaders()
{
    QStringList list = SingleShotController::getSystemLogHeaders();

    list << "givenDHRPM" << "calculatedDHRPM";

    return list;
}



void DerivedRPMController::initialize(QHash<QString,QString> props)
{

    QHash<QString,QString> initprops;
    QString header = "unix_time,time,target Toolface,topdrive RPM,downhole RPM,toolface,Mode,RPM Error Scaling,TF Error Scaling,RPM Error Steering Scaling,Pulse Width,Steering Threshold(RPM),RPM Error,ToolFace Error,Single Shot,Correction Mode,RPM Correction,New RPM,RPM Null Value,Steering Value,Hold Value";
    QHash<QString,QString> loggingprops;

    loggingprops.insert(HEADERSTRING, header);
    loggingprops.insert(CONTROLLER_STARTLOG, STATUS_TRUE);
    emit updateLogInfo(loggingprops);


    m_currentMode = OFF;

    initprops.insert(SHOWDIAGNOSTICS, STATUS_FALSE);
    emit initialized(initprops);
    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

    if(props.contains(DERIVEDRPMCONTROLLER_LAST_CONFIG))
    {
        QString path = props.value(DERIVEDRPMCONTROLLER_LAST_CONFIG);

        m_lastconfig = path;

        loadProps(path);
    }

    if(props.contains(DERIVEDRPMCONTROLLER_FILES))
    {
        QString filelist = props.value(DERIVEDRPMCONTROLLER_FILES);
        m_configFiles = filelist.split("#");
    }

    ControllerBase::initialize(props);
}


void DerivedRPMController::configure()
{
    SingleShotControllerConfiguration dlg;

    dlg.setConfigurations(m_configFiles, m_lastconfig);
    dlg.setControllerConfig(m_rpmScaling, m_tfScaling, m_rpmSteeringScaling, m_pulseWidth, m_steeringThreshold);
    dlg.setLoEndScalingConfig(m_useLoEndScaling, m_useLinearScaling, m_tfErrorThreshold, m_maxLoEndScalePercent);


    if(dlg.exec() == QDialog::Accepted)
    {
        m_rpmScaling = dlg.getRPMScaling();
        m_tfScaling = dlg.getTFScaling();
        m_rpmSteeringScaling = dlg.getRPMScalingSteering();
        m_pulseWidth = dlg.getPulseWidth();
        m_steeringThreshold = dlg.getSteeringThreshold();
        m_configFiles = dlg.getConfigurations();
        m_lastconfig = dlg.getCurrentConfiguration();

        m_useLoEndScaling = dlg.getUseLoEndScaling();
        m_useLinearScaling = dlg.getUseLinearScaling();
        m_tfErrorThreshold = dlg.getLoEndScalingThreshold();
        m_maxLoEndScalePercent = dlg.getMaxLoEndScaling();

        writeConfig(DERIVEDRPMCONTROLLER_LAST_CONFIG, DERIVEDRPMCONTROLLER_FILES);
    }
}
