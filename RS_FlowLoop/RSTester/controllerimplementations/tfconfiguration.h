#ifndef TFCONFIGURATION_H
#define TFCONFIGURATION_H

#include <QDialog>

namespace Ui {
class TFConfiguration;
}

class TFConfiguration : public QDialog
{
    Q_OBJECT

public:
    explicit TFConfiguration(float rpmoffset, float mulitipler, int ignorewindow, float lowendthresh, float lowendadjustment, bool usenulling, float rpmscaling, bool uselowendadjustment, bool uselinearscaling, QWidget *parent = 0);
    ~TFConfiguration();

private slots:
    void on_steeringm_editingFinished();

    void on_rpmoffset_editingFinished();

    void on_ignorewindow_editingFinished();

    void on_userpmnulling_clicked(bool checked);

    void on_rpmscaling_editingFinished();

    void on_uselowendadjustment_clicked(bool checked);

    void on_uselinearscaling_clicked(bool checked);

    void on_lowendthreshold_editingFinished();

    void on_maxadjustment_editingFinished();

private:
    Ui::TFConfiguration *ui;

public:
    float m_rpmOffset;
    float m_steeringMultiplier;
    int m_ignoreWindow;
    bool m_useRPMNulling;
    float m_rpmScaling;

    bool m_useLoEndScaling;
    bool m_useLinearScaling;
    float m_tfErrorThreshold;
    float m_maxLoEndScalePercent;

};

#endif // TFCONFIGURATION_H
