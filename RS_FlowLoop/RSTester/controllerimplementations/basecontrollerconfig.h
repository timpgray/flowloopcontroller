#ifndef BASECONTROLLERCONFIG_H
#define BASECONTROLLERCONFIG_H

#include <qdialog.h>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <qhash.h>

class BaseControllerConfig : public QDialog
{
    Q_OBJECT
public:
    explicit BaseControllerConfig(QWidget *parent = 0);

    void setConfigurations(QStringList configs, QString current);
    QStringList getConfigurations();
    QString getCurrentConfiguration();
    void configChanged();


    virtual QComboBox* getDropdown() = 0;
    virtual QPushButton* getRefreshButton()=0;
    virtual void propertiesLoaded(QHash<QString,QString> props) = 0;
    virtual QHash<QString,QString> getCurrentProperties() = 0;
    virtual void refresh();
    virtual int exec();

signals:

public slots:

    void onCurrentIndexChanged(int index);

    void onNewconfiguration_clicked();

    void onSaveconfig_clicked();

    void onRemoveconfig_clicked();

    void updateFromCurrent();

protected:
    QStringList m_configurations;
    QString m_currentConfiguration;
    QString m_configPath;

};

#endif // BASECONTROLLERCONFIG_H
