#ifndef DERIVEDRPMCONTROLLER_H
#define DERIVEDRPMCONTROLLER_H

#include <QObject>
#include <QPointF>
#include <qlist.h>
#include "singleshotcontroller.h"

class DerivedRPMController : public SingleShotController
{
    Q_OBJECT
public:
    explicit DerivedRPMController(QObject *parent = 0);
    virtual ~DerivedRPMController();

    virtual QString getDescription();
    virtual QString getLogFileName();
    virtual QString getName();
    virtual void initialize(QHash<QString,QString> props);

protected:
    void cacheToolFace(float toolface, long long timestamp);
    float calculateRPM(float tferr, bool* isvalid);
    bool isInRegulation(float currenterror, QHash<QString,QString>* pmap=nullptr);

    virtual void handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap);
    virtual QStringList getSystemLogHeaders();

signals:

public slots:
    virtual void updateToolFace(float toolface);
    virtual void updateDownholeRPM(float rpm);
    virtual void setTargetToolface(float target);
    virtual void configure();

private:
    QList<QPointF> m_tfPoints;
    int m_useRPMNULL;

    QList<float> m_errorhistory;
    int m_inRegCounter;
    long long m_timeOffset;

    float m_givenDHRPM;
    float m_calculatedDHRPM;


};

#endif // DERIVEDRPMCONTROLLER_H
