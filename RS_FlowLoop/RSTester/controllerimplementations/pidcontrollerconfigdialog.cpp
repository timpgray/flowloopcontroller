#include "pidcontrollerconfigdialog.h"
#include "ui_pidcontrollerconfigdialog.h"
#include "rs_constants.h"

PIDControllerConfigDialog::PIDControllerConfigDialog(QWidget *parent) :
    BaseControllerConfig(parent),
    ui(new Ui::PIDControllerConfigDialog)
{
    ui->setupUi(this);

    connect(ui->configurations, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));
    connect(ui->newconfiguration, SIGNAL(clicked(bool)), this, SLOT(onNewconfiguration_clicked()));
    connect(ui->removeconfig, SIGNAL(clicked(bool)), this, SLOT(onRemoveconfig_clicked()));
    connect(ui->saveconfig, SIGNAL(clicked(bool)), this, SLOT(onSaveconfig_clicked()));

}

PIDControllerConfigDialog::~PIDControllerConfigDialog()
{
    delete ui;
}


int PIDControllerConfigDialog::exec()
{
    ui->rpmproportionalc->setText(QString::number(m_rpmProportionalC, 'f', 3));
    ui->rpmintegralconstant->setText(QString::number(m_rpmIntegralC, 'f', 3));
    ui->tfproportionalconstant->setText(QString::number(m_tfProportionalC, 'f', 3));
    ui->tfintegralconstant->setText(QString::number(m_tfIntegralC, 'f', 3));
    ui->tfderivativeconstant->setText(QString::number(m_tfDeriativeC, 'f', 3));
    ui->steeringthreshold->setText(QString::number(m_steeringThresh, 'f', 1));

    return BaseControllerConfig::exec();
}

void PIDControllerConfigDialog::on_rpmproportionalc_editingFinished()
{
    m_rpmProportionalC = ui->rpmproportionalc->text().toFloat();
    configChanged();
}

void PIDControllerConfigDialog::on_rpmintegralconstant_editingFinished()
{
    m_rpmIntegralC = ui->rpmintegralconstant->text().toFloat();
    configChanged();
}

void PIDControllerConfigDialog::on_tfproportionalconstant_editingFinished()
{
    m_tfProportionalC = ui->tfproportionalconstant->text().toFloat();
    configChanged();
}

void PIDControllerConfigDialog::on_tfintegralconstant_editingFinished()
{
    m_tfIntegralC = ui->tfintegralconstant->text().toFloat();
    configChanged();
}

void PIDControllerConfigDialog::on_tfderivativeconstant_editingFinished()
{
    m_tfDeriativeC = ui->tfderivativeconstant->text().toFloat();
    configChanged();
}

void PIDControllerConfigDialog::on_steeringthreshold_editingFinished()
{
    m_steeringThresh = ui->steeringthreshold->text().toFloat();
    configChanged();
}


QComboBox* PIDControllerConfigDialog::getDropdown()
{
   return ui->configurations;
}


void PIDControllerConfigDialog::propertiesLoaded(QHash<QString,QString> props)
{
    if(props.contains(RPM_PROPORTIONAL_SCALE) && props.contains(RPM_INTEGRAL_SCALE) &&
            props.contains(TF_PROPORTIONAL_SCALE) && props.contains(TF_INTEGRAL_SCALE) &&
            props.contains(TF_DERIVATIVE_SCALE) && props.contains(STEERINGTHRESHOLD_RPM))
    {
        m_rpmProportionalC = props.value(RPM_PROPORTIONAL_SCALE).toFloat();
        m_rpmIntegralC = props.value(RPM_INTEGRAL_SCALE).toFloat();
        m_tfProportionalC = props.value(TF_PROPORTIONAL_SCALE).toFloat();
        m_tfIntegralC = props.value(TF_INTEGRAL_SCALE).toFloat();
        m_tfDeriativeC = props.value(TF_DERIVATIVE_SCALE).toFloat();
        m_steeringThresh = props.value(STEERINGTHRESHOLD_RPM).toFloat();

        ui->rpmproportionalc->setText(QString::number(m_rpmProportionalC, 'f', 3));
        ui->rpmintegralconstant->setText(QString::number(m_rpmIntegralC, 'f', 3));
        ui->tfproportionalconstant->setText(QString::number(m_tfProportionalC, 'f', 3));
        ui->tfintegralconstant->setText(QString::number(m_tfIntegralC, 'f', 3));
        ui->tfderivativeconstant->setText(QString::number(m_tfDeriativeC, 'f', 3));
        ui->steeringthreshold->setText(QString::number(m_steeringThresh, 'f', 1));

    }
}

QHash<QString,QString> PIDControllerConfigDialog::getCurrentProperties()
{
    QHash<QString,QString> props;
    QString rpmpropstr = QString::number(m_rpmProportionalC, 'f', 3);
    QString rpmintegralstr = QString::number(m_rpmIntegralC, 'f', 3);
    QString tfpropstr = QString::number(m_tfProportionalC, 'f', 3);
    QString tfintegralstr = QString::number(m_tfIntegralC, 'f', 3);
    QString tfderivativestr = QString::number(m_tfDeriativeC, 'f', 3);
    QString steeringthreshstr = QString::number(m_steeringThresh, 'f', 3);

    props.insert(RPM_PROPORTIONAL_SCALE, rpmpropstr);
    props.insert(RPM_INTEGRAL_SCALE, rpmintegralstr);
    props.insert(TF_PROPORTIONAL_SCALE, tfpropstr);
    props.insert(TF_INTEGRAL_SCALE, tfintegralstr);
    props.insert(TF_DERIVATIVE_SCALE, tfderivativestr);
    props.insert(STEERINGTHRESHOLD_RPM, steeringthreshstr);

    return props;
}

QPushButton* PIDControllerConfigDialog::getRefreshButton()
{
    return ui->refreshconfig;
}

