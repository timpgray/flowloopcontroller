#ifndef TFBASEDCONTROLLER_H
#define TFBASEDCONTROLLER_H

#include <QObject>
#include "controllerbase.h"
#include <QPointF>
#include <qlist.h>

class TFBasedController : public ControllerBase
{
    Q_OBJECT
public:
    explicit TFBasedController(QObject *parent = 0);
    virtual void initialize(QHash<QString,QString> props);

    virtual QString getDescription();
    virtual QString getLogFileName();
    virtual bool loadProps(QString path);
    virtual QString getName();
    virtual QStringList getSystemLogHeaders();
    virtual bool providesDownholeRPM(){return true;}


protected:
    virtual void handleSteering(float tferror, float rpmerror, bool steeringstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void handleDrillAhead(float tferror, float rpmerror, bool drillaheadstarted, QHash<QString,QString>* pmap=nullptr);
    virtual void populateLogDataMap(QHash<QString,QString>* pmap=nullptr);
    virtual void handleBulkDrillAheadAdjustment();

    float calcRPMFromToolface(float toolface, long long timestamp, bool* isvalid);
    float calculateRPM(float tferr, bool* isvalid, float* ptimediff);
    float calculateRPMFromFullData(float tferr, bool* isvalid, float* ptimediff);
    bool isInRegulation(float currenterror, QHash<QString,QString>* pmap=nullptr);
    float calculateTFDiff(float current, float last);

    float calcRPMAdjustment(float tferror, float steeringrpmdelta);

signals:

public slots:

    virtual void setTargetToolface(float target);
    virtual void updateToolFace(float toolface);
    virtual void updateDownholeRPM(float rpm);
    virtual void configure();
    virtual void showDiagnostics();
    virtual void updateToolFaceFullRate(float toolface);


private:
    long long m_lastTFUpdateTime;
    long long m_timeOffset;
    float m_calculatedRPM;

    QList<QPointF> m_tfPoints;
    QList<QPointF> m_fullRateTFPoints;

    float m_rpmSteeringScaling;
    int m_pulseWidth;
    int m_useRPMNULL;
    float m_rpmOffset;

    QList<float> m_errorhistory;
    int m_inRegCounter;

    int m_ignoreWindow;
    float m_lowendThreshold;
    float m_lowendAdjustment;
    bool m_useRPMNulling;
    float m_rpmAdjustScaling;
    bool m_useLoEndScaling;
    bool m_useLinearScaling;

    bool m_delaySteering;
    int m_steeringSampleDelay;
    int m_steeringSampleDelayCounter;


};

#endif // TFBASEDCONTROLLER_H
