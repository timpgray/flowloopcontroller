#include "controllerbase.h"
#include "utils.h"
#include "rs_constants.h"
#include <qdatetime.h>
#include <qloggingcategory.h>


#define TOOLFACEFALUE "toolfacevalue"
#define RPMVALUE "rpmvalue"


QLoggingCategory CONTROLLERBASE("ControllerBase");


ControllerBase::ControllerBase(QObject *parent) : Controller(parent)
{
    m_currentMode = OFF;
    m_steeringStarted = false;
    m_drillAheadStarted = false;
    m_drillAheadOffset = DEFAULT_DRILLAHEADOFFSET;
    m_lastSteeringRPM = -1;
    m_maxCacheAgeMillis = 1000;
}

void ControllerBase::initialize(QHash<QString,QString> props)
{
    if(props.contains(CONTROLLER_MAX_CACHE_AGE_MILLIS))
    {
        m_maxCacheAgeMillis = props.value(CONTROLLER_MAX_CACHE_AGE_MILLIS).toInt();
    }

    if(props.contains(CURRENTTOOLFACETARGET))
    {
        m_targetToolFace = props.value(CURRENTTOOLFACETARGET).toFloat();
    }

    if(props.contains(CONTROLLER_DRILLAHEADOFFSET))
    {
        m_drillAheadOffset = props.value(CONTROLLER_DRILLAHEADOFFSET).toFloat();
    }
}


void ControllerBase::updateTopDriveRPM(float rpm)
{
    m_topDriveRPM = rpm;    
}

void ControllerBase::updateToolFace(float toolface)
{
    checkUpdateTimeAgainstCache();
    m_toolFace = toolface;
    m_cachedToolValues.insert(TOOLFACEFALUE, m_toolFace);
    handleToolFaceUpdate(&m_cachedToolValues);

}

void ControllerBase::handleToolFaceUpdate(QHash<QString, float> *valuemap)
{
    if(valuemap->contains(TOOLFACEFALUE) && valuemap->contains(RPMVALUE))
    {
        m_toolFace = valuemap->value(TOOLFACEFALUE);
        m_downHoleRPM = valuemap->value(RPMVALUE);

        QString timestring = QDateTime::currentDateTime().toString();
        unsigned int unixtime = QDateTime::currentDateTime().toTime_t();
        QString mode = Utils::getModeDesc(m_currentMode);
        QHash<QString,QString> map;
        QString msg = timestring  + QString("   DH RPM:") + QString::number(m_downHoleRPM, 'f', 2) + QString("   TF:") + QString::number(m_toolFace, 'f', 0) + "   Mode " + mode + QString("   Target TF:") + QString::number(m_targetToolFace, 'f', 0);

        map.insert("topdrive RPM", QString::number(m_topDriveRPM, 'f', 2));
        map.insert("downhole RPM", QString::number(m_downHoleRPM, 'f', 2));
        map.insert("toolface", QString::number(m_toolFace, 'f', 1));
        map.insert("target Toolface", QString::number(m_targetToolFace, 'f', 1));
        map.insert("Mode", mode);
        map.insert("time", timestring);
        map.insert(SYSTEMLOGUNIXTIME, QString::number(unixtime));
        populateLogDataMap(&map);


        float rpmerror = m_downHoleRPM;
        float tferror = m_targetToolFace - m_toolFace;

        if (tferror < -180.0)
        {
            tferror += 360.0;
        }
        if (tferror > 180.0)
        {
            tferror -= 360.0;
        }

        tferror *= -1;


        switch(m_currentMode)
        {
        case Steering:
            handleSteering(tferror, rpmerror, m_steeringStarted, &map);
            m_steeringStarted = false;
            m_lastSteeringRPM = m_topDriveRPM;
            break;
        case DrillAhead:
            handleDrillAhead(tferror, rpmerror, m_drillAheadStarted, &map);
            m_drillAheadStarted = false;
            break;
        default:
            break;
        }

        qCDebug(CONTROLLERBASE) <<  "!!! inside updateToolface() mode = " << m_currentMode << " map contents = " << map;

        emit updateLogInfo(map);
        emit systemLogUpdateEntries(map);

        emit info(msg);
        valuemap->clear();
    }
}

float ControllerBase::calculateTFError(float currenttoolface)
{
    float tferror = m_targetToolFace - currenttoolface;

    if (tferror < -180.0)
    {
        tferror += 360.0;
    }
    if (tferror > 180.0)
    {
        tferror -= 360.0;
    }

    tferror *= -1;

    return tferror;
}

void ControllerBase::updateControllerMode(ControllerMode mode)
{
    m_steeringStarted = m_currentMode == DrillAhead && mode == Steering;
    m_drillAheadStarted = m_currentMode != DrillAhead && mode == DrillAhead;

    qCDebug(CONTROLLERBASE) << "new mode = " << mode << " current mode = " << m_currentMode << " steering started = " << m_steeringStarted << " drillahead started = " << m_drillAheadStarted;

    m_currentMode = mode;
    emit updateMode("Mode:" + Utils::getModeDesc(m_currentMode));

    handleBulkDrillAheadAdjustment();

    if(mode == OFF || mode == Idle)
    {
        emit updateRPMCorrection(0, false);
    }
}

void ControllerBase::setTargetToolface(float target)
{
    m_targetToolFace = target;
}

void ControllerBase::updateDownholeRPM(float rpm)
{
    checkUpdateTimeAgainstCache();
    m_downHoleRPM = rpm;
    m_cachedToolValues.insert(RPMVALUE, m_downHoleRPM);
    handleToolFaceUpdate(&m_cachedToolValues);
}


void ControllerBase::checkUpdateTimeAgainstCache()
{
    unsigned int currenttime = QDateTime::currentDateTime().toTime_t();
    unsigned int delta = currenttime - m_lastUpdate;

    if(delta > m_maxCacheAgeMillis/1000)
    {
         m_cachedToolValues.clear();
    }

    m_lastUpdate = currenttime;
}


void ControllerBase::updateDrillAheadOffset(float newoffset)
{
    m_drillAheadOffset = newoffset;
}
