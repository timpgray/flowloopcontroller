#include "simplecontrollerconfig.h"
#include "ui_simplecontrollerconfig.h"
#include "rs_constants.h"

SimpleControllerConfig::SimpleControllerConfig(QWidget *parent) :
    BaseControllerConfig(parent),
    ui(new Ui::SimpleControllerConfig)
{
    ui->setupUi(this);

    connect(ui->configurations, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));
    connect(ui->newconfiguration, SIGNAL(clicked(bool)), this, SLOT(onNewconfiguration_clicked()));
    connect(ui->removeconfig, SIGNAL(clicked(bool)), this, SLOT(onRemoveconfig_clicked()));
    connect(ui->saveconfig, SIGNAL(clicked(bool)), this, SLOT(onSaveconfig_clicked()));
}

SimpleControllerConfig::~SimpleControllerConfig()
{
    delete ui;
}

void SimpleControllerConfig::setScaleFactors(float rpmscaling, float tfscaling)
{
    QString rpmstr = QString::number(rpmscaling, 'f', 3);
    QString tfstr = QString::number(tfscaling, 'f', 3);

    ui->rpmscaling->setText(rpmstr);
    ui->tfscaling->setText(tfstr);

    m_rpmScaling = rpmscaling;
    m_tfScaling = tfscaling;
}

void SimpleControllerConfig::on_rpmscaling_editingFinished()
{
    QString text = ui->rpmscaling->text();

    m_rpmScaling = text.toFloat();
    configChanged();
}

void SimpleControllerConfig::on_tfscaling_editingFinished()
{
   QString text = ui->tfscaling->text();

   m_tfScaling = text.toFloat();
   configChanged();
}



QComboBox* SimpleControllerConfig::getDropdown()
{
   return ui->configurations;
}

void SimpleControllerConfig::propertiesLoaded(QHash<QString,QString> props)
{
    if(props.contains(RPM_SCALING) && props.contains(TF_ANGLE_SCALING))
    {
        m_rpmScaling = props.value(RPM_SCALING).toFloat();
        m_tfScaling = props.value(TF_ANGLE_SCALING).toFloat();

        setScaleFactors(m_rpmScaling, m_tfScaling);

    }
}

QHash<QString,QString> SimpleControllerConfig::getCurrentProperties()
{
    QHash<QString,QString> props;
    QString rpmstr = QString::number(m_rpmScaling, 'f', 3);
    QString tfstr = QString::number(m_tfScaling, 'f', 3);

    props.insert(RPM_SCALING, rpmstr);
    props.insert(TF_ANGLE_SCALING, tfstr);

    return props;
}

QPushButton* SimpleControllerConfig::getRefreshButton()
{
    return ui->refreshconfig;
}


