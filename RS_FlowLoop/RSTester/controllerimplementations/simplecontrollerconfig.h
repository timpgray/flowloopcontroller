#ifndef SIMPLECONTROLLERCONFIG_H
#define SIMPLECONTROLLERCONFIG_H

#include <QDialog>
#include "basecontrollerconfig.h"

namespace Ui {
class SimpleControllerConfig;
}

class SimpleControllerConfig : public BaseControllerConfig
{
    Q_OBJECT

public:
    explicit SimpleControllerConfig(QWidget *parent = 0);
    ~SimpleControllerConfig();

    virtual QComboBox* getDropdown();
    virtual void propertiesLoaded(QHash<QString,QString> props);
    virtual QHash<QString,QString> getCurrentProperties();
    virtual QPushButton* getRefreshButton();

    void setScaleFactors(float rpmscaling, float tfscaling);
    float getRPMScale(){return m_rpmScaling;}
    float getTFScale(){return m_tfScaling;}


private slots:
    void on_rpmscaling_editingFinished();

    void on_tfscaling_editingFinished();

private:
    Ui::SimpleControllerConfig *ui;

    float m_rpmScaling;
    float m_tfScaling;
};

#endif // SIMPLECONTROLLERCONFIG_H
