#include "tfconfiguration.h"
#include "ui_tfconfiguration.h"

TFConfiguration::TFConfiguration(float rpmoffset, float mulitipler, int ignorewindow, float lowendthresh, float lowendadjustment, bool usenulling, float rpmscaling, bool uselowendadjustment, bool uselinearscaling, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TFConfiguration)
{
    ui->setupUi(this);

    m_rpmOffset = rpmoffset;
    m_steeringMultiplier = mulitipler;
    m_ignoreWindow = ignorewindow;
    m_tfErrorThreshold = lowendthresh;
    m_maxLoEndScalePercent = lowendadjustment;
    m_useRPMNulling = usenulling;
    m_rpmScaling = rpmscaling;
    m_useLinearScaling = uselinearscaling;
    m_useLoEndScaling = uselowendadjustment;

    ui->rpmoffset->setText(QString::number(rpmoffset));
    ui->steeringm->setText(QString::number(mulitipler));
    ui->lowendthreshold->setText(QString::number(m_tfErrorThreshold));
    ui->maxadjustment->setText(QString::number(m_maxLoEndScalePercent));
    ui->ignorewindow->setText(QString::number(m_ignoreWindow));
    ui->userpmnulling->setChecked(m_useRPMNulling);
    ui->rpmscaling->setText(QString::number(m_rpmScaling));
    ui->uselinearscaling->setChecked(m_useLinearScaling);
    ui->uselowendadjustment->setChecked(m_useLoEndScaling);

}

TFConfiguration::~TFConfiguration()
{
    delete ui;
}

void TFConfiguration::on_steeringm_editingFinished()
{
    m_steeringMultiplier = ui->steeringm->text().toFloat();
}

void TFConfiguration::on_rpmoffset_editingFinished()
{
    m_rpmOffset = ui->rpmoffset->text().toFloat();
}

void TFConfiguration::on_ignorewindow_editingFinished()
{
    m_ignoreWindow = ui->ignorewindow->text().toInt();
}


void TFConfiguration::on_userpmnulling_clicked(bool checked)
{
    m_useRPMNulling = checked;
}

void TFConfiguration::on_rpmscaling_editingFinished()
{
    m_rpmScaling = ui->rpmscaling->text().toFloat();
}

void TFConfiguration::on_uselowendadjustment_clicked(bool checked)
{
    m_useLoEndScaling = checked;
}

void TFConfiguration::on_uselinearscaling_clicked(bool checked)
{
    m_useLinearScaling = checked;
}

void TFConfiguration::on_lowendthreshold_editingFinished()
{
    m_tfErrorThreshold = ui->lowendthreshold->text().toFloat();
}

void TFConfiguration::on_maxadjustment_editingFinished()
{
    m_maxLoEndScalePercent = ui->maxadjustment->text().toFloat();
}
