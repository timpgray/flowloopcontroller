#include "singleshotcontrollerconfiguration.h"
#include "ui_singleshotcontrollerconfiguration.h"
#include "rs_constants.h"

SingleShotControllerConfiguration::SingleShotControllerConfiguration(QWidget *parent) :
    BaseControllerConfig(parent),
    ui(new Ui::SingleShotControllerConfiguration)
{
    ui->setupUi(this);

    m_rpmScaling = 0.5;
    m_tfScaling = 1.0;
    m_rpmSteeringScaling = 1.0;
    m_pulseWidth = 1000;
    m_steeringThreshold = 4.0;

    setControllerConfig(0.5, 1.0, 1.0, 1000, 4.0);

    connect(ui->configurations, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));
    connect(ui->newconfiguration, SIGNAL(clicked(bool)), this, SLOT(onNewconfiguration_clicked()));
    connect(ui->removeconfig, SIGNAL(clicked(bool)), this, SLOT(onRemoveconfig_clicked()));
    connect(ui->saveconfig, SIGNAL(clicked(bool)), this, SLOT(onSaveconfig_clicked()));
}

SingleShotControllerConfiguration::~SingleShotControllerConfiguration()
{
    delete ui;
}



void SingleShotControllerConfiguration::setControllerConfig(float rpmscaling, float tfscaling, float rpmscalingsteering, int pulsewidth, float steeringthresh)
{
    m_rpmScaling = rpmscaling;
    m_tfScaling = tfscaling;
    m_rpmSteeringScaling = rpmscalingsteering;
    m_pulseWidth = pulsewidth;
    m_steeringThreshold = steeringthresh;

    ui->rpmscaling->setText(QString::number(m_rpmScaling, 'f', 3));
    ui->tfscaling->setText(QString::number(m_tfScaling, 'f', 3));
    ui->rpmscaling_steering->setText(QString::number(m_rpmSteeringScaling, 'f', 3));
    ui->pulseduration->setText(QString::number(m_pulseWidth/1000));
    ui->steeringthreshold->setText(QString::number(m_steeringThreshold, 'f', 3));

}

void SingleShotControllerConfiguration::setLoEndScalingConfig(bool uselowendscaling, bool uselinearscaling, float lowendthreshold, float maxloendscaling)
{
    ui->uselowendadjustment->setChecked(uselowendscaling);
    ui->uselinearscaling->setChecked(uselinearscaling);
    ui->lowendthreshold->setText(QString::number(lowendthreshold, 'f', 2));
    ui->maxadjustment->setText(QString::number(maxloendscaling, 'f', 2));
}

int SingleShotControllerConfiguration::exec()
{
    int retval = BaseControllerConfig::exec();

    m_rpmScaling = ui->rpmscaling->text().toFloat();
    m_tfScaling = ui->tfscaling->text().toFloat();
    m_rpmSteeringScaling = ui->rpmscaling_steering->text().toFloat();
    m_pulseWidth = ui->pulseduration->text().toInt() * 1000;
    m_steeringThreshold = ui->steeringthreshold->text().toFloat();

    m_useLoEndScaling = ui->uselowendadjustment->isChecked();
    m_useLinearScaling = ui->uselinearscaling->isChecked();
    m_tfErrorThreshold = ui->lowendthreshold->text().toFloat();
    m_maxLoEndScalePercent = ui->maxadjustment->text().toFloat();


    return retval;
}

QComboBox* SingleShotControllerConfiguration::getDropdown()
{
    return ui->configurations;
}


void SingleShotControllerConfiguration::propertiesLoaded(QHash<QString,QString> props)
{
    if(props.contains(RPM_SCALING) && props.contains(TF_ANGLE_SCALING) &&
            props.contains(SINGLESHOTPULSEWIDTH) && props.contains(RPM_STEERING_SCALING) &&
            props.contains(STEERINGTHRESHOLD_RPM))
    {
        m_rpmScaling = props.value(RPM_SCALING).toFloat();
        m_tfScaling = props.value(TF_ANGLE_SCALING).toFloat();
        m_rpmSteeringScaling = props.value(RPM_STEERING_SCALING).toFloat();
        m_pulseWidth = props.value(SINGLESHOTPULSEWIDTH).toInt();
        m_steeringThreshold = props.value(STEERINGTHRESHOLD_RPM).toFloat();

        setControllerConfig(m_rpmScaling, m_tfScaling, m_rpmSteeringScaling, m_pulseWidth, m_steeringThreshold);

    }

    if(props.contains(SINGLESHOT_USE_LOWEND_SCALING) && props.contains(SINGLESHOT_USE_LINEAR_SCALING) &&
            props.contains(SINGLESHOT_LOWEND_THRESHOLD) && props.contains(SINGLESHOT_MAX_LOWEND_ADJUSTMENT))
    {
        m_useLoEndScaling = props.value(SINGLESHOT_USE_LOWEND_SCALING) == "true" ? true:false;
        m_tfErrorThreshold = props.value(SINGLESHOT_LOWEND_THRESHOLD).toFloat();
        m_useLinearScaling = props.value(SINGLESHOT_USE_LINEAR_SCALING) == "true" ? true:false;;
        m_maxLoEndScalePercent = props.value(SINGLESHOT_MAX_LOWEND_ADJUSTMENT).toFloat();
    }
    else
    {
        m_useLoEndScaling = true;
        m_tfErrorThreshold = 10.0;
        m_useLinearScaling = true;
        m_maxLoEndScalePercent = 2.0;
    }

    setLoEndScalingConfig(m_useLoEndScaling, m_useLinearScaling, m_tfErrorThreshold, m_maxLoEndScalePercent);

}

QHash<QString,QString> SingleShotControllerConfiguration::getCurrentProperties()
{
    QHash<QString,QString> props;

    props.insert(RPM_SCALING, QString::number(m_rpmScaling, 'f', 3));
    props.insert(TF_ANGLE_SCALING, QString::number(m_tfScaling, 'f', 3));
    props.insert(RPM_STEERING_SCALING, QString::number(m_rpmSteeringScaling, 'f', 3));
    props.insert(SINGLESHOTPULSEWIDTH, QString::number(m_pulseWidth));
    props.insert(STEERINGTHRESHOLD_RPM, QString::number(m_steeringThreshold, 'f', 3));

    props.insert(SINGLESHOT_USE_LOWEND_SCALING, (m_useLoEndScaling ? "true":"false"));
    props.insert(SINGLESHOT_USE_LINEAR_SCALING, (m_useLinearScaling ? "true":"false"));
    props.insert(SINGLESHOT_LOWEND_THRESHOLD, QString::number(m_tfErrorThreshold, 'f', 3));
    props.insert(SINGLESHOT_MAX_LOWEND_ADJUSTMENT, QString::number(m_maxLoEndScalePercent, 'f', 3));

    return props;
}


void SingleShotControllerConfiguration::on_rpmscaling_editingFinished()
{
    m_rpmScaling = ui->rpmscaling->text().toFloat();
    configChanged();
}

void SingleShotControllerConfiguration::on_tfscaling_editingFinished()
{
    m_tfScaling = ui->tfscaling->text().toFloat();
    configChanged();
}

void SingleShotControllerConfiguration::on_rpmscaling_steering_editingFinished()
{
    m_rpmSteeringScaling = ui->rpmscaling_steering->text().toFloat();
    configChanged();
}

void SingleShotControllerConfiguration::on_pulseduration_editingFinished()
{
    m_pulseWidth = ui->pulseduration->text().toInt() * 1000;
    configChanged();
}

void SingleShotControllerConfiguration::on_steeringthreshold_editingFinished()
{
    m_steeringThreshold = ui->steeringthreshold->text().toFloat();
    configChanged();
}

QPushButton* SingleShotControllerConfiguration::getRefreshButton()
{
    return ui->refreshconfig;
}

void SingleShotControllerConfiguration::on_lowendthreshold_editingFinished()
{
    m_tfErrorThreshold = ui->lowendthreshold->text().toFloat();
}

void SingleShotControllerConfiguration::on_maxadjustment_editingFinished()
{
    m_maxLoEndScalePercent = ui->maxadjustment->text().toFloat();
}

void SingleShotControllerConfiguration::on_uselowendadjustment_clicked(bool checked)
{
    m_useLoEndScaling = checked;
}

void SingleShotControllerConfiguration::on_uselinearscaling_clicked(bool checked)
{
    m_useLinearScaling = checked;
}
