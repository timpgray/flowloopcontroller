#include "controllerviewfactory.h"
#include "controllerviewimplementations/controllerviewimpl.h"


ControllerViewFactory::ControllerViewFactory()
{

}


ControllerView* ControllerViewFactory::getControllerView(Ui::TestMainWindow *ui, QString name)
{
    Q_UNUSED(name);

    return new ControllerViewImpl(ui);
}
