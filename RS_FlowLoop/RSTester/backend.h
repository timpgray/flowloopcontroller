#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include "baseinitializer.h"
#include <qstringlist.h>

#include "mudpump.h"
#include "tool.h"
#include "topdrive.h"

class BackEnd : public BaseInitializer
{
    Q_OBJECT
public:
    explicit BackEnd(QObject *parent = 0);
    virtual ~BackEnd();

    ///
    /// \brief getToolInterfaces called to get the names of availble Tool implementations
    /// \return returns a QStringList containing the names of availble interface implementations
    ///
    virtual QStringList getToolInterfaces(){return m_toolInterfaceNames;}
    ///
    /// \brief getMudPumpInterfaces called to get the names of availble MudPump implementations
    /// \return returns a QStringList containing the names of availble interface implementations
    ///
    virtual QStringList getMudPumpInterfaces(){return m_mudPumpInterfaceNames;}
    ///
    /// \brief getTopDriveInterfaces called to get the names of availble TopDrive implementations
    /// \return returns a QStringList containing the names of availble interface implementations
    ///
    virtual QStringList getTopDriveInterfaces(){return m_topDriveInterfaceNames;}

    ///
    /// \brief getMudPump called to get a particular implementation of the MudPump interface
    /// \param name is a QString that is the name of the desired interface implementation
    /// \return returns a MudPump* specified by name if it is availble.  Returns null otherwise
    ///
    virtual MudPump* getMudPump(QString name="")=0;
    ///
    /// \brief getTool called to get a particular implementation of the Tool interface
    /// \param name is a QString that is the name of the desired interface implementation
    /// \return returns a Tool* specified by name if it is availble.  Returns null otherwise
    ///
    virtual Tool* getTool(QString name="")=0;
    ///
    /// \brief getTopDrive called to get a particular implementation of the TopDrive interface
    /// \param name is a QString that is the name of the desired interface implementation
    /// \return returns a TopDrive* specified by name if it is availble.  Returns null otherwise
    ///
    virtual TopDrive* getTopDrive(QString name="")=0;

    virtual void showDiagnostics(){}

    virtual void setRPMLimits(float topdrivemax, float mudpumpmax);
    virtual void setToolSampleRateMillis(int millis);
    virtual void setToolPropogationDelayMillis(int millis);



protected:

    QStringList m_toolInterfaceNames;
    QStringList m_topDriveInterfaceNames;
    QStringList m_mudPumpInterfaceNames;
    TopDrive* m_pTopDrive;
    Tool* m_pTool;
    MudPump* m_pMudPump;


signals:
    void updatePressureTop(float pressure);
    void updatePressureBotton(float pressure);
    void updatePressureDrop(float pressure);
    void updateFlowRate(float flowrate);
    void updateLimits(float maxmudpumprpm, float maxtopdriverpm);
    void updateRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);
    void updateTorque(float torque);


public slots:
    virtual void startFacility(bool start)=0;
    virtual void quickStopFacility()=0;
    virtual void setLimits(float maxmudpumprpm, float maxtopdriverpm)=0;
    virtual void setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)=0;

};

#endif // BACKEND_H
