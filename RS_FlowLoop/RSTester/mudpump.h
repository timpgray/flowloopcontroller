#ifndef MUDPUMP_H
#define MUDPUMP_H

#include <QObject>
#include "baseinitializer.h"

class MudPump : public BaseInitializer
{
    Q_OBJECT
public:
    explicit MudPump(QObject *parent = 0);
    virtual ~MudPump(){}

    virtual void setMaxRPM(float maxval){m_maximumRPM = maxval;}

signals:
    void newActualRPM(float rpm);

public slots:
    virtual void updateRPM(float rpm)=0;
    virtual void pumpsOn(bool status)=0;

protected:
    float m_maximumRPM;
};

#endif // MUDPUMP_H
