#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <QObject>

#include "controller.h"
#include "controllerview.h"
#include "backend.h"
#include "mudpump.h"
#include "topdrive.h"
#include "tool.h"
#include "controller.h"

class ConnectionManager : public QObject
{
    Q_OBJECT
public:
    explicit ConnectionManager(QObject *parent = 0);

    void addTool(Tool* ptool);
    void removeTool(Tool* ptool);

    void addMudPump(MudPump* pmudpump);
    void removeMudPump(MudPump* pmudpump);

    void addTopDrive(TopDrive* ptopdrive);
    void removeTopDrive(TopDrive* ptopdrive);

    void addControllerView(ControllerView* pcontrollerview);
    void removeControllerView(ControllerView* pcontrollerview);

    void addBackEnd(BackEnd* pbackend);
    void removeBackEnd(BackEnd* pbackend);

    void addController(Controller* pcontroller);
    void removeController(Controller* pcontroller);

    void mudPumpRpmChanged(float newrpm);
    void topDriveRpmChanged(float newrpm);
    void targetToolFaceChanged(float newTFTarget);
    void toolSampleIntervalChanged(int sampleinterval);
    void toolPropogationDelayChanged(int delay);

protected:
    void startSystemLog();
    void writeSystemLogRowCache(bool writeall=false);

signals:
    void newRPM(float rpm);

public slots:

    void updateControllerLogInfo(QHash<QString,QString> datamap);
    void newRPMValue(QHash<QString,QString> controlvalues);
    void handleDrillHeadAdjustment(float rpm);
    void resetRPMValue();
    void updateSystemLogInfo(QHash<QString,QString> datamap);



private:
    Tool* m_pTool;
    TopDrive* m_pTopDrive;
    MudPump* m_pMudPump;
    BackEnd* m_pBackend;
    Controller* m_pController;
    ControllerView* m_pControllerView;
    QStringList m_header;
    int m_logFileHandle;
    float m_rpmResetValue;
    QStringList m_systemLogHeader;
    int m_sysremLogFileHandle;
    QHash<QString,QString> m_currentSystemLogRow;
    QHash<unsigned int,QHash<QString,QString>> m_currentSystemLogRowCache;
    bool m_correctionPending;
};

#endif // CONNECTIONMANAGER_H
