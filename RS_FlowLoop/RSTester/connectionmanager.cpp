#include "connectionmanager.h"

#include <qtimer.h>
#include <qloggingcategory.h>
#include <qdatetime.h>

#include "utils.h"
#include "rs_constants.h"

QLoggingCategory CONNECTIONMANAGER("ConnectionManager");


ConnectionManager::ConnectionManager(QObject *parent) : QObject(parent)
{
    m_pTool = nullptr;
    m_pTopDrive = nullptr;
    m_pMudPump = nullptr;
    m_pBackend = nullptr;
    m_pController = nullptr;
    m_pControllerView = nullptr;

    m_sysremLogFileHandle = -1;
    m_logFileHandle = -1;
    m_correctionPending = false;
}

void ConnectionManager::addMudPump(MudPump* pmudpump)
{
    if(pmudpump != nullptr)
    {
        if(m_pControllerView != nullptr)
        {
            connect(m_pControllerView, SIGNAL(runMudPump(bool)), pmudpump, SLOT(pumpsOn(bool)));
            connect(m_pControllerView, SIGNAL(mudPumpRPMChanged(float)), pmudpump, SLOT(updateRPM(float)));
            connect(pmudpump, SIGNAL(newStatus(bool)), m_pControllerView, SLOT(updateMudPumpStatus(bool)));
            connect(pmudpump, SIGNAL(newActualRPM(float)), m_pControllerView, SLOT(updateActualMudPumpRPM(float)));
        }

        m_pMudPump = pmudpump;
    }
}

void ConnectionManager::removeMudPump(MudPump* pmudpump)
{
    if(pmudpump != nullptr && pmudpump == m_pMudPump)
    {
        disconnect(pmudpump, nullptr, nullptr, nullptr);

        if(m_pControllerView != nullptr)
        {
            disconnect(m_pControllerView, SIGNAL(runMudPump(bool)), pmudpump, SLOT(pumpsOn(bool)));
            disconnect(m_pControllerView, SIGNAL(mudPumpRPMChanged(float)), pmudpump, SLOT(updateRPM(float)));
        }

        m_pMudPump = nullptr;
    }
}

void ConnectionManager::addTopDrive(TopDrive* ptopdrive)
{
    if(ptopdrive != nullptr)
    {
        if(m_pControllerView != nullptr)
        {
            connect(m_pControllerView, SIGNAL(runTopDrive(bool)), ptopdrive, SLOT(topDriveOn(bool)));
            connect(m_pControllerView, SIGNAL(topDriveRPMChanged(float)), ptopdrive, SLOT(updateRPM(float)));
            connect(ptopdrive, SIGNAL(newActualRPM(float)), m_pControllerView, SLOT(updateActualTopDriveRPM(float)));
            connect(ptopdrive, SIGNAL(newStatus(bool)), m_pControllerView, SLOT(updateTopDriveStatus(bool)));
        }

        if(m_pController != nullptr)
        {
            connect(ptopdrive, SIGNAL(newActualRPM(float)), m_pController, SLOT(updateTopDriveRPM(float)));
            connect(ptopdrive, SIGNAL(newRPMValue(float)), m_pController, SLOT(updateRPM(float)));
            connect(m_pController, SIGNAL(newRPMValue(float)), ptopdrive, SLOT(updateRPM(float)));
        }

        m_pTopDrive = ptopdrive;

        connect(this, SIGNAL(newRPM(float)), ptopdrive, SLOT(updateRPM(float)));
    }
}

void ConnectionManager::removeTopDrive(TopDrive* ptopdrive)
{
    if(ptopdrive != nullptr && ptopdrive == m_pTopDrive)
    {
        disconnect(ptopdrive, nullptr, nullptr, nullptr);

        if(m_pControllerView != nullptr)
        {
            disconnect(m_pControllerView, SIGNAL(runTopDrive(bool)), ptopdrive, SLOT(topDriveOn(bool)));
            disconnect(m_pControllerView, SIGNAL(topDriveRPMChanged(float)), ptopdrive, SLOT(updateRPM(float)));
        }

        if(m_pController != nullptr)
        {
            disconnect(m_pController, SIGNAL(newRPMValue(float)), ptopdrive, SLOT(updateRPM(float)));
        }

        disconnect(this, SIGNAL(newRPM(float)), ptopdrive, SLOT(updateRPM(float)));

        m_pTopDrive = nullptr;
    }
}


void ConnectionManager::addTool(Tool* ptool)
{
    qCDebug(CONNECTIONMANAGER) << "inisde add tool";
    // Tool sends data to ControllerView and Controller
    if(ptool != nullptr)
    {
        qCDebug(CONNECTIONMANAGER) << "inside add tool connecting tool name " << ptool->getName();
        if(m_pControllerView != nullptr)
        {
            qCDebug(CONNECTIONMANAGER) << "inside add tool connecting to controller view " << m_pControllerView->getName();
            connect(ptool, SIGNAL(newRPMValue(float)), m_pControllerView, SLOT(updateDownHoleRPM(float)));
            connect(ptool, SIGNAL(newToolfaceValue(float)), m_pControllerView, SLOT(updateDownHoleToolface(float)));
            connect(ptool, SIGNAL(newTelemetryValue(QString,float)), m_pControllerView, SLOT(updateDownHoleValue(QString,float)));
            connect(ptool, SIGNAL(newWITSMsg(QString,QString,float)), m_pControllerView, SLOT(newWITSData(QString,QString,float)));
        }

        if(m_pController != nullptr)
        {
            qCDebug(CONNECTIONMANAGER) << "inside add tool connecting to controller " << m_pController->getName();
            connect(ptool, SIGNAL(newRPMValue(float)), m_pController, SLOT(updateDownholeRPM(float)));
            connect(ptool, SIGNAL(newToolfaceValue(float)), m_pController, SLOT(updateToolFace(float)));
            connect(ptool, SIGNAL(newToolfaceValueFullRate(float)), m_pController, SLOT(updateToolFaceFullRate(float)));

            if(m_pController->providesDownholeRPM() && (m_pControllerView != nullptr))
            {
                disconnect(ptool, SIGNAL(newRPMValue(float)), m_pControllerView, SLOT(updateDownHoleRPM(float)));
                connect(m_pController, SIGNAL(updateDownholeRPMFromController(float)), m_pControllerView, SLOT(updateDownHoleRPM(float)));
            }
        }

        connect(ptool, SIGNAL(systemLogUpdateEntries(QHash<QString,QString>)), this, SLOT(updateSystemLogInfo(QHash<QString,QString>)));
        m_pTool = ptool;

        startSystemLog();
    }

}

void ConnectionManager::removeTool(Tool* ptool)
{
  // tool sends data to Controller and ControllerView
  if(ptool != nullptr && ptool == m_pTool)
  {
      disconnect(ptool, nullptr, nullptr, nullptr);
      m_pTool = nullptr;
  }
}

void ConnectionManager::addControllerView(ControllerView* pcontrollerview)
{
    // ControllerView sends signals to MudPump, TopDrive, and Backend
    // receives signals from Tool, MudPump, TopDrive, and Backend

    if(pcontrollerview != nullptr)
    {
        if(m_pMudPump != nullptr)
        {
            connect(pcontrollerview, SIGNAL(runMudPump(bool)), m_pMudPump, SLOT(pumpsOn(bool)));
            connect(pcontrollerview, SIGNAL(mudPumpRPMChanged(float)), m_pMudPump, SLOT(updateRPM(float)));
            connect(m_pMudPump, SIGNAL(newStatus(bool)), pcontrollerview, SLOT(updateMudPumpStatus(bool)));
            connect(m_pMudPump, SIGNAL(newActualRPM(float)), pcontrollerview, SLOT(updateActualMudPumpRPM(float)));
        }

        if(m_pTopDrive != nullptr)
        {
            connect(pcontrollerview, SIGNAL(runTopDrive(bool)), m_pTopDrive, SLOT(topDriveOn(bool)));
            connect(pcontrollerview, SIGNAL(topDriveRPMChanged(float)), m_pTopDrive, SLOT(updateRPM(float)));
            connect(m_pTopDrive, SIGNAL(newActualRPM(float)), pcontrollerview, SLOT(updateActualTopDriveRPM(float)));
            connect(m_pTopDrive, SIGNAL(newStatus(bool)), pcontrollerview, SLOT(updateTopDriveStatus(bool)));
        }

        if(m_pTool != nullptr)
        {
            connect(m_pTool, SIGNAL(newRPMValue(float)), pcontrollerview, SLOT(updateDownHoleRPM(float)));
            connect(m_pTool, SIGNAL(newToolfaceValue(float)), pcontrollerview, SLOT(updateDownHoleToolface(float)));
            connect(m_pTool, SIGNAL(newTelemetryValue(QString,float)), pcontrollerview, SLOT(updateDownHoleValue(QString,float)));
            connect(m_pTool, SIGNAL(newWITSMsg(QString,QString,float)), pcontrollerview, SLOT(newWITSData(QString,QString,float)));
        }

        if(m_pBackend != nullptr)
        {
            connect(pcontrollerview, SIGNAL(facilityOn(bool)), m_pBackend, SLOT(startFacility(bool)));
            connect(pcontrollerview, SIGNAL(facilityQuickStop()), m_pBackend, SLOT(quickStopFacility()));
            connect(pcontrollerview, SIGNAL(setLimits(float,float)), m_pBackend, SLOT(setLimits(float,float)));
            connect(pcontrollerview, SIGNAL(setRanges(float,float,float,float)), m_pBackend, SLOT(setRanges(float,float,float,float)));

            connect(m_pBackend, SIGNAL(statusUpdate(QHash<QString,QString>)), pcontrollerview, SLOT(newStatusUpdate(QHash<QString,QString>)));
            connect(m_pBackend, SIGNAL(updateFlowRate(float)), pcontrollerview, SLOT(updateFlowRate(float)));
            connect(m_pBackend, SIGNAL(updatePressureBotton(float)), pcontrollerview, SLOT(updatePressureBotton(float)));
            connect(m_pBackend, SIGNAL(updatePressureDrop(float)), pcontrollerview,  SLOT(updatePressureDrop(float)));
            connect(m_pBackend, SIGNAL(updatePressureTop(float)), pcontrollerview, SLOT(updatePressureTop(float)));
            connect(m_pBackend, SIGNAL(updateLimits(float,float)), pcontrollerview, SLOT(updateLimits(float,float)));
            connect(m_pBackend, SIGNAL(updateRanges(float,float,float,float)), pcontrollerview, SLOT(updateRanges(float,float,float,float)));
            connect(m_pBackend, SIGNAL(updateTorque(float)), pcontrollerview, SLOT(updateTopDriveTorque(float)));
        }

        if(m_pController != nullptr)
        {
            connect(pcontrollerview, SIGNAL(configure()), m_pController, SLOT(configure()));
//            connect(pcontrollerview, SIGNAL(showDiagnostics()), m_pController, SLOT(showDiagnostics()));
            connect(pcontrollerview, SIGNAL(controllerModeChanged(ControllerMode)), m_pController, SLOT(updateControllerMode(ControllerMode)));
            connect(pcontrollerview, SIGNAL(TargetToolfaceChanged(float)), m_pController, SLOT(setTargetToolface(float)));
            connect(pcontrollerview, SIGNAL(topDriveRPMChanged(float)), m_pController, SLOT(updateTopDriveRPM(float)));
            connect(m_pController, SIGNAL(info(QString)), pcontrollerview, SLOT(updateControllerInfo(QString)));
            connect(m_pController, SIGNAL(newRPMValue(float)), pcontrollerview, SLOT(updateActualTopDriveRPM(float)));
            connect(m_pController, SIGNAL(initialized(QHash<QString,QString>)), pcontrollerview, SLOT(controllerInitialized(QHash<QString,QString>)));
            connect(m_pController, SIGNAL(updateMode(QString)), pcontrollerview, SLOT(updateModeStatus(QString)));
            connect(m_pController, SIGNAL(updateRPMCorrection(float,bool)), pcontrollerview, SLOT(handleNewRPMCorrection(float,bool)));

            if(m_pController->providesDownholeRPM() && (m_pTool != nullptr))
            {
                disconnect(m_pTool, SIGNAL(newRPMValue(float)), pcontrollerview, SLOT(updateDownHoleRPM(float)));
                connect(m_pController, SIGNAL(updateDownholeRPMFromController(float)), pcontrollerview, SLOT(updateDownHoleRPM(float)));
            }

        }

        connect(this, SIGNAL(newRPM(float)), pcontrollerview, SLOT(updateTopDriveTargetRPM(float)));
        m_pControllerView = pcontrollerview;
    }

}

void ConnectionManager::removeControllerView(ControllerView* pcontrollerview)
{
    // ControllerView sends signals to MudPump, TopDrive, and Backend
    // receives signals from Tool, MudPump, TopDrive, and Backend
    if(pcontrollerview != nullptr && pcontrollerview == m_pControllerView)
    {
        disconnect(pcontrollerview, nullptr, nullptr, nullptr);
        m_pControllerView = nullptr;
    }

    if(pcontrollerview != nullptr)
    {
        if(m_pTool != nullptr)
        {
            disconnect(m_pTool, SIGNAL(newRPMValue(float)), pcontrollerview, SLOT(updateDownHoleRPM(float)));
            disconnect(m_pTool, SIGNAL(newToolfaceValue(float)), pcontrollerview, SLOT(updateDownHoleToolface(float)));
            disconnect(m_pTool, SIGNAL(newTelemetryValue(QString,float)), pcontrollerview, SLOT(updateDownHoleValue(QString,float)));
            disconnect(m_pTool, SIGNAL(newWITSMsg(QString,QString,float)), pcontrollerview, SLOT(newWITSData(QString,QString,float)));
        }

        if(m_pMudPump != nullptr)
        {
            disconnect(m_pMudPump, SIGNAL(newStatus(bool)), pcontrollerview, SLOT(updateMudPumpStatus(bool)));
            disconnect(m_pMudPump, SIGNAL(newActualRPM(float)), pcontrollerview, SLOT(updateActualMudPumpRPM(float)));
        }

        if(m_pTopDrive != nullptr)
        {
            disconnect(m_pTopDrive, SIGNAL(newActualRPM(float)), pcontrollerview, SLOT(updateActualTopDriveRPM(float)));
            disconnect(m_pTopDrive, SIGNAL(newStatus(bool)), pcontrollerview, SLOT(updateTopDriveStatus(bool)));
        }

        if(m_pBackend != nullptr)
        {
            disconnect(m_pBackend, SIGNAL(statusUpdate(QHash<QString,QString>)), pcontrollerview, SLOT(newStatusUpdate(QHash<QString,QString>)));
            disconnect(m_pBackend, SIGNAL(updateFlowRate(float)), pcontrollerview, SLOT(updateFlowRate(float)));
            disconnect(m_pBackend, SIGNAL(updatePressureBotton(float)), pcontrollerview, SLOT(updatePressureBotton(float)));
            disconnect(m_pBackend, SIGNAL(updatePressureDrop(float)), pcontrollerview,  SLOT(updatePressureDrop(float)));
            disconnect(m_pBackend, SIGNAL(updatePressureTop(float)), pcontrollerview, SLOT(updatePressureTop(float)));
            disconnect(m_pBackend, SIGNAL(updateLimits(float,float)), pcontrollerview, SLOT(updateLimits(float,float)));
            disconnect(m_pBackend, SIGNAL(updateRanges(float,float,float,float)), pcontrollerview, SLOT(updateRanges(float,float,float,float)));
            disconnect(m_pBackend, SIGNAL(updateTorque(float)), pcontrollerview, SLOT(updateTopDriveTorque(float)));
        }

        if(m_pController != nullptr)
        {
            disconnect(m_pController, SIGNAL(info(QString)), pcontrollerview, SLOT(updateControllerInfo(QString)));
            disconnect(m_pController, SIGNAL(newRPMValue(float)), pcontrollerview, SLOT(updateActualTopDriveRPM(float)));
            disconnect(m_pController, SIGNAL(initialized(QHash<QString,QString>)), pcontrollerview, SLOT(controllerInitialized(QHash<QString,QString>)));
            disconnect(m_pController, SIGNAL(updateMode(QString)), pcontrollerview, SLOT(updateModeStatus(QString)));
            disconnect(m_pController, SIGNAL(updateRPMCorrection(float,bool)), pcontrollerview, SLOT(handleNewRPMCorrection(float,bool)));
        }

        disconnect(this, SIGNAL(newRPM(float)), pcontrollerview, SLOT(updateTopDriveTargetRPM(float)));
    }
}


void ConnectionManager::addBackEnd(BackEnd* pbackend)
{
    if(pbackend != nullptr)
    {
        if(m_pControllerView != nullptr)
        {
            connect(m_pControllerView, SIGNAL(facilityOn(bool)), pbackend, SLOT(startFacility(bool)));
            connect(m_pControllerView, SIGNAL(facilityQuickStop()), pbackend, SLOT(quickStopFacility()));
            connect(m_pControllerView, SIGNAL(setLimits(float,float)), pbackend, SLOT(setLimits(float,float)));
            connect(m_pControllerView, SIGNAL(setRanges(float,float,float,float)), pbackend, SLOT(setRanges(float,float,float,float)));

            connect(pbackend, SIGNAL(statusUpdate(QHash<QString,QString>)), m_pControllerView, SLOT(newStatusUpdate(QHash<QString,QString>)));
            connect(pbackend, SIGNAL(updateFlowRate(float)), m_pControllerView, SLOT(updateFlowRate(float)));
            connect(pbackend, SIGNAL(updatePressureBotton(float)), m_pControllerView, SLOT(updatePressureBotton(float)));
            connect(pbackend, SIGNAL(updatePressureDrop(float)), m_pControllerView,  SLOT(updatePressureDrop(float)));
            connect(pbackend, SIGNAL(updatePressureTop(float)), m_pControllerView, SLOT(updatePressureTop(float)));
            connect(pbackend, SIGNAL(updateLimits(float,float)), m_pControllerView, SLOT(updateLimits(float,float)));
            connect(pbackend, SIGNAL(updateRanges(float,float,float,float)), m_pControllerView, SLOT(updateRanges(float,float,float,float)));
            connect(pbackend, SIGNAL(updateTorque(float)), m_pControllerView, SLOT(updateTopDriveTorque(float)));
            connect(pbackend, SIGNAL(systemLogUpdateEntries(QHash<QString,QString>)), this, SLOT(updateSystemLogInfo(QHash<QString,QString>)));
        }

        addTool(pbackend->getTool());
        addMudPump(pbackend->getMudPump());
        addTopDrive(pbackend->getTopDrive());
        m_pBackend = pbackend;
        startSystemLog();
    }
}

void ConnectionManager::removeBackEnd(BackEnd* pbackend)
{
    if(pbackend != nullptr && pbackend == m_pBackend)
    {
        disconnect(pbackend, nullptr, nullptr, nullptr);

        if(m_pControllerView != nullptr)
        {
            disconnect(m_pControllerView, SIGNAL(facilityOn(bool)), pbackend, SLOT(startFacility(bool)));
            disconnect(m_pControllerView, SIGNAL(facilityQuickStop()), pbackend, SLOT(quickStopFacility()));
            disconnect(m_pControllerView, SIGNAL(setLimits(float,float)), pbackend, SLOT(setLimits(float,float)));
            disconnect(m_pControllerView, SIGNAL(setRanges(float,float,float,float)), pbackend, SLOT(setRanges(float,float,float,float)));
        }

        removeTool(pbackend->getTool());
        removeMudPump(pbackend->getMudPump());
        removeTopDrive(pbackend->getTopDrive());

        m_pBackend = nullptr;
    }

}


void ConnectionManager::addController(Controller* pcontroller)
{
    if(pcontroller != nullptr)
    {
        if(m_pTopDrive != nullptr)
        {
            connect(m_pTopDrive, SIGNAL(newActualRPM(float)), pcontroller, SLOT(updateTopDriveRPM(float)));
//            connect(m_pTopDrive, SIGNAL(newRPMValue(float)), pcontroller, SLOT(updateRPM(float)));
            connect(pcontroller, SIGNAL(newRPMValue(float)), m_pTopDrive, SLOT(updateRPM(float)));
        }

        if(m_pTool != nullptr)
        {
            connect(m_pTool, SIGNAL(newRPMValue(float)), pcontroller, SLOT(updateDownholeRPM(float)));
            connect(m_pTool, SIGNAL(newToolfaceValue(float)), pcontroller, SLOT(updateToolFace(float)));
            connect(m_pTool, SIGNAL(newToolfaceValueFullRate(float)), pcontroller, SLOT(updateToolFaceFullRate(float)));
        }

        if(m_pControllerView != nullptr)
        {
            connect(m_pControllerView, SIGNAL(configure()), pcontroller, SLOT(configure()));
//            connect(m_pControllerView, SIGNAL(showDiagnostics()), pcontroller, SLOT(showDiagnostics()));
            connect(m_pControllerView, SIGNAL(controllerModeChanged(ControllerMode)), pcontroller, SLOT(updateControllerMode(ControllerMode)));
            connect(m_pControllerView, SIGNAL(TargetToolfaceChanged(float)), pcontroller, SLOT(setTargetToolface(float)));
            connect(m_pControllerView, SIGNAL(topDriveRPMChanged(float)), pcontroller, SLOT(updateTopDriveRPM(float)));
            connect(pcontroller, SIGNAL(info(QString)), m_pControllerView, SLOT(updateControllerInfo(QString)));
            connect(pcontroller, SIGNAL(newRPMValue(float)), m_pControllerView, SLOT(updateActualTopDriveRPM(float)));
            connect(pcontroller, SIGNAL(initialized(QHash<QString,QString>)), m_pControllerView, SLOT(controllerInitialized(QHash<QString,QString>)));
            connect(pcontroller, SIGNAL(updateMode(QString)), m_pControllerView, SLOT(updateModeStatus(QString)));
            connect(pcontroller, SIGNAL(updateRPMCorrection(float,bool)), m_pControllerView, SLOT(handleNewRPMCorrection(float,bool)));
        }

        connect(pcontroller, SIGNAL(updateLogInfo(QHash<QString,QString>)), this, SLOT(updateControllerLogInfo(QHash<QString,QString>)));
        connect(pcontroller, SIGNAL(newRPMValue(QHash<QString,QString>)), this, SLOT(newRPMValue(QHash<QString,QString>)));
        connect(pcontroller, SIGNAL(newDrillAheadAdjustment(float)), this, SLOT(handleDrillHeadAdjustment(float)));
        connect(pcontroller, SIGNAL(systemLogUpdateEntries(QHash<QString,QString>)), this, SLOT(updateSystemLogInfo(QHash<QString,QString>)));

        if(pcontroller->providesDownholeRPM() && (m_pTool != nullptr) && m_pControllerView != nullptr)
        {
            disconnect(m_pTool, SIGNAL(newRPMValue(float)), m_pControllerView, SLOT(updateDownHoleRPM(float)));
            connect(pcontroller, SIGNAL(updateDownholeRPMFromController(float)), m_pControllerView, SLOT(updateDownHoleRPM(float)));
        }

        m_pController = pcontroller;
        startSystemLog();
    }
}

void ConnectionManager::removeController(Controller* pcontroller)
{
    if(pcontroller != nullptr && pcontroller == m_pController)
    {
        disconnect(pcontroller, nullptr, nullptr, nullptr);

        if(m_pTool != nullptr)
        {
            disconnect(m_pTool, SIGNAL(newRPMValue(float)), pcontroller, SLOT(updateDownholeRPM(float)));
            disconnect(m_pTool, SIGNAL(newToolfaceValue(float)), pcontroller, SLOT(updateToolFace(float)));
            disconnect(m_pTool, SIGNAL(newToolfaceValueFullRate(float)), pcontroller, SLOT(updateToolFaceFullRate(float)));
        }

        if(m_pTopDrive != nullptr)
        {
            disconnect(m_pTopDrive, SIGNAL(newActualRPM(float)), pcontroller, SLOT(updateTopDriveRPM(float)));
            disconnect(m_pTopDrive, SIGNAL(newRPMValue(float)), pcontroller, SLOT(updateRPM(float)));
        }

        if(m_pControllerView != nullptr)
        {
            disconnect(m_pControllerView, SIGNAL(configure()), pcontroller, SLOT(configure()));
//            disconnect(m_pControllerView, SIGNAL(showDiagnostics()), pcontroller, SLOT(showDiagnostics()));
            disconnect(m_pControllerView, SIGNAL(controllerModeChanged(ControllerMode)), pcontroller, SLOT(updateControllerMode(ControllerMode)));
            disconnect(m_pControllerView, SIGNAL(TargetToolfaceChanged(float)), pcontroller, SLOT(setTargetToolface(float)));
            disconnect(m_pControllerView, SIGNAL(topDriveRPMChanged(float)), pcontroller, SLOT(updateTopDriveRPM(float)));
        }

        m_pController = nullptr;

        if(m_logFileHandle != -1)
        {
            Utils::closeFile(m_logFileHandle);
            m_logFileHandle = -1;
        }

        m_header.clear();
    }
}

void ConnectionManager::updateControllerLogInfo(QHash<QString,QString> datamap)
{
    qCDebug(CONNECTIONMANAGER) << "!!! map from controller " << datamap;
    bool writethisline = true;

    if(datamap.contains(CONTROLLER_STARTLOG))
    {
       m_header.clear();
       qCDebug(CONNECTIONMANAGER) << "Clearing controller log header ";
    }

    if(m_header.isEmpty())
    {
        QStringList keys = datamap.keys();
        QString header;

        if(datamap.contains(HEADERSTRING))
        {
            QString headerlist = datamap.value(HEADERSTRING);
            QStringList parts = headerlist.split(",");


            for(int i=0;i<parts.size();i++)
            {
                m_header << parts.at(i).trimmed();
            }

            writethisline = false;
            qCDebug(CONNECTIONMANAGER) << "Using this header " << m_header;
        }
        else
        {
            qCDebug(CONNECTIONMANAGER) << "Using default  header ";
            m_header = keys;
        }

        for(int i=0;i<m_header.size();i++)
        {
            header += m_header.at(i);

            if(i<m_header.size()-1)
            {
                header += ",";
            }
        }

        if(m_pController != nullptr)
        {
            QString logfilepath = Utils::getControllerLogPath() + "/" + m_pController->getLogFileName();

            m_logFileHandle = Utils::createLogFile(logfilepath);
            Utils::writeLogFile(m_logFileHandle, header);
            qCDebug(CONNECTIONMANAGER) << "created new file with this header " << header;
        }
    }

    if(writethisline)
    {
        QString line;

        for(int i=0;i<m_header.size();i++)
        {
            QString key = m_header.at(i);
            QString part = datamap.contains(key)?datamap.value(key):"";

            line += part;

            if(i<m_header.size()-1)
            {
                line += ",";
            }
        }

        Utils::writeLogFile(m_logFileHandle, line);
    }
}


void ConnectionManager::newRPMValue(QHash<QString,QString> controlvalues)
{
    qCDebug(CONNECTIONMANAGER) << "inside newRPMValue with these props " << controlvalues;

    if(!m_correctionPending && controlvalues.contains(NEW_RPM_VALUE))
    {
        QString newrpmstr = controlvalues.value(NEW_RPM_VALUE);
        float newrpmvalue = newrpmstr.toFloat();

        qCDebug(CONNECTIONMANAGER) << "setting this rpm value " << newrpmvalue;

        emit newRPM(newrpmvalue);

        if(controlvalues.contains(RPM_HOLD_TIME) && controlvalues.contains(RPM_END_VALUE))
        {
            QString holdtimestr = controlvalues.value(RPM_HOLD_TIME);
            QString resetrpmstr = controlvalues.value(RPM_END_VALUE);
            int holdtime = holdtimestr.toInt();

            m_rpmResetValue = resetrpmstr.toFloat();
            QTimer::singleShot(holdtime, this, SLOT(resetRPMValue()));
            qCDebug(CONNECTIONMANAGER) << "scheduling this rpm " << m_rpmResetValue << " for " << holdtime << " milliseconds";
            m_correctionPending = true;
        }
    }

}

void ConnectionManager::resetRPMValue()
{
    if(m_rpmResetValue > 0)
    {
        qCDebug(CONNECTIONMANAGER) << "holding at this rpm " << m_rpmResetValue;
        emit newRPM(m_rpmResetValue);
        m_rpmResetValue = -1.0;
    }

    m_correctionPending = false;
}


void ConnectionManager::handleDrillHeadAdjustment(float rpm)
{
    if(m_rpmResetValue > 0)
    {
       qCDebug(CONNECTIONMANAGER) << "Drill ahead adjustment overriding single shot reset value with " << rpm;
       m_rpmResetValue = rpm;
    }
    else
    {
       qCDebug(CONNECTIONMANAGER) << "Drill ahead ajustment setting RPM to " << rpm << " now";
       emit newRPM(rpm);
    }
}


void ConnectionManager::startSystemLog()
{
    QStringList header;
    QList<BaseInitializer*> components;


    if(m_sysremLogFileHandle != -1)
    {
        writeSystemLogRowCache(true);
        Utils::closeFile(m_sysremLogFileHandle);
        m_sysremLogFileHandle = -1;
    }

    m_systemLogHeader.clear();
    m_currentSystemLogRow.clear();

    if(m_pBackend != nullptr)
    {
        components << m_pBackend;
    }

    if(m_pTool != nullptr)
    {
        components << m_pTool;
    }

    if(m_pController != nullptr)
    {
        components << m_pController;
    }

    for(int i=0;i<components.size();i++)
    {
        QStringList h = components.at(i)->getSystemLogHeaders();

        h.removeAll(SYSTEMLOGUNIXTIME);
        header.append(h);
    }

    header.insert(0, SYSTEMLOGUNIXTIME);
    header.insert(0, SYSTEMLOGTIME);
    m_systemLogHeader = header;

    QString logfilepath = Utils::getControllerLogPath() + "/" + "rsSystemData.csv";
    QString systemlogheader = m_systemLogHeader.join(',');

    m_sysremLogFileHandle = Utils::createLogFile(logfilepath);
    Utils::writeLogFile(m_sysremLogFileHandle, systemlogheader);
    qCDebug(CONNECTIONMANAGER) << "created system log file with this header " << systemlogheader;

}

void ConnectionManager::updateSystemLogInfo(QHash<QString,QString> datamap)
{
    if(datamap.contains(SYSTEMLOGUNIXTIME))
    {
        QString timekeystr = datamap.value(SYSTEMLOGUNIXTIME);
        unsigned int timekey = timekeystr.toUInt();
        QHash<QString,QString> logrow;

        if(m_currentSystemLogRowCache.contains(timekey))
        {
            logrow = m_currentSystemLogRowCache.value(timekey);
        }

        datamap.remove(timekeystr);

        QList<QString> keylist = datamap.keys();

        for(int i=0;i<keylist.size();i++)
        {
            QString rowkey = keylist.at(i);

            logrow.insert(rowkey, datamap.value(rowkey));
        }

        m_currentSystemLogRowCache.insert(timekey, logrow);

        if(m_currentSystemLogRowCache.size() > 10)
        {
            writeSystemLogRowCache();
        }
    }
}

void ConnectionManager::writeSystemLogRowCache(bool writeall)
{
    QList<unsigned int> keylist = m_currentSystemLogRowCache.keys();

    qSort(keylist);

    int rowstowrite = keylist.size();

    if(!writeall)
    {
        rowstowrite = keylist.size() > 3 ? (rowstowrite - 3):0;
    }

    for(int i=0;i<rowstowrite;i++)
    {
        unsigned int key = keylist.at(i);
        QHash<QString,QString> entries = m_currentSystemLogRowCache.value(key);
        QList<QString> rowvalues;

        entries.insert(SYSTEMLOGUNIXTIME, QString::number(key));
        entries.insert(SYSTEMLOGTIME, QDateTime::fromTime_t(key).toString("MM/dd/yy hh:mm:ss"));

        m_currentSystemLogRowCache.remove(key);

        for(int j=0;j<m_systemLogHeader.size();j++)
        {
            QString headervalue = m_systemLogHeader.at(j);

            if(entries.contains(headervalue))
            {
                rowvalues << entries.value(headervalue);
            }
            else
            {
                rowvalues << "";
            }
        }

        QString row = rowvalues.join(',');

        Utils::writeLogFile(m_sysremLogFileHandle, row);

    }
}


void ConnectionManager::mudPumpRpmChanged(float newrpm)
{
    if(m_pControllerView != nullptr)
    {
        emit m_pControllerView->mudPumpRPMChanged(newrpm);
    }

}

void ConnectionManager::topDriveRpmChanged(float newrpm)
{
    if(m_pControllerView != nullptr)
    {
        emit m_pControllerView->topDriveRPMChanged(newrpm);
    }

}

void ConnectionManager::targetToolFaceChanged(float newTFTarget)
{
    if(m_pControllerView != nullptr)
    {
        emit m_pControllerView->TargetToolfaceChanged(newTFTarget);
    }
}


void ConnectionManager::toolSampleIntervalChanged(int sampleinterval)
{
    if(m_pBackend != nullptr)
    {
        m_pBackend->setToolSampleRateMillis(sampleinterval);
    }
}

void ConnectionManager::toolPropogationDelayChanged(int delay)
{
    if(m_pBackend != nullptr)
    {
        m_pBackend->setToolPropogationDelayMillis(delay);
    }
}
