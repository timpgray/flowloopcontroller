#include "backendfactory.h"
#include "rs_constants.h"
#include "backendimplementations/impl/backendimpl.h"
#include "backendimplementations/legacy/legacybackendimpl.h"
#include "backendimplementations/testing/testbackendimpl.h"
#include "backendimplementations/testing/testingbackendimpla.h"

BackEndFactory::BackEndFactory()
{

}



QStringList BackEndFactory::listBackEnds()
{
    QStringList backendlist;

    backendlist << "Testing1" << TEST_BACKEND << LEGACY_BACKEND << CURRENT_BACKEND;

    return backendlist;
}

BackEnd* BackEndFactory::getBackEnd(QString name)
{
    BackEnd* pbackend = nullptr;

    if(name.isEmpty() || name == TEST_BACKEND)
    {
        pbackend = new TestingBackEndImplA();
    }

    if(name == LEGACY_BACKEND)
    {
        pbackend = new LegacyBackEndImpl();
    }
    if(name == CURRENT_BACKEND)
    {
        pbackend = new BackEndImpl();
    }


    return pbackend;
}
