#include "simplerose.h"
#include <qrect.h>
#include <qpainter.h>
#include <qdebug.h>
#include <qpen.h>
#include <qbrush.h>

#define RECTANGLE_ADJUSTMENT 28
#define COMPASS_STEPS 14

SimpleRose::SimpleRose(QWidget *parent) : QWidget(parent)
{
    m_targetAngle = 0;

    for(int i=0;i<COMPASS_STEPS;i++)
    {
        m_history.enqueue(-1000.0);
    }

    m_switchButton = new QPushButton(this);
    m_switchButton->setMinimumHeight(10);
    m_switchButton->setMinimumWidth(15);
    m_switchButton->setText("Grav");
    connect(m_switchButton, SIGNAL(clicked(bool)), this, SLOT(switchToMag()));

    m_clearButton = new QPushButton(this);
    m_clearButton->setMinimumHeight(10);
    m_clearButton->setMinimumWidth(15);
    m_clearButton->setText("Clear");
    connect(m_clearButton, SIGNAL(clicked(bool)), this, SLOT(clear()));

    m_unitsList << "0" << "45" << "90" << "135" << "180" << "-135" << "-90" << "-45";
}


void SimpleRose::addToolFaceValue(float value)
{
    m_history.enqueue(-value + 90);

    if(m_history.size() > COMPASS_STEPS)
    {
        m_history.dequeue();
    }

    update();
}


void SimpleRose::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);
    QPainter p(this);
    QRect initial = constrainGeometry();
    QRect geo = QRect(RECTANGLE_ADJUSTMENT, RECTANGLE_ADJUSTMENT, initial.width()-(RECTANGLE_ADJUSTMENT*2), initial.height()-(RECTANGLE_ADJUSTMENT*2));
    int x = RECTANGLE_ADJUSTMENT;
    int y = RECTANGLE_ADJUSTMENT;
    int wwidth = geo.width();
    int wheight = geo.height();
    int step = wheight/40.0;
    float innertoolface = 0;

    p.setRenderHint(QPainter::Antialiasing);

    for(int i=0;i<m_history.size();i++)
    {
        x += step;
        y += step;
        wwidth -= step*2;
        wheight -= step*2;

        if(m_history.at(i) != -1000.0)
        {
            p.setPen(QPen(QBrush(Qt::red), 5));
            p.drawArc(x,y,wwidth,wheight, (m_history.at(i)*16) - (3*16), 16*6);
        }

        p.setPen(QPen(QBrush(Qt::gray), 1));
        p.drawEllipse(x+2.5,y+2.5,wwidth-5,wheight-5);
        innertoolface = m_history.at(i);
    }

    QString tfstr;

    if(innertoolface != -1000.0)
    {
        innertoolface = -(innertoolface -90);
        tfstr = QString::number(innertoolface, 'f', 1);
    }
    else
    {
        tfstr = "----";
    }

    int centerx = geo.width()/2;
    int centery = geo.height()/2;

    centerx += RECTANGLE_ADJUSTMENT;
    centery += RECTANGLE_ADJUSTMENT;

    QRect textrec(centerx-14, centery-7, 28, 14);

    p.setPen(QPen(QBrush(Qt::black), 1));
    p.drawText(textrec, Qt::AlignCenter, tfstr);
    p.setPen(QPen(QBrush(Qt::green), 5));
    drawTick(m_targetAngle, &p, true);
    p.setPen(QPen(QBrush(Qt::black), 1));

    float angle = 0;
    p.setPen(QPen(QBrush(Qt::gray), 1));

    for(int i=0;i<16;i++)
    {
        drawTick(angle, &p);
        angle += 22.5;
    }

    p.setPen(QPen(QBrush(Qt::black), 2));
    drawReferenceLine(m_targetAngle, &p);
    drawUnits(&p);
}


void SimpleRose::drawTick(float angle, QPainter* p, bool inside)
{
    angle -= 90;

    QRect initial = constrainGeometry();
    QRect geo = QRect(RECTANGLE_ADJUSTMENT, RECTANGLE_ADJUSTMENT, initial.width()-(RECTANGLE_ADJUSTMENT*2), initial.height()-(RECTANGLE_ADJUSTMENT*2));
    int step = geo.height()/40;
    int centerx = geo.width()/2;
    int centery = geo.height()/2;
    int innerradius = geo.width()/2 - step*COMPASS_STEPS;
    int outerradius = geo.width()/2;

    centerx += RECTANGLE_ADJUSTMENT;
    centery += RECTANGLE_ADJUSTMENT;

    if(inside)
    {
//        innerradius = geo.width()/2 - step*19;
//        outerradius = geo.width()/2 - step*18.5;
        innerradius = geo.width()/2 - step*(COMPASS_STEPS +1);
        outerradius = geo.width()/2 - step*(COMPASS_STEPS + 0.5);
    }

    float radians =  angle/360.0F * 2 * 3.1415;
    float starty = sin(radians) * innerradius;
    float startx = cos(radians) * innerradius;
    float endy = sin(radians) * outerradius;
    float endx = cos(radians) * outerradius;

    starty = centery + starty;
    startx = centerx + startx;
    endy = centery + endy;
    endx = centerx + endx;

    p->drawLine(startx, starty, endx, endy);

}

void SimpleRose::drawReferenceLine(float angle, QPainter* p)
{
    angle -= 90;

    QRect initial = constrainGeometry();
    QRect geo = QRect(RECTANGLE_ADJUSTMENT, RECTANGLE_ADJUSTMENT, initial.width()-(RECTANGLE_ADJUSTMENT*2), initial.height()-(RECTANGLE_ADJUSTMENT*2));
    int step = geo.height()/40;
    int centerx = geo.width()/2;
    int centery = geo.height()/2;
    int innerradius = geo.width()/2 - step*COMPASS_STEPS;
    int outerradius = geo.width()/2;

    centerx += RECTANGLE_ADJUSTMENT;
    centery += RECTANGLE_ADJUSTMENT;


    float radians =  angle/360.0F * 2 * 3.1415;
    float starty = sin(radians) * innerradius;
    float startx = cos(radians) * innerradius;
    float endy = sin(radians) * outerradius;
    float endx = cos(radians) * outerradius;

    starty = centery + starty;
    startx = centerx + startx;
    endy = centery + endy;
    endx = centerx + endx;

    p->drawLine(startx, starty, endx, endy);

}

void SimpleRose::drawUnits(QPainter* p)
{
    QRect textrec(0, 0, 28, 14);
    QList<float> angles;
    QList<QString> textvalues;
    QRect initial = constrainGeometry();
    QRect geo = QRect(RECTANGLE_ADJUSTMENT, RECTANGLE_ADJUSTMENT, initial.width()-(RECTANGLE_ADJUSTMENT*2), initial.height()-(RECTANGLE_ADJUSTMENT*2));
    int centerx = geo.width()/2;
    int centery = geo.height()/2;
    int outerradius = geo.width()/2;

    centerx += RECTANGLE_ADJUSTMENT;
    centery += RECTANGLE_ADJUSTMENT;

    angles << 0 << 45 << 90 <<  135 << 180 << 225 << 270 << 315;

    for(int i=0;i<angles.size();i++)
    {
        float nextangle = angles.at(i);
        QString nexttext = m_unitsList.at(i);

        nextangle -= 90;


        float radians =  nextangle/360.0F * 2 * 3.1415;
        float endy = sin(radians) * outerradius;
        float endx = cos(radians) * outerradius;

        endy = centery + endy;
        endx = centerx + endx;


        switch((int)angles.at(i))
        {
        case 0:
            textrec.moveTo(endx - textrec.width()/2, endy - textrec.height());
            break;
        case 45:
            textrec.moveTo(endx, endy - textrec.height());
            break;
        case 90:
            textrec.moveTo(endx, endy - textrec.height()/2);
            break;
        case 135:
            textrec.moveTo(endx, endy);
            break;
        case 180:
            textrec.moveTo(endx - textrec.width()/2, endy);
            break;
        case 225:
            textrec.moveTo(endx - textrec.width(), endy);
            break;
        case 270:
            textrec.moveTo(endx - textrec.width(), endy - textrec.height()/2);
            break;
        case 315:
            textrec.moveTo(endx - textrec.width(), endy - textrec.height());
            break;
        }

        p->drawText(textrec, Qt::AlignCenter, nexttext);
    }

}


QRect SimpleRose::constrainGeometry()
{
    QRect initial = geometry();
    int width = initial.width();
    int height = initial.height();
    int limit = (width > height) ? height:((height > width) ? width:height);

    initial.moveTo((initial.x() + (width-limit)/2), (initial.y() + (height-limit)/2));
    initial.setWidth(limit);
    initial.setHeight(limit);
    return initial;
}

void SimpleRose::resizeEvent(QResizeEvent *event)
{
   QSize sz = size();
   int w = sz.width();
   int h = sz.height();

   if(m_switchButton != nullptr)
   {
        m_switchButton->setGeometry(w-30, h-20, 30, 20);
   }

   if(m_clearButton != nullptr)
   {
        m_clearButton->setGeometry(15, h-20, 30, 20);
   }
}

void SimpleRose::switchToMag()
{
    if(m_switchButton->text() == "Grav")
    {
        m_switchButton->setText("Mag");
        m_unitsList.clear();
        m_unitsList << "0" << "45" << "90" << "135" << "180" << "225" << "270" << "315";
    }
    else
    {
        m_switchButton->setText("Grav");
        m_unitsList.clear();
        m_unitsList << "0" << "45" << "90" << "135" << "180" << "-135" << "-90" << "-45";
    }

    update();
}


void SimpleRose::clear()
{
    m_history.clear();

    for(int i=0;i<COMPASS_STEPS;i++)
    {
        m_history.enqueue(-1000.0);
    }

    update();
}
