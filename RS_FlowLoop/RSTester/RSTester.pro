#-------------------------------------------------
#
# Project created by QtCreator 2017-05-05T17:35:15
#
#-------------------------------------------------

QT       += core gui serialport quickwidgets qml charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RSTester
TEMPLATE = app

RC_FILE = rstester.rc


SOURCES += main.cpp\
        testmainwindow.cpp \
    LED.cpp \
    backend.cpp \
    topdrive.cpp \
    mudpump.cpp \
    tool.cpp \
    baseinitializer.cpp \
    controller.cpp \
    controllerview.cpp \
    backendfactory.cpp \
    backendimplementations/testing/testbackendimpl.cpp \
    backendimplementations/legacy/legacybackendimpl.cpp \
    backendimplementations/impl/backendimpl.cpp \
    controllerviewimplementations/controllerviewimpl.cpp \
    controllerviewfactory.cpp \
    controllerimplementations/simplecontroller.cpp \
    controllerfactory.cpp \
    backendimplementations/testing/mudpumptestingimpl.cpp \
    backendimplementations/testing/tooltestingimpl.cpp \
    backendimplementations/testing/topdrivetestingimpl.cpp \
    connectionmanager.cpp \
    utils.cpp \
    numberinput.cpp \
    controllerimplementations/simplecontrollerconfig.cpp \
    backendimplementations/testing/flowtester.cpp \
    backendimplementations/testing/speedo_meter.cpp \
    backendimplementations/testing/testingbackendimpla.cpp \
    backendimplementations/legacy/legacymudpumpimpl.cpp \
    backendimplementations/legacy/legacytopdriveimpl.cpp \
    backendimplementations/legacy/legacytoolimpl.cpp \
    backendimplementations/legacy/iecrunner.cpp \
    controllerimplementations/pidcontroller.cpp \
    controllerimplementations/pidcontrollerconfigdialog.cpp \
    simplerose.cpp \
    controllerimplementations/basecontrollerconfig.cpp \
    controllerimplementations/controllerbase.cpp \
    controllerimplementations/singleshotcontroller.cpp \
    controllerimplementations/singleshotcontrollerconfiguration.cpp \
    backendimplementations/legacy/witstool.cpp \
    graphparser.cpp \
    controllerimplementations/tfbasedcontroller.cpp \
    controllerimplementations/derivedrpmcontroller.cpp \
    controllerimplementations/tfconfiguration.cpp

HEADERS  += testmainwindow.h \
    LED.h \
    backend.h \
    topdrive.h \
    mudpump.h \
    tool.h \
    baseinitializer.h \
    controller.h \
    controllerview.h \
    rs_constants.h \
    backendfactory.h \
    backendimplementations/testing/testbackendimpl.h \
    backendimplementations/legacy/legacybackendimpl.h \
    backendimplementations/impl/backendimpl.h \
    controllerviewimplementations/controllerviewimpl.h \
    controllerviewfactory.h \
    controllerimplementations/simplecontroller.h \
    controllerfactory.h \
    backendimplementations/testing/mudpumptestingimpl.h \
    backendimplementations/testing/tooltestingimpl.h \
    backendimplementations/testing/topdrivetestingimpl.h \
    connectionmanager.h \
    utils.h \
    numberinput.h \
    controllerimplementations/simplecontrollerconfig.h \
    backendimplementations/testing/flowtester.h \
    backendimplementations/testing/speedo_meter.h \
    backendimplementations/testing/testingbackendimpla.h \
    backendimplementations/legacy/legacymudpumpimpl.h \
    backendimplementations/legacy/legacytopdriveimpl.h \
    backendimplementations/legacy/legacytoolimpl.h \
    backendimplementations/legacy/iecrunner.h \
    controllerimplementations/pidcontroller.h \
    controllerimplementations/pidcontrollerconfigdialog.h \
    simplerose.h \
    controllerimplementations/basecontrollerconfig.h \
    controllerimplementations/controllerbase.h \
    controllerimplementations/singleshotcontroller.h \
    controllerimplementations/singleshotcontrollerconfiguration.h \
    backendimplementations/legacy/witstool.h \
    graphparser.h \
    controllerimplementations/tfbasedcontroller.h \
    controllerimplementations/derivedrpmcontroller.h \
    controllerimplementations/tfconfiguration.h

FORMS    += testmainwindow.ui \
    numberinput.ui \
    controllerimplementations/simplecontrollerconfig.ui \
    backendimplementations/testing/flowtester.ui \
    controllerimplementations/pidcontrollerconfigdialog.ui \
    controllerimplementations/singleshotcontrollerconfiguration.ui \
    controllerimplementations/tfconfiguration.ui

RESOURCES += \
    resources.qrc



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../qwt_libs/debug/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../qwt_libs/debug/ -lqwtd
else:unix: LIBS += -L$$PWD/../../qwt_libs/debug/ -lqwtd

INCLUDEPATH += $$PWD/../../qwt_libs/includes
DEPENDPATH += $$PWD/../../qwt_libs/includes

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../legacybackend/release/ -llegacybackend
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../legacybackend/debug/ -llegacybackend
else:unix: LIBS += -L$$OUT_PWD/../legacybackend/ -llegacybackend

INCLUDEPATH += $$PWD/../legacybackend
DEPENDPATH += $$PWD/../legacybackend

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../legacybackend/release/liblegacybackend.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../legacybackend/debug/liblegacybackend.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../legacybackend/release/legacybackend.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../legacybackend/debug/legacybackend.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../legacybackend/liblegacybackend.a
