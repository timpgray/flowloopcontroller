#ifndef BASEINITIALIZER_H
#define BASEINITIALIZER_H

#include <QObject>
#include <qstring.h>
#include <qhash.h>
#include <qstringlist.h>

class BaseInitializer : public QObject
{
    Q_OBJECT
public:
    explicit BaseInitializer(QObject *parent = 0);
    virtual ~BaseInitializer(){}

    virtual void initialize(QHash<QString,QString>) = 0;
    virtual QString getName()=0;

    virtual QStringList getSystemLogHeaders(){return QStringList();}

signals:
    void newStatus(bool status);
    void statusUpdate(QHash<QString,QString>);
    void systemLogUpdateEntries(QHash<QString,QString> entries);

public slots:
};

#endif // BASEINITIALIZER_H
