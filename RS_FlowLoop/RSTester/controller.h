#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "baseinitializer.h"
#include <qhash.h>
#include <qstring.h>

enum ControllerMode
{
    OFF,
    Idle,
    DrillAhead,
    RPMNull,
    Steering
};

class Controller : public BaseInitializer
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = 0);
    virtual ~Controller(){}

    virtual QString getDescription()=0;
    virtual QString getLogFileName()=0;
    virtual bool loadProps(QString path) = 0;
    virtual bool writeConfig(QString currentconfigkey, QString configlistkey);
    virtual bool providesDownholeRPM(){return false;}

signals:
    void newRPMValue(float rpm);
    void newRPMValue(QHash<QString,QString> controlvalues);
    void newDrillAheadAdjustment(float rpm);
    void newStatusDesc(QString desc);
    void info(QString information);
    void updateLogInfo(QHash<QString,QString>);
    void initialized(QHash<QString,QString>);
    void updateMode(QString msg);
    void updateRPMCorrection(float rpmdelta, bool isrunning);
    void updateDownholeRPMFromController(float rpm);

public slots:
    virtual void updateTopDriveRPM(float rpm)=0;
    virtual void updateToolFace(float toolface)=0;
    virtual void updateControllerMode(ControllerMode mode)=0;
    virtual void setTargetToolface(float target) = 0;
    virtual void updateDownholeRPM(float rpm)=0;
    virtual void configure()=0;
    virtual void showDiagnostics()=0;
    virtual void updateDrillAheadOffset(float newoffset)=0;
    virtual void updateToolFaceFullRate(float toolface){}

protected:
    QStringList m_configFiles;
    QString m_lastconfig;
};

#endif // CONTROLLER_H
