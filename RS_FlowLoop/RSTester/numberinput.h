#ifndef NUMBERINPUT_H
#define NUMBERINPUT_H

#include <QDialog>

namespace Ui {
class NumberInput;
}

class NumberInput : public QDialog
{
    Q_OBJECT

public:
    explicit NumberInput(QWidget *parent = 0);
    ~NumberInput();

    void setLabel(QString labelvalue);
    void setValue(float current);
    float getValue();

    virtual int exec();
    virtual void  focusInEvent(QFocusEvent *event);
private slots:
    void on_uservalue_editingFinished();

private:
    Ui::NumberInput *ui;
    float m_userValue;
};

#endif // NUMBERINPUT_H
