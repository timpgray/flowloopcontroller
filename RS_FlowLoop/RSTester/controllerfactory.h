#ifndef CONTROLLERFACTORY_H
#define CONTROLLERFACTORY_H

#include <qstring.h>
#include "controller.h"

class ControllerFactory
{
private:
    ControllerFactory();

public:
    static Controller* getController(QString name);
    static QStringList getControllerNames();
};

#endif // CONTROLLERFACTORY_H
