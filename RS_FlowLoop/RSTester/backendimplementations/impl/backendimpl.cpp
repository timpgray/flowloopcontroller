#include "backendimpl.h"
#include "rs_constants.h"

BackEndImpl::BackEndImpl(QObject *parent) : BackEnd(parent)
{
    m_toolInterfaceNames << TOOL_WITS << TOOL_ROTONAV;
    m_topDriveInterfaceNames << TOPDRIVE_DEFAULT;
    m_mudPumpInterfaceNames << MUDPUMP_DEFAULT;

}

void BackEndImpl::initialize(QHash<QString,QString>)
{
    QHash<QString,QString> initmap;

    initmap.insert(MUDPUMP_STATUS, STATUS_FALSE);
    initmap.insert(TOOL_STATUS, STATUS_FALSE);
    initmap.insert(TOPDRIVE_STATUS, STATUS_FALSE);

    emit statusUpdate(initmap);
}

QString BackEndImpl::getName()
{
    return CURRENT_BACKEND;
}


MudPump* BackEndImpl::getMudPump(QString name)
{
    Q_UNUSED(name);

    return nullptr;
}

Tool* BackEndImpl::getTool(QString name)
{
    Q_UNUSED(name);

    return nullptr;
}

TopDrive* BackEndImpl::getTopDrive(QString name)
{
    Q_UNUSED(name);

    return nullptr;
}


void BackEndImpl::startFacility(bool start)
{
    Q_UNUSED(start);
    QHash<QString,QString> initmap;

    initmap.insert(FACILITY_STATUS, STATUS_FALSE);
    initmap.insert(FACILITY_STARTED, STATUS_FALSE);

    emit statusUpdate(initmap);
}

void BackEndImpl::quickStopFacility()
{
    QHash<QString,QString> initmap;

    initmap.insert(FACILITY_STATUS, STATUS_FALSE);
    initmap.insert(FACILITY_STARTED, STATUS_TRUE);

    emit statusUpdate(initmap);
}


void BackEndImpl::setLimits(float maxmudpumprpm, float maxtopdriverpm)
{
    Q_UNUSED(maxmudpumprpm);
    Q_UNUSED(maxtopdriverpm);
}

void BackEndImpl::setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)
{
    Q_UNUSED(toprange);
    Q_UNUSED(topoffset);
    Q_UNUSED(bottomrange);
    Q_UNUSED(bottomoffset);
}
