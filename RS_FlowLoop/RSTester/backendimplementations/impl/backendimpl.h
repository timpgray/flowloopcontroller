#ifndef BACKENDIMPL_H
#define BACKENDIMPL_H

#include <QObject>
#include "backend.h"

class BackEndImpl : public BackEnd
{
    Q_OBJECT
public:
    explicit BackEndImpl(QObject *parent = 0);
    virtual ~BackEndImpl(){}

    virtual void initialize(QHash<QString,QString>);
    virtual QString getName();


    virtual MudPump* getMudPump(QString name="");
    virtual Tool* getTool(QString name="");
    virtual TopDrive* getTopDrive(QString name="");

signals:

public slots:

    virtual void startFacility(bool start);
    virtual void quickStopFacility();
    virtual void setLimits(float maxmudpumprpm, float maxtopdriverpm);
    virtual void setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);

};

#endif // BACKENDIMPL_H
