#ifndef LEGACYTOOLIMPL_H
#define LEGACYTOOLIMPL_H

#include <QObject>
#include "tool.h"
#include <QTimerEvent>

#include <windows.h>

#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "RotaryNavCommand.h"
#include "RS232Port.h"
#include <qhash.h>


class LegacyToolImpl : public Tool
{
    Q_OBJECT
public:
    explicit LegacyToolImpl(QObject *parent = 0);
    virtual ~LegacyToolImpl();
    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();


    virtual void timerEvent(QTimerEvent* event);
    virtual void setToolPropogationDelayMillis(int delay){m_propogationDelay = delay;}
    virtual void setToolSampleRateMillis(int samplerate);

    virtual QStringList getSystemLogHeaders();


protected:
    int getToolData(RotaryNavDataGroup4Structure* tooldata);
signals:

public slots:

private:
    int m_timerId;
    bool m_initialized;
    int m_timerIntervalMillis;
    int m_timeoutCounter;

    int m_propogationDelay;
    QHash<int,RotaryNavDataGroup4Structure*> m_cachedData;
    int m_toolSampleRateDecimator;
    int m_toolSampleRateCounter;
};

#endif // LEGACYTOOLIMPL_H
