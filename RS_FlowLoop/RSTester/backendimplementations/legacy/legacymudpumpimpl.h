#ifndef LEGACYMUDPUMPIMPL_H
#define LEGACYMUDPUMPIMPL_H

#include <QObject>
#include "mudpump.h"
#include "legacybackendimpl.h"

class LegacyMudPumpImpl : public MudPump
{
    Q_OBJECT
public:
    explicit LegacyMudPumpImpl(QObject *parent = 0);
    virtual ~LegacyMudPumpImpl();
    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();

    void setBackend(LegacyBackEndImpl* pbackend){m_pBackend = pbackend;}

signals:

public slots:
    virtual void updateRPM(float rpm);
    virtual void pumpsOn(bool status);

    virtual void actualPumpRPMChanged(float rpm);

private:
    LegacyBackEndImpl* m_pBackend;
    bool m_currentState;
    float m_currentRPM;
};

#endif // LEGACYMUDPUMPIMPL_H
