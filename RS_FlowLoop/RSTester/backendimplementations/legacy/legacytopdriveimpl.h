#ifndef LEGACYTOPDRIVEIMPL_H
#define LEGACYTOPDRIVEIMPL_H

#include <QObject>
#include "topdrive.h"
#include "legacybackendimpl.h"

class LegacyTopDriveImpl : public TopDrive
{
    Q_OBJECT
public:
    explicit LegacyTopDriveImpl(QObject *parent = 0);
    virtual ~LegacyTopDriveImpl();
    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();

    void setBackend(LegacyBackEndImpl* pbackend){m_pBackend = pbackend;}

signals:

public slots:
    virtual void updateRPM(float rpm);
    virtual void topDriveOn(bool status);

    void actualTopDriveRPMChanged(float rpm);

private:
    LegacyBackEndImpl* m_pBackend;

    bool m_currentState;
    float m_currentRPM;
};

#endif // LEGACYTOPDRIVEIMPL_H
