#include "iecrunner.h"

#include <qloggingcategory.h>

QLoggingCategory IECRUNNER("IECRunner");


IECRunner::IECRunner(IECThreadClass *iecthread, QObject *parent) : QThread(parent)
{
    m_pIECThread = iecthread;
}

IECRunner::~IECRunner()
{

}



void IECRunner::run()
{
//    while(true)
//    {
//        qCDebug(IECRUNNER) << " inside run....";
//        msleep(5000);
//    }

    if(m_pIECThread != nullptr)
    {
        qCDebug(IECRUNNER) << " inside run...running IECThread";
        m_pIECThread->Execute();
    }
    else
    {
        qCDebug(IECRUNNER) << " inside run...thread object is null";
    }

    qCDebug(IECRUNNER) << " inside run...exiting";

}
