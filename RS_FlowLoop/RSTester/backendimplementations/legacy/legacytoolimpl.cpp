#include "legacytoolimpl.h"

#include <qloggingcategory.h>

#include "rs_constants.h"

#include <windows.h>

#include "CcsCompiler.h"
#include "NodeCommon.h"
#include "RotaryNavCommand.h"
#include "RS232Port.h"

#include <qfile.h>
#include <qdatetime.h>

extern char RotaryNavFirmwareId[2];
extern RotaryNavDataGroup3Structure RotaryNavData3;
extern RotaryNavDataGroup4Structure RotaryNavData4;

QLoggingCategory LEGACYTOOLIMPL("LegacyToolImpl");


LegacyToolImpl::LegacyToolImpl(QObject *parent) : Tool(parent)
{
    m_propogationDelay = 0;
    m_toolSampleRateDecimator = 1;
    m_toolSampleRateCounter = 0;
}

LegacyToolImpl::~LegacyToolImpl()
{
    RotaryNavCommand.close();
}


void LegacyToolImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(LEGACYTOOLIMPL)  << "initializing with these properties " << props;

    if(props.contains(LEGACY_TOOL_TIMERINTERVAL))
    {
        m_sampleRateMillis = props.value(LEGACY_TOOL_TIMERINTERVAL).toInt();
    }

    if(props.contains(ROTONAV_COMPORT) && props.contains(ROTONAV_BAUDRATE))
    {
        QString comportstr = props.value(ROTONAV_COMPORT);
        QString baudratestr = props.value(ROTONAV_BAUDRATE);
        int baudrate = baudratestr.toInt();
        int comport = comportstr.toInt();

//        RotaryNavCommand.m_serialPort.setBaudRate(baudrate);
//        RotaryNavCommand.m_serialPort.setPortName(QString("COM") + QString::number(comport));
//        bool initialized = RotaryNavCommand.m_serialPort.open(QFile::ReadWrite);
//        bool initialized = RS232Port2.Initialize(comport, baudrate, 8, PARITY_NONE, DUPLEX_HALF);
        bool initialized = RotaryNavCommand.initialize(comport, baudrate);

        qCDebug(LEGACYTOOLIMPL)  << "Serial port initiazed port = " << comport << " baudrate = " << baudrate << " status = " << initialized << " initialize flag = " << RS232Port2.InitializeFlagGet();

        if(initialized)
        {
            qCDebug(LEGACYTOOLIMPL)  << "Serial port initialized - starting timer";
            m_timerID = startTimer(m_sampleRateMillis);
        }

        QHash<QString,QString> statusmap;
        QString status = initialized ? STATUS_TRUE:STATUS_FALSE;

        statusmap.insert(TOOL_STATUS, status);
        emit statusUpdate(statusmap);
        m_toolSampleRateDecimator = 1;
        m_toolSampleRateCounter = 0;
    }
}

QString LegacyToolImpl::getName()
{
   return TOOL_ROTONAV_LEGACY;
}


void LegacyToolImpl::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    int timerid = event->timerId();

    if(m_cachedData.contains(timerid))
    {
        RotaryNavDataGroup4Structure*  tooldata = m_cachedData.value(timerid);
        float toolface = tooldata->GravityToolFaceDegrees;
        float rpm = tooldata->RevsPerMinute;
        float temp = tooldata->TemperatureCelsius;
        float inc = tooldata->InclinationDegrees;
        float batv = tooldata->BatteryVolts;
        float tempf = (temp * (9.0/5.0)) + 32.0;

        m_cachedData.remove(timerid);
        killTimer(timerid);

        emit newRPMValue(rpm);
        emit newToolfaceValue(toolface);

        emit newTelemetryValue(RPM, rpm);
        emit newTelemetryValue(TOOLFACE, toolface);
        emit newTelemetryValue(INCLINATION, inc);
        emit newTelemetryValue(BATV, batv);
        emit newTelemetryValue(TEMPERATURE, tempf);

        QHash<QString,QString> systemlogvalues;
        QString timestr = QString::number(QDateTime::currentDateTime().toTime_t());

        systemlogvalues.insert(SYSTEMLOGUNIXTIME, timestr);
        systemlogvalues.insert(TOOLFACE, QString::number(tooldata->GravityToolFaceDegrees, 'f', 3));
        systemlogvalues.insert(RPM, QString::number(tooldata->RevsPerMinute, 'f', 3));
        systemlogvalues.insert(BATV, QString::number(tooldata->BatteryVolts, 'f', 3));
        systemlogvalues.insert(TEMPERATURE, QString::number(tooldata->TemperatureCelsius, 'f', 3));
        systemlogvalues.insert(INCLINATION, QString::number(tooldata->InclinationDegrees, 'f', 3));
        systemlogvalues.insert(AZIMUTH, QString::number(tooldata->AzimuthDegrees, 'f', 3));
        systemlogvalues.insert(DIPANGLE, QString::number(tooldata->MagneticDipDegrees, 'f', 3));
        systemlogvalues.insert(TOTALMAG, QString::number(tooldata->TotalMagneticFieldGauss, 'f', 3));
        systemlogvalues.insert(TOTOLGRAV, QString::number(tooldata->TotalGravityFieldG, 'f', 3));
        emit systemLogUpdateEntries(systemlogvalues);


        delete tooldata;
    }
    else
    {
        qCDebug(LEGACYTOOLIMPL) <<  "inside timerEvent()";
        RotaryNavDataGroup4Structure*  tooldata = new RotaryNavDataGroup4Structure();
        int status = getToolData(tooldata);

        if(((m_toolSampleRateCounter++ % m_toolSampleRateDecimator) == 0) && (status == COM_GOOD))
        {
            qCDebug(LEGACYTOOLIMPL) <<  "inside timerEvent() - using decimated data";
            int id = startTimer(m_propogationDelay);
            m_cachedData.insert(id, tooldata);

            float toolface = tooldata->GravityToolFaceDegrees;
            float rpm = tooldata->RevsPerMinute;
            float temp = tooldata->TemperatureCelsius;
            float inc = tooldata->InclinationDegrees;
            float batv = tooldata->BatteryVolts;
            float tempf = (temp * (9.0/5.0)) + 32.0;


//            emit newToolfaceValue(toolface);
//            emit newRPMValue(rpm);

//            emit newTelemetryValue(TOOLFACE, toolface);
//            emit newTelemetryValue(RPM, rpm);
//            emit newTelemetryValue(INCLINATION, inc);
//            emit newTelemetryValue(BATV, batv);
//            emit newTelemetryValue(TEMPERATURE, tempf);

            qCDebug(LEGACYTOOLIMPL) << "read tool data toolface = " << toolface << " rpm = " << rpm <<
                                       " temp = " << tempf << " inc = " << inc << " battv = " << batv;

        }
        else
        {
            qCDebug(LEGACYTOOLIMPL) << "read of tool data failed status = " << status;
            delete tooldata;

        }

        QString toolstatus = (status == COM_GOOD) ? STATUS_TRUE:STATUS_WARN;
        QHash<QString,QString> statusmap;

        statusmap.insert(TOOL_STATUS, toolstatus);

        if(status != COM_GOOD)
        {
            statusmap.insert(BATTERY_STATUS, STATUS_FALSE);
        }

        emit statusUpdate(statusmap);
    }

}



int LegacyToolImpl::getToolData(RotaryNavDataGroup4Structure* tooldata)
{
    int status = COM_ERROR;
    RotaryNavDataGroup4Structure* datasrc = nullptr;


    status = RotaryNavCommand.DatagroupRead(ROTARYNAV_DATAGROUP_4);

    if (status == COM_GOOD)
    {
        datasrc = &RotaryNavData4;

        QString battstatus = datasrc->BatteryVolts > 20.0 ? STATUS_TRUE:STATUS_WARN;
        QHash<QString,QString> statusmap;

        statusmap.insert(BATTERY_STATUS, battstatus);
        emit statusUpdate(statusmap);



//        QHash<QString,QString> systemlogvalues;
//        QString timestr = QString::number(QDateTime::currentDateTime().toTime_t());

//        systemlogvalues.insert(SYSTEMLOGUNIXTIME, timestr);
//        systemlogvalues.insert(TOOLFACE, QString::number(datasrc->GravityToolFaceDegrees, 'f', 3));
//        systemlogvalues.insert(RPM, QString::number(datasrc->RevsPerMinute, 'f', 3));
//        systemlogvalues.insert(BATV, QString::number(datasrc->BatteryVolts, 'f', 3));
//        systemlogvalues.insert(TEMPERATURE, QString::number(datasrc->TemperatureCelsius, 'f', 3));
//        systemlogvalues.insert(INCLINATION, QString::number(datasrc->InclinationDegrees, 'f', 3));
//        systemlogvalues.insert(AZIMUTH, QString::number(datasrc->AzimuthDegrees, 'f', 3));
//        systemlogvalues.insert(DIPANGLE, QString::number(datasrc->MagneticDipDegrees, 'f', 3));
//        systemlogvalues.insert(TOTALMAG, QString::number(datasrc->TotalMagneticFieldGauss, 'f', 3));
//        systemlogvalues.insert(TOTOLGRAV, QString::number(datasrc->TotalGravityFieldG, 'f', 3));
//        emit systemLogUpdateEntries(systemlogvalues);

    }



    // copy the data
    if (tooldata != nullptr && datasrc != nullptr)
    {
        tooldata->Mode = datasrc->Mode;
        tooldata->NodeStatus = datasrc->NodeStatus;
        tooldata->TemperatureCelsius = datasrc->TemperatureCelsius;
        tooldata->AzimuthDegrees = datasrc->AzimuthDegrees;
        tooldata->TotalGravityFieldG = datasrc->TotalGravityFieldG;
        tooldata->TotalMagneticFieldGauss =
        datasrc->TotalMagneticFieldGauss;
        tooldata->MagneticDipDegrees = datasrc->MagneticDipDegrees;
        tooldata->InclinationDegrees = datasrc->InclinationDegrees;
        tooldata->BatteryVolts = datasrc->BatteryVolts;
        tooldata->GravityToolFaceDegrees =
        datasrc->GravityToolFaceDegrees;
        tooldata->RevsPerMinute = datasrc->RevsPerMinute;
        tooldata->MagneticToolFaceDegrees =
        datasrc->MagneticToolFaceDegrees;
    }




    return status;
}


void LegacyToolImpl::setToolSampleRateMillis(int samplerate)
{
    Tool::setToolSampleRateMillis(1000);
    int decimator = samplerate / 1000;

    if(decimator == 0)
    {
        decimator = 1;
    }

    m_toolSampleRateDecimator = decimator;
}


QStringList LegacyToolImpl::getSystemLogHeaders()
{
    QStringList headers;

    headers << SYSTEMLOGUNIXTIME << TOOL_START << TOOLFACE << RPM << BATV << TEMPERATURE << INCLINATION << AZIMUTH << DIPANGLE << TOTALMAG << TOTOLGRAV;
    return headers;
}
