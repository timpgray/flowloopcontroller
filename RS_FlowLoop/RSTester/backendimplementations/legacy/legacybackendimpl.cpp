

#include "legacybackendimpl.h"
#include "rs_constants.h"
#include "legacymudpumpimpl.h"
#include "legacytoolimpl.h"
#include "legacytopdriveimpl.h"
#include "iecnotifier.h"
#include "witstool.h"
#include "utils.h"

#include <qloggingcategory.h>
#include <qdatetime.h>

QLoggingCategory LEGACYBACKENDIMPL("LegacyBackEndImpl");


LegacyBackEndImpl::LegacyBackEndImpl(QObject *parent) : BackEnd(parent)
{
    m_toolInterfaceNames << TOOL_ROTONAV_LEGACY << TOOL_WITS_LEGACY;
    m_topDriveInterfaceNames << TOPDRIVE_LEGACY;
    m_mudPumpInterfaceNames << MUDPUMP_LEGACY;
    m_iecRunner = nullptr;
    m_pIECThreadObject = nullptr;
    m_pIECClassObject = nullptr;

    m_pTopDrive = new LegacyTopDriveImpl();
    m_pTool = createDefaultTool();
    m_pMudPump = new LegacyMudPumpImpl();
    m_controlGranted = false;
    m_commsCounter = 0;

    m_propogationDelay = 0;
}

LegacyBackEndImpl::~LegacyBackEndImpl()
{
    if(m_pIECThreadObject != nullptr)
    {
        m_pIECThreadObject->Stop();
        QThread::currentThread()->msleep(500);  // this is brute force - should fix it.
        delete m_pIECThreadObject;
    }

    if(m_iecRunner != nullptr)
    {
        delete m_iecRunner;
    }


    if(m_pIECClassObject != nullptr)
    {
        delete m_pIECClassObject;
    }

    qCDebug(LEGACYBACKENDIMPL) << "inside destructor for LegacyBackend";
}

void LegacyBackEndImpl::initialize(QHash<QString,QString> props)
{
    QHash<QString,QString> initmap;

    qCDebug(LEGACYBACKENDIMPL)  << "initializing with these properties " << props;

    QString iecstatus = STATUS_FALSE;

    if(props.contains(IEC_COMPORT) && props.contains(IEC_BAUDRATE))
    {
        QString comportstr = props.value(IEC_COMPORT);
        QString baudratestr = props.value(IEC_BAUDRATE);
        int baudrate = baudratestr.toInt();
        int comport = comportstr.toInt();
        IECNotifier* pnotifier = new IECNotifier();


        m_pIECThreadObject = new IECThreadClass(false, pnotifier);
        m_pIECClassObject = new IECClass();
        m_iecRunner = new IECRunner(m_pIECThreadObject);

        connect(pnotifier, SIGNAL(iecDataChanged()), this, SLOT(iecUpdated()));

        int status = m_pIECThreadObject->initialize(comport, baudrate);
        qCDebug(LEGACYBACKENDIMPL)  << "Serial port initiazed port = " << comport << " baudrate = " << baudrate << " status = " << status;

        m_iecRunner->start();
        m_pIECClassObject->Start(m_pIECThreadObject);

        m_pIECClassObject->RequestRotoSlideControl(true);
        iecstatus = status == 0 ? STATUS_FALSE:STATUS_TRUE;

        startTimer(2000);
    }


    initmap.insert(MUDPUMP_STATUS, iecstatus);
    initmap.insert(TOPDRIVE_STATUS, iecstatus);


    connect(this, SIGNAL(mudPumpActualRPMChanged(float)), m_pMudPump, SLOT(actualPumpRPMChanged(float)));
    connect(this, SIGNAL(topDriveActualRPMChanged(float)), m_pTopDrive, SLOT(actualTopDriveRPMChanged(float)));
    ((LegacyMudPumpImpl*)m_pMudPump)->setBackend(this);
    ((LegacyTopDriveImpl*)m_pTopDrive)->setBackend(this);

    connect(m_pTopDrive, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
    connect(m_pTool, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
    connect(m_pMudPump, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));


    m_pTopDrive->initialize(props);
    m_pTool->initialize(props);
    m_pMudPump->initialize(props);


    emit statusUpdate(initmap);
}

QString LegacyBackEndImpl::getName()
{
    return LEGACY_BACKEND;
}


MudPump* LegacyBackEndImpl::getMudPump(QString name)
{
    Q_UNUSED(name);
    return m_pMudPump;
}

Tool* LegacyBackEndImpl::getTool(QString name)
{
    if(name.isEmpty())
    {   // either returns existing tool or default implementation
        if(m_pTool == nullptr)
        {
            m_pTool = new LegacyToolImpl();
            connect(m_pTool, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
            Utils::updateConfigFile(CURRENTLEGACYTOOL, m_pTool->getName());
        }
    }
    else
    {
        if((m_pTool != nullptr) && (m_pTool->getName() != name))
        {   // if it is diferent from existing, delete it
            m_pTool->deleteLater();
            m_pTool = nullptr;

            if(name == TOOL_ROTONAV_LEGACY)
            {
                m_pTool = new LegacyToolImpl();
            }
            else
            {
                if(name == TOOL_WITS_LEGACY)
                {
                    m_pTool = new WitsTool();
                }
                else
                {
                    m_pTool = new LegacyToolImpl();
                }
            }

            if(m_pTool != nullptr)
            {
                connect(m_pTool, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
                Utils::updateConfigFile(CURRENTLEGACYTOOL, m_pTool->getName());
            }

        }

    }

    return m_pTool;
}

TopDrive* LegacyBackEndImpl::getTopDrive(QString name)
{
    Q_UNUSED(name);

    return m_pTopDrive;
}


void LegacyBackEndImpl::startFacility(bool start)
{
    QHash<QString,QString> initmap;

    QString startstatus = start ? STATUS_TRUE:STATUS_FALSE;

    initmap.insert(FACILITY_STATUS, STATUS_TRUE);
    initmap.insert(FACILITY_STARTED, startstatus);

    if(m_pIECClassObject != nullptr)
    {
        m_pIECClassObject->SetPumpRun(start);
    }

    emit statusUpdate(initmap);
}

void LegacyBackEndImpl::quickStopFacility()
{
    QHash<QString,QString> initmap;

    initmap.insert(FACILITY_STATUS, STATUS_FALSE);
    initmap.insert(FACILITY_STARTED, STATUS_TRUE);

    emit statusUpdate(initmap);

    if(m_pIECClassObject != nullptr)
    {
//        m_pIECClassObject->SetPumpRun(false);
//        m_pIECClassObject->SetTopDriveRun(false);
    }

}


void LegacyBackEndImpl::setLimits(float maxmudpumprpm, float maxtopdriverpm)
{
    Q_UNUSED(maxmudpumprpm);
    Q_UNUSED(maxtopdriverpm);

}

void LegacyBackEndImpl::setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)
{
    Q_UNUSED(toprange);
    Q_UNUSED(topoffset);
    Q_UNUSED(bottomrange);
    Q_UNUSED(bottomoffset);
}


void LegacyBackEndImpl::mudPumpsOn(bool status, float rpm)
{
    qCDebug(LEGACYBACKENDIMPL) << "Pump status = " << status << " speed " << rpm;
    if(m_pIECClassObject != nullptr)
    {
        m_pIECClassObject->SetPumpRun(status);
        m_pIECClassObject->SetPumpSpeed(rpm);
    }
}

void LegacyBackEndImpl::topDriveOn(bool status, float rpm)
{
    qCDebug(LEGACYBACKENDIMPL) << "Top drive status = " << status << " speed " << rpm;
    if(m_pIECClassObject != nullptr)
    {
        m_pIECClassObject->SetTopDriveRun(status);
        m_pIECClassObject->SetTopDriveSpeed(rpm);
    }
}


void LegacyBackEndImpl::iecUpdated()
{
    m_pIECClassObject->ReadIecStructure();
    float flowrate = IecInterfaceRead.FlowRateGpm;
    float toppsi = IecInterfaceRead.TopPressurePsi;
    float bottompsi = IecInterfaceRead.BottomPressurePsi;
    float actualtopdriverpm = IecInterfaceRead.TopDriveSpeedCommandRpmX100/100.0;
    float actualmudpumprpm = IecInterfaceRead.PumpSpeedCommandRpmX10/10.0;
    float pressuredrop = toppsi - bottompsi;
    unsigned short version = (unsigned short)IecInterfaceRead.SoftwareInterfaceRev;
    float torque = IecInterfaceRead.TorqueLimitFtLbsX10/10.0;

    m_controlGranted = (IecInterfaceRead.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL) == IEC_CONTROLWORD_ROTOSLIDE_CONTROL;

    emit updatePressureBotton(bottompsi);
    emit updatePressureTop(toppsi);
    emit updatePressureDrop(pressuredrop);
    emit updateFlowRate(flowrate);
    emit updateTorque(torque);

    if(m_pMudPump != nullptr)
    {
        emit m_pMudPump->newActualRPM(actualmudpumprpm);
    }

    int timerid = startTimer(m_propogationDelay);
    m_delayedTopDriveRPM.insert(timerid, actualtopdriverpm);
//    if(m_pTopDrive != nullptr)
//    {
//        emit m_pTopDrive->newActualRPM(actualtopdriverpm);
//    }

    if(m_pTool != nullptr)
    {
        float granted = (IecInterfaceRead.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)? 1.0 : 0.0;
        emit m_pTool->newTelemetryValue("control granted", granted);
        emit m_pTool->newTelemetryValue("Read torque ", IecInterfaceRead.TorqueLimitFtLbsX10/10.0);
    }

    qCDebug(LEGACYBACKENDIMPL) << "IEC has been updated";
    qCDebug(LEGACYBACKENDIMPL) << "Control Granted = " << ((IecInterfaceRead.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)?"true":"false");
    qCDebug(LEGACYBACKENDIMPL) << "Heartbeat = " << ((IecInterfaceRead.ControlWord & IEC_CONTROLWORD_HEARTBEAT)?"true":"false");
    qCDebug(LEGACYBACKENDIMPL) << "AutoDrill control = " << ((IecInterfaceRead.ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL)?"true":"false");

    m_commsCounter = 0;

    QHash<QString,QString> systemlogvalues;
    QString timestr = QString::number(QDateTime::currentDateTime().toTime_t());

    systemlogvalues.insert(SYSTEMLOGUNIXTIME, timestr);
    systemlogvalues.insert(FLOWRATE, QString::number(flowrate, 'f', 3));
    systemlogvalues.insert(TOP_PRESSURE, QString::number(toppsi, 'f', 3));
    systemlogvalues.insert(LOWER_PRESSURE, QString::number(bottompsi, 'f', 3));
    systemlogvalues.insert(TOPDRIVE_RPM, QString::number(actualtopdriverpm, 'f', 6));
    systemlogvalues.insert(MUDPUMP_RPM, QString::number(actualmudpumprpm, 'f', 6));
    systemlogvalues.insert(TORQUE, QString::number((IecInterfaceRead.TorqueLimitFtLbsX10/10.0), 'f', 3));
    systemlogvalues.insert(IEC_START, QString("V 0x") + QString::number(version, 16));
    emit systemLogUpdateEntries(systemlogvalues);

}


void LegacyBackEndImpl::timerEvent(QTimerEvent* event)
{
    int timerid = event->timerId();

    if(m_delayedTopDriveRPM.contains(timerid))
    {
        float newtopdriverpm = m_delayedTopDriveRPM.value(timerid);

        if(m_pTopDrive != nullptr)
        {
            emit m_pTopDrive->newActualRPM(newtopdriverpm);
        }

        killTimer(timerid);
    }
    else
    {
        QString status = (m_commsCounter++ > 3) ? STATUS_WARN:STATUS_TRUE;
        QString control = m_controlGranted ? STATUS_TRUE:STATUS_FALSE;
        QHash<QString,QString> statusmap;
        int badcrc = m_pIECThreadObject->m_badCRCCounter;
        int overruns = m_pIECThreadObject->m_overrunEventCounter;


        if(m_commsCounter++ > 3)
        {
            control = STATUS_FALSE;

            qCDebug(LEGACYBACKENDIMPL) << "!!!IEC comms counter = " <<  m_commsCounter;
        }

        statusmap.insert(MUDPUMP_STATUS, status);
        statusmap.insert(TOPDRIVE_STATUS, status);
        statusmap.insert(CONTROL_GRANTED, control);
        statusmap.insert(OVERRUNEVENTS, QString::number(overruns));
        statusmap.insert(BADCRCS, QString::number(badcrc));

        emit statusUpdate(statusmap);
    }
}


void LegacyBackEndImpl::setToolPropogationDelayMillis(int millis)
{
    BackEnd::setToolPropogationDelayMillis(millis);

    m_propogationDelay = millis;
}

QStringList LegacyBackEndImpl::getSystemLogHeaders()
{
    QStringList headers;

    headers << SYSTEMLOGUNIXTIME << IEC_START << TOPDRIVE_RPM << MUDPUMP_RPM << FLOWRATE << TOP_PRESSURE << LOWER_PRESSURE << TORQUE;
    return headers;
}



Tool* LegacyBackEndImpl::createDefaultTool()
{
    QHash<QString,QString> props = Utils::loadConfigFile();
    QString toolname = props.contains(CURRENTLEGACYTOOL) ? props.value(CURRENTLEGACYTOOL):"";
    Tool* ptool = nullptr;


    qCDebug(LEGACYBACKENDIMPL)  << "creating default tool named " << toolname;

    if(toolname == TOOL_ROTONAV_LEGACY)
    {
        qCDebug(LEGACYBACKENDIMPL)  << "creating rotonav tool";
        ptool = new LegacyToolImpl();
    }

    if(toolname == TOOL_WITS_LEGACY)
    {
        qCDebug(LEGACYBACKENDIMPL)  << "creating WITS tool";
        ptool = new WitsTool();
    }

    if(ptool == nullptr)
    {
        qCDebug(LEGACYBACKENDIMPL)  << "creating rotonav tool(default)";
        ptool = new LegacyToolImpl();
    }

    return ptool;
}

