#ifndef LEGACYBACKENDIMPL_H
#define LEGACYBACKENDIMPL_H

#include <QObject>
#include "backend.h"
#include "iecrunner.h"
#include "IECThread.h"
#include "IEC.h"
#include <qhash.h>

class LegacyBackEndImpl : public BackEnd
{
    Q_OBJECT
public:
    explicit LegacyBackEndImpl(QObject *parent = 0);
    virtual ~LegacyBackEndImpl();

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();


    virtual MudPump* getMudPump(QString name="");
    virtual Tool* getTool(QString name="");
    virtual TopDrive* getTopDrive(QString name="");

    void mudPumpsOn(bool status, float rpm);
    void topDriveOn(bool status, float rpm);
    virtual void timerEvent(QTimerEvent* event);
    virtual void setToolPropogationDelayMillis(int millis);
    virtual QStringList getSystemLogHeaders();


signals:
    void topDriveActualRPMChanged(float rpm);
    void mudPumpActualRPMChanged(float rpm);

public slots:
    virtual void startFacility(bool start);
    virtual void quickStopFacility();
    virtual void setLimits(float maxmudpumprpm, float maxtopdriverpm);
    virtual void setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);
    void iecUpdated();

protected:
    Tool* createDefaultTool();

private:
    IECRunner* m_iecRunner;
    IECThreadClass* m_pIECThreadObject;
    IECClass* m_pIECClassObject;
    int m_commsCounter;
    bool m_controlGranted;

    QHash<int,float> m_delayedTopDriveRPM;
    int m_propogationDelay;
};

#endif // LEGACYBACKENDIMPL_H
