#ifndef IECRUNNER_H
#define IECRUNNER_H

#include <QObject>
#include <qthread.h>
#include "IECThread.h"

class IECRunner : public QThread
{
    Q_OBJECT
public:
    explicit IECRunner(IECThreadClass* iecthread, QObject *parent = 0);
    virtual ~IECRunner();

protected:
    virtual void run();

signals:

public slots:

private:
    IECThreadClass* m_pIECThread;
};

#endif // IECRUNNER_H
