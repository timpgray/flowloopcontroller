#include "legacymudpumpimpl.h"
#include <qloggingcategory.h>

#include "rs_constants.h"

QLoggingCategory LEGACYMUDPUMPIMPL("LegacyMudPumpImpl");

LegacyMudPumpImpl::LegacyMudPumpImpl(QObject *parent) : MudPump(parent)
{
    m_currentState = false;
    m_currentRPM = 0;
}

LegacyMudPumpImpl::~LegacyMudPumpImpl()
{

}

void LegacyMudPumpImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(LEGACYMUDPUMPIMPL)  << "initializing with these properties " << props;

}

QString LegacyMudPumpImpl::getName()
{
   return MUDPUMP_LEGACY;
}


void LegacyMudPumpImpl::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        rpm = m_maximumRPM;
    }

    if(rpm < 0)
    {
        rpm = 0;
    }

    m_currentRPM = rpm;

    if(m_pBackend != nullptr)
    {
        m_pBackend->mudPumpsOn(m_currentState, m_currentRPM);
    }
}

void LegacyMudPumpImpl::pumpsOn(bool status)
{
    m_currentState = status;

    if(m_pBackend != nullptr)
    {
        m_pBackend->mudPumpsOn(m_currentState, m_currentRPM);
        emit newStatus(status);
    }

}


void LegacyMudPumpImpl::actualPumpRPMChanged(float rpm)
{
    Q_UNUSED(rpm);

}
