#include "witstool.h"
#include "utils.h"
#include "rs_constants.h"

#include <qloggingcategory.h>

QLoggingCategory WITSTOOL("WitsTool");



WitsTool::WitsTool(QObject *parent) : Tool(parent)
{
    m_witsHandler = nullptr;
    m_toolSampleRateDecimator = 1;
    m_toolSampleRateCounter = 0;

}



WitsTool::~WitsTool()
{
    if(m_witsHandler != nullptr)
    {
        delete m_witsHandler;
    }
}

void WitsTool::initialize(QHash<QString,QString> props)
{
    if(props.contains(WITS_CONFIGFILE))
    {
        QString filepath = props.value(WITS_CONFIGFILE);

        qCDebug(WITSTOOL) << "Configuring WITS interface from this config file " << filepath;
        qCDebug(WITSTOOL) << "Config properties = " << props;
        m_witsHandler = new WitsMsgHandlerA();

        bool status = m_witsHandler->loadConfigFile(filepath.toStdString());

        if(status)
        {
            status = m_witsHandler->init(true, filepath.toStdString());

            if(status)
            {
                qCDebug(WITSTOOL) << "WITS tool configured  - starting";
                startTimer(500);
                m_witsHandler->addProcessedListener(this);
                m_witsHandler->addRawListener(this);
                m_timeoutCounter = 0;
            }
            else
            {
                qCWarning(WITSTOOL) << "initialization of com port failed";
            }
        }
        else
        {
            qCWarning(WITSTOOL) << "loading of config file failed";
        }

        if(!status)
        {
            delete m_witsHandler;
            m_witsHandler = nullptr;
        }
    }
    else
    {
        qCWarning(WITSTOOL) << "No config file specified";
    }

}

QString WitsTool::getName()
{
    return TOOL_WITS_LEGACY;
}


void WitsTool::timerEvent(QTimerEvent* pevent)
{
    if(m_witsHandler != nullptr)
    {
        qCWarning(WITSTOOL) << "Handling timer event";
        m_witsHandler->tick();
    }

    if(m_timeoutCounter++ > 90)
    {
        QString toolstatus = STATUS_WARN;
        QHash<QString,QString> statusmap;

        statusmap.insert(TOOL_STATUS, toolstatus);
        emit statusUpdate(statusmap);
    }
}


void WitsTool::handleProcessedWITSMsg(string witsmsg, string name, float value)
{
    QHash<QString,QString> statusmap;
    QString witsstr = QString::fromStdString(witsmsg);
    QString varname = QString::fromStdString(name);

    qCDebug(WITSTOOL) << "handleProcessedWITSMsg() msg = " << witsstr << " name = " <<  varname << " value = " << QString::number(value);
    emit newWITSMsg(witsstr, varname, value);

    if(name == TOOLFACE)
    {
        emit newToolfaceValueFullRate(value);

        if(((m_toolSampleRateCounter++ % m_toolSampleRateDecimator) == 0))
        {
            emit newToolfaceValue(value);
        }
    }

    if(name == RPM)
    {
        emit newRPMValue(value);
    }


    if(name == BATV)
    {
        QString battstatus = value > 20.0 ? STATUS_TRUE:STATUS_WARN;

        statusmap.insert(BATTERY_STATUS, battstatus);
    }

    statusmap.insert(TOOL_STATUS, STATUS_TRUE);
    emit statusUpdate(statusmap);
    emit newTelemetryValue(varname, value);
}

void WitsTool::handleRawWITSMsg(string name, string value)
{
    QString varname = QString::fromStdString(name);
    QString valuestr = QString::fromStdString(value);

    qCDebug(WITSTOOL) << "handleRawWITSMsg() name = " <<  varname << " value = " << valuestr;
    m_timeoutCounter = 0;


}



QStringList WitsTool::getSystemLogHeaders()
{
    QStringList headers;


    return headers;
}

void WitsTool::setToolSampleRateMillis(int samplerate)
{
    int decimator = samplerate / 1000;

    if(decimator == 0)
    {
        decimator = 1;
    }

    m_toolSampleRateDecimator = decimator;


    Tool::setToolSampleRateMillis(samplerate);
}

