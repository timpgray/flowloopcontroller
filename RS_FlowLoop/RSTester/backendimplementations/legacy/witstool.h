#ifndef WITSTOOL_H
#define WITSTOOL_H

#include <QObject>
#include "tool.h"
#include "witsmsghandler.h"

class WitsTool : public Tool, public WitsProcessedMsgHandler, public WitsRawMsgHandler
{
    Q_OBJECT
public:
    explicit WitsTool(QObject *parent = 0);
    virtual ~WitsTool();

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();
    virtual QStringList getSystemLogHeaders();


    virtual void timerEvent(QTimerEvent* pevent);

    virtual void handleProcessedWITSMsg(string witsmsg, string name, float value);
    virtual void handleRawWITSMsg(string name, string value);
    virtual void setToolSampleRateMillis(int samplerate);



signals:

public slots:

private:
    WitsMsgHandler* m_witsHandler;
    int m_timeoutCounter;
    int m_toolSampleRateDecimator;
    int m_toolSampleRateCounter;
};

#endif // WITSTOOL_H
