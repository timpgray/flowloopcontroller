#include "legacytopdriveimpl.h"

#include <qloggingcategory.h>
#include "rs_constants.h"

QLoggingCategory LEGACYTOPDRIVEIMPL("LegacyTopDriveImpl");

LegacyTopDriveImpl::LegacyTopDriveImpl(QObject *parent) : TopDrive(parent)
{
    m_currentState = false;
    m_currentRPM = 0;
}

LegacyTopDriveImpl::~LegacyTopDriveImpl()
{

}


void LegacyTopDriveImpl::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        rpm = m_maximumRPM;
    }

    if(rpm < 0)
    {
        rpm = 0;
    }

    m_currentRPM = rpm;

    if(m_pBackend != nullptr)
    {
        m_pBackend->topDriveOn(m_currentState, m_currentRPM);
    }
}

void LegacyTopDriveImpl::topDriveOn(bool status)
{
    m_currentState = status;

    if(m_pBackend != nullptr)
    {
        m_pBackend->topDriveOn(m_currentState, m_currentRPM);
        emit newStatus(status);
    }
}


void LegacyTopDriveImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(LEGACYTOPDRIVEIMPL)  << "initializing with these properties " << props;

}

QString LegacyTopDriveImpl::getName()
{
   return TOPDRIVE_LEGACY;
}


void LegacyTopDriveImpl::actualTopDriveRPMChanged(float rpm)
{
    Q_UNUSED(rpm);


}
