#include "mudpumptestingimpl.h"

#include <qloggingcategory.h>

QLoggingCategory MUDPUMPTESTINGIMPL("MudPumpTestingImpl");

MudPumpTestingImpl::MudPumpTestingImpl(QObject *parent) : MudPump(parent)
{
    m_currentRPM = 0;
}

MudPumpTestingImpl::~MudPumpTestingImpl()
{
    qCDebug(MUDPUMPTESTINGIMPL) << "!!!inside destructor";
}


void MudPumpTestingImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(MUDPUMPTESTINGIMPL) << "initializing with these properties " << props;
}

QString MudPumpTestingImpl::getName()
{
    return "Mud Pump Testing";
}


void MudPumpTestingImpl::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        qCDebug(MUDPUMPTESTINGIMPL) << "specified rpm " << rpm << " is greater than max - limiting to " << m_maximumRPM;
        rpm = m_maximumRPM;
    }

    qCDebug(MUDPUMPTESTINGIMPL) << "updating RPM new value = " << rpm;
    m_currentRPM = rpm;
}

void MudPumpTestingImpl::pumpsOn(bool status)
{
    qCDebug(MUDPUMPTESTINGIMPL) << "pumps on status = " << status;

    if(status)
    {
        m_timerId = startTimer(7000);
    }
    else
    {
        killTimer(m_timerId);
    }

    emit newStatus(status);
}


void MudPumpTestingImpl::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    m_currentRPM += 1;

    emit newActualRPM(m_currentRPM);
}
