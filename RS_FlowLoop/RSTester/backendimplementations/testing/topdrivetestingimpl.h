#ifndef TOPDRIVETESTINGIMPL_H
#define TOPDRIVETESTINGIMPL_H

#include <QObject>
#include "topdrive.h"

class TopDriveTestingImpl : public TopDrive
{
    Q_OBJECT
public:
    explicit TopDriveTestingImpl(QObject *parent = 0);
    ~TopDriveTestingImpl();

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();

    virtual void timerEvent(QTimerEvent* event);

signals:

public slots:
    virtual void updateRPM(float rpm);
    virtual void topDriveOn(bool status);

private:
    float m_currentRPM;
    int m_timerId;
};

#endif // TOPDRIVETESTINGIMPL_H
