#include "topdrivetestingimpl.h"

#include <qloggingcategory.h>

QLoggingCategory TOPDRIVETESTINGIMPL("TopDriveTestingImpl");

TopDriveTestingImpl::TopDriveTestingImpl(QObject *parent) : TopDrive(parent)
{
    m_currentRPM = 0;
}

TopDriveTestingImpl::~TopDriveTestingImpl()
{
    qCDebug(TOPDRIVETESTINGIMPL) << "!!!inside destructor";
}

void TopDriveTestingImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(TOPDRIVETESTINGIMPL) << "initializing with these arguments " << props;
}

QString TopDriveTestingImpl::getName()
{
    return "Top Drive Testing";
}

void TopDriveTestingImpl::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        qCDebug(TOPDRIVETESTINGIMPL) << "specified rpm " << rpm << " is greater than max - limiting to " << m_maximumRPM;
        rpm = m_maximumRPM;
    }

    qCDebug(TOPDRIVETESTINGIMPL) << "updating rpm to this value " << rpm;
    m_currentRPM = rpm;
}

void TopDriveTestingImpl::topDriveOn(bool status)
{
    qCDebug(TOPDRIVETESTINGIMPL) << "top drive on = " << status;

    if(status)
    {
//        m_currentRPM = 0;
        m_timerId = startTimer(2000);
    }
    else
    {
        killTimer(m_timerId);
    }

    emit newStatus(status);
}


void TopDriveTestingImpl::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    m_currentRPM += 1;

    emit newActualRPM(m_currentRPM);
}
