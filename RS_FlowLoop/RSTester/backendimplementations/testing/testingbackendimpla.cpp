#include "testingbackendimpla.h"
#include <QTimerEvent>

TestingBackEndImplA::TestingBackEndImplA(QObject *parent) : TestBackEndImpl(parent)
{
    m_pFlowTester = new FlowTester();

    if(m_pMudPump != nullptr)
    {
        delete m_pMudPump;
    }

    if(m_pTool != nullptr)
    {
        delete m_pTool;
    }

    if(m_pTopDrive != nullptr)
    {
        delete m_pTopDrive;
    }

    m_pMudPump = new MudPumpTestingImplA(m_pFlowTester);
    m_pTool = new ToolTestingImplA(m_pFlowTester);
    m_pTopDrive = new TopDriveTestingImplA(m_pFlowTester);

}

TestingBackEndImplA::~TestingBackEndImplA()
{
    if(m_pFlowTester != nullptr)
    {
        delete m_pFlowTester;
    }
}

void TestingBackEndImplA::showDiagnostics()
{
    m_pFlowTester->show();
}




MudPumpTestingImplA::MudPumpTestingImplA(FlowTester* ft, QObject *parent) : MudPumpTestingImpl(parent)
{
    m_pFlowTester = ft;
}

MudPumpTestingImplA::~MudPumpTestingImplA()
{

}

void MudPumpTestingImplA::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    if(m_pFlowTester != nullptr)
    {
        float rpm = m_pFlowTester->getMudMotorRPM();
        emit newActualRPM(rpm);
    }
}

void MudPumpTestingImplA::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        rpm = m_maximumRPM;
    }

    if(rpm < 0)
    {
        rpm = 0;
    }

   m_pFlowTester->setLowerRPM(rpm);
}

ToolTestingImplA::ToolTestingImplA(FlowTester* ft, QObject *parent) : ToolTestingImpl(parent)
{
    m_pFlowTester = ft;
    m_propogationDelay = 0;
}

ToolTestingImplA::~ToolTestingImplA()
{

}

void ToolTestingImplA::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    int timerid = event->timerId();

    if(m_delayedToolData.contains(timerid))
    {
        ToolTestingData* ptooldata = m_delayedToolData.value(timerid);
        float toolface = ptooldata->m_toolface;
        float rpm = ptooldata->m_toolRPM;

        emit newToolfaceValue(toolface);
        emit newRPMValue(rpm);

        emit newTelemetryValue("gtfa", toolface);
        emit newTelemetryValue("rpm", rpm);

        delete ptooldata;
        killTimer(timerid);
        m_delayedToolData.remove(timerid);
    }
    else
    {
        if(m_pFlowTester != nullptr)
        {
            float toolface = m_pFlowTester->getToolFace();
            float rpm = m_pFlowTester->getLowerRPM();
            ToolTestingData* ptooldata = new ToolTestingData(toolface, rpm);
            int timerid = startTimer(m_propogationDelay);

            m_delayedToolData.insert(timerid, ptooldata);


            //        emit newToolfaceValue(toolface);
            //        emit newRPMValue(rpm);

            //        emit newTelemetryValue("gtfa", toolface);
            //        emit newTelemetryValue("rpm", rpm);

        }
    }
}


TopDriveTestingImplA::TopDriveTestingImplA(FlowTester* ft, QObject *parent) : TopDriveTestingImpl(parent)
{
    m_pFlowTester = ft;
}

TopDriveTestingImplA::~TopDriveTestingImplA()
{

}

void TopDriveTestingImplA::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    if(m_pFlowTester != nullptr)
    {
        float rpm = m_pFlowTester->getTopDriveRPM();
        emit newActualRPM(rpm);
    }
}

void TopDriveTestingImplA::updateRPM(float rpm)
{
    if(rpm > m_maximumRPM)
    {
        rpm = m_maximumRPM;
    }

    if(rpm < 0)
    {
        rpm = 0;
    }

    m_pFlowTester->setUpperRPM(rpm);
}
