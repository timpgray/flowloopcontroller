#include "tooltestingimpl.h"

#include <qloggingcategory.h>

QLoggingCategory TOOLTESTINGIMPL("ToolTestingImpl");

ToolTestingImpl::ToolTestingImpl(QObject *parent) : Tool(parent)
{
    m_currentToolFace = 0;
    m_currentRPM = 0;
    m_timerID = startTimer(m_sampleRateMillis);
}


ToolTestingImpl::~ToolTestingImpl()
{
    qCDebug(TOOLTESTINGIMPL) << "!!! inside destructor";
}

void ToolTestingImpl::initialize(QHash<QString,QString> props)
{
    qCDebug(TOOLTESTINGIMPL) << "initializing with these properties " << props;
}


QString ToolTestingImpl::getName()
{
    return "Testing Tool";
}


void ToolTestingImpl::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    m_currentToolFace += 1;
    m_currentRPM += 1;

    emit newToolfaceValue(m_currentToolFace);
    emit newRPMValue(m_currentRPM);

    emit newTelemetryValue("gtfa", m_currentToolFace);
    emit newTelemetryValue("rpm", m_currentRPM);

}
