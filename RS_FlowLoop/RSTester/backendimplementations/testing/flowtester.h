#ifndef FLOWTESTER_H
#define FLOWTESTER_H

#include <QMainWindow>
#include <qwt_compass.h>
#include "speedo_meter.h"
#include <qtimer.h>


namespace Ui {
class FlowTester;
}



class Motor
{
public:
    Motor();
    virtual void setSpeed(float rpm){m_speedTarget = rpm;}
    virtual float getSpeed(){return m_currentSpeed;}
    virtual void tick();
    virtual float getToolFace(){return m_lastToolFace;}
    virtual void setMillisPerTick(unsigned int millis){m_millisPerTick = millis;}
    virtual void setRPMoffset(float offset){m_speedOffset = offset;}
    virtual float getRelativeRPM(){return m_currentSpeed + m_speedOffset;}

protected:
    float m_speedTarget;
    float m_currentSpeed;
    float m_lastSpeed;
    float m_lastToolFace;
    unsigned int m_millisPerTick;
    float m_speedOffset;
};

class FlowTester : public QWidget
{
    Q_OBJECT

public:
    explicit FlowTester(QWidget *parent = 0);
    ~FlowTester();

    float getMudMotorRPM();
    float getLowerRPM();
    float getTopDriveRPM();
    float getToolFace();

    void setLowerRPM(float rpm);
    void setUpperRPM(float rpm);

protected:
    virtual void timerEvent(QTimerEvent* event);

public slots:
    void on_lowerrpm_valueChanged(int value);

    void on_topdriverrpm_valueChanged(int value);

    void sendWITsData();

private:
    Ui::FlowTester *ui;

    QwtCompass* m_dial;
    SpeedoMeter* m_speedo;
    int m_position;
    Motor m_motor;
    Motor m_topDrive;
    QTimer* m_witsTimer;
};

#endif // FLOWTESTER_H
