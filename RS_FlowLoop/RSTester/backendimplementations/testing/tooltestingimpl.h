#ifndef TOOLTESTINGIMPL_H
#define TOOLTESTINGIMPL_H

#include <QObject>
#include "tool.h"

class ToolTestingImpl : public Tool
{
    Q_OBJECT
public:
    explicit ToolTestingImpl(QObject *parent = 0);
    virtual ~ToolTestingImpl();

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();

    virtual void timerEvent(QTimerEvent* event);


signals:

public slots:

private:
    float m_currentToolFace;
    float m_currentRPM;
};

#endif // TOOLTESTINGIMPL_H
