#ifndef TESTINGBACKENDIMPLA_H
#define TESTINGBACKENDIMPLA_H

#include <QObject>
#include "testbackendimpl.h"
#include "mudpumptestingimpl.h"
#include "tooltestingimpl.h"
#include "topdrivetestingimpl.h"
#include "flowtester.h"

class ToolTestingData
{
public:
    ToolTestingData(float toolface, float rpm){m_toolface = toolface; m_toolRPM = rpm;}
    float m_toolface;
    float m_toolRPM;
};

class MudPumpTestingImplA : public MudPumpTestingImpl
{
    Q_OBJECT
public:
    explicit MudPumpTestingImplA(FlowTester* ft, QObject *parent = 0);
    virtual ~MudPumpTestingImplA();
    virtual void timerEvent(QTimerEvent* event);

    FlowTester* m_pFlowTester;

public slots:
    virtual void updateRPM(float rpm);

};

class ToolTestingImplA : public ToolTestingImpl
{
    Q_OBJECT
public:
    explicit ToolTestingImplA(FlowTester* ft, QObject *parent = 0);
    virtual ~ToolTestingImplA();
    virtual void setToolPropogationDelayMillis(int delay){m_propogationDelay = delay;}

    virtual void timerEvent(QTimerEvent* event);

    FlowTester* m_pFlowTester;

public slots:

private:
    QHash<int,ToolTestingData*> m_delayedToolData;
    int m_propogationDelay;

};


class TopDriveTestingImplA : public TopDriveTestingImpl
{
    Q_OBJECT
public:
    explicit TopDriveTestingImplA(FlowTester* ft, QObject *parent = 0);
    virtual ~TopDriveTestingImplA();
    virtual void timerEvent(QTimerEvent* event);

    FlowTester* m_pFlowTester;

public slots:
    virtual void updateRPM(float rpm);

};


class TestingBackEndImplA : public TestBackEndImpl
{
    Q_OBJECT
public:
    explicit TestingBackEndImplA(QObject *parent = 0);
    virtual ~TestingBackEndImplA();

    virtual void showDiagnostics();

signals:

public slots:

private:
    FlowTester* m_pFlowTester;

};

#endif // TESTINGBACKENDIMPLA_H
