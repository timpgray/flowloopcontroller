#ifndef MUDPUMPTESTINGIMPL_H
#define MUDPUMPTESTINGIMPL_H

#include <QObject>
#include "mudpump.h"

class MudPumpTestingImpl : public MudPump
{
    Q_OBJECT
public:
    explicit MudPumpTestingImpl(QObject *parent = 0);
    ~MudPumpTestingImpl();

    virtual void initialize(QHash<QString,QString> props);
    virtual QString getName();

    virtual void timerEvent(QTimerEvent* event);

signals:

public slots:
    virtual void updateRPM(float rpm);
    virtual void pumpsOn(bool status);

private:
    float m_currentRPM;
    int m_timerId;

};

#endif // MUDPUMPTESTINGIMPL_H
