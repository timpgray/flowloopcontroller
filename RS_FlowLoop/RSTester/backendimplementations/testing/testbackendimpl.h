#ifndef TESTBACKENDIMPL_H
#define TESTBACKENDIMPL_H

#include <QObject>
#include "backend.h"

class TestBackEndImpl : public BackEnd
{
    Q_OBJECT
public:
    explicit TestBackEndImpl(QObject *parent = 0);
    virtual ~TestBackEndImpl();

    virtual void initialize(QHash<QString,QString>);
    virtual QString getName();


    virtual MudPump* getMudPump(QString name="");
    virtual Tool* getTool(QString name="");
    virtual TopDrive* getTopDrive(QString name="");

    virtual void timerEvent(QTimerEvent* event);
    virtual void showDiagnostics();



signals:

public slots:

    virtual void startFacility(bool start);
    virtual void quickStopFacility();
    virtual void setLimits(float maxmudpumprpm, float maxtopdriverpm);
    virtual void setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset);

private:
    float m_pressureTop;
    float m_pressureBottom;
    float m_pressureDrop;
    float m_flowRate;
    int m_timerId;

};

#endif // TESTBACKENDIMPL_H
