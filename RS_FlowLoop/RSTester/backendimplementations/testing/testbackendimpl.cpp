
#include "testbackendimpl.h"
#include "rs_constants.h"
#include "mudpumptestingimpl.h"
#include "topdrivetestingimpl.h"
#include "tooltestingimpl.h"
#include "flowtester.h"


TestBackEndImpl::TestBackEndImpl(QObject *parent) : BackEnd(parent)
{
    m_toolInterfaceNames << TOOL_WITS << TOOL_ROTONAV;
    m_topDriveInterfaceNames << TOPDRIVE_DEFAULT;
    m_mudPumpInterfaceNames << MUDPUMP_DEFAULT;

    m_pMudPump = new MudPumpTestingImpl();
    m_pTool = new ToolTestingImpl();
    m_pTopDrive = new TopDriveTestingImpl();

    m_pressureTop = 0;
    m_pressureBottom = 0;
    m_pressureDrop = 0;
}


TestBackEndImpl::~TestBackEndImpl()
{
//   if(m_pMudPump != nullptr)
//   {
//       delete m_pMudPump;
//   }

//   if(m_pTool != nullptr)
//   {
//       delete m_pTool;
//   }

//   if(m_pTopDrive != nullptr)
//   {
//       delete m_pTopDrive;
//   }


}


void TestBackEndImpl::initialize(QHash<QString,QString>)
{
    QHash<QString,QString> initmap;

    initmap.insert(MUDPUMP_STATUS, STATUS_TRUE);
    initmap.insert(TOOL_STATUS, STATUS_TRUE);
    initmap.insert(TOPDRIVE_STATUS, STATUS_TRUE);
    initmap.insert(SHOWDIAGNOSTICS, STATUS_TRUE);


    emit statusUpdate(initmap);

    emit updateLimits(400, 120);
    emit updateRanges(5000, 0, 500, 0);

    connect(m_pTopDrive, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
    connect(m_pTool, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));
    connect(m_pMudPump, SIGNAL(statusUpdate(QHash<QString,QString>)), this, SIGNAL(statusUpdate(QHash<QString,QString>)));

}

QString TestBackEndImpl::getName()
{
    return TEST_BACKEND;
}


MudPump* TestBackEndImpl::getMudPump(QString name)
{
    Q_UNUSED(name);

    return m_pMudPump;
}

Tool* TestBackEndImpl::getTool(QString name)
{
    Q_UNUSED(name);

    return m_pTool;
}

TopDrive* TestBackEndImpl::getTopDrive(QString name)
{
    Q_UNUSED(name);

    return m_pTopDrive;
}



void TestBackEndImpl::startFacility(bool start)
{
    QHash<QString,QString> initmap;
    QString startstatus = start ? STATUS_TRUE:STATUS_FALSE;

    initmap.insert(FACILITY_STATUS, STATUS_TRUE);
    initmap.insert(FACILITY_STARTED, startstatus);
    initmap.insert(SHOWDIAGNOSTICS, STATUS_TRUE);

    emit statusUpdate(initmap);

    if(start)
    {
        m_timerId = startTimer(5000);

        m_pressureTop = 5000;
        m_pressureBottom = 2000;
        m_pressureDrop = 1000;
        m_flowRate = 200;
    }
    else
    {
        killTimer(m_timerId);
    }
}

void TestBackEndImpl::quickStopFacility()
{
    QHash<QString,QString> initmap;

    initmap.insert(FACILITY_STATUS, STATUS_TRUE);
    initmap.insert(FACILITY_STARTED, STATUS_FALSE);

    emit statusUpdate(initmap);
}


void TestBackEndImpl::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);

    m_pressureTop += 1;
    m_pressureBottom += 1;
    m_pressureDrop += 1;
    m_flowRate += 1;

    emit updatePressureTop(m_pressureTop);
    emit updatePressureBotton(m_pressureBottom);
    emit updatePressureDrop(m_pressureDrop);
    emit updateFlowRate(m_flowRate);

}

void TestBackEndImpl::setLimits(float maxmudpumprpm, float maxtopdriverpm)
{
    Q_UNUSED(maxmudpumprpm);
    Q_UNUSED(maxtopdriverpm);

}

void TestBackEndImpl::setRanges(float toprange, float topoffset, float bottomrange, float bottomoffset)
{
    Q_UNUSED(toprange);
    Q_UNUSED(topoffset);
    Q_UNUSED(bottomrange);
    Q_UNUSED(bottomoffset);

}


void TestBackEndImpl::showDiagnostics()
{
    FlowTester* tester = new FlowTester();

    tester->show();
}
