#ifndef CONTROLLERVIEWFACTORY_H
#define CONTROLLERVIEWFACTORY_H

#include "testmainwindow.h"
#include "controllerview.h"
#include "qstring.h"


class ControllerViewFactory
{
private:
    ControllerViewFactory();

public:
    static ControllerView* getControllerView(Ui::TestMainWindow *ui, QString name=QString(""));
};

#endif // CONTROLLERVIEWFACTORY_H
