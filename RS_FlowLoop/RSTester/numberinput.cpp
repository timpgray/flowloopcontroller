#include "numberinput.h"
#include "ui_numberinput.h"

NumberInput::NumberInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NumberInput)
{
    ui->setupUi(this);
}

NumberInput::~NumberInput()
{
    delete ui;
}

void NumberInput::on_uservalue_editingFinished()
{
    QString text = ui->uservalue->text();

    m_userValue = text.toFloat();
}


void NumberInput::setLabel(QString labelvalue)
{
    ui->variablename->setText(labelvalue);
}

void NumberInput::setValue(float current)
{
    QString valuestr = QString::number(current, 'f', 2);

    m_userValue = current;
    ui->uservalue->setText(valuestr);
}

float NumberInput::getValue()
{
    return m_userValue;
}

int NumberInput::exec()
{
    return QDialog::exec();
}

void  NumberInput::focusInEvent(QFocusEvent *event)
{

    QDialog::focusInEvent(event);
    ui->uservalue->setFocus();
    ui->uservalue->selectAll();

}
