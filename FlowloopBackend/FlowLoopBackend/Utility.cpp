//---------------------------------------------------------------------------
// 2015.01.12
// 2014.08.16
//---------------------------------------------------------------------------

#include "Utility.h"

#define BYTE3   16777216
#define BYTE2   65536
#define BYTE1   256
#define BYTE0   1

UtilityClass Utility;

//---------------------------------------------------------------------------
unsigned short UtilityClass::SwitchEndianShort(unsigned short Word)
{
    unsigned short Result;
    unsigned char LoByte,HiByte;

    LoByte = (unsigned char)Word;
    HiByte = (unsigned char)(Word/256);
    Result = (unsigned short)((unsigned short)LoByte *256 + (unsigned short)HiByte);

    return Result;
}
//---------------------------------------------------------------------------
unsigned long UtilityClass::SwitchEndianLong(unsigned long Word)
{
    unsigned long Result;
    unsigned char Byte[4];

    Byte[0] = (unsigned char)(Word/BYTE0);
    Byte[1] = (unsigned char)(Word/BYTE1);
    Byte[2] = (unsigned char)(Word/BYTE2);
    Byte[3] = (unsigned char)(Word/BYTE3);
    Result = (unsigned long)
    (
     (unsigned long)Byte[0]*BYTE3
    +(unsigned long)Byte[1]*BYTE2
    +(unsigned long)Byte[2]*BYTE1
    +(unsigned long)Byte[3]*BYTE0
    );

    return Result;
}
//---------------------------------------------------------------------------
float UtilityClass::SwitchEndianFloat(float Variable)
{
    unsigned char Buffer[4];
    float NewVariable;

    memcpy(Buffer,&Variable,sizeof(float));

    NewVariable = CharBufferToFloat(Buffer);

    return(NewVariable);
}
//---------------------------------------------------------------------------
float UtilityClass::IeeeFloatToMicroChip(float Variable)
{
    unsigned char Buffer[4], Buffer2[4];
    int SignBit;
    int Lsb;
    float NewVariable;

    memcpy(Buffer,&Variable,sizeof(float));

    SignBit = 0;
    if (Buffer[3] & 0x80)
        SignBit = 1;
    Lsb = 0;
    if (Buffer[2] & 0x80)
        Lsb = 1;

    Buffer[3] <<= 1;
    Buffer[2] &= ~0x80;
    if (SignBit)
        Buffer[2] |= 0x80;

    Buffer[3] &= ~0x01;
    if (Lsb)
        Buffer[3] |= 0x01;

    Buffer2[0] = Buffer[3];
    Buffer2[1] = Buffer[2];
    Buffer2[2] = Buffer[1];
    Buffer2[3] = Buffer[0];
    NewVariable = *(float *)Buffer2;

    return(NewVariable);
}
//---------------------------------------------------------------------------
float UtilityClass::MicroChipToIeeeFloat(float Variable)
{
    unsigned char Buffer[4], Buffer2[4];
    int SignBit;
    int Lsb;
    float NewVariable;

    memcpy(Buffer,&Variable,sizeof(float));

    SignBit = 0;
    if (Buffer[1] & 0x80)
        SignBit = 1;
    Lsb = 0;
    if (Buffer[0] & 0x01)
        Lsb = 1;

    Buffer[0] >>= 1;
    Buffer[0] &= ~0x80;
    if (SignBit)
        Buffer[0] |= 0x80;

    Buffer[1] &= ~0x80;
    if (Lsb)
        Buffer[1] |= 0x80;

    Buffer2[0] = Buffer[3];
    Buffer2[1] = Buffer[2];
    Buffer2[2] = Buffer[1];
    Buffer2[3] = Buffer[0];
    NewVariable = *(float *)Buffer2;

    return(NewVariable);
}
//---------------------------------------------------------------------------
float UtilityClass::CharBufferToFloat(unsigned char *CharPointer)
{
    unsigned char Buffer[4];

    // reverse the byte order to convert from Motorola to Intel format
    Buffer[0] = *(CharPointer +3);
    Buffer[1] = *(CharPointer +2);
    Buffer[2] = *(CharPointer +1);
    Buffer[3] = *(CharPointer +0);

    return(*(float *)Buffer);
}
//---------------------------------------------------------------------------
unsigned char UtilityClass::ConvertAlhpaCharacterToHex(char A)
{
    unsigned char B;
    switch(A)
    {
        case '0': B = 0; break;
        case '1': B = 1; break;
        case '2': B = 2; break;
        case '3': B = 3; break;
        case '4': B = 4; break;
        case '5': B = 5; break;
        case '6': B = 6; break;
        case '7': B = 7; break;
        case '8': B = 8; break;
        case '9': B = 9; break;
        case 'A': B = 10; break;
        case 'B': B = 11; break;
        case 'C': B = 12; break;
        case 'D': B = 13; break;
        case 'E': B = 14; break;
        case 'F': B = 15; break;
        case 'a': B = 10; break;
        case 'b': B = 11; break;
        case 'c': B = 12; break;
        case 'd': B = 13; break;
        case 'e': B = 14; break;
        case 'f': B = 15; break;
    }
    
    return(B);
}
//---------------------------------------------------------------------------
unsigned char UtilityClass::AsciiToInteger(unsigned char AsciiCharacter)
{
    switch(AsciiCharacter)
    {
        case '0':
            return(0);
        case '1':
            return(1);
        case '2':
            return(2);
        case '3':
            return(3);
        case '4':
            return(4);
        case '5':
            return(5);
        case '6':
            return(6);
        case '7':
            return(7);
        case '8':
            return(8);
        case '9':
            return(9);
        default:
            return(0xFF);
    }

}
//---------------------------------------------------------------------------
int UtilityClass::ConvertCharStringToHexString(unsigned char *Buffer)
{
    int i;
    unsigned char HexBuffer[32];
    int Index;
    unsigned char HexCode;
    int HexCount;

    HexCount = 0;
    Index = 0;
    while (Buffer[Index] != 0)
    {
        if (Buffer[Index] != ',')
        {
            HexCode = (unsigned char)(ConvertAlhpaCharacterToHex(Buffer[Index]) <<4);
            Index++;
            HexCode += ConvertAlhpaCharacterToHex(Buffer[Index]);
            Index++;
            HexBuffer[HexCount++] = HexCode;
        }
        else
            Index++;
    }

    for (i=0; i<HexCount; i++)
        Buffer[i] = HexBuffer[i];

    return(HexCount);
}
//---------------------------------------------------------------------------
unsigned char UtilityClass::ConvertTwoCharacterToHexByte(char C1, char C2)
{
    unsigned char HexCode;

    switch (C1)
    {
        case '0':
            HexCode = 0x00;
            break;
        case '1':
            HexCode = 0x10;
            break;
        case '2':
            HexCode = 0x20;
            break;
        case '3':
            HexCode = 0x30;
            break;
        case '4':
            HexCode = 0x40;
            break;
        case '5':
            HexCode = 0x50;
            break;
        case '6':
            HexCode = 0x60;
            break;
        case '7':
            HexCode = 0x70;
            break;
        case '8':
            HexCode = 0x80;
            break;
        case '9':
            HexCode = 0x90;
            break;
        case 'a':
            HexCode = 0xa0;
            break;
        case 'b':
            HexCode = 0xb0;
            break;
        case 'c':
            HexCode = 0xc0;
            break;
        case 'd':
            HexCode = 0xd0;
            break;
        case 'e':
            HexCode = 0xe0;
            break;
        case 'f':
            HexCode = 0xf0;
            break;
        case 'A':
            HexCode = 0xa0;
            break;
        case 'B':
            HexCode = 0xb0;
            break;
        case 'C':
            HexCode = 0xc0;
            break;
        case 'D':
            HexCode = 0xd0;
            break;
        case 'E':
            HexCode = 0xe0;
            break;
        case 'F':
            HexCode = 0xf0;
            break;
    }

    switch (C2)
    {
        case '0':
            HexCode |= 0x00;
            break;
        case '1':
            HexCode |= 0x01;
            break;
        case '2':
            HexCode |= 0x02;
            break;
        case '3':
            HexCode |= 0x03;
            break;
        case '4':
            HexCode |= 0x04;
            break;
        case '5':
            HexCode |= 0x05;
            break;
        case '6':
            HexCode |= 0x06;
            break;
        case '7':
            HexCode |= 0x07;
            break;
        case '8':
            HexCode |= 0x08;
            break;
        case '9':
            HexCode |= 0x09;
            break;
        case 'a':
            HexCode |= 0x0a;
            break;
        case 'b':
            HexCode |= 0x0b;
            break;
        case 'c':
            HexCode |= 0x0c;
            break;
        case 'd':
            HexCode |= 0x0d;
            break;
        case 'e':
            HexCode |= 0x0e;
            break;
        case 'f':
            HexCode |= 0x0f;
            break;
        case 'A':
            HexCode |= 0x0a;
            break;
        case 'B':
            HexCode |= 0x0b;
            break;
        case 'C':
            HexCode |= 0x0c;
            break;
        case 'D':
            HexCode |= 0x0d;
            break;
        case 'E':
            HexCode |= 0x0e;
            break;
        case 'F':
            HexCode |= 0x0f;
            break;
    }

    return(HexCode);
}
//---------------------------------------------------------------------------
unsigned char UtilityClass::ConvertHexToBcd(unsigned char HexCode)
{
    unsigned char Bcd1;
    unsigned char Bcd0;
    unsigned char BcdCode;

    Bcd1 = HexCode /10;
    Bcd0 = HexCode -(Bcd1 *10);
    BcdCode = (Bcd1 << 4) +Bcd0;

    return(BcdCode);
}
//---------------------------------------------------------------------------
unsigned char UtilityClass::ConvertBcdToHex(unsigned char BcdCode)
{
    unsigned char Hex1;
    unsigned char Hex0;
    unsigned char HexCode;

    Hex1 = (BcdCode >> 4) *10;
    Hex0 = (BcdCode & 0x0F);
    HexCode = Hex1 +Hex0;

    return(HexCode);
}
//---------------------------------------------------------------------------
float UtilityClass::ConvertCelsiusToFahrenheit(float Celsius)
{
    float Fahrenheit;

    Fahrenheit = Celsius *(9.0/5.0) +32.0;

    return(Fahrenheit);
}
//---------------------------------------------------------------------------
float UtilityClass::ConvertFahrenheitToCelsius(float Fahrenheit)
{
    float Celsius;

    Celsius = (Fahrenheit -32.0) *(5.0/9.0);

    return(Celsius);
}
//---------------------------------------------------------------------------
unsigned long UtilityClass::GetFileSize(FILE *FileHandle)
{
    unsigned long FileSizeBytes;
    struct stat stat_buf;
    fstat(fileno(FileHandle), &stat_buf);
    FileSizeBytes = stat_buf.st_size;

    return(FileSizeBytes);
}
//---------------------------------------------------------------------------
int UtilityClass::SplitDirectoryAndFileName(char *CompleteFileName, char *DirectoryName, char *FileName)
{
    int StringLength;
    int i;

    StringLength = strlen(CompleteFileName);
    DirectoryName[0] = 0;
    FileName[0] = 0;
    // Just in case no directory is included...
    strcpy(FileName,CompleteFileName);

    FileName[0] = 0;
    for (i=StringLength -1; i>0; i--)
    {
        if (CompleteFileName[i] == '\\')
        {
            strcpy(DirectoryName,CompleteFileName);
            DirectoryName[i +1] = 0;
            strcpy(FileName,CompleteFileName +i +1);
            break;
        }
    }

    return(true);

}
//---------------------------------------------------------------------------
unsigned short UtilityClass::CalcBufferCrc16(void *Buffer, unsigned short Length)
{
    int i;
    unsigned short Crc16;

    Crc16 = 0;
    for (i=0; i<Length; i++)
        Crc16 = Crc16Calc(Crc16, *(((char *)Buffer) +i));

    return(Crc16);
}
//---------------------------------------------------------------------------
unsigned short UtilityClass::Crc16Calc(unsigned short Crc16, char InputCharacter)
{
    unsigned short Crc16Index;

    Crc16Index = (unsigned char)(InputCharacter ^ (unsigned char)Crc16);
    Crc16 >>= 8;
    Crc16 ^= CrcArray[Crc16Index];

    return(Crc16);
}
//---------------------------------------------------------------------------
unsigned short UtilityClass::ModRTU_CRC(unsigned char *buf, int len)
{
  unsigned short crc = 0xFFFF;

  for (int pos = 0; pos < len; pos++) {
    crc ^= (unsigned short)buf[pos];          // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
    }
  }
  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
  return crc;
}
//---------------------------------------------------------------------------

