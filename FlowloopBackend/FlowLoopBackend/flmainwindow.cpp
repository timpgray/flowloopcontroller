#include "flmainwindow.h"
#include "ui_flmainwindow.h"
#include <qfile.h>
#include <qtimer.h>

#include <qloggingcategory.h>
#include <qdatetime.h>
#include "Utility.h"


QLoggingCategory FLMAINWINDOW("FLMainWindow");
QLoggingCategory IECTRAFFIC("IECTraffic");

#define TIMEFORMAT "hh:mm:ss:zzz"


FLMainWindow::FLMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FLMainWindow)
{
    ui->setupUi(this);
    m_pToolComs = nullptr;
    m_pIECComs = nullptr;
    m_pWITSComs = nullptr;

    QHash<QString,QString> props =  loadPropsFile("config.csv");

    initialize(props);

    m_pendingIECCommandSize = -1;
    ui->grantcontrol->setEnabled(false);
    m_controlGranted = false;
    m_responseCounter = 0;

    m_badRespCounter = 0;

    m_witsPublishCounter = 0;
    m_witsPublishDivider = 1;
}

FLMainWindow::~FLMainWindow()
{
    delete ui;
}

//#define WITS_COMPORT "witscomport"
//#define WITS_BAUDRATE "witsbaudrate"
//#define IEC_COMPORT "iec_comport"
//#define IEC_BAUDRATE "iec_baudrate"
//#define ROTONAV_COMPORT "rotonavcomport"
//#define ROTONAV_BAUDRATE "rotonavbaudrate"


void FLMainWindow::initialize(QHash<QString,QString> properties)
{
    qCDebug(FLMAINWINDOW) << "inside initialize with properties " <<  properties;

    if(properties.contains(ROTONAV_COMPORT) && properties.contains(ROTONAV_BAUDRATE))
    {
        QString port = QString("COM") + properties.value(ROTONAV_COMPORT).trimmed();
        int baudrate = properties.value(ROTONAV_BAUDRATE).trimmed().toInt();
        QSerialPort* serialport = new QSerialPort(port);

        serialport->setBaudRate(baudrate);

        bool status = serialport->open(QFile::ReadWrite);

        qCDebug(FLMAINWINDOW) << "RotoNav comport opened port " << port << " baud rate " << baudrate << " status " << status;

        if(status)
        {
            m_pToolComs = serialport;

            connect(m_pToolComs, SIGNAL(readyRead()), this, SLOT(toolDataReady()));
        }
        else
        {
            delete serialport;
        }
    }


    if(properties.contains(IEC_COMPORT) && properties.contains(IEC_BAUDRATE))
    {
        QString port = QString("COM") + properties.value(IEC_COMPORT).trimmed();
        int baudrate = properties.value(IEC_BAUDRATE).trimmed().toInt();
        QSerialPort* serialport = new QSerialPort(port);

        serialport->setBaudRate(baudrate);

        bool status = serialport->open(QFile::ReadWrite);

        qCDebug(FLMAINWINDOW) << "IEC comport opened port " << port << " baud rate " << baudrate << " status " << status;

        if(status)
        {
            m_pIECComs = serialport;

            connect(m_pIECComs, SIGNAL(readyRead()), this, SLOT(iecDataReady()));
            m_timerId = startTimer(1000);
        }
        else
        {
            delete serialport;
        }
    }


    if(properties.contains(WITS_COMPORT) && properties.contains(WITS_BAUDRATE))
    {
        QString port = QString("COM") + properties.value(WITS_COMPORT).trimmed();
        int baudrate = properties.value(WITS_BAUDRATE).trimmed().toInt();
        QSerialPort* serialport = new QSerialPort(port);

        serialport->setBaudRate(baudrate);

        bool status = serialport->open(QFile::WriteOnly);

        qCDebug(FLMAINWINDOW) << "WITS comport opened port " << port << " baud rate " << baudrate << " status " << status;

        if(status)
        {
            m_pWITSComs = serialport;
        }
        else
        {
            delete serialport;
        }


        m_witsKeyMap.insert("inc", "0114");
        m_witsKeyMap.insert("battv", "0115");
        m_witsKeyMap.insert("temp", "0116");
        m_witsKeyMap.insert("azm", "0112");
        m_witsKeyMap.insert("dipa", "0113");
        m_witsKeyMap.insert("magt", "0117");
        m_witsKeyMap.insert("gravt", "0123");
        m_witsKeyMap.insert("gtfa", "0124");
        m_witsKeyMap.insert("rpm", "0121");

    }


    m_commandHandlerMap.insert(02,&handleCmd02);
    m_commandHandlerMap.insert(03,&handleCmd03);
    m_commandHandlerMap.insert(0x0f,&handleCmd0f);
    m_commandHandlerMap.insert(0x10,&handleCmd10);

    int function[] = {0x02, 0x03, 0x0f, 0x10};
    int functionsize[] = {7, 37, 8, 8};

    for(int i=0;i<4;i++)
    {
        m_IECCommandLengths.insert(function[i], functionsize[i]);
    }

    m_iecCommandList << 0x070f00 << 0x070320 << 0x070202 << 0x071000;

    unsigned char cmd1[] = {0x7, 0xf, 0x0, 0x0, 0x0, 0xf, 0x2, 0x0, 0x0, 0xce, 0x54};
    unsigned char cmd2[] = {0x7, 0x3, 0x0, 0x0, 0x0, 0x10, 0x44, 0x60};
    unsigned char cmd3[] = {0x7, 0x2, 0x0, 0x0, 0x0, 0x10, 0x79, 0xa0};
    unsigned char cmd4[] = {0x7, 0x10, 0x0, 0x0, 0x0, 0x10, 0x20, 0x0, 0x0, 0x2, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x7d, 0x0, 0x18, 0x0, 0x0, 0x0, 0x0, 0x3d, 0x2d};

    QList<unsigned char*> cmdlist;
    QList<int> sizelist;
    QList<unsigned char> functionlist;

    cmdlist << cmd1 << cmd2 << cmd3 << cmd4;
    sizelist <<  (sizeof(cmd1)/sizeof(unsigned char)) <<  (sizeof(cmd2)/sizeof(unsigned char))
                  <<  (sizeof(cmd3)/sizeof(unsigned char)) <<  (sizeof(cmd4)/sizeof(unsigned char));
    functionlist << 0x0f << 0x03 << 0x02 << 0x10;

    for(int i=0;i<cmdlist.size();i++)
    {
        int listsize = sizelist.at(i);
        unsigned char* cmdbuffer = cmdlist.at(i);
        unsigned char fnc = functionlist.at(i);
        QList<unsigned char> templist;

        for(int j=0;j<listsize;j++)
        {
            templist.append(cmdbuffer[j]);
        }

        m_iecCommandMap.insert(fnc, templist);
    }


//    m_iecCommandSequence << 0xf << 0xf << 0xf << 0x3 << 0x2  << 0x10 << 0x3 << 0x2 << 0x10;
//    m_iecCommandSequence << 0x3 << 0x10 << 0x3 << 0x10;
//    m_iecCommandSequence << 0x3 << 0x02 << 0x10 << 0x0f;

}

void FLMainWindow::toolDataReady()
{
    QByteArray bytes = m_pToolComs->readAll();

    qCDebug(FLMAINWINDOW) << "RotoNav read this many bytes " << bytes.size();

    for(int i=0;i<bytes.size();i++)
    {
        m_tooldata.append(bytes.at(i));

        if(m_tooldata.size() == 7)
        {
            QString msg("Received ");

            for(int j=0;j<m_tooldata.size();j++)
            {
                msg += QString::number(m_tooldata.at(j), 16) + ",";
            }

            qCDebug(FLMAINWINDOW) << "RotoNav received this message " << msg;

            handleRotoNavData(m_tooldata);
            m_tooldata.clear();
//            QTimer::singleShot(50, this, SLOT(handleRotoNavData()));
        }
    }

}

void FLMainWindow::iecDataReady()
{
    QByteArray bytes = m_pIECComs->readAll();
    QString desc1;

    for(int i=0;i<bytes.size();i++)
    {
        desc1 += QString::number((unsigned char)bytes.at(i), 16) + ",";
    }

    qCDebug(IECTRAFFIC) << "IEC read this many bytes " << bytes.size() << " values =  " << desc1;

    for(int i=0;i<bytes.size();i++)
    {
        m_iecData.append(bytes.at(i));
    }

    if(m_pendingIECCommandSize == -1)
    {
        int functioncode = getCommandValue(m_iecData);

        if(functioncode > -1)
        {
            m_pendingIECCommandSize = getCommandSize(functioncode);
            qCDebug(IECTRAFFIC) <<  "starting command with function 0x" << QString::number(functioncode) << " command size = " << m_pendingIECCommandSize;
        }
        else
        {
            if(m_pendingIECCommandSize == 3)
            {
                m_iecData.removeFirst();
                qCDebug(IECTRAFFIC) << " command out of sync";
            }
        }


    }

    if(m_pendingIECCommandSize > -1)
    {
        if(m_iecData.size() >= m_pendingIECCommandSize)
        {
            QList<unsigned char> command;   // = m_iecData.mid(0, m_pendingIECCommandSize);
            QByteArray dummytx;

            for(int i=0;i<m_pendingIECCommandSize;i++)
            {
                command.append(m_iecData.first());
                dummytx.append(m_iecData.first());
                m_iecData.removeFirst();
            }

            QString desc;

            for(int i=0;i<command.size();i++)
            {
                desc += QString::number(command.at(i), 16) + ",";
            }

            qCDebug(IECTRAFFIC) << "handling this command " << desc;

            unsigned char* buffer = (unsigned char*) malloc(command.size());

            for(int i=0;i<command.size();i++)
            {
                buffer[i] = command.at(i);
            }

            unsigned short calculatedcrc = UtilityClass::ModRTU_CRC(buffer, command.size() -2);
            unsigned short rxcrc = 0;


            rxcrc = (buffer[command.size() -2]) << 8;
            rxcrc += (buffer[command.size() -1]) << 0;
            rxcrc = UtilityClass::SwitchEndianShort(rxcrc);

            bool crcerror = calculatedcrc != rxcrc;

            qCDebug(IECTRAFFIC) << "crc match = " << crcerror << " calculated = 0x" << QString::number(calculatedcrc, 16) << " received crc = 0x" << QString::number(rxcrc, 16);

            free(buffer);

            m_pIECComs->write(dummytx);     // send the received data first...
            handleIECResponse(command);

            m_pendingIECCommandSize = -1;
        }
    }

}

void FLMainWindow::handleIECResponse(QList<unsigned char> command)
{
    int functioncode = command.at(1);

    m_receivedCommand = functioncode;

    if(m_commandHandlerMap.contains(functioncode))
    {
        CMD_HANDLER fcn  = m_commandHandlerMap.value(functioncode);

        (this->*fcn)(command);

        m_responseCounter = 0;
    }

    sendNextCommand();

}

int FLMainWindow::getCommandSize(int functioncode)
{
    int size = -1;


    if(m_IECCommandLengths.contains(functioncode))
    {
        size = m_IECCommandLengths.value(functioncode);
    }

    return size;
}

int FLMainWindow::getCommandValue(QList<unsigned char> data)
{
    int command = -1;
    unsigned long testvalue = 0;

    if(data.size() >= 3)
    {
        for(int i=0;i<3;i++)
        {
            testvalue <<= 8;
            testvalue |= (unsigned int) data.at(i);
        }

        if(m_iecCommandList.contains(testvalue))
        {
            command = data.at(1);
        }
    }

    return command;
}

QHash<QString,QString> FLMainWindow::loadPropsFile(QString filepath, QString separator)
{
    QHash<QString,QString> props;
    QFile inputfile(filepath);
    bool status = inputfile.open(QFile::ReadOnly);

    while(!inputfile.atEnd())
    {
        QString inputline = inputfile.readLine();
        QStringList parts = inputline.split(separator);

        if(parts.size() >= 3 && parts.at(1).startsWith("\"") && parts.last().endsWith("\"") )
        {
            QString key = parts.first();

            parts.removeFirst();

            QString value = parts.join(separator);

            parts.clear();
            parts << key << value;
        }

        if(parts.size() > 1)
        {
            props.insert(parts.first().trimmed(), parts.at(1).trimmed());
        }
    }

    return props;
}


void FLMainWindow::handleRotoNavData(QList<unsigned char> data)
{
    unsigned char response[] = {1, 5, 89, 0, 5, 4, 8, 1, 4, 0, 163, 45, 0, 0, 10, 1, 1, 3, 14, 43, 208, 48, 152, 65, 209, 71, 246, 65, 17, 150, 128, 191, 30, 52, 168, 60, 202, 10, 240, 185, 243, 156, 128, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 13, 180, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 243, 156, 128, 63, 108, 136, 149, 66, 225, 149, 135, 67, 27, 115, 148, 186, 51};
    int txsize = sizeof(response)/sizeof(unsigned char);
    QByteArray responsemsg;
    unsigned char checksum = 0;
    QByteArray xmitdata;


    // !!! transmiting the received data...
    for(int i=0;i<data.size();i++)
    {
        xmitdata.append(data.at(i));
    }

    m_pToolComs->write(xmitdata);


//    for(int i=0;i<txsize-1;i++)
//    {
//        checksum += response[i];
//    }

//    qCDebug(FLMAINWINDOW) << "calculated checksum  = " << checksum << " actual = " << response[txsize-1];

    QList<unsigned char> tfresponse = generateRotoNavToolfaceData(ui->controls->getToolFace(), 85, 240, 24, ui->controls->getLowerRPM(), 57, 0.99, 0.498);
    qCDebug(FLMAINWINDOW) << "sending this many bytes " << tfresponse.size();

    for(int i=0;i<tfresponse.size();i++)
    {
        responsemsg.append(tfresponse[i]);
    }

    m_pToolComs->write(responsemsg);
}


void FLMainWindow::handleRotoNavData()
{
    qCDebug(FLMAINWINDOW) << "!!!sending rotonav data...";
    unsigned char response[] = {1, 5, 89, 0, 5, 4, 8, 1, 4, 0, 163, 45, 0, 0, 10, 1, 1, 3, 14, 43, 208, 48, 152, 65, 209, 71, 246, 65, 17, 150, 128, 191, 30, 52, 168, 60, 202, 10, 240, 185, 243, 156, 128, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 13, 180, 66, 0, 0, 0, 0, 0, 0, 0};
    int txsize = sizeof(response)/sizeof(unsigned char);
    QByteArray responsemsg;

    for(int i=0;i<txsize;i++)
    {
        responsemsg.append(response[i]);
    }

    m_pToolComs->write(responsemsg);
}


void FLMainWindow::timerEvent(QTimerEvent* pevent)
{
    m_iecCommandSequence.clear();
    m_iecCommandSequence << 0x3 << 0x02 << 0x10 << 0x0f;
    sendNextCommand();

    if(m_responseCounter++ > 5)
    {
        m_controlGranted = false;
        ui->grantcontrol->setEnabled(false);
        m_localIEC.ControlWord &= ~IEC_CONTROLWORD_ROTOSLIDE_CONTROL;

    }

    if((m_witsPublishCounter++ % m_witsPublishDivider) == 0)
    {
        publishToWits(ui->controls->getToolFace(), 85, 240, 24, ui->controls->getLowerRPM(), 57, 0.99, 0.498);
    }

}




void FLMainWindow::handleCmd02(QList<unsigned char> list)
{
    qCDebug(IECTRAFFIC) << "Received command 02 at time " << QDateTime::currentDateTime().toString(TIMEFORMAT);
}

void FLMainWindow::handleCmd03(QList<unsigned char> list)
{
    qCDebug(IECTRAFFIC) << "Received command 03 at time " << QDateTime::currentDateTime().toString(TIMEFORMAT);
    QByteArray bytearray;

    QString testmsg;

    for(int i=0;i<list.size();i++)
    {
        testmsg += QString::number((unsigned char)list.at(i), 16) + ", ";
    }

    qCDebug(FLMAINWINDOW) << "!!! command03 " << list.size() << " message = " << testmsg;

    for(int i=0;i<list.size();i++)
    {
        bytearray.append(list.at(i));
    }

//    char* pdata = bytearray.data();
    char* pdata = (char*)malloc(list.size());

    for(int i=0;i<list.size();i++)
    {
        pdata[i] = (char)list.at(i);
    }

    IecInterfaceStructure iecdata;

    unPackIEC((unsigned char*) (pdata+3), &iecdata);

    bool autodrill = (iecdata.ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL) == IEC_CONTROLWORD_AUTODRILL_CONTROL;
    bool rscontrol = (iecdata.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL) == IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
    bool heartbeat = (iecdata.ControlWord & IEC_CONTROLWORD_HEARTBEAT) == IEC_CONTROLWORD_HEARTBEAT;
    bool pumprun = (iecdata.ControlWord & IEC_CONTROLWORD_PUMP_RUN) == IEC_CONTROLWORD_PUMP_RUN;
    bool topdriverun = (iecdata.ControlWord & IEC_CONTROLWORD_TOPDRIVE_RUN) == IEC_CONTROLWORD_TOPDRIVE_RUN;
    bool motorforward = (iecdata.ControlWord & IEC_CONTROLWORD_MOTOR_FORWARD) == IEC_CONTROLWORD_MOTOR_FORWARD;
    bool motorreverse = (iecdata.ControlWord & IEC_CONTROLWORD_MOTOR_REVERSE) == IEC_CONTROLWORD_MOTOR_REVERSE;

    qCDebug(FLMAINWINDOW) << "IEC structure from RS = 0x" << QString::number((unsigned short)iecdata.ControlWord, 16);
    qCDebug(FLMAINWINDOW) << "    W Control Granted = " << (rscontrol?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W Heartbeat = " << (heartbeat?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W AutoDrill control = " << (autodrill?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W Pump Run = " << (pumprun?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W Motor Forward = " << (motorforward?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W Motor Reverse = " << (motorreverse?"true":"false");
    qCDebug(FLMAINWINDOW) << "    W TopDrive Run = " << (topdriverun?"true":"false");

//    qCDebug(FLMAINWINDOW) << "IEC structure from RS = 0x" << QString::number((unsigned short)iecdata.ControlWord, 16);
//    qCDebug(FLMAINWINDOW) << "    W Control Granted = " << ((iecdata.ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W Heartbeat = " << ((iecdata.ControlWord & IEC_CONTROLWORD_HEARTBEAT)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W AutoDrill control = " << ((iecdata.ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W Pump Run = " << ((iecdata.ControlWord & IEC_CONTROLWORD_PUMP_RUN)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W Motor Forward = " << ((iecdata.ControlWord & IEC_CONTROLWORD_MOTOR_FORWARD)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W Motor Reverse = " << ((iecdata.ControlWord & IEC_CONTROLWORD_MOTOR_REVERSE)?"true":"false");
//    qCDebug(FLMAINWINDOW) << "    W TopDrive Run = " << ((iecdata.ControlWord & IEC_CONTROLWORD_TOPDRIVE_RUN)?"true":"false");

    ui->autodrill_2->setChecked(autodrill);
    ui->rscontrol_2->setChecked(rscontrol);
    ui->heartbeat_2->setChecked(heartbeat);
    ui->pumprun_2->setChecked(pumprun);
    ui->topdriverun_2->setChecked(topdriverun);
    ui->motorforward_2->setChecked(motorforward);
    ui->motorreverse_2->setChecked(motorreverse);

    ui->grantcontrol->setEnabled(rscontrol);

    float pumpspeed = iecdata.PumpSpeedCommandRpmX10/10.0;
    float topdrivespeed = iecdata.TopDriveSpeedCommandRpmX100/100.0;
    QString pumptext = QString("Pump RPM: ") + QString::number(pumpspeed, 'f', 1);
    QString topdrivetext = QString("TopDrive RPM: ") + QString::number(topdrivespeed, 'f', 2);

    ui->pumpspeed_2->setText(pumptext);
    ui->topdrivespeed_2->setText(topdrivetext);

    m_localIEC.TopDriveSpeedCommandRpmX100 = iecdata.TopDriveSpeedCommandRpmX100;
    m_localIEC.PumpSpeedCommandRpmX10 = iecdata.PumpSpeedCommandRpmX10;

    unsigned short controlword = 0;
    unsigned short mask = IEC_CONTROLWORD_PUMP_RUN | IEC_CONTROLWORD_TOPDRIVE_RUN | IEC_CONTROLWORD_MOTOR_FORWARD | IEC_CONTROLWORD_MOTOR_REVERSE;

    if(pumprun)
    {
        controlword |= IEC_CONTROLWORD_PUMP_RUN;
        ui->controls->setLowerRPM(pumpspeed);
    }
    else
    {
        ui->controls->setLowerRPM(0);
    }

    if(topdriverun)
    {
        controlword |= IEC_CONTROLWORD_TOPDRIVE_RUN;
        ui->controls->setUpperRPM(topdrivespeed);
    }
    else
    {
        ui->controls->setUpperRPM(0);
    }

    if(motorforward)
    {
        controlword |= IEC_CONTROLWORD_MOTOR_FORWARD;
    }

    if(motorreverse)
    {
        controlword |= IEC_CONTROLWORD_MOTOR_REVERSE;
    }

    m_localIEC.SoftwareInterfaceRev = 0xabcd;
    m_localIEC.ControlWord &= ~mask;
    m_localIEC.ControlWord |= controlword;

    m_localIEC.TopDriveSpeedCommandRpmX100 = ui->controls->getTopDriveRPM() * 100;
    m_localIEC.PumpSpeedCommandRpmX10 = ui->controls->getMudMotorRPM() * 10;
    m_localIEC.TopPressurePsi = 100;
    m_localIEC.BottomPressurePsi = 110;
    m_localIEC.FlowRateGpm = 56;

    m_localIEC.SoftwareInterfaceRev = 0xaabb;

    QList<unsigned char> cmd =  FLMainWindow::generateIECsendCommand(&m_localIEC);

    m_iecCommandMap.insert(0x10, cmd);  // update the outgoing command

    free(pdata);

    updateLocalIECDisplay(&m_localIEC);

}

void FLMainWindow::handleCmd0f(QList<unsigned char> list)
{
    qCDebug(IECTRAFFIC) << "Received command 0F at time " << QDateTime::currentDateTime().toString(TIMEFORMAT);
}

void FLMainWindow::handleCmd10(QList<unsigned char> list)
{
    qCDebug(IECTRAFFIC) << "Received command 10 at time " << QDateTime::currentDateTime().toString(TIMEFORMAT);
}

void FLMainWindow::sendNextCommand()
{
    if(m_pIECComs != nullptr)
    {
        if(m_expectedResponse != m_receivedCommand)
        {
            m_badRespCounter++;
            qCDebug(IECTRAFFIC) << "!!!did not receive expected response expected " << m_expectedResponse << " received " << m_receivedCommand;
            ui->batv->setText(QString::number(m_badRespCounter));
        }
        else
        {
            qCDebug(IECTRAFFIC) << "received expected response expected " << m_expectedResponse << " received " << m_receivedCommand;
        }

        if(!m_iecCommandSequence.empty())
        {
           unsigned functioncode = m_iecCommandSequence.dequeue();

           m_expectedResponse = functioncode;
           m_receivedCommand  = -1;

           if(m_iecCommandMap.contains(functioncode))
           {
               QList<unsigned char> cmdvalues = m_iecCommandMap.value(functioncode);
               QByteArray txbytes;

               for(int i=0;i<cmdvalues.size();i++)
               {
                   txbytes.append(cmdvalues.at(i));
               }

               QString testmsg;

               for(int i=0;i<txbytes.size();i++)
               {
                   testmsg += QString::number((unsigned char)txbytes.at(i), 16) + ", ";
               }

               qCDebug(IECTRAFFIC) << QDateTime::currentDateTime().toString(TIMEFORMAT) << " !!! sending IEC data sending this many bytes " << txbytes.size() << " message = " << testmsg;
               m_pIECComs->write(txbytes);

           }
        }
    }
}

void FLMainWindow::unPackIEC(unsigned char* pdata, IecInterfaceStructure* pIEC)
{
    memcpy(pIEC, pdata, sizeof(IecInterfaceStructure));

    pIEC->SoftwareInterfaceRev = Utility.SwitchEndianShort(pIEC->SoftwareInterfaceRev);
    pIEC->ControlWord = Utility.SwitchEndianShort(pIEC->ControlWord);
    pIEC->TopDriveSpeedCommandRpmX100 = Utility.SwitchEndianShort(pIEC->TopDriveSpeedCommandRpmX100);
    pIEC->TorqueLimitFtLbsX10 = Utility.SwitchEndianShort(pIEC->TorqueLimitFtLbsX10);
    pIEC->AutoDriveRateOfPenetrationFtPerHourX10 = Utility.SwitchEndianShort(pIEC->AutoDriveRateOfPenetrationFtPerHourX10);
    pIEC->AutoDriveWeightOnBitKlbsX100 = Utility.SwitchEndianShort(pIEC->AutoDriveWeightOnBitKlbsX100);
    pIEC->AutoDrivePressureControlDrillingPsi = Utility.SwitchEndianShort(pIEC->AutoDrivePressureControlDrillingPsi);
    pIEC->OffBottomHookLoadKlbsX10 = Utility.SwitchEndianShort(pIEC->OffBottomHookLoadKlbsX10);
    pIEC->OffBottomPressurePsi = Utility.SwitchEndianShort(pIEC->OffBottomPressurePsi);
    pIEC->TotalStrokes = Utility.SwitchEndianShort(pIEC->TotalStrokes);
    pIEC->FlowRateGpm = Utility.SwitchEndianShort(pIEC->FlowRateGpm);
    pIEC->PumpSpeedCommandRpmX10 = Utility.SwitchEndianShort(pIEC->PumpSpeedCommandRpmX10);
    pIEC->TopPressurePsi = Utility.SwitchEndianShort(pIEC->TopPressurePsi);
    pIEC->BottomPressurePsi = Utility.SwitchEndianShort(pIEC->BottomPressurePsi);
    pIEC->HoleDepthFeet = Utility.SwitchEndianShort(pIEC->HoleDepthFeet);
    pIEC->BitDepthFeet = Utility.SwitchEndianShort(pIEC->BitDepthFeet);

}

void FLMainWindow::packIEC(IecInterfaceStructure* pIEC, unsigned char* pdata )
{
    pIEC->SoftwareInterfaceRev = Utility.SwitchEndianShort(pIEC->SoftwareInterfaceRev);
    pIEC->ControlWord = Utility.SwitchEndianShort(pIEC->ControlWord);
    pIEC->TopDriveSpeedCommandRpmX100 = Utility.SwitchEndianShort(pIEC->TopDriveSpeedCommandRpmX100);
    pIEC->TorqueLimitFtLbsX10 = Utility.SwitchEndianShort(pIEC->TorqueLimitFtLbsX10);
    pIEC->AutoDriveRateOfPenetrationFtPerHourX10 = Utility.SwitchEndianShort(pIEC->AutoDriveRateOfPenetrationFtPerHourX10);
    pIEC->AutoDriveWeightOnBitKlbsX100 = Utility.SwitchEndianShort(pIEC->AutoDriveWeightOnBitKlbsX100);
    pIEC->AutoDrivePressureControlDrillingPsi = Utility.SwitchEndianShort(pIEC->AutoDrivePressureControlDrillingPsi);
    pIEC->OffBottomHookLoadKlbsX10 = Utility.SwitchEndianShort(pIEC->OffBottomHookLoadKlbsX10);
    pIEC->OffBottomPressurePsi = Utility.SwitchEndianShort(pIEC->OffBottomPressurePsi);
    pIEC->TotalStrokes = Utility.SwitchEndianShort(pIEC->TotalStrokes);
    pIEC->FlowRateGpm = Utility.SwitchEndianShort(pIEC->FlowRateGpm);
    pIEC->PumpSpeedCommandRpmX10 = Utility.SwitchEndianShort(pIEC->PumpSpeedCommandRpmX10);
    pIEC->TopPressurePsi = Utility.SwitchEndianShort(pIEC->TopPressurePsi);
    pIEC->BottomPressurePsi = Utility.SwitchEndianShort(pIEC->BottomPressurePsi);
    pIEC->HoleDepthFeet = Utility.SwitchEndianShort(pIEC->HoleDepthFeet);
    pIEC->BitDepthFeet = Utility.SwitchEndianShort(pIEC->BitDepthFeet);
    memcpy(pdata, pIEC, sizeof(IecInterfaceStructure));

}


// 0x7, 0x10, 0x0, 0x0, 0x0, 0x10, 0x20,
QList<unsigned char> FLMainWindow::generateIECsendCommand(IecInterfaceStructure* piec)
{
    IecInterfaceStructure local = *piec;
    QList<unsigned char> buffer;
    unsigned char rawbuffer[50];
    int index = 0;
    int cmdlen = 41;

    rawbuffer[index++] = 0x7;
    rawbuffer[index++] = 0x10;
    rawbuffer[index++] = 0x0;
    rawbuffer[index++] = 0x0;
    rawbuffer[index++] = 0x0;
    rawbuffer[index++] = 0x10;
    rawbuffer[index++] = 0x20;

    if(m_controlGranted)
    {
        local.ControlWord |= IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
    }

    packIEC(&local, (rawbuffer + index));

    unsigned short calculatedcrc = UtilityClass::ModRTU_CRC(rawbuffer, (cmdlen - 2));

    rawbuffer[cmdlen -2] = calculatedcrc >> 0;
    rawbuffer[cmdlen -1] = calculatedcrc >> 8;

    for(int i=0;i<cmdlen;i++)
    {
        buffer.append(rawbuffer[i]);
    }

    return buffer;
}


void FLMainWindow::updateLocalIEC(IecInterfaceStructure* piec)
{
    m_localIEC.ControlWord = piec->ControlWord;
    m_localIEC.PumpSpeedCommandRpmX10 = piec->PumpSpeedCommandRpmX10;
    m_localIEC.TopDriveSpeedCommandRpmX100 = piec->TopDriveSpeedCommandRpmX100;
    m_localIEC.TopPressurePsi = piec->TopPressurePsi;
    m_localIEC.FlowRateGpm = piec->FlowRateGpm;
    m_localIEC.OffBottomPressurePsi = piec->OffBottomPressurePsi;
    m_localIEC.BottomPressurePsi = piec->BottomPressurePsi;
}


void FLMainWindow::updateLocalIECDisplay(IecInterfaceStructure* piec)
{
    bool autodrill = (piec->ControlWord & IEC_CONTROLWORD_AUTODRILL_CONTROL) == IEC_CONTROLWORD_AUTODRILL_CONTROL;
    bool rscontrol = (piec->ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL) == IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
    bool heartbeat = (piec->ControlWord & IEC_CONTROLWORD_HEARTBEAT) == IEC_CONTROLWORD_HEARTBEAT;
    bool pumprun = (piec->ControlWord & IEC_CONTROLWORD_PUMP_RUN) == IEC_CONTROLWORD_PUMP_RUN;
    bool topdriverun = (piec->ControlWord & IEC_CONTROLWORD_TOPDRIVE_RUN) == IEC_CONTROLWORD_TOPDRIVE_RUN;
    bool motorforward = (piec->ControlWord & IEC_CONTROLWORD_MOTOR_FORWARD) == IEC_CONTROLWORD_MOTOR_FORWARD;
    bool motorreverse = (piec->ControlWord & IEC_CONTROLWORD_MOTOR_REVERSE) == IEC_CONTROLWORD_MOTOR_REVERSE;
    bool rscontrolrequested = (piec->ControlWord & IEC_CONTROLWORD_ROTOSLIDE_CONTROL) == IEC_CONTROLWORD_ROTOSLIDE_CONTROL;


    ui->autodrill->setChecked(autodrill);
    ui->rscontrol->setChecked(rscontrol);
    ui->heartbeat->setChecked(heartbeat);
    ui->pumprun->setChecked(pumprun);
    ui->topdriverun->setChecked(topdriverun);
    ui->motorforward->setChecked(motorforward);
    ui->motorreverse->setChecked(motorreverse);

    float pumpspeed = piec->PumpSpeedCommandRpmX10/10.0;
    float topdrivespeed = piec->TopDriveSpeedCommandRpmX100/100.0;
    QString pumptext = QString("Pump RPM: ") + QString::number(pumpspeed, 'f', 1);
    QString topdrivetext = QString("TopDrive RPM: ") + QString::number(topdrivespeed, 'f', 2);
    QString toppressure = QString("Top Pressure: ") + QString::number(piec->TopPressurePsi);
    QString bottompressure = QString("Bottom Pressure: ") + QString::number(piec->BottomPressurePsi);
    QString flowrate = QString("Flow Rate: ") + QString::number(piec->FlowRateGpm);


    ui->pumpspeed->setText(pumptext);
    ui->topdrivespeed->setText(topdrivetext);
    ui->toppressure->setText(toppressure);
    ui->bottompressure->setText(bottompressure);

}

//     unsigned char response[] = {1, 5, 89, 0, 5, 4, 8, 1, 4, 0, 163, 45, 0, 0, 10, 1, 1, 3, 14, 43, 208, 48, 152, 65, 209, 71, 246, 65, 17, 150, 128, 191, 30, 52, 168, 60, 202, 10, 240, 185, 243, 156, 128, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 13, 180, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 243, 156, 128, 63, 108, 136, 149, 66, 225, 149, 135, 67, 27, 115, 148, 186, 51};

QList<unsigned char> FLMainWindow::generateRotoNavToolfaceData(float toolface, float inc, float azm, float batv, float rpm, float magdip, float totalgrav, float totalmag)
{
    int cmdsize = 89;
    unsigned char buffer[100];
    int index = 0;
    RotaryNavDataGroup4Structure tf;

    tf.AzimuthDegrees = azm;
    tf.InclinationDegrees = inc;
    tf.BatteryVolts = batv;
    tf.RevsPerMinute = rpm;
    tf.MagneticDipDegrees = magdip;
    tf.TotalGravityFieldG = totalgrav;
    tf.TotalMagneticFieldGauss = totalmag;
    tf.Bx = 0;
    tf.By = 0;
    tf.Bz = 0;
    tf.Gx = 0;
    tf.Gz = 0;
    tf.Gy = 0;
    tf.Mode = 41728;
    tf.NodeStatus = 1025;
    tf.TemperatureCelsius = 32;
    tf.TimeStampSeconds = 0;
    tf.MagneticToolFaceDegrees = toolface;
    tf.GravityToolFaceDegrees = toolface;

    for(int i=0;i<6;i++)
    {
        tf.TimeStampDate[i] = 0;
    }

    buffer[index++] = 1;
    buffer[index++] = 5;
    buffer[index++] = 89;
    buffer[index++] = 0;
    buffer[index++] = 5;
    buffer[index++] = 4;
//    buffer[index++] = 8;

    packRotoNavToolfaceData(&tf, (buffer + index));

    unsigned char checksum = 0;

    for(int i=0;i<cmdsize-1;i++)
    {
        checksum += buffer[i];
    }

    qCDebug(FLMAINWINDOW) << "calculated checksum = " << (unsigned int) checksum;

    buffer[cmdsize - 1] = checksum;

    QList<unsigned char> xmitbuffer;

    for(int i=0;i<cmdsize;i++)
    {
        xmitbuffer.append(buffer[i]);
    }

    QString testmsg;

    for(int i=0;i<cmdsize;i++)
    {
        testmsg += QString::number((unsigned char)buffer[i]) + ", ";
    }

    qCDebug(FLMAINWINDOW) << "!!! sending RotoNav TF data sending this many bytes " << xmitbuffer.size() << " message = " << testmsg;


    return xmitbuffer;
}


void FLMainWindow::publishToWits(float toolface, float inc, float azm, float batv, float rpm, float magdip, float totalgrav, float totalmag)
{

    if(m_pWITSComs != nullptr)
    {
        qCDebug(FLMAINWINDOW) << "publishing WITS Data";
        QHash<QString,float> valuemap;
        QString witsmsg = "!!\n";

        valuemap.insert("gtfa", toolface);
        valuemap.insert("inc", inc);
        valuemap.insert("azm", azm);
        valuemap.insert("battv", batv);
        valuemap.insert("rpm", rpm);
        valuemap.insert("dipa", magdip);
        valuemap.insert("gravt", totalgrav);
        valuemap.insert("magt", totalmag);

        QList<QString> keys = valuemap.keys();

        for(int i=0;i<keys.size();i++)
        {
            QString key = keys.at(i);

            if(m_witsKeyMap.contains(key))
            {
                QString witskey = m_witsKeyMap.value(key);

                witsmsg += witskey;
                witsmsg += QString::number(valuemap.value(key)) + "\n";
            }
        }

        witsmsg += "&&\n";

        qCDebug(FLMAINWINDOW) << "WITS publishing this value " << witsmsg;
        m_pWITSComs->write(witsmsg.toLatin1());
    }

}

void FLMainWindow::packRotoNavToolfaceData(RotaryNavDataGroup4Structure* ptoolface, unsigned char* praw)
{
    unsigned int16* p16 = (unsigned int16*)  praw;

    *p16++ = ptoolface->NodeStatus;
    *p16++ = ptoolface->Mode;

    unsigned int32* p32 =  (unsigned int32*) p16;

    *p32++ = ptoolface->TimeStampSeconds;

    unsigned int8* p8 = (unsigned int8*) p32;

    for(int i=0;i<6;i++)
    {
        *p8++ = ptoolface->TimeStampDate[i];
    }

    float* pfloat = (float*) p8;

    *pfloat++ = ptoolface->BatteryVolts;
    *pfloat++ = ptoolface->TemperatureCelsius;
    *pfloat++ = ptoolface->Gx;
    *pfloat++ = ptoolface->Gy;
    *pfloat++ = ptoolface->Gz;
    *pfloat++ = ptoolface->Gxy;
    *pfloat++ = ptoolface->Bx;
    *pfloat++ = ptoolface->By;
    *pfloat++ = ptoolface->Bz;
    *pfloat++ = ptoolface->InclinationDegrees;
    *pfloat++ = ptoolface->AzimuthDegrees;
    *pfloat++ = ptoolface->MagneticDipDegrees;
    *pfloat++ = ptoolface->TotalMagneticFieldGauss;
    *pfloat++ = ptoolface->TotalGravityFieldG;
    *pfloat++ = ptoolface->MagneticToolFaceDegrees;
    *pfloat++ = ptoolface->GravityToolFaceDegrees;
    *pfloat++ = ptoolface->RevsPerMinute;

    unsigned char* p = (unsigned char*)&ptoolface->BatteryVolts;
    qCDebug(FLMAINWINDOW) << "raw BV = " << QString::number(*(p+3)) << " " << QString::number(*(p+2))  << " " << QString::number(*(p+1))  << " " << QString::number(*p);
}

void FLMainWindow::on_grantcontrol_clicked()
{
    m_localIEC.ControlWord |= IEC_CONTROLWORD_ROTOSLIDE_CONTROL;
    m_controlGranted = true;
}

void FLMainWindow::on_iecrate_editingFinished()
{
    int newrate = ui->iecrate->text().toInt();

    killTimer(m_timerId);
    m_timerId = startTimer(newrate);

}

void FLMainWindow::on_witsmodcounter_editingFinished()
{
    m_witsPublishDivider = ui->witsmodcounter->text().toInt();
}
