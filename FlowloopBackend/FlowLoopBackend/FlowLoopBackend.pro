#-------------------------------------------------
#
# Project created by QtCreator 2017-05-16T11:51:54
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FlowLoopBackend
TEMPLATE = app


SOURCES += main.cpp\
        flmainwindow.cpp \
    Utility.cpp \
    flowtester.cpp \
    speedo_meter.cpp

HEADERS  += flmainwindow.h \
    Utility.h \
    CcsCompiler.h \
    flowtester.h \
    speedo_meter.h

FORMS    += flmainwindow.ui \
    flowtester.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../qwt_libs/release/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../qwt_libs/release/ -lqwtd
else:unix: LIBS += -L$$PWD/../../qwt_libs/release/ -lqwtd

INCLUDEPATH += $$PWD/../../qwt_libs/includes
DEPENDPATH += $$PWD/../../qwt_libs/includes
