#ifndef FLMAINWINDOW_H
#define FLMAINWINDOW_H

#include <QMainWindow>
#include <qstring.h>
#include <qhash.h>
#include <QtSerialPort/qserialport.h>
#include <qlist.h>
#include <qtimer.h>
#include <qqueue.h>

#include "CcsCompiler.h"



#define WITS_COMPORT "witscomport"
#define WITS_BAUDRATE "witsbaudrate"
#define IEC_COMPORT "iec_comport"
#define IEC_BAUDRATE "iec_baudrate"
#define ROTONAV_COMPORT "rotonavcomport"
#define ROTONAV_BAUDRATE "rotonavbaudrate"



// Communications.
enum {IEC_MSG_NODEID=0, IEC_MSG_CMD, IEC_MSG_ADDRESS1, IEC_MSG_ADDRESS0, IEC_MSG_DATA};
#define IEC_MSG_OVERHEAD            8
#define MODBUS_ADDRESS_ROTOSLIDE    7
#define MODBUS_ADDRESS_IEC          10
#define IEC_BUAD_RATE               19200
#define IEC_REGISTER_ADDRESS        0x0000
#define MODBUS_CMD_WRITE_WORDS      0x10
#define MODBUS_CMD_READ_WORDS       0x0F

// IEC ControlWord bits.
#define IEC_CONTROLWORD_AUTODRILL_CONTROL           (unsigned short)0x0001
#define IEC_CONTROLWORD_ROP_REQUEST                 (unsigned short)0x0002
#define IEC_CONTROLWORD_WOB_REQUEST                 (unsigned short)0x0004
#define IEC_CONTROLWORD_PCD_REQUEST                 (unsigned short)0x0008
#define IEC_CONTROLWORD_PUMP_REQUEST                (unsigned short)0x0010
#define IEC_CONTROLWORD_PUMP_RUN                    (unsigned short)0x0020
#define IEC_CONTROLWORD_SPARE3_6                    (unsigned short)0x0040
#define IEC_CONTROLWORD_HEARTBEAT                   (unsigned short)0x0080
#define IEC_CONTROLWORD_ROTOSLIDE_CONTROL           (unsigned short)0x0100
#define IEC_CONTROLWORD_MOTOR_FORWARD               (unsigned short)0x0200
#define IEC_CONTROLWORD_MOTOR_REVERSE               (unsigned short)0x0400
#define IEC_CONTROLWORD_TOPDRIVE_RUN                (unsigned short)0x0800
#define IEC_CONTROLWORD_SPARE2_4                    (unsigned short)0x1000
#define IEC_CONTROLWORD_SPARE2_5                    (unsigned short)0x2000
#define IEC_CONTROLWORD_SPARE2_6                    (unsigned short)0x4000
#define IEC_CONTROLWORD_SPARE2_7                    (unsigned short)0x8000



// Everything here needs to be on even byte boundaries (16 bits)

// 32 bytes.
typedef struct IEC_INTERFACE_STRUCTURE
{
    short SoftwareInterfaceRev;
    short ControlWord;
    short TopDriveSpeedCommandRpmX100;
    short TorqueLimitFtLbsX10;
    short AutoDriveRateOfPenetrationFtPerHourX10;
    short AutoDriveWeightOnBitKlbsX100;
    short AutoDrivePressureControlDrillingPsi;
    short OffBottomHookLoadKlbsX10;
    short OffBottomPressurePsi;
    short TotalStrokes;
    short FlowRateGpm;
    short PumpSpeedCommandRpmX10;
    short TopPressurePsi;
    short BottomPressurePsi;
    short HoleDepthFeet;
    short BitDepthFeet;
}IecInterfaceStructure;

typedef struct ROTARYNAV_DATAGROUP_4_STRUCTURE
{
    unsigned int16 NodeStatus;
    unsigned int16 Mode;
    unsigned int32 TimeStampSeconds;
    unsigned int8 TimeStampDate[6];

    // Circulating survey
    float BatteryVolts;
    float TemperatureCelsius;
    float Gx, Gy, Gz, Gxy;
    float Bx, By, Bz;
    float InclinationDegrees;
    float AzimuthDegrees;
    float MagneticDipDegrees;
    float TotalMagneticFieldGauss;
    float TotalGravityFieldG;
    float MagneticToolFaceDegrees;
    float GravityToolFaceDegrees;
    float RevsPerMinute;

}RotaryNavDataGroup4Structure;


namespace Ui {
class FLMainWindow;
}

class FLMainWindow;

typedef void (FLMainWindow::*CMD_HANDLER)(QList<unsigned char>);

class FLMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FLMainWindow(QWidget *parent = 0);
    ~FLMainWindow();

    void initialize(QHash<QString,QString> properties);

    QHash<QString,QString> loadPropsFile(QString filepath, QString separator=",");

    void handleRotoNavData(QList<unsigned char> data);
    virtual void timerEvent(QTimerEvent* pevent);

    int getCommandValue(QList<unsigned char> data);
    int getCommandSize(int functioncode);
    void handleIECResponse(QList<unsigned char> command);

    void handleCmd02(QList<unsigned char> list);
    void handleCmd03(QList<unsigned char> list);
    void handleCmd0f(QList<unsigned char> list);
    void handleCmd10(QList<unsigned char> list);
    void sendNextCommand();

    void unPackIEC(unsigned char* pdata, IecInterfaceStructure* pIEC);
    void packIEC(IecInterfaceStructure* pIEC, unsigned char* pdata );
    QList<unsigned char> generateIECsendCommand(IecInterfaceStructure* piec);
    QList<unsigned char> generateRotoNavToolfaceData(float toolface, float inc, float azm, float batv, float rpm, float magdip, float totalgrav, float totalmag);
    void packRotoNavToolfaceData(RotaryNavDataGroup4Structure* ptoolface, unsigned char* praw);
    void publishToWits(float toolface, float inc, float azm, float batv, float rpm, float magdip, float totalgrav, float totalmag);

    void updateLocalIEC(IecInterfaceStructure* piec);
    void updateLocalIECDisplay(IecInterfaceStructure* piec);


private slots:
    void toolDataReady();
    void iecDataReady();
    void handleRotoNavData();

    void on_grantcontrol_clicked();

    void on_iecrate_editingFinished();

    void on_witsmodcounter_editingFinished();

private:
    Ui::FLMainWindow *ui;

    QSerialPort* m_pToolComs;
    QSerialPort* m_pIECComs;
    QSerialPort* m_pWITSComs;


    QList<unsigned char> m_tooldata;
    QList<unsigned char> m_iecData;
    int m_pendingIECCommandSize;
    QHash<int,CMD_HANDLER> m_commandHandlerMap;
    QHash<int,int> m_IECCommandLengths;
    QList<unsigned long> m_iecCommandList;
    QHash<int,QList<unsigned char>> m_iecCommandMap;
    QQueue<unsigned char> m_iecCommandSequence;

    IecInterfaceStructure m_localIEC;
    bool m_controlGranted;
    int m_responseCounter;

    int m_expectedResponse;
    int m_receivedCommand;
    int m_timerId;
    int m_badRespCounter;

    QHash<QString,QString> m_witsKeyMap;

    int m_witsPublishCounter;
    int m_witsPublishDivider;
};

#endif // FLMAINWINDOW_H
