#include "flowtester.h"
#include "ui_flowtester.h"
#include <qwt_compass.h>
#include <qwt_compass_rose.h>
#include <qwt_dial_needle.h>
#include <qdebug.h>


FlowTester::FlowTester(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FlowTester)
{
    ui->setupUi(this);

    QwtCompass *compass = ui->widget;
    compass->setLineWidth( 4 );
    compass->setFrameShadow(/*QwtCompass::Sunken*/ QwtCompass::Raised );


    compass->setLineWidth( 0 );

    QMap<double, QString> map;
    for ( double d = 0.0; d < 360.0; d += 60.0 )
    {
        QString label;
        label.sprintf( "%.0f", d );
        map.insert( d, label );
    }

    QwtCompassScaleDraw *scaleDraw =
        new QwtCompassScaleDraw( map );
    scaleDraw->enableComponent( QwtAbstractScaleDraw::Ticks, true );
    scaleDraw->enableComponent( QwtAbstractScaleDraw::Labels, true );
//    scaleDraw->enableComponent( QwtAbstractScaleDraw::Backbone, true );
    scaleDraw->setTickLength( QwtScaleDiv::MinorTick, 0 );
    scaleDraw->setTickLength( QwtScaleDiv::MediumTick, 0 );
    scaleDraw->setTickLength( QwtScaleDiv::MajorTick, 3 );

    compass->setScaleDraw( scaleDraw );

    compass->setScaleMaxMajor( 36 );
    compass->setScaleMaxMinor( 5 );

    compass->setNeedle( new QwtDialSimpleNeedle( QwtDialSimpleNeedle::Arrow,
        true, Qt::black ) );
    compass->setOrigin( 270.0 );
    compass->setValue( 20.0 );

//    compass->setGeometry(QRect(0,0,150,150));
//    compass->setVisible(true);

    m_dial = compass;
//    startTimer(250);
    startTimer(50);
    m_position = 0;

    SpeedoMeter* speedo = ui->widget_2;
//    speedo->setGeometry(QRect(150,0,150,150));
//    speedo->setVisible(true);
    m_speedo = speedo;
//    m_motor.setMillisPerTick(250);
    m_motor.setMillisPerTick(50);
    m_motor.setSpeed(0);
    m_topDrive.setSpeed(0);

    m_witsTimer = new QTimer(this);
    connect(m_witsTimer, SIGNAL(timeout()), this, SLOT(sendWITsData()));
    m_witsTimer->setInterval(10000);
    m_witsTimer->start(10000);

    ui->loadfactor->setText("Load Factor = 1.0");
    ui->mudmotorrpmindicator->setText("");
    ui->topdriverpmindicator->setText("");
}

FlowTester::~FlowTester()
{
    delete ui;
}


void FlowTester::timerEvent(QTimerEvent* event)
{
//    m_dial->setValue(m_position++);
//    m_speedo->setValue(m_position % 10);

    m_topDrive.tick();
    ui->widget_3->setValue(m_topDrive.getSpeed());

    m_motor.setRPMoffset(m_topDrive.getSpeed());
    m_motor.tick();
    m_dial->setValue(m_motor.getToolFace());
    m_speedo->setValue(-m_motor.getSpeed());

    ui->topdriverpmindicator->setText("Top Drive RPM = " + QString::number(m_topDrive.getSpeed(), 'f', 2));
    ui->mudmotorrpmindicator->setText("Mud Motor RPM = " + QString::number(m_motor.getSpeed(), 'f', 2));
    ui->downholerpm->setText("Downhole RPM = " + QString::number(m_motor.getRelativeRPM(), 'f', 2));
}

void FlowTester::sendWITsData()
{
    float speed = m_motor.getRelativeRPM();
    float toolface = m_motor.getToolFace();

    qDebug() << "RPM = " << QString::number(speed, 'f', 6) << " toolface = " << toolface;
}

float FlowTester::getMudMotorRPM()
{
    return m_motor.getSpeed();
}

float FlowTester::getLowerRPM()
{
    return m_motor.getRelativeRPM();
}

float FlowTester::getTopDriveRPM()
{
    return m_topDrive.getSpeed();
}

float FlowTester::getToolFace()
{
    return m_motor.getToolFace();
}





Motor::Motor()
{
    m_speedTarget = 0;
    m_currentSpeed = 0;
    m_lastSpeed = 0;
    m_lastToolFace = 0;
    m_millisPerTick = 100;
    m_speedOffset = 0;
    m_loadFactor = 1.0;
}

void Motor::tick()
{
    float averagespeed = (m_currentSpeed + m_lastSpeed)/2.0;
    float displacement = (averagespeed + m_speedOffset) * ((float)m_millisPerTick/(1000.0 * 60.0)) * 360;

    m_lastToolFace += displacement;

    if(m_lastToolFace > 360)
    {
        m_lastToolFace -= 360;
    }
    else
    {
        if(m_lastToolFace < 0)
        {
            m_lastToolFace += 360;
        }
    }

    if(fabs(m_currentSpeed - (m_speedTarget/m_loadFactor)) > 0.0001)
    {
        float step = (m_currentSpeed < (m_speedTarget/m_loadFactor)) ? 0.25:-0.25;

        if(fabs(m_currentSpeed - (m_speedTarget/m_loadFactor)) < 0.25)
        {
            step = (m_speedTarget/m_loadFactor) - m_currentSpeed;
        }

        m_lastSpeed = m_currentSpeed;
        m_currentSpeed += step;
    }
    else
    {
        m_lastSpeed = m_currentSpeed;
    }
}


void FlowTester::setLowerRPM(float rpm)
{
    m_motor.setSpeed(-rpm);
    ui->lowerrpm->blockSignals(true);
    ui->lowerrpm->setValue((int) rpm);
    ui->lowerrpm->blockSignals(false);

}

void FlowTester::setUpperRPM(float rpm)
{
    m_topDrive.setSpeed(rpm);
    ui->topdriverrpm->blockSignals(true);
    ui->topdriverrpm->setValue((int)rpm);
    ui->topdriverrpm->blockSignals(false);
}

void FlowTester::on_lowerrpm_valueChanged(int value)
{
    m_motor.setSpeed(-value);
    ui->widget_2->setLabel(QString::number(value));

}

void FlowTester::on_topdriverrpm_valueChanged(int value)
{
    m_topDrive.setSpeed(value);

    ui->widget_3->setLabel(QString::number(value));

}

void FlowTester::on_loadslider_valueChanged(int value)
{
    QString labelvalue = "Load Factor = " + QString::number(getLoadMultiplier(), 'f', 1);

    ui->loadfactor->setText(labelvalue);
    m_motor.setLoadFactor(getLoadMultiplier());
}


float FlowTester::getLoadMultiplier()
{
    int load = ui->loadslider->value();
    float multiplier = (100.0 + load)/100.0;

    return multiplier;
}
