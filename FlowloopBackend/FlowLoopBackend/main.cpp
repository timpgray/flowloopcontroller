#include "flmainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FLMainWindow w;
    w.show();

    return a.exec();
}
